<?php

return array(
    'shop_currency' => '£',
	'currency_code' => 'GBP',
     'billing_mode' => [
            //'FULL_CARD',
            //'ADV_CARD',
			'FULL_BARCLEY_CARD',
            'ADV_BARCLEY_CARD',
			'FULL_PAYPALL',
			'ADV_PAYPALL',
            'FULL_TRANSFER',
            'ADV_TRANSFER',
			//'COD'
    ],
    'shipping_mode' => [
		'PERSONAL_LONDON',
		'COURIER',
		//'MPL',
        //'OTHER',
    ],
    'advance_enable_limit' => 10000000, //Előleg bekapcsolásának limitje - Nem használjuk.
    'advance_x' => 0.30, //Az előleg osztója
    
    'maps_api_key' => 'AIzaSyD-2e09J6RRvWiyp_p0PR_zpiQuzM0mWqk',
    'default_city' => 'London',
    'central_address' => '27 Cricklewood Broadway, NW2 3JX',
    'default_city_zip_start' => 1, //Budapest irányítószám
    'default_city_zip_price' => 89, //Ha 1-es az irányítószám, akkor Budapesti
    'default_city_zone' => 67000, //Ennyi méterig a budapest határa...
    'default_city_zone_price' => 89,
    //'other_city_km' => 110, //A zónán kívül ennyi az ár KM-enként.
	'other_city_km' => 125, //A zónán kívül ennyi az ár KM-enként.
    'other_city_price' => 0, //16900, //A többi telephelyen ennyi az átvétel díja.
	'floor_cost' => 4,
    
    'textile_extra_once_price' => 15, //bútoron 2 ilyen szövet van, mert a harmadik 1-es kategóriás szövetnél van egy fix, egyszeri 5000 ft-os felár.

    /*
    'unicredit_ecomm_server_url'     => 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler',
    'unicredit_ecomm_client_url'     => 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler',
	'unicredit_cert_url'             => '/var/www/clients/client1/web9/web/storage/app/public/test/IF000271keystore.pem', //full path to keystore file
	'unicredit_cert_pass'            => '84ZKBJb*e', //keystore password teszt
	*/
	
	'unicredit_ecomm_server_url'     => 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler',
    'unicredit_ecomm_client_url'     => 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler',
	'unicredit_cert_url'             => '/var/www/clients/client1/web9/web/storage/app/public/test/IF000536keystore.pem', //full path to keystore file
	'unicredit_cert_pass'            => '84ZKBJb*e', //keystore password teszt
	
	/*
	'unicredit_ecomm_server_url'     => 'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler',
    'unicredit_ecomm_client_url'     => 'https://secureshop.firstdata.lv/ecomm/ClientHandler',
    'unicredit_cert_url'             => '/var/www/clients/client1/web9/web/storage/app/public/IF000536keystore.pem', //full path to keystore file
    'unicredit_cert_pass'            => 'n4k3Rh26rt', //keystore password éles
	*/
	
    'unicredit_currency'             => '348', //978=EUR 840=USD 941=RSD 703=SKK 440=LTL 233=EEK 643=RUB 891=YUM
    //'unicredit_ip'                   => '74.124.217.20',
	'unicredit_ip'                   => '89.107.253.82',
	'ziptype' => 'text',
	
	'paypal_username' => 'vamosi_api1.sofaartlondon.com',
	'paypal_password' => 'P7HKAVGL2W9JZMWK',
	'paypal_signature' => 'AKCSFgAIdDyomZChB1p4dFvwcFhOAT72lyB0S-0Llei4nXYAjYcArI4C',
	'paypal_sandbox' => false,
);
