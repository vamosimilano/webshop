<?php
use OAuth\Common\Storage\Session;
return [

    /*
    |--------------------------------------------------------------------------
    | oAuth Config
    |--------------------------------------------------------------------------
    */

    /**
     * Storage
     */
    'storage' => new Session(),

    /**
     * Consumers
     */
    'consumers' => [

        /**
         * Facebook
         */
		 /*
        'Facebook' => [
            'client_id'     => '167404730371647',
            'client_secret' => 'd9594e1a817c17da150db1a66bd8baea',
            'scope'         => ['email'],
        ],
		*/
		'Facebook' => [
            'client_id'     => '279645012445636',
            'client_secret' => 'f3ccc9b9f2585d6eb83936cf271fd2f6',
            'scope'         => ['email'],
        ],

    ]

];
