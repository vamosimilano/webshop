<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        \Seo::setTitle(t('Error'));
        // 404 page when a model is not found
		global $_GET;
		global $_POST;
        if ($e instanceof ModelNotFoundException) {

            return response()->view('errors.404', [], 404);

        }

        try {
            if ($e->getMessage() != '') {
                \Mail::send('emails.error', ['error' => 'GET:'.var_export($_GET,true).'\nPOST:'.var_export($_POST,true).'\n'.$e->getMessage()], function ($m) {
                    //$m->to('errors@syslab.hu', 'Kruska Roland')->subject($_SERVER['HTTP_HOST'].' - '.date('Y-m-d H:i:s'));
					$m->to('kada.gabor@bellaitaliabutorhaz.hu', 'Kada G�bor')->to('andras.ori@vamosimilano.hu', 'Kada G�bor')->subject($_SERVER['HTTP_HOST'].' - '.date('Y-m-d H:i:s'));
                });
            }
        }catch(Exception $e){}

        if ($this->isHttpException($e)) {
            return $this->renderHttpException($e);
        } else {
            // Custom error 500 view on production
            if (app()->environment() == 'localhost') {
                return response()->view('errors.500', [], 500);
            }
            return parent::render($request, $e);
        }
    }



}
