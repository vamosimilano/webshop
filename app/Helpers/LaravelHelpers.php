<?php

function t($id, $lang_file = 'global', $parameters = array() ) {
	if ($lang_file == '') $lang_file='global';
	$lang_file = strtolower($lang_file);

	$id_default = $id;

	$prefix = $lang_file.'.';
	$id = $prefix.$id;

	if (!Lang::has($id)){
		$not_found_lang ="'".$id_default."' => '".$id_default."'".", //".$lang_file;
		$trans = \Session::get('not_found_lang');
		$trans[$id_default] = $not_found_lang;
		\Session::put('not_found_lang', $trans);

	}

	$result = app('translator')->trans($id, $parameters);
	if (substr($result, 0, strlen($prefix)) == $prefix) {
		$result = substr($result, strlen($prefix));
	}
	return $result;
}

function getUid() {
    if (Auth::user()){
        return Auth::user()->customer_id;
    }
    return 0;
}


class Input {
    public static function get($name){
        return Request::input($name);
    }
    public static function has($name){
        return Request::has($name);
    }
    public static function old($name){
        return Request::old($name);
    }
    public static function all(){
        return Request::all();
    }
    public static function flash(){
        return Request::flash();
    }
    public static function reflash(){
        return Request::session()->reflash();
    }
    public static function merge($data){
        return Request::merge((array)$data);
    }
    public static function except($data){
        return Request::except((array)$data);
    }
    public static function addOld($data){
        $data = (array)$data;
        foreach ($data as $key => $value) {
            Request::session()->flash($key, $value);
        }

    }
    public static function file($name){
        return Request::file($name);
    }
}


function CutTextSpace ($text, $lenght) {
    if (strlen($text) < $lenght) {
        return $text;
    }
    $text = strip_tags($text);
    $text = urldecode($text);
    $tmp = explode(" ", $text);
    $ret = "";
    foreach ($tmp as $k=>$v) {
        $next = $ret." ".$v;
        if (strlen($next) <= $lenght) {
            $ret.= " ".$v;
        }else{
            return $ret."...";
        }
    }
    return $ret."...";
}

function addString($string, $add_string, $lenght, $right = true) {
    $lenght_ = strlen($string);
    $lenght_all = $lenght - $lenght_;
    $i = 1;
    $new_string = '';
    while ($i <= $lenght_all) {
        if ($right) {
            $new_string.=$add_string;
        }else{
            $new_string= $add_string.$new_string;
        }
        $i++;
    }

    return ($right ? $string.$new_string : $new_string.$string);
}

function stringXor($string = '', $xor) {
    if ($xor == 0) {
        return '';
    }
    $return = '';
    $i = 1;
    while($i <= $xor) {
        $return.=$string;
        $i++;
    }
    return $return;
}

function ago($date){

    if (empty($date) or $date == "0000-00-00 00:00:00")
    {
        return t('Soha');
    }

    $ptime = strtotime($date);

    $etime = time() - $ptime;

    if ($etime < 1)
    {
        return '0 seconds';
    }

    $a = array( 365 * 24 * 60 * 60  =>  t('year'),
                 30 * 24 * 60 * 60  =>  t('month'),
                      24 * 60 * 60  =>  t('day'),
                           60 * 60  =>  t('hour'),
                                60  =>  t('minute'),
                                 1  =>  t('second')
                );
    $a_plural = array( t('year')   => t('years'),
                       t('month')  => t('months'),
                       t('day')    => t('days'),
                       t('hour')   => t('hours'),
                       t('minute') => t('minutes'),
                       t('second') => t('seconds')
                );

    foreach ($a as $secs => $str)
    {
        $d = $etime / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . '' . ($r > 1 ? $a_plural[$str] : $str) . ' '.t('ago');
        }
    }
}
/**
 * getShopCode function.
 * @Todo: Shop code meghatározása
 *
 * @access public
 * @return void
 */
function getShopCode(){
    return App::getLocale();
}

function getCurrency(){
    return Config::get('shop.'.getShopCode().'.shop_currency');
}

/**
 * get404Image function.
 * 404 kép generálás
 *
 * @access public
 * @param mixed $width
 * @param mixed $height
 * @return void
 */
function get404Image($width,$height){



    $new_file_path = Config::get('website.404imagePath')."".$width."_".$height.".png";
    $new_file_url = Config::get('website.404imageUrl')."".$width."_".$height.".png";

    if(is_file($new_file_path)){
        return $new_file_url;
    }
    $img = Image::make(Config::get('website.404image'));
    $img->fit($width,$height);
    $img->save($new_file_path);
    return $new_file_url;
}

function getUnit($cm, $string = false) {
    //return ($cm*0.39);
    return $cm.($string ? ' cm' : '');
}


/**
 * Mobile eszköz detektálása
 * @return int
 */
function isMobile() {
    if(isTablet()) return false;
	if (isset($_SERVER['HTTP_USER_AGENT'])) {
		return preg_match("/Mobile|Android|BlackBerry|iPhone|Windows Phone/", $_SERVER['HTTP_USER_AGENT']);
	} else {
		return false;
	}
}

/**
 * Táblagép detektálás
 * @return int
 */
function isTablet() {
	if (isset($_SERVER['HTTP_USER_AGENT'])) {
		return preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']));
	} else {
		return false;
	}
}

/**
 * Desktop detektálás
 * @return bool
 */
function isDesktop() {
    if(isMobile() || isTablet()) {
        return false;
    } else {
        return true;
    }
}
?>