<?php

namespace App\Database;

use DateTime;

use App\Databaselog\DbLog;

use Illuminate\Database\Eloquent\Model as Eloquent;

use Illuminate\Support\Facades\Auth as Auth;
use Request;

class R_DB extends Eloquent
{

    /**
     * Validálási szabályok
     * @var array
     */
    private $validation_rules = array();

    /**
     * Validálandó adatok rendes nevei
     * @var array
     */
    private $validation_msg = array();

    private $enable_log = true;

    function __construct()
    {

         parent::__construct();
    }

    /**
     * disableLog function.
     * Logolás kikapcsolása
     *
     * @access public
     * @return void
     */
    public function disableLog()
    {
        $this->enable_log = false;
    }

    /**
     * enableLog function.
     * Logolás bekapcsolása
     *
     * @access public
     * @return void
     */
    public function enableLog()
    {
        $this->enable_log = true;
    }

    public function save(array $options = array())
    {
        $query = $this->newQuery();

        // If the "saving" event returns false we'll bail out of the save and return
        // false, indicating that the save failed. This gives an opportunities to
        // listeners to cancel save operations if validations fail or whatever.
        if ($this->fireModelEvent('saving') === false) {
            return false;
        }

        // If the model already exists in the database we can just update our record
        // that is already in this database using the current IDs in this "where"
        // clause to only update this model. Otherwise, we'll just insert them.
        if ($this->exists) {
            $this->lg($this, 'edit');
            $saved = $this->performUpdate($query);
        } // If the model is brand new, we'll insert it into our database and set the
        // ID attribute on the model to the value of the newly inserted row's ID
        // which is typically an auto-increment value managed by the database.
        else {
            $saved = $this->performInsert($query);
            $this->lg($this);
        }

        if ($saved) {
            $this->finishSave($options);
        }

        return $saved;
    }

    public function delete()
    {
        if (is_null($this->primaryKey)) {
            throw new \Exception("No primary key defined on model.");
        }

        if ($this->exists) {
            if ($this->fireModelEvent('deleting') === false) {
                return false;
            }

            // Here, we'll touch the owning models, verifying these timestamps get updated
            // for the models. This will allow any caching to get broken on the parents
            // by the timestamp. Then we will go ahead and delete the model instance.
            $this->touchOwners();

            $this->performDeleteOnModel();

            $this->exists = false;

            $this->lg($this, 'del');

            // Once the model has been deleted, we will fire off the deleted event so that
            // the developers may hook into post-delete operations. We will then return
            // a boolean true as the delete is presumably successful on the database.
            $this->fireModelEvent('deleted', false);

            return true;
        }
    }


    /**
     * MannaCenter
     * @desc : Logolás
     *
     * @param: object $model Modell
     * @param: string $action művelet
     *
     * @return string
     * @Auth : Engelsz Milán
     * @2014.05.10.
     */
    private function lg($model, $action = 'add')
    {
        if (count($model) == 0 || $action == '') {
            return false;
        }

        if ($this->enable_log == false) {
            return false;
        }

        //Átmeneti tömb
        $temp = array();

        $not = [
          'notice',
        ];
        if (in_array($model['table'], $not)) {
            return false;
        }

        $date = new DateTime();

        $log = new DbLog();
        $log->database = 'default';
        $log->object_table = $model['table'];
        $log->uid = (Auth::check()) ? Auth::user()->customer_id : 0;
        $log->ip = Request::getClientIp();
        $log->url = Request::url();
        $log->created = $date->format('Y-m-d H:i:s');


        switch ($action) {
            case 'add':
                $log->object_id = $model['attributes'][$model['primaryKey']];
                $log->action = $action;
                $log->content = serialize($model['attributes']);


                break;
            case 'edit':
                $log->object_id = $model['original'][$model['primaryKey']];
                if (count($model['attributes']) > 0) {
                    foreach ($model['attributes'] as $i => $e) {
                        if ($e != $model['original'][$i]) {
                            $temp[$i] = $e;
                        }
                    }
                }

                if (count($temp) == 0) {
                    $temp['sys_msg'] = 'Nem történt változás!';
                }

                $log->action = $action;
                $log->content = serialize($temp);

                break;
            case 'del':
                $log->object_id = $model['original'][$model['primaryKey']];
                $log->action = $action;
                $temp['sys_msg'] = 'Record törölve!';
                $log->content = serialize($temp);

                break;
            default:
                return false;
                break;
        }
        if ($model['table'] == 'order' or $model['table'] == 'order_item') {
            $log->save();
        }
       // $log->save();
    }

    /**
     * Validáció
     *
     * @param $data
     *
     * @return bool
     */
    public function validation($data)
    {
        if (is_array($data)) {
            //Validátor példányosítása validálandó adatokkal és validácós szabályokkal
            $v = Validator::make($data, $this->validation_rules);

            //Változók neveinek megadása
            $v->setAttributeNames($this->validation_msg);

            if ($v->passes()) {
                return true;
            }

            addMsg('error', t('Sikertelen művelet'), $v->messages()->first());
        } else {
            addMsg('error', t('Sikertelen művelet'));
        }

        return false;
    }

    /**
     * Validációs szabályok és változó nevek megadása
     *
     * @param $validation_rules szabályok
     * @param $validation_msg   változó nevek
     */
    public function setValidator($validation_rules, $validation_msg)
    {
        $this->validation_rules = $validation_rules;
        $this->validation_msg = $validation_msg;
    }
}
