<?php

namespace App\Databaselog;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Adatbázis logolás
 *
 * !!!Ennek mindig Eloquentből kell származnia különben rekurzivan meghivja magát a loglás!!!
 */

class DbLog extends Eloquent
{

    
    protected $table = 'log';
    protected $primaryKey = 'log_id';
    public $timestamps = false;
}
