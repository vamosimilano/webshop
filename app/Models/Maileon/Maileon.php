<?php

/**
 * Maileon class.
 * Dokumentáció: http://dev.maileon.com/api/rest-api-1-0/?lang=en
 */
class Maileon {




	/**
	 * getContact function.
	 * E-mail cím alapján a felhasználó lekérdezése Maileonból.
	 * http://dev.maileon.com/api/rest-api-1-0/contacts/get-contacts-by-email/?lang=en
	 *
	 * @access public
	 * @param mixed $email
	 * @param array $standard_fields (default: array())
	 * @param array $custom_fields (default: array())
	 * @return void
	 */
	public function getContact($email,$standard_fields = array(),$custom_fields = array()){
		$standard_fields[] = 'FIRSTNAME';
		$standard_fields[] = 'LASTNAME';

		$params = array();
		if($standard_fields){
			foreach($standard_fields as $k=>$v){
				$params['standard_field'][] = $v;
			}
		}
		if($custom_fields){
			foreach($custom_fields as $k=>$v){
				$params['custom_field'][] = $v;
			}
		}

		$array = MaileonCore::request("contacts/email/".urlencode($email)."?".MaileonCore::arrayToUrl($params),Config::get('maileon.'.getShopCode().'.API_KEY'));

		if(sizeof($array) and $array != false){
			$array['contact'] = $array;
			//Objektum összeállítása
			$contact = new stdClass();
			$contact->email = $array['contact']['email'];
			$contact->uid = $array['contact']['external_id'];
			$contact->maileonid = $array['contact']['id'];
			foreach($array['contact']['standard_fields']['field'] as $index=>$item){
				$contact->$item['name'] = $item['value'];
			}
			if($array['contact']['custom_fields']){
				foreach($array['contact']['custom_fields']['field'] as $index=>$item){
					$contact->$item['name'] = $item['value'];
				}
			}
			$this->maileoncontact = $contact;
		}else{
			$this->maileoncontact = false;
		}

	}

	/**
	 * saveContact function.
	 * Felhasználó mentése.
	 * Create: http://dev.maileon.com/api/rest-api-1-0/contacts/create-contact/?lang=en
	 * Update: http://dev.maileon.com/api/rest-api-1-0/contacts/update-contact-by-maileon-id/?lang=en
	 * Todo: Webshop URL
	 * @access public
	 * @return void
	 */
	public function saveContact(){


		$this->getContact($this->contact->email);

		//Leiratkozó linkek tartalma.
		$this->getContactUnsLinks();



		$this->setContactDefaultField();



		//Leiratkozottakkal nem foglalkozunk
		if($this->isUnsubscribed($this->contact->email)){

			return false;
		}


		$params = $this->getContactParams();
		if($this->maileoncontact) {

			$array = MaileonCore::request("contacts/contact?".MaileonCore::arrayToUrl($params),Config::get('maileon.'.getShopCode().'.API_KEY'),'PUT','xml',($this->contact));

			if(!$array){
				return false;
			}else{
				return true;
			}
		}else{
			//Ha nem létezik, akkor create
			$array = MaileonCore::request("contacts/".$this->contact->email."?".MaileonCore::arrayToUrl($params),Config::get('maileon.'.getShopCode().'.API_KEY'),'POST2','xml',$this->contact);

			if(!$array){
				return false;
			}else{
				return true;
			}
		}

	}

	/**
	 * saveEvent function.
	 *
	 *
	 * @access public
	 * @param mixed $email
	 * @param mixed $type
	 * @return void
	 */
	public function saveEvent($email, $type){
		$this->getContact($email);
		if(!sizeof($this->maileoncontact)){
			return false;
		}
		if($this->maileoncontact == false){
			return false;
		}

		$event = array();
		$event['type'] = Config::get('maileon.'.getShopCode().'.'.$type.'_API_KEY');
		$event['contact'] = array('id'=>$this->maileoncontact->maileonid,'email'=>$email);
		$event['content'] = array();
		foreach($this->event as $key=>$value){
			$event['content'][$key] = $value;
		}

		$array = MaileonCore::request("transactions",Config::get('maileon.'.getShopCode().'.API_KEY'),'POST','json',json_encode(array($event)));

		try {
			if(sizeof($array)){
				if($array->reports->queued){
					return true;
				}
			}
		} catch (Exception $e) {

		}
		return false;
	}


	/**
	 * getContactUnsLinks function.
	 * leiratkozó linkek generálása.
	 *
	 * @access public
	 * @return void
	 */
	public function getContactUnsLinks(){
		//Leiratkozó linkek generálása
		$string = $this->contact->email."::VEGLEGES";
		$this->contact->custom_fields['Vegleges_leiratkozas'] = Config::get('maileon.'.getShopCode().'.UnsubLink').$this->unsEncode($string);
	}

	/**
	 * getContactParams function.
	 * A $this->contact mezőit rendezi link paraméterré.
	 *
	 * @access public
	 * @return void
	 */
	public function getContactParams(){
		$params = array();
		if($this->maileoncontact){
			$params['id'] = $this->maileoncontact->maileonid;
			$params['ignore_checksum'] =  "true";
			$params['checksum'] =  $this->maileoncontact->maileonid;
			$params['triggerdoi'] = "true";
		}else{

			$params['permission'] =  Config::get('maileon.'.getShopCode().'.PERMISSION');
			$params['src'] =  Config::get('maileon.'.getShopCode().'.SRC');
			$params['sync_mode'] = 2;
			$params['doi'] = true;
			$params['doiplus'] = true;

		}
		return $params;
	}




	/**
	 * unsEncode function.
	 * Leiratkozó link kódolása
	 *
	 * @access public
	 * @param mixed $string
	 * @return void
	 */
	public function unsEncode($string){

		$encoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(Config::get('maileon.'.getShopCode().'.UNS_KEY')), $string, MCRYPT_MODE_CBC, md5(md5(Config::get('maileon.'.getShopCode().'.UNS_KEY')))));
		return $encoded;
	}

	/**
	 * unsDecode function.
	 * Leiratkozó link dekódolása
	 *
	 * @access public
	 * @param mixed $string
	 * @return void
	 */
	public function unsDecode($string){
		$decoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(Config::get('maileon.'.getShopCode().'.UNS_KEY')), base64_decode($string), MCRYPT_MODE_CBC, md5(md5(Config::get('maileon.'.getShopCode().'.UNS_KEY')))), "\0");
		return $decoded;
	}

	/**
	 * isUnsubscribed function.
	 * Visszaadja, hogy az adott e-mail cím LEVAN-e iratkozva a listáról.
	 *
	 * @access public
	 * @param mixed $email
	 * @return void
	 */
	public function isUnsubscribed($email){
		$unsubscribed = Newsletter::select('email')
        ->withTrashed()
        ->whereNotNull('deleted_at')
		->where("email",$email)
		->first();
		if(sizeof($unsubscribed)){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * setContactDefaultField function.
	 * Alapértelmezett mezők meghatározása a $this->contact objektumba.
	 *
	 * @access public
	 * @return void
	 */
	public function setContactDefaultField(){

		if(getShopId() == 1){
			$this->contact->standard_fields['FULLNAME'] = ($this->contact->standard_fields['LASTNAME']." ".$this->contact->standard_fields['FIRSTNAME']);
		}else{
			$this->contact->standard_fields['FULLNAME'] = ($this->contact->standard_fields['FIRSTNAME']	." ".$this->contact->standard_fields['LASTNAME']);
		}
		$this->contact->permission = Config::get('maileon.'.getShopCode().'.PERMISSION');

	}

	/**
	 * __construct function.
	 * Indításkor a mailon beállítása
	 *
	 * @access public
	 * @return void
	 */
	function __construct()
    {
        /*$this->config();*/
        $this->event = new stdClass();
        $this->contact = new stdClass();
    }



}
