<?php

namespace App;

use Omnipay\Omnipay;

/**
 * Class PayPal
 * @package App
 */
class PayPal
{
    /**
     * @return mixed
     */
    public function gateway()
    {
        $gateway = Omnipay::create('PayPal_Express');

        $gateway->setUsername(getConfig('paypal_username'));
        $gateway->setPassword(getConfig('paypal_password'));
        $gateway->setSignature(getConfig('paypal_signature'));
        $gateway->setTestMode(getConfig('paypal_sandbox'));
        return $gateway;
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public function purchase(array $parameters)
    {
        $response = $this->gateway()
            ->purchase($parameters)
            ->send();

        return $response;
    }

    /**
     * @param array $parameters
     */
    public function complete(array $parameters)
    {
        $response = $this->gateway()
            ->completePurchase($parameters)
            ->send();

        return $response;
    }

    /**
     * @param $amount
     */
    public function formatAmount($amount)
    {
        return number_format($amount, 2, '.', '');
    }

    /**
     * @param $order
     */
    public function getCancelUrl($order)
    {
        return route('paypal.checkout.cancelled', $order);
    }

    /**
     * @param $order
     */
    public function getReturnUrl($order)
    {
        return route('paypal.checkout.completed', $order);
    }

    /**
     * @param $order
     */
    public function getNotifyUrl($order)
    {
        $env = getConfig('paypal_sandbox') ? "sandbox" : "live";

        return route('webhook.paypal.ipn', [$order, $env]);
    }
}
