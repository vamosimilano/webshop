<?php


use App\Database\R_DB;

class CartItem extends R_DB
{

	protected $table = 'cart_item';
	protected $primaryKey = 'cart_item_id';
    public $timestamps = false;

}
