<?php

use App\Database\R_DB;
/**
 * CartHelper class.
 * Kosár adatbázis műveletek
 */
class CartHelper extends R_DB  {

	protected $table = 'cart';
	protected $primaryKey = 'cart_id';
    public $timestamps = true;
	 use Illuminate\Database\Eloquent\SoftDeletes;

    /**
     * saveCart function.
     * Kosár mentése
     *
     * @access public
     * @static
     * @return void
     */
    public static function saveCart(){
        $cart_items = Cart::getContent();

        $cart = CartHelper::where('session_id', Session::getId())->first();
        if (!sizeof($cart)) {
           $cart = new CartHelper();
           $cart->session_id = Session::getId();
        }

        $user = Auth::user();
        if (sizeof($user)) {
            $cart->uid = $user->customer_id;
            $cart->firstname = $user->firstname;
            $cart->lastname = $user->lastname;
            $cart->email = $user->email;
        }else{
            if (Session::has('cart_data')) {
                $cart_data = Session::get('cart_data');
                $cart->firstname = (isset($cart_data['user_firstname']) ? $cart_data['user_firstname'] : '');
                $cart->lastname = (isset($cart_data['user_lastname']) ? $cart_data['user_lastname'] : '');
                $cart->email = (isset($cart_data['email']) ? $cart_data['email'] : '');
            }
        }


        $cart->save();
        $cart->saveItems();

    }

    /**
     * saveItems function.
     * Elemek mentése az order_item táblába
     *
     * @access public
     * @return void
     */
    public function saveItems(){
        $cart_items = Cart::getContent();
        CartItem::where('cart_id', $this->cart_id)->forceDelete();

        if (!sizeof($cart_items)) {
            return false;
        }

        foreach ($cart_items as $item) {
            if ($item->id != 'floor') {
                $product = Product::lang()->active()->find($item['attributes']->product_id);

                $options = [];
                foreach ($item['attributes'] as $k => $v){
                    $options[$k] = $v;
                }

                $cart_item = new CartItem();
                $cart_item->cart_id = $this->cart_id;
                $cart_item->price = $item->price;
                $cart_item->qty = ($item->qty ? $item->qty : 1);
                $cart_item->product_id = $item->attributes['product_id'];
                $cart_item->feature_id = 0;
                $cart_item->product_name = $product->getName();
                $cart_item->type = $product->type;
                $cart_item->opt = serialize($options);
                $cart_item->save();
            }
        }
    }

    /**
     * delete function.
     * Kosár törlése
     *
     * @access public
     * @static
     * @return void
     */
    public static function deleteItems(){
        $cart = CartHelper::where('session_id', Session::getId())->first();
        if (empty($cart)){
            return false;
        }
        CartItem::where('cart_id', $cart->cart_id)->forceDelete();
        $cart->forceDelete();

    }

    /**
     * removeFromArray function.
     * A kapott tömbböl kitöröljük azokat az elemeket, amelyek bent vannak a kosárban!
     *
     * @access public
     * @static
     * @param mixed $products
     * @return void
     */
    public static function removeFromArray($products) {

        $cart_items = Cart::content();

        foreach ($cart_items as $item) {
            if (in_array($item['attributes']->product_id, $products)){
                unset($products[$item['attributes']->product_id]);
            }
        }
        return $products;
    }

    /**
     * addToCart function.
     * Kosár visszaállítás
     *
     * @access public
     * @static
     * @param mixed $data
     * @return void
     */
    public static function addToCart($data) {
        $product_id = $data['product_id'];
    	$qty = $data['qty'];

    	$product = Product::lang()->find($product_id);
    	$options = [];
    	$options['product_id'] = $product_id;
        $options['type'] = $product->type;
        $options['custom_price'] = (isset($data['custom_price']) ? $data['custom_price'] : 0); //A terméknek meglehet adni cutom árat!
        $options['parent_cartitem_id'] = (isset($data['parent_cartitem_id']) ? $data['parent_cartitem_id'] : 0); //A parent cartitem id mentése.
        $options['sizes'] = [];
        $options['textile'] = [];
        $options['related'] = 0;
    	$price = 0;
    	if (isset($data['sizes'])){
            $options['sizes'] = $data['sizes'];
        }
        if (isset($data['related'])){
            $options['related'] = $data['related'];
        }
        if (isset($data['mounting'])) {
            $options['mounting'] = $data['mounting'];
        }
		if (isset($data['seat'])) {
            $options['seat'] = $data['seat'];
        }
        if (isset($data['textile'])) {
			
			//egész / 2színű / többszínű kezelése
			if (isset($data['textile'])) {
				if ((isset($data['textile']['butor'])) and ($data['textile']['butor']!="")) { //egész bútor
					//minden mást megszüntetünk
					foreach ($data['textile'] as $key => $textile_id) {
						if ($key != 'butor') unset($data['textile'][$key]);
					}
				}
				if (((isset($data['textile']['also'])) and ($data['textile']['also']!="")) or ((isset($data['textile']['felso'])) and ($data['textile']['felso']!=""))) { //2 színű
					//minden mást megszüntetünk
					foreach ($data['textile'] as $key => $textile_id) {
						if (($key != 'also') and ($key != 'felso')) unset($data['textile'][$key]);
					}
					if ((((isset($data['textile']['also'])) and ($data['textile']['also']=="")) or ((isset($data['textile']['felso'])) and ($data['textile']['felso']==""))) or ($data['textile']['also']==$data['textile']['felso'])) {
						//vagy also vagy felso nincs megadva -> egésznek számoljuk, vagy also = felso -> egész
						if ((isset($data['textile']['also'])) and ($data['textile']['also']=="")) {
							$data['textile']['butor'] = $data['textile']['felso'];
						} else {
							$data['textile']['butor'] = $data['textile']['also'];
						}
						unset($data['textile']['also']);
						unset($data['textile']['felso']);
					}
				}
				if (sizeof($data['textile'])>2) {
					//több szövet van megadva, ellenőrizni kell, hogy esetleg, nem egész, vagy 2színű-e
					$egesz=false;
					$egeszteszt=[];
					$alsoteszt=[];
					$felsoteszt=[];
					foreach ($data['textile'] as $key => $textile_id) {
						if ($textile_id!="") {
							$egeszteszt[$textile_id] = true;
							if (($key == "keret") or ($key == "karfa") or ($key == "tamla")) {
								$alsoteszt[$textile_id] = true;
							} else {
								$felsoteszt[$textile_id] = true;
							}
						}
					}
					if (sizeof($egeszteszt)==1) {
						//egyszínű
						foreach ($data['textile'] as $key => $textile_id) {
							if ($textile_id!="") {
								$data['textile']['butor'] = $textile_id;
								break;
							}
						}
						foreach ($data['textile'] as $key => $textile_id) {
							if ($key != 'butor') unset($data['textile'][$key]);
						}
					} else {
						if ((sizeof($alsoteszt)==1) and (sizeof($felsoteszt)==1)) {
							//2 színű
							foreach ($data['textile'] as $key => $textile_id) {
								if ($textile_id!="") {
									$egeszteszt[$textile_id] = true;
									if (($key == "keret") or ($key == "karfa") or ($key == "tamla")) {
										$data['textile']['also'] = $textile_id;
									} else {
										$data['textile']['felso'] = $textile_id;
									}
								}
							}
							foreach ($data['textile'] as $key => $textile_id) {
								if (($key != 'also') and ($key != 'felso')) unset($data['textile'][$key]);
							}
						} else {
							//többszínű
							foreach ($data['textile'] as $key => $textile_id) {
								if ($textile_id == '') unset($data['textile'][$key]);
							}
						}
					}
				}
			}
			
            foreach ($data['textile'] as $key => $value) {
                if ($value) {
                    $options['textile'][$key] = $value;
                }
            }
        }
    	if (isset($data['sizes'])){
            $price = $product->getDefaultPrice();
    	}
        /*if ($product->type == 'general') {
            $cart_id = $product->product_id;
        }else{*/
            $cart_id = bin2hex(mcrypt_create_iv(22, MCRYPT_DEV_URANDOM));
        //}
        if (isset($data['sizes'])){
            $price = CartHelper::calculateItemPrice($options);
        }else{
            $price = $product->getDefaultPrice();
        }
        if (($options['custom_price'] > 0) or ($options['custom_price'] < 0)) {
            $price = $options['custom_price'];
        }
        $options['current_cartitem_id'] = $cart_id;
    	Cart::add(array(
    	    'id' => $cart_id,
    	    'name' => $product->getName(),
    	    'quantity' => $qty,
    	    'price' => $price,
    	    'attributes' => $options
    	    )
        );
        return $cart_id;
    }

    /**
     * isEnableAdv function.
     * Elérte-e a vásárló az előleget.
     *
     * @access public
     * @static
     * @return void
     */
    public static function isEnableAdv(){
        return (CartHelper::getAdvPrice() > 0 ? true : false);
    }

    /**
     * getAdvPrice function.
     * Előleg számítás:
     * A tervezhető bútorok 30%-a.
     *
     * @access public
     * @static
     * @return void
     */
    public static function getAdvPrice() {
        $cart_items = Cart::getContent();
		$flooradvance = 0;
        if (sizeof($cart_items)) {
            $furniture_all = 0;
            foreach ($cart_items as $item) {
                if ($item->id == 'floor') {
					$flooradvance = $item->quantity * $item->price;
                    continue;
                }
                $product = Product::lang()->active()->find($item['attributes']->product_id);
				if (!isset($product)) {
					//CartHelper::removeItem($item->cart_item_id);
					continue;
				}
                //if ($product->type != 'general') {
				if (($product->advance > 0) or ($item['attributes']->parent_cartitem_id)) {
					//die(var_export($item));
                    $price_array = CartHelper::calculateItemPrice($item['attributes'],true);
					//die(print_r($price_array));
                    if ($price_array['furniture_price']>0) {
						if ($item['attributes']->parent_cartitem_id) {
							$furniture_all+=($item->price*$item->quantity)*1;
						} else {
							$furniture_all+=($price_array['furniture_price']*$item->quantity)*getConfig('advance_x');
						}
                    } else {
						$furniture_all+=($item->price*$item->quantity)*$product->advance;
					}
					if ($price_array['textile_price']) {
                        $furniture_all+=$price_array['textile_price']*$item->quantity;
                    }
                }
            }
			if ($furniture_all>0) return ($furniture_all+Cart::getShipping(true)+$flooradvance);
        }
        return 0;
    }
	
	public static function getAdvPriceTeszt() {
		$data = array();
		$i=0;
        $cart_items = Cart::getContent();
        if (sizeof($cart_items)) {
            $furniture_all = 0;
            foreach ($cart_items as $item) {
                if ($item->id == 'floor') {
                    continue;
                }
                $product = Product::lang()->active()->find($item['attributes']->product_id);
				$data[$i]["item"] = $item;
				$data[$i]["product_id"] = $item['attributes']->product_id;
				$data[$i]["advance"] = $product->advance;
				$data[$i]["price_array"] = array();
				$data[$i]["parent_cartitem_id"] = $item['attributes']->parent_cartitem_id;
                //if ($product->type != 'general') {
				if (($product->advance > 0) or ($item['attributes']->parent_cartitem_id)) {
					//die(var_export($item));
					
                    $price_array = CartHelper::calculateItemPrice($item['attributes'],true);
					$data[$i]["price_array"] = $price_array;
					//die(print_r($price_array));
                    if ($price_array['furniture_price']>0) {
						if ($item['attributes']->parent_cartitem_id) {
							$furniture_all+=($item->price*$item->quantity)*1;
							$data[$i]["c"]=($item->price*$item->quantity)*1;
						} else {
							$furniture_all+=($price_array['furniture_price']*$item->quantity)*getConfig('advance_x');
							$data[$i]["a"]=($price_array['furniture_price']*$item->quantity)*getConfig('advance_x');
						}
                    } else {
						$furniture_all+=($item->price*$item->quantity)*$product->advance;
						$data[$i]["b"]=($item->price*$item->quantity)*$product->advance;
					}
					if ($price_array['textile_price']) {
                        $furniture_all+=$price_array['textile_price']*$item->quantity;

                    }
                }
				$i++;
            }
			if ($furniture_all>0) return (array($furniture_all+Cart::getShipping(true),$data));
        }
        return 0;
    }

    /**
     * calculateItemPrice function.
     * Kiszámolja a kosár paramétereke alapján a termék árát.
     *
     * @access public
     * @param mixed $options
     * @return void
     */
    public static function calculateItemPrice($options, $return_array = false) {
        return PriceCalculator::calculateItemPrice($options, $return_array);
    }

    public static function getFloorCost($cart_data){
        if(isset($cart_data['floor']) and Cart::get('floor')){
            return Cart::get('floor')->getPriceSum();
        }
        return 0;
    }

    public static function writeDataByOptions($options){
        if (!isset($options['product_id'])) {
            return '';
        }
        $string = '';

        $exist_sizes = ProductSize::where('product_id', $options['product_id'])->first();
        if (isset($options['sizes']) and !empty($exist_sizes)){
            $size_count = sizeof($options['sizes']);
            if ($size_count != 1){
                $string.='<b>'.t('Méretek').":</b> <br>";
            }
            foreach($options['sizes'] as $size_item => $size_name){
                if ($size_count == 1){
                    $string.=t('Méret').": ".$size_name." ".ProductSize::getSizeString($options['product_id'],$size_item,$size_name,false)."<br>";
                }else{
                    $string.=t('oldal_'.$size_item).": ".ProductSize::getSizeString($options['product_id'],$size_item,$size_name, true)."<br>";
                }
            }
        }
        if (isset($options['mounting'])){
            $string.='<b>'.t('Elrendezés')."</b>: ".($options['mounting'] == 'left' ? t('Balos') : t('Jobbos'))."<br>";
        }
		if (isset($options['seat'])){
            $string.='<b>'.t('Ülőfelület')."</b>: ".($options['seat'] == 'szivacs' ? t('Szivacs') : t('Rugó'))."<br>";
        }
        if (isset($options['related']) and $options['related'] > 0){
            $string.='<b>'.t('Kapcsolódó termék')."</b>: ".ProductRelated::renderToCart($options['related'])."<br>";
        }
        if (isset($options['textile'])){
            if (sizeof($options['textile'])){
                $string.='<b>'.t('Színek').":</b> <br>";
                foreach ($options['textile'] as $key => $textile_id){
                    $part = ProductPart::lang()->where('admin_name', $key)->first();
                    $textile = ProductTextile::lang()->where('product_textiles.textile_id', $textile_id)->first();
                    $string.=$part->name.": ".$textile->name."<br>";
                }
            }
        }
        return $string;
    }
	
	public static function ecommercevariant($options){
        if (!isset($options['product_id'])) {
            return '';
        }
        $string = '';

        $exist_sizes = ProductSize::where('product_id', $options['product_id'])->first();
        if (isset($options['sizes']) and !empty($exist_sizes)){
            $size_count = sizeof($options['sizes']);
            foreach($options['sizes'] as $size_item => $size_name){
				$string.=$size_name." ";
            }
        }
        if (isset($options['mounting'])){
            $string.=$options['mounting'];
        }
		if (isset($options['seat'])){
            $string.=$options['seat'];
        }
        return $string;
    }

    public static function saveCoupon($code, $data) {
        $data['coupon'] = $code;
        Session::put('cart_data', $data);
    }
    public static function removeCoupon($data) {
        if (isset($data['coupon'])) {
            unset($data['coupon']);
        }
        Session::put('cart_data', $data);
    }

    public static function removeItem($rowId) {
        $item = Cart::get($rowId);
		$pitem = -1;

        //Ha van megadva parent itemid, akkor azt is töröljük. Related termékeknél van jelentősége
        if (isset($item['attributes']['parent_cartitem_id'])
            and $item['attributes']['parent_cartitem_id'] != '0'
            and $item['attributes']['parent_cartitem_id'] != '') {
			$pitem=$item['attributes']['parent_cartitem_id'];
            Cart::remove($item['attributes']['parent_cartitem_id']);
        }

        //Megkeressük, hogy valahol máshol van-e megadva caritem_id.
        $items = Cart::getContent();
        if (sizeof($items)) {
            foreach ($items as $item_) {
				if (isset($item_['attributes']['parent_cartitem_id'])) {
					if ($item_['attributes']['parent_cartitem_id'] != ''
						and $item_['attributes']['parent_cartitem_id'] != '0'
						and (
							($item_['attributes']['parent_cartitem_id'] == $rowId) or ($item_['attributes']['parent_cartitem_id'] == $pitem))
						) {
						Cart::remove($item_['id']);
					}
				}
            }
        }

    	Cart::remove($rowId);
    	CartHelper::saveCart();
    }
	
	public static function getCartMainItemsCount() {
		$items = Cart::getContent();
		$sum=0;
        if (sizeof($items)) {
            foreach ($items as $item) {
				if ($item->id == 'floor') {
                    continue;
                }
				$product = Product::lang()->active()->find($item['attributes']->product_id);
				if ((isset($product)) and (in_array($product->category_id, getConfig('maincategory')))) $sum = $sum + $item->quantity;
			}
		}
		return $sum;
	}
	
	public static function getCartAdditionalItemsCount() {
		$items = Cart::getContent();
		$sum=0;
        if (sizeof($items)) {
            foreach ($items as $item) {
				if ($item->id == 'floor') {
                    continue;
                }
				$product = Product::lang()->active()->find($item['attributes']->product_id);
				if ((isset($product)) and (in_array($product->category_id, getConfig('additionalcategory')))) $sum = $sum + $item->quantity;
			}
		}
		return $sum;
	}
	
	public static function getCartOutletItemsCount() {
		$items = Cart::getContent();
		$sum=0;
        if (sizeof($items)) {
            foreach ($items as $item) {
				if ($item->id == 'floor') {
                    continue;
                }
				$product = Product::lang()->active()->find($item['attributes']->product_id);
				if ((isset($product)) and (in_array($product->category_id, getConfig('outletcategory')))) $sum = $sum + $item->quantity;
			}		
		}
		return $sum;
	}
	
	public static function getCartItemsSum() {
		$items = Cart::getContent();
		$sum=0;
        if (sizeof($items)) {
            foreach ($items as $item) {
				if ($item->id == 'floor') {
                    continue;
                }
				$product = Product::lang()->active()->find($item['attributes']->product_id);
				if ((isset($product)) and (
					(in_array($product->category_id, getConfig('maincategory'))) or 
					(in_array($product->category_id, getConfig('additionalcategory'))) or 
					(in_array($product->category_id, getConfig('outletcategory')))
				)) $sum = $sum + ($item->price * $item->quantity);
			}		
		}
		return $sum;
	}
	
	public static function getCartManufactureItemsSum() {		$items = Cart::getContent();		$sum=0;        if (sizeof($items)) {            foreach ($items as $item) {				if ($item->id == 'floor') {                    continue;                }				$product = Product::lang()->active()->find($item['attributes']->product_id);				if ((isset($product)) and (in_array($product->category_id, getConfig('manufacturecategory')))) $sum = $sum + ($item->price * $item->quantity);			}				}		return $sum;	}
	
	public static function getCartOutletItemsSum() {		$items = Cart::getContent();		$sum=0;        if (sizeof($items)) {            foreach ($items as $item) {				if ($item->id == 'floor') {                    continue;                }				$product = Product::lang()->active()->find($item['attributes']->product_id);				if ((isset($product)) and (in_array($product->category_id, getConfig('outletcategory')))) $sum = $sum + ($item->price * $item->quantity);			}				}		return $sum;	}		public static function getCartOtherItemsSum() {		$items = Cart::getContent();		$sum=0;        if (sizeof($items)) {            foreach ($items as $item) {				if ($item->id == 'floor') {                    continue;                }				$product = Product::lang()->active()->find($item['attributes']->product_id);				if ((isset($product)) and (!in_array($product->category_id, getConfig('manufacturecategory'))) and (!in_array($product->category_id, getConfig('outletcategory')))) $sum = $sum + ($item->price * $item->quantity);			}				}		return $sum;	}		public static function getCartManufactureRelatedItemsSum() {		$items = Cart::getContent();		$sum=0;        if (sizeof($items)) {            foreach ($items as $item) {				if ($item->id == 'floor') {                    continue;                }				$product = Product::lang()->active()->find($item['attributes']->product_id);				if (isset($item['attributes']['parent_cartitem_id']) 					and $item['attributes']['parent_cartitem_id'] != '0'					and $item['attributes']['parent_cartitem_id'] != '') {					if ((isset($product)) and (!in_array($product->category_id, getConfig('manufacturecategory'))) and (!in_array($product->category_id, getConfig('outletcategory')))) {
						//ha gyártandó a szülő
						$pitem = Cart::get($item['attributes']['parent_cartitem_id']);
						$pproduct = Product::lang()->active()->find($pitem['attributes']->product_id);
						if ((isset($pproduct)) and (in_array($pproduct->category_id, getConfig('manufacturecategory')))) {
							$sum = $sum + ($item->price * $item->quantity);
						}
					}				}			}		}		return $sum;	}
	
	public static function getCartOutletRelatedItemsSum() {
		$items = Cart::getContent();
		$sum=0;
        if (sizeof($items)) {
            foreach ($items as $item) {
				if ($item->id == 'floor') {
                    continue;
                }
				$product = Product::lang()->active()->find($item['attributes']->product_id);
				if (isset($item['attributes']['parent_cartitem_id']) 
					and $item['attributes']['parent_cartitem_id'] != '0'
					and $item['attributes']['parent_cartitem_id'] != '') {
					if ((isset($product)) and (!in_array($product->category_id, getConfig('manufacturecategory'))) and (!in_array($product->category_id, getConfig('outletcategory')))) {
						//ha outletes a szülő
						$pitem = Cart::get($item['attributes']['parent_cartitem_id']);
						$pproduct = Product::lang()->active()->find($pitem['attributes']->product_id);
						if ((isset($pproduct)) and (in_array($pproduct->category_id, getConfig('outletcategory')))) {
							$sum = $sum + ($item->price * $item->quantity);
						}
					}
				}
			}
		}
		return $sum;
	}
}