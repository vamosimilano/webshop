<?php
class PriceCalculator
{

    /**
     * calculateItemPrice function.
     * Kiszámolja a kosár paramétereke alapján a termék árát.
     *
     * @access public
     * @param mixed $options
     * @return void
     */
    public static function calculateItemPrice($options, $return_array = false) {
        if (empty($options['sizes'])){
            return 0;
        }

        $debug = ['textiles' => []];

        $max = 'S';
		
		if (count($options['sizes'])==1) {
			foreach ($options['sizes'] as $size) {
				if (isset($size->size_name)) {
					$max = $size->size_name;
				} else {
					$max = $size;
				}
			}
		} else {
			foreach ($options['sizes'] as $size) {
				if ($max == 'S' and ($size == 'M' or $size == 'L' or $size == 'XL')){
					$max = $size;
				}elseif ($max == 'M' and ($size == 'L' or $size == 'XL')){
					$max = $size;
				}elseif ($max == 'L' and ($size == 'XL')){
					$max = $size;
				}elseif ($size == 'XL'){
					$max = $size;
				}
			}
		}
		//die($max);

        $debug['max'] = $max;

        $size = ProductSize::lang()->where('product_id', $options['product_id'])
            ->where('size_name', $max)
            ->first();
        if (!empty($size)){
            //A bútor alapára.
            $furniture_price = $all_price = $size->getPrice();
            //Ha van választva kapcsolódó termék, akkor arra változik a termék alapára
			/* kivettem a checkbox miatt, nem lehet alapárat változtatni checkboxokkal
            if (isset($options['related']) and $options['related'] != '0' and $options['related'] != '') {
                $related_ = ProductRelated::find($options['related']);
                if ($related_->price > 0){
                    $furniture_price = $all_price = ($related_->price == 1 ? 0 : $related_->price);
                }
            }
			*/

        }else{
            $product = Product::lang()->active()->where('products.product_id', $options['product_id'])->first();
            $furniture_price = $all_price = $product->getDefaultPrice();
        }


        //Összeszedjük az összes kiegészítőhőz tartozó szorzót, amivel majd fel kell szorozni a textilt.
        $partsizes = Product::getPartSizes($options['product_id']);

        $textile_price = 0;

        //Ingyenes textilek száma
        $free_count = 0;

        //Textilek száma
        $textile_num = 0;

        //Kispárna nagypárna
        $kn = $np = 0;

        //Különbőző textilek
        $textile_different = [];
		
		try {
			//egész / 2színű / többszínű kezelése
			if (isset($options['textile'])) {
				if ((isset($options['textile']['butor'])) and ($options['textile']['butor']!="")) { //egész bútor
					//minden mást megszüntetünk
					foreach ($options['textile'] as $key => $textile_id) {
						if ($key != 'butor') unset($options['textile'][$key]);
					}
				}
				if (((isset($options['textile']['also'])) and ($options['textile']['also']!="")) or ((isset($options['textile']['felso'])) and ($options['textile']['felso']!=""))) { //2 színű
					//minden mást megszüntetünk
					foreach ($options['textile'] as $key => $textile_id) {
						if (($key != 'also') and ($key != 'felso')) unset($options['textile'][$key]);
					}
					if ((((isset($options['textile']['also'])) and ($options['textile']['also']=="")) or ((isset($options['textile']['felso'])) and ($options['textile']['felso']==""))) or ($options['textile']['also']==$options['textile']['felso'])) {
						//vagy also vagy felso nincs megadva -> egésznek számoljuk, vagy also = felso -> egész
						if ((isset($options['textile']['also'])) and ($options['textile']['also']=="")) {
							$options['textile']['butor'] = $options['textile']['felso'];
						} else {
							$options['textile']['butor'] = $options['textile']['also'];
						}
						unset($options['textile']['also']);
						unset($options['textile']['felso']);
					}
				}
				if (sizeof($options['textile'])>2) {
					//több szövet van megadva, ellenőrizni kell, hogy esetleg, nem egész, vagy 2színű-e
					$egesz=false;
					$egeszteszt=[];
					$alsoteszt=[];
					$felsoteszt=[];
					foreach ($options['textile'] as $key => $textile_id) {
						if ($textile_id!="") {
							$egeszteszt[$textile_id] = true;
							if (($key == "keret") or ($key == "karfa") or ($key == "tamla")) {
								$alsoteszt[$textile_id] = true;
							} else {
								$felsoteszt[$textile_id] = true;
							}
						}
					}
					if (sizeof($egeszteszt)==1) {
						//egyszínű
						foreach ($options['textile'] as $key => $textile_id) {
							if ($textile_id!="") {
								$options['textile']['butor'] = $textile_id;
								break;
							}
						}
						foreach ($options['textile'] as $key => $textile_id) {
							if ($key != 'butor') unset($options['textile'][$key]);
						}
					} else {
						if ((sizeof($alsoteszt)==1) and (sizeof($felsoteszt)==1)) {
							//2 színű
							foreach ($options['textile'] as $key => $textile_id) {
								if ($textile_id!="") {
									$egeszteszt[$textile_id] = true;
									if (($key == "keret") or ($key == "karfa") or ($key == "tamla")) {
										$options['textile']['also'] = $textile_id;
									} else {
										$options['textile']['felso'] = $textile_id;
									}
								}
							}
							foreach ($options['textile'] as $key => $textile_id) {
								if (($key != 'also') and ($key != 'felso')) unset($options['textile'][$key]);
							}
						} else {
							//többszínű
							foreach ($options['textile'] as $key => $textile_id) {
								if ($textile_id == '') unset($options['textile'][$key]);
							}
						}
					}
				}
			}
		} catch (Exception $e) {
			//
		}
        if (isset($options['textile'])) {
            $textile_num = sizeof($options['textile']);
			
			//imagerender kezdete
			if (((isset($options['textile']['also'])) and (isset($options['textile']['felso']))) or (isset($options['textile']['butor']))) {
				if ((isset($options['textile']['also'])) and (isset($options['textile']['felso']))) {
					$also = $options['textile']['also'];
					$felso = $options['textile']['felso'];
				} else {
					$also = $options['textile']['butor'];
					$felso = $options['textile']['butor'];
				}
				$renderdir ="/var/www/clients/client1/web1/web/public/static/ir/".$options['product_id']; 
				$renderfilename = "Render_".$options['product_id']."_".$also."_".$felso."_1.png";
				if (!is_dir($renderdir)) {
					mkdir($renderdir);
				}
				if (!is_file($renderdir."/".$renderfilename)) {
					$q = DB::select("SELECT count(*) as ertek FROM product_images_gen WHERE product_id=".$options['product_id']." and also_textile_id=".$also." and felso_textile_id=".$felso." and imageurl1 is null");
					foreach($q as $p) {
						if ($p->ertek == 0) {
							
							$t1 = ProductTextile::lang()->where('product_textiles.textile_id', $also)->first();
							$t2 = ProductTextile::lang()->where('product_textiles.textile_id', $felso)->first();
							
							DB::table('product_images_gen')->insert(
								[	
									'product_id' => $options['product_id'], 
									'also_colorcode' => $t1->colorcode,
									'also_textile_id' => $also,
									'felso_colorcode' => $t2->colorcode,
									'felso_textile_id' => $felso
								]
							);
						}
					}
				}
			}
			//imagerender vége

            foreach ($options['textile'] as $key => $textile_id) {

                $extra_price = ProductTextile::ExtraPrice()->where('product_textiles.textile_id', $textile_id)->first();

                if (!empty($extra_price)) {
                    if ((isset($partsizes[$key])) or (isset($partsizes["elemes-".$key]))) {
						$maxt=$max;
						if (strpos($max,"-")) $maxt=substr($max,0,strpos($max,"-"));
						if (isset($partsizes[$key])) {
							$multiple = $partsizes[$key][$maxt];
						} else {
							$multiple = $partsizes["elemes-".$key][$maxt];
						}
                    }else{
                        $multiple = 1;
                    }


                    $debug['textiles'][] =
                    [
                        'id' => $textile_id,
                        'extra_price' => $extra_price->extra_price,
                        'multiple' => $multiple
                    ];

                    $all_price+=($extra_price->extra_price*$multiple);
                    $textile_price+=($extra_price->extra_price*$multiple);

                    if ($extra_price->extra_price == 0) {
                        $free_count++;
                    }
                    //ha többszínű, és CSAK a kispárna színe tér el, akkor fix 5000 forint felár van.
                    if ($textile_num > 2) {
                        if ($key == 'nagyparna') {
                            $np = $textile_id;
                        }
                        if ($key == 'kisparna') {
                            $kn = $textile_id;
                        }
                    }
					$textile_different[$textile_id] = true;
                }
            }
        }
        //ha többszínű, és CSAK a kispárna színe tér el, akkor fix 5000 forint felár van.
        if ($np != 0 and $kn != 0) {
            if ($np != $kn) {
                $all_price+=getConfig('textile_extra_once_price');
                $textile_price+=getConfig('textile_extra_once_price');
                $debug['kisparnanagyparna'] = 'added';
            }
        }

        $percent = 0;
        switch (sizeof($textile_different)) {
            case '3':
                $percent = 0.04;
            break;
            case '4':
                $percent = 0.05;
            break;
            case '5':
                $percent = 0.06;
            break;
            case '6':
                $percent = 0.07;
            break;
            case '7':
                $percent = 0.08;
            break;
        }
        if ($percent > 0) {
            $plus_price = ($furniture_price*$percent);
            $all_price+=$plus_price;
            $textile_price+=$plus_price;
            $debug['percent'] = $percent;
            $debug['plus_price'] = $plus_price;
        }


        if ($return_array) {
			$debug['options'] = $options;
            return [
                'all_price' => $all_price,
                'textile_price' => $textile_price,
                'furniture_price' => $furniture_price,
                'debug' => $debug,
            ];
        }
        return $all_price;
    }


}
