<?php

use App\Database\R_DB;

class ImageCache extends R_DB{

    protected $table = 'image_cache';
    protected $primaryKey = 'cache_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

    public static function cache($filename, $filetype){
        return $filename;
        try {

            if ($filename) {
                $exist = self::where('image_url', url($filename))->first();
                if (!empty($exist)){
                    $base64 = $exist->cache;
                }else{
                    set_time_limit(2);
                    $opts = array('http' =>
                        array(
                            'method'  => 'GET',
                            'timeout' => 120
                        )
                    );
                    $filename_path = str_replace(Config::get('website.static_root_url'), Config::get('website.static_root_path'), $filename);

                    if (is_file($filename_path)) {
                        $imgbinary = file_get_contents($filename_path);
                    }else{
                        return $filename;
                    }



                    $base64 = base64_encode($imgbinary);
                    $cache = new self();
                    $cache->image_url = url($filename);
                    $cache->cache = $base64;
                    $cache->save();


                }

                return 'data:image/' . $filetype . ';base64,' .$base64;
            }
        } catch (Exception $e) {
           return $filename;
        }
    }

    public static function download(){
		
		die('/Volumes/Munka TAL�LVA!');

        /*$textiles = ProductTextile::all();
        foreach ($textiles as $textile) {
            if ($textile->admin_name != ''){
                if (!is_file('/var/www/clients/client1/web1/vamosi/download/textiles/'.$textile->image)){
                    $data = @file_get_contents("http://vamosimilano.hu/kepek/textil/".$textile->admin_name."/large2");
                    if ($data){
                    file_put_contents('/var/www/clients/client1/web1/vamosi/download/textiles/'.$textile->image, $data);
                    }
                }
            }

        }
        die('RUN');
*/

        if (Input::get('merge1')){
        $images = ProductImage::all();
        foreach ($images as $image) {
            if ($image->image_url != ''){
                $product = Product::find($image->product_id);
                if (!is_file('/var/www/clients/client1/web1/vamosi/download/images/'.$image->image_url)){
                    $data = @file_get_contents("http://vamosimilano.hu/kepek/termek/".$product->admin_name."/".$image->num."?size=4");
                    if ($data){
                    file_put_contents('/var/www/clients/client1/web1/vamosi/download/images/'.$image->image_url, $data);
                    }
                }
            }

        }
        die('RUN');
        }

        if (Input::get('merge')){

            $images = ProductGenImage::all();
            foreach ($images as $image) {
                if ($image->image != ''){
                    $product = Product::find($image->product_id);
                    if (!is_file('/var/www/clients/client1/web1/vamosi/download/images_gen/'.$image->image)){
                        $data = @file_get_contents("http://vamosimilano.hu/kepek/altalanos/".$product->admin_name);
                        if ($data){
                        file_put_contents('/var/www/clients/client1/web1/vamosi/download/images_gen/'.$image->image, $data);

                        $iimage = Image::make('/var/www/clients/client1/web1/vamosi/download/images_gen/'.$image->image);

                        $canvas = Image::canvas(600, 375);

                        $iimage->resize(600, 375, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                        $canvas->insert($iimage, 'center');
                        $canvas->save('/var/www/clients/client1/web1/vamosi/download/images_gen_600/'.$image->image);

                        }
                    }
                }

            }
            die('RUN');
        }

    }


}
