<?php
use App\Database\R_DB;

class Carousels extends R_DB
{

    protected $table = 'carousel';
    protected $primaryKey = 'carousel_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;
    public static $rules = array(
        'title'=>'required|min:2',
        'link'=>'required|min:2',
        'type'=>'required',

    );

    public static function getFieldNames()
    {
        return array(
            'title' => t('Cím'),
            'link' => t('Hivatkozás'),
            'type' => t('Típus'),
        );
    }

	function getImageUrl(){

    	$url = Config::get('website.carousel_image_url');
    	if($this->image_url == ''){
           	return get404Image(600,375);
        }
        return $url."/full/".$this->image_url;


	}
	function getMobilImageUrl(){
    	$url = Config::get('website.carousel_image_url');
    	if($this->mobil_img == ''){
           	return $this->getImageUrl();
        }
        return $url."/full/".$this->mobil_img;


	}

	function scopeActive($query){
    	$date = new DateTime();
    	return $query->where('active', 1)
    	->where('date_from','<',$date->format('Y-m-d H:i:s'))
    	->where('date_to','>',$date->format('Y-m-d H:i:s'));
	}
}
