<?php
use App\Database\R_DB;

class ShippingCost extends R_DB
{
    protected $table = 'shipping_cost';
    protected $primaryKey = 'id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

}
