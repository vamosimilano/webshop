<?php
use App\Database\R_DB;

class Cities extends R_DB
{

    protected $table = 'zipcode';
    protected $primaryKey = 'id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;
    public static $rules = array(
        'zipcode'=>'required',
        'settlement'=>'required',




    );

    public static function getFieldNames()
    {
        return array(
            'zipcode' => t('Irányítószám'),
            'settlement' => t('Város'),

        );
    }

}
