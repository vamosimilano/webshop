<?php
use App\Database\R_DB;

class Shipping
{
    public static function getShippingCostMiles($params)
    {


        //Budapest, XXI. kerület, Déli utca 8, 1211
        /*$params = [
            'city' => 'Tököl',
            'address' => 'Ibolya utca 8/2',
            'zip' => '2316',
            'delivery_mode' => 'PERSONAL_BP', //'PERSONAL_BP','PERSONAL_DEB','PERSONAL_VES','COURIER','OTHER'
        ];*/
        $params['meter'] = 0;
        if (strstr($params['delivery_mode'], 'PERSONAL') and $params['delivery_mode'] != 'PERSONAL_LONDON') {
            $params['price'] = getConfig('other_city_price');
            return $params;
        }


        if ($params['delivery_mode'] == 'PERSONAL_LONDON') {
            $params['price'] = 0;
            return $params;
        }
		
		if ($params['delivery_mode'] == 'MPL') {
            $params['price'] = 8000;
            return $params;
        }

		//fix 89
		//$params['price'] = 89;
        //return $params;
		
        $zip_array = str_split($params['zip']);
        $dest = trim($params['address']) . ", " . trim($params['city']) . ", " . trim($params['zip']);

        if (Cache::has(base64_encode($dest)))
        {
            return Cache::get(base64_encode($dest));
        }
		
		/*
        if ($zip_array[0] == getConfig('default_city_zip_start')){
            $params['price'] = getConfig('default_city_zip_price');
            return $params;
        }
		
		*/
		
		$city = Cities::where('shop_id',getShopId())
			->where(DB::raw("instr('".$params['zip']."',zipcode)"), '=', 1)
			->first();

		if (empty($city)) {
			//$params['fn'] = "flash('error','".t('Ellenőrizze a címet, vagy a megadott címre nem tudunk házhozszállítást vállalni!')."');";
			//$params['price'] = 0;
			//return $params;
		}else{
			$params['price'] = getConfig('default_city_zip_price');
			return $params;
		}
		
		//csak londonba van kiszállítás, más város = hiba, a lenti kód nem fut le jelenleg!
		
        $options = array(
            'origin'        => getConfig('central_address'),
            'destination'   => $dest,
            'sensor'        => 'true',
            'units'         => 'imperial',
            'mode'          => 'driving',
            'language'      => 'en',
            'key'           => getConfig('maps_api_key')
        );

        $params_string = "";
        foreach($options as $var => $val){
            $params_string .= '&' . $var . '=' . urlencode($val);
        }

        $url = "https://maps.googleapis.com/maps/api/directions/json?".ltrim($params_string, '&');

        for ($i = 0; $i < 100; $i++)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
            $return = curl_exec($ch);

            $json = json_decode($return);
            if ($json->status == "OK") {
                break;
            }
        }
        if (!count($json->routes)) {
            $meter = 5000000;
			//$params['fn'] = "flash('error','".t('Ellenőrizze a címet, vagy a megadott címre nem tudunk házhozszállítást vállalni!')."');";
			$params['error'] = t('Ellenőrizze a címet!');
			$params['price'] = 0;
        }else {
            $meter = $json->routes[0]->legs[0]->distance->value;
        }

        if ($meter <= getConfig('default_city_zone')/1000) {
            $price = getConfig('default_city_zone_price'); //15000 helyett 17990
			$params['error'] = 'default_city_zone_price';
        } else {
			/*
            $price = ceil($meter / 1760.0) * 2 * getConfig('other_city_km');
            $price += 880;
            $price = intval($price / 1760);
            $price = $price * 1760;
			*/
			
			$mile = ceil($meter / 1609.3) ;
			$params['mile'] = $mile;
			if ($mile<=25) {
				$price = 89;
			} else {
				$price = (($mile - 25) * getConfig('other_city_km') * 2) + 89;
			}
        }

        $params['meter'] = $meter;
        $params['price'] = $price;
		$params['debug'] = $url;

        //Cachelünk, hogy gyorsabb legyen a következő lekérdezés
        Cache::put(base64_encode($dest), $params, 15);

        return $params;
    }
	
	public static function getShippingCost($params)
    {


        //Budapest, XXI. kerület, Déli utca 8, 1211
        /*$params = [
            'city' => 'Tököl',
            'address' => 'Ibolya utca 8/2',
            'zip' => '2316',
            'delivery_mode' => 'PERSONAL_BP', //'PERSONAL_BP','PERSONAL_DEB','PERSONAL_VES','COURIER','OTHER'
        ];*/
        $params['meter'] = 0;
        if (strstr($params['delivery_mode'], 'PERSONAL') and $params['delivery_mode'] != 'PERSONAL_BP') {
            $params['price'] = getConfig('other_city_price');
            return $params;
        }

        if ($params['delivery_mode'] == 'PERSONAL_BP') {
            $params['price'] = 0;
            return $params;
        }
		
		if ($params['delivery_mode'] == 'MPL') {
            $params['price'] = 8000;
            return $params;
        }


        $zip_array = str_split($params['zip']);
        $dest = trim($params['address']) . ", " . trim($params['city']) . ", " . trim($params['zip']);

        if (Cache::has(base64_encode($dest)))
        {
            return Cache::get(base64_encode($dest));
        }

        if ($zip_array[0] == getConfig('default_city_zip_start')){
            $params['price'] = getConfig('default_city_zip_price');
            return $params;
        }

        $options = array(
            'origin'        => getConfig('central_address'),
            'destination'   => $dest,
            'sensor'        => 'true',
            'units'         => 'metric',
            'mode'          => 'driving',
            'language'      => 'hu',
            'key'           => getConfig('maps_api_key')
        );

        $params_string = "";
        foreach($options as $var => $val){
            $params_string .= '&' . $var . '=' . urlencode($val);
        }

        $url = "https://maps.googleapis.com/maps/api/directions/json?".ltrim($params_string, '&');

        for ($i = 0; $i < 100; $i++)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
            $return = curl_exec($ch);

            $json = json_decode($return);
            if ($json->status == "OK") {
                break;
            }
        }
        if (!count($json->routes)) {
            $meter = 500000;
        }else {
            $meter = $json->routes[0]->legs[0]->distance->value;
        }



        if ($meter <= getConfig('default_city_zone'))
            $price = getConfig('default_city_zone_price'); //15000 helyett 17990
        else
        {
			if (ceil($meter / 1000.0)<=119) {
				//180 Ft/km 70-120km
				$price = ceil($meter / 1000.0) * 180;
				$price += 500;
				$price = intval($price / 1000);
				$price = $price * 1000;
			} else {
				//140 Ft/km 120km fölött
				$price = ceil($meter / 1000.0) * 140;
				$price += 500;
				$price = intval($price / 1000);
				$price = $price * 1000;
			}
        }

        $params['meter'] = $meter;
        $params['price'] = $price;

        //Cachelünk, hogy gyorsabb legyen a következő lekérdezés
        Cache::put(base64_encode($dest), $params, 15);

        return $params;
    }
	
	public static function getMPLCost($params) {
		$mplcost = 0;
		$meter = 0;
		foreach (Cart::getContent() as $item) {
			if ($item->id== 'floor') {
                continue;
            }
			if (((isset($item['attributes']['related']) and ($item['attributes']['related'] != '0')) or (isset($item['attributes']['parent_cartitem_id']) and ($item['attributes']['parent_cartitem_id'] != '0')))) {
				//continue;
			}

            $product = Product::lang()->active()->find($item['attributes']->product_id);
			
			if ($params['delivery_mode'] != "MPL") {
				if ((isset($product)) and (in_array($product->category_id, getConfig('outletcategory')))) continue;
				if ((isset($product)) and (in_array($product->category_id, getConfig('manufacturecategory')))) continue;
			}
			
			if ($product->ship_type == "mpl") {
				if ($product->ship_price == "none") {
					//miért nincs???
				} elseif ($product->ship_price == "percent") {
					//súly alapján lekérdezni a költséget * percentel
					$cost = ShippingCost::where('weight', '<=', $product->weight)->orderby('weight','DESC')->first();
					if (!isset($cost)) {
						$cost = ShippingCost::where('weight', 1)->first();
					}
					if ($product->ship_price_multi>0) {
						$mplcost += $cost->price * $product->ship_price_multi * $item->quantity;
					} else {
						$mplcost += $cost->price * $item->quantity;
					}
				} elseif ($product->ship_price == "fixed") {
					$cost = ShippingCost::where('weight', 1)->first();
					if ($product->ship_price_multi > $cost->price) {
						$mplcost += $product->ship_price_multi * $item->quantity;
					} else {
						$mplcost += $cost->price * $item->quantity;
					}
				} else {
					$mplcost += 8000 * $item->quantity;
				}
			} else {
				//$mplcost += 8000 * $item->quantity;
			}
		}
		$params['meter'] = $meter;
        $params['price'] = $mplcost;
		return $params;
	}
	
	public static function MPLOK() {
		foreach (Cart::getContent() as $item) {
			if ($item->id== 'floor') {
                continue;
            }
            $product = Product::lang()->active()->find($item['attributes']->product_id);
			if ($product->ship_type != "mpl") {
				return false;
			}
		}
		return true;
	}
}
