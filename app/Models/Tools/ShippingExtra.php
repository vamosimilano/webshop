<?php
use App\Database\R_DB;

class ShippingExtra extends R_DB
{

    protected $table = 'shipping_extra';
    protected $primaryKey = 'id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;


    public static function scopeLang($query)
    {
        return $query->where('shop_id', getShopId());
    }

}
