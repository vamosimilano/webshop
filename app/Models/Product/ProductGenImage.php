<?php

use App\Database\R_DB;

class ProductGenImage extends R_DB
{

    protected $table = 'product_general_images';
    protected $primaryKey = 'image_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

     /**
	 * getImageUrl function.
	 * Kép méret
	 *
	 * @access public
	 * @return void
	 */
	function getImageUrl($size = 'default'){

        $sizes = Config::get('website.product_sizes');
    	$url = Config::get('website.product_image_url');
    	$path = Config::get('website.product_image_path');
    	if ($size == 'full') {
        	return $url."/".$size."/".$this->image;
    	}
    	foreach($sizes as $width => $height){
        	if($size == 'default' or $size == $width){
            	if(!is_file($path.'/'.$width.'/'.$this->image)){
                	return get404Image($width, $height);
            	}
        	    return $url."/".$width."/".$this->image;
            }
    	}

	}

	public static function getHoverImageUrl($image_id, $size = 'default') {
    	return self::find($image_id)->getImageUrl($size);
	}


}
