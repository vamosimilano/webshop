<?php

use App\Database\R_DB;

class ProductImage extends R_DB
{

    protected $table = 'product_images';
    protected $primaryKey = 'image_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

     /**
	 * getImageUrl function.
	 * Kép méret
	 *
	 * @access public
	 * @return void
	 */
	function getImageUrl($size = 'default', $check = false){

        $sizes = Config::get('website.product_sizes');
    	$url = Config::get('website.product_image_url');
    	$path = Config::get('website.product_image_path');
    	if ($size == 'full') {
        	return $url."/".$size."/".str_replace(' ','%20',$this->image_url);
    	}
    	foreach($sizes as $width => $height){
        	if($size == 'default' or $size == $width){
            	if ($check){
                	if(!is_file($path.'/'.$width.'/'.$this->image_url)){
                     return get404Image($width, $height);
                	}
            	}
        	    return $url."/".$width."/".str_replace(' ','%20',$this->image_url);
            }
    	}

	}

	public static function getHoverImageUrl($image_id, $size = 'default') {
    	return self::find($image_id)->getImageUrl($size);
	}


}
