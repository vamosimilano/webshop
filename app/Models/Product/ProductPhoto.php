<?php


use App\Database\R_DB;

/**
 * ProductPhoto class.
 * A termékhez tartozó marketing fotókat tárolja.
 * Hónap terméke esetén a kategória slider, termék adatlap sliderek.
 *
 * @extends R_DB
 */
class ProductPhoto extends R_DB
{

    protected $table = 'product_photos';
    protected $primaryKey = 'photo_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

     /**
	 * getImageUrl function.
	 * Kép méret
	 *
	 * @access public
	 * @return void
	 */
	function getImageUrl(){

    	$url = Config::get('website.product_photo_url');
    	$path = Config::get('website.product_photo_path');
    	if(!is_file($path.'/full/'.$this->image_url)){
            return get404Image(100, 100);
        }
        return $url."/full/".$this->image_url;

	}

}
