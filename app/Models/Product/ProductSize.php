<?php

use App\Database\R_DB;

class ProductSize extends R_DB
{

    protected $table = 'product_sizes';
    protected $primaryKey = 'size_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

    public function scopeLang($query) {
        return $query->where('shop_id', getShopId());
    }

    public function getPrice(){
        //Hónap terméke-e
        $product = Product::lang()->active()->select('menu_top')->where('products.product_id', $this->product_id)->first();
        if ((isset($product)) and ($product->menu_top == 'yes')) {
            return ($this->monthly_price > 0 ? $this->monthly_price : $this->price);
        }
		
        return $this->price;
    }

    public static function getSizeString($product_id, $size_item = 'a', $size_name = 'XL', $show_text = true, $disable_html = false){
        $return = '';
        $feature = self::where('product_id', $product_id)
            ->where('size_name', $size_name)
            ->first();
        if (empty($feature)) {
            return $return;
        }
        if ($size_item == 'a' and $feature->asize_x > 0) {
            $return = '<span class="size">'.getUnit($feature->asize_x).'x'.getUnit($feature->asize_y).'</span>'.($show_text ? ' ('.$feature->size_name.')' : '');
        }elseif ($size_item == 'b' and $feature->bsize_x > 0) {
            $return = '<span class="size">'.getUnit($feature->bsize_x).'x'.getUnit($feature->bsize_y).'</span>'.($show_text ? ' ('.$feature->size_name.')' : '');
        }elseif ($size_item == 'c' and $feature->csize_x > 0) {
            $return = '<span class="size">'.getUnit($feature->csize_x).'x'.getUnit($feature->csize_y).'</span>'.($show_text ? ' ('.$feature->size_name.')' : '');
        }
        if ($disable_html) {
            return strip_tags($return);
        }
        return $return;

    }
	
	public static function getNormalSizeString($product_id, $size_item = 'a', $size_name = 'XL', $show_text = true, $disable_html = false){
        $return = '';
        $feature = self::where('product_id', $product_id)
            ->where('size_name', $size_name)
            ->first();
        if (empty($feature)) {
            return $return;
        }
        if ($size_item == 'a' and $feature->asize_x > 0) {
            $return = getUnit($feature->asize_x).'x'.getUnit($feature->asize_y).($show_text ? ' ('.$feature->size_name.')' : '');
        }elseif ($size_item == 'b' and $feature->bsize_x > 0) {
            $return = getUnit($feature->bsize_x).'x'.getUnit($feature->bsize_y).($show_text ? ' ('.$feature->size_name.')' : '');
        }elseif ($size_item == 'c' and $feature->csize_x > 0) {
            $return = getUnit($feature->csize_x).'x'.getUnit($feature->csize_y).($show_text ? ' ('.$feature->size_name.')' : '');
        }
        if ($disable_html) {
            return strip_tags($return);
        }
        return $return;

    }
}
