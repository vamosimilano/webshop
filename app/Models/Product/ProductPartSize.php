<?php

use App\Database\R_DB;

class ProductPartSize extends R_DB
{

    protected $table = 'product_part_sizes';
    protected $primaryKey = 'size_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

    function scopePartData($query){
        return $query->join('product_parts_lang',function($query){
            return $query->on('product_part_sizes.part_id','=','product_parts_lang.part_id')
                ->where('shop_id','=', getShopId());
        })
        ->leftjoin('product_parts','product_parts.part_id','=','product_parts_lang.part_id')
        ->select(['product_parts_lang.*','product_part_sizes.*','product_parts.type','product_parts.admin_name as admin_name']);

    }

}
