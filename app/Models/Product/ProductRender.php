<?php

use App\Database\R_DB;

class ProductRender extends R_DB
{

    protected $table = 'product_render';
    protected $primaryKey = 'product_render_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

}
