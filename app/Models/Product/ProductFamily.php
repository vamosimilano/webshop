<?php

use App\Database\R_DB;

class ProductFamily extends R_DB
{

    protected $table = 'product_families';
    protected $primaryKey = 'family_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

    }
