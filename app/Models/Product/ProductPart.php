<?php

use App\Database\R_DB;

class ProductPart extends R_DB
{

    protected $table = 'product_parts';
    protected $primaryKey = 'part_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

    public function scopeLang($query) {
        return $query->join('product_parts_lang', 'product_parts_lang.part_id', '=', 'product_parts.part_id')
			//->join('product_parts_sizes', 'product_parts_sizes.part_id', '=', 'product_parts.part_id')
            ->whereNull('product_parts_lang.deleted_at')
            //->whereNull('product_parts_sizes.deleted_at')
            ->where('shop_id', getShopId());
    }
}
