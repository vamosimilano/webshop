<?php
use App\Database\R_DB;


class Order extends R_DB  {

	protected $table = 'order';
	protected $primaryKey = 'order_id';
    public $timestamps = true;
	use Illuminate\Database\Eloquent\SoftDeletes;

	// Validálási szabályok
    static $validation_rules = array(
        'email' => 'required|email|max:64',
        'lastname' => 'required|max:64',
        'firstname' => 'required|max:64',
        'country' => 'required|max:100',
        'city' => 'required|max:100',
        'address' => 'required|max:100|regex:/[0-9](.*)/',
        'zip' => 'required|min:4|regex:/^(?![0]{3,5})(.*)/',
        'phone' => 'required|min:5|numeric',
        'message' => 'max:100',
        'message_courier' => 'max:100',
        //'billing_name' => 'required|max:100',
        //'billing_country' => 'required|max:100',
        //'billing_city' => 'required|max:100',
        //'billing_address' => 'required|max:100|regex:/[0-9](.*)/',
        //'billing_zip' => 'required|max:10',
        'billing_message' => 'max:100',
        'billing_tax_number' => 'max:25',
        'check_aszf' => 'required',
        //'check_in' => 'required',
        'payment' => 'required',
        'delivery_mode' => 'required',

    );
	
	static $validation_rules_billing = array(
        'email' => 'required|email|max:64',
        //'lastname' => 'required|max:64',
        //'firstname' => 'required|max:64',
        //'country' => 'required|max:100',
        //'city' => 'required|max:100',
        //'address' => 'required|max:100|regex:/[0-9](.*)/',
        //'zip' => 'required|min:4|regex:/^(?![0]{3,5})(.*)/',
        'phone' => 'required|min:5|numeric',
        'message' => 'max:100',
        'message_courier' => 'max:100',
        'billing_name' => 'required|max:100',
        //'billing_country' => 'required|max:100',
        'billing_city' => 'required|max:100',
        'billing_address' => 'required|max:100|regex:/[0-9](.*)/',
        'billing_zip' => 'required|max:10',
        'billing_message' => 'max:100',
        'billing_tax_number' => 'max:25',
        'check_aszf' => 'required',
        //'check_in' => 'required',
        'payment' => 'required',
        'delivery_mode' => 'required',

    );

    public $validation_msg = array(
        'email' => 'email',
        'lastname' => 'lastname',
        'firstname' => 'firstname',
        'country' => 'country',
        'city' => 'city',
        'address' => 'address',
        'zip' => 'zip',
        'phone' => 'phone',
        'message' => 'message',
        'message_courier' => 'message_courier',
        'billing_name' => 'billing_name',
        'billing_country' => 'billing_country',
        'billing_city' => 'billing_city',
        'billing_address' => 'billing_address',
        'billing_zip' => 'billing_zip',
        'billing_message' => 'billing_message',
        'billing_tax_number' => 'billing_tax_number',
		'check_aszf' => 'check_aszf',

        'payment' => 'billing_mode',
        'delivery_mode' => 'delivery_mode',

    );
    function __construct()
    {
        parent::__construct();
    }

    public static function getFieldNames(){
        return array(
                'email' => t('E-mail'),
                'lastname' => t('Vezetéknév'),
                'firstname' => t('Keresztnév'),
                'country' => t('Ország'),
                'city' => t('Város'),
                'address' => t('Cím és házszám'),
                'zip' => t('Irányítószám'),
                'phone' => t('Telefonszám'),
                'message' => t('Megjegyzés'),
                'message_courier' => t('Üzenet a futárnak'),




                'billing_name' => t('Számlázási név'),
                'billing_country' => t('Számlázási ország'),
                'billing_city' => t('Számlázási város'),
                'billing_address' => t('Számlázási cím'),
                'billing_zip' => t('Számlázási irányítószám'),
                'billing_message' => t('Számlázás megjegyzés'),
                'billing_tax_number' => t('Adószám'),


                'payment' => t('Fizetési mód'),
                'delivery_mode' => t('Szállítási mód'),

                 'check_aszf' => t('ÁSZF'),
                 'check_in' => t('Szabályzat'),

            );
    }

    /**
     * items function.
     * Rendelés tételek
     *
     * @access public
     * @return void
     */
    function items(){
        return OrderItem::where('order_id', $this->order_id)->get();
    }

    /**
     * getOrderNumber function.
     * Rendelés azonosító generálás
     *
     * @access public
     * @static
     * @return void
     */
    public static function getOrderNumber(){
        $max = Order::withTrashed()->max('order_number');
        return ($max + 1);
    }


    /**
     * sendMail function.
     * Rendelés feladásról e-mail
     *
     * @access public
     * @return void
     */
    public function sendMail(){

        $view = [
            'order' => $this,
            'date' => (new DateTime()),
            'items' => OrderItem::where('order_id', $this->order_id)->get(),
        ];



        $order = $this;
        Mail::send
        (
            'emails.order.order',
            $view,
            function ($message) use ($order) {
                $message->to($this->email)
                    ->subject(t('Sikeres rendelés feladás'));
            }
        );
    }
	
	public function sendOutletMail(){
        $view = [
            'order' => $this,
            'date' => (new DateTime()),
            'items' => OrderItem::where('order_id', $this->order_id)->get(),
        ];

        $order = $this;
        Mail::send
        (
            'emails.order.outlet',
            $view,
            function ($message) use ($order) {
                $message->to('ertekesites@vamosimilano.hu')
					->to('ertekesitessz@vamosimilano.hu')
					->to('kada.gabor@vamosimilano.hu')
					->to('andras.ori@vamosimilano.hu')
                    ->subject(t('Outlet rendelés'));
            }
        );
    }

    /**
     * sendSuccessTransaction function.
     * Sikeres tranzakcióról e-mail küldés
     *
     * @access public
     * @param mixed $result
     * @return void
     */
    public function sendSuccessTransaction($result) {
        $view = [
            'order' => $this,
            'date' => (new DateTime()),
            'result' => $result,
        ];
        $order = $this;
        Mail::send
        (
            'emails.order.transactionsuccess',
            $view,
            function ($message) use ($order) {
                $message->to($this->email)
                    ->subject(t('Sikeres tranzakció'));
            }
        );
    }
	
	public function sendSuccessTransactionBarclay($result) {
        $view = [
            'order' => $this,
            'date' => (new DateTime()),
            'result' => $result,
        ];
        $order = $this;
        Mail::send
        (
            'emails.order.transactionsuccessbarclay',
            $view,
            function ($message) use ($order) {
                $message->to($this->email)
                    ->subject(t('Sikeres tranzakció'));
            }
        );
    }

    /**
     * sendErrorTransaction function.
     * Sikertelen tranzakció e-mail
     *
     * @access public
     * @param mixed $result
     * @return void
     */
    public function sendErrorTransaction($result) {
        $view = [
            'order' => $this,
            'date' => (new DateTime()),
            'result' => $result,
        ];
        $order = $this;
        Mail::send
        (
            'emails.order.transactionerror',
            $view,
            function ($message) use ($order) {
                $message->to($this->email)
                    ->subject(t('Sikertelen tranzakció'));
            }
        );
    }


    public function getStatus(){
        switch ($this->status) {
            case 'NEW':
                return 'Új';
            break;
             case 'PENDING':
                return 'Feldolgozás alatt';
            break;
             case 'DONE':
                return 'Lezárva';
            break;
             case 'DELETED':
                return 'Törölve';
            break;


        }
    }
    public function sendNewStatus(){
        $view = [
            'order' => $this,
            'date' => (new DateTime()),
            'items' => OrderItem::where('order_id', $this->order_id)->get(),
        ];
        $order = $this;
        Mail::send
        (
            'emails.order.status',
            $view,
            function ($message) use ($order) {
                $message->to($this->email)
                    ->subject(t('Rendelésének státusza módosult'));
            }
        );
    }

    function scopeLang($query){
        return $query->where('shop_id', getShopId());
    }

}
