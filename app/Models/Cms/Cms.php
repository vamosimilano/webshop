<?php

use App\Database\R_DB;

class Cms extends R_DB
{

    protected $table = 'cms_content';
    protected $primaryKey = 'cms_id';
    public $timestamps = true;
     use Illuminate\Database\Eloquent\SoftDeletes;

    public static $rules = array(
        'title'=>'required|string|min:4',
        'url'=>'required|string|min:4',
        'description'=>'required|string|min:4',

    );
    public static function getFieldNames()
    {
        return array(
            'title' => t('Tartalom címe'),
            'url' => t('Hivatkozás'),
            'description' => t('Leírás'),
        );
    }


    public function scopeLang($query) {
        return $query->where('shop_id', getShopId());
    }
    public function scopeActive($query)
    {
        $date = new DateTime();
        return $query->where('is_active', 1)->where('published_at', '<=', $date->format('Y-m-d H:i:s'));
    }

    /**
     * getImageUrl function.
     * Kép méret
     *
     * @access public
     * @return void
     */
    function getImageUrl($size = 'default')
    {
        $sizes = Config::get('website.image_sizes');
        $url = Config::get('website.cms_image_url');
        foreach ($sizes as $width => $height) {
            if ($size == 'default' or $size == $width) {
                return $url."/".$width."/".$this->image_url;
            }
        }
    }



     /**
     * getDefaultImageUrl function.
     * Kép méret
     *
     * @access public
     * @return void
     */
    function getDefaultImageUrl($size = 'default')
    {

        $sizes = Config::get('website.image_sizes');
        $url = Config::get('website.cms_image_url');
        foreach ($sizes as $width => $height) {
            if ($size == 'default' or $size == $width) {
                if ($this->image_url == '') {
                    return get404Image($width, $height);
                }
                return $url."/".$width."/".$this->image_url;
            }
        }
    }

    /**
     * getUrl function.
     * Tartalom hivatkozása
     *
     * @access public
     * @return void
     */
    function getUrl()
    {
        if ($this->cms_type == 'blog') {
            return action('BlogController@index', $this->category."/".$this->url);
        } elseif ($this->cms_type == 'karrier') {
            return action('BlogController@karrier', $this->url);
        } {
            return action('CmsController@index', $this->url);
        }
    }

    public static function getCustomerReviews($limit = 10){
        return Cms::where('shop_id', getShopId())
                ->where('cms_type', 'review')
                ->where('is_active', 1)
                ->orderBy(DB::raw('RAND()'))
                ->take($limit)
                ->get();
    }
}
