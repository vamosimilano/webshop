<?php

/**
 * Class Unicredit
 *
 */

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class UniCredit
{
    /**
     * @var string ügyfélkódunk
     */
    protected $pid;

    /**
     * @var string Unicredit kulcsfájl helye
     */
    private $keyFile;

    /**
     * @var string Pénznem
     */
    protected $currency = 'HUF';

    /**
     * @var string Fizetés oldal nyelve
     */
    protected $lang = 'hu';

    /**
     * @var int Egyedi tranzakció ID
     */
    public $transactionId = 0;


    public function __construct()
    {
        $this->pid = getConfig('unicredit.pid');
        $this->keyFile = getConfig('unicredit.keyFile');
    }

    public function startPayment($order, $type = 'full') {

        //Todo-előleg adatai
        $amount = $order->payable_price;
        if ($type == 'advance') {
            $amount = $order->advance_price;
        }
        if ($type == 'different') {
            $amount = ($order->payable_price-$order->advance_price);
        }
        $amount = $amount*1;
        $currency     = getConfig('unicredit_currency');
        $ip           = getConfig('unicredit_ip');
        $description  = $order->order_number."-".$type;
        $language     = 'lv';

        $merchant = new Merchant(getConfig('unicredit_ecomm_server_url'), getConfig('unicredit_cert_url'), getConfig('unicredit_cert_pass'), 1);

        $resp = $merchant -> startSMSTrans($amount."00", $currency, $ip, $description, $language);

        if (substr($resp,0,14)=="TRANSACTION_ID") {
            $trans_id = substr($resp,16,28);
            $url = getConfig('unicredit_ecomm_client_url')."?trans_id=". urlencode($trans_id);

            $log = new Payment();
            $log->shop_id = getShopId();
            $log->order_number = $order->order_number;
            $log->order_ref = $trans_id;
            $log->status = 'START';
            $log->price = $amount;
            $log->card_type = 'UNICREDIT';
            $log->type = $type;
            $log->data = serialize([]);
            $log->save();

            return $url;
        }else{
            Log::useFiles(storage_path().'/logs/unicredit_Error.log');
            Log::error($resp);
            return false;
        }
    }

    public static function CheckPayment($trans_id){
        $merchant = new Merchant(getConfig('unicredit_ecomm_server_url'), getConfig('unicredit_cert_url'), getConfig('unicredit_cert_pass'), 1);
        $resp = $merchant -> getTransResult(urlencode($trans_id), getConfig('unicredit_ip'));
        $return = Unicredit::parseResponse($resp);
        $log = Payment::where('order_ref', $trans_id)
                ->where('status', 'START')
                ->orderBy('created_at', 'DESC')
                ->first();
        $return['trans_id'] = $trans_id;
        $return['price'] = $log->price;
        if ($return['result'] == 'OK') {

            $log->status = 'SUCCESS';
            $log->payment_at = (new DateTime())->format('Y-m-d H:i:s');
            $log->data = serialize(['post' => Input::all(), 'return' => $return]);
            $log->save();

        }elseif ($return['result'] != 'OK') {

            $log->status = $return['result'];
            $log->payment_at = (new DateTime())->format('Y-m-d H:i:s');
            $log->data = serialize(['post' => Input::all(), 'return' => $return]);
            $log->save();

        }
        return $return;
    }

    public static function parseResponse($resp) {

        $result = $result_3dsecure = $result_code = $card_number = $rrn_number = $approval_code = '';

        if (strstr($resp, 'RESULT:')) {
            $result = explode('RESULT: ', $resp);
            $result = preg_split( '/\r\n|\r|\n/', $result[1] );
            $result = $result[0];
        }else{
            $result = '';
        }

        if (strstr($resp, 'RESULT_CODE:')) {
            $result_code = explode('RESULT_CODE: ', $resp);
            $result_code = preg_split( '/\r\n|\r|\n/', $result_code[1] );
            $result_code = $result_code[0];
        }else{
            $result_code = '';
        }

        if (strstr($resp, '3DSECURE:')) {
            $result_3dsecure = explode('3DSECURE: ', $resp);
            $result_3dsecure = preg_split( '/\r\n|\r|\n/', $result_3dsecure[1] );
            $result_3dsecure = $result_3dsecure[0];
        }else{
            $result_3dsecure = '';
        }

        if (strstr($resp, 'CARD_NUMBER:')) {
            $card_number = explode('CARD_NUMBER: ', $resp);
            $card_number = preg_split( '/\r\n|\r|\n/', $card_number[1] );
            $card_number = $card_number[0];
        }else{
            $card_number = '';
        }

        if (strstr($resp, 'RRN:')) {
            $rrn_number = explode('RRN: ', $resp);
            $rrn_number = preg_split( '/\r\n|\r|\n/', $rrn_number[1] );
            $rrn_number = $rrn_number[0];
        }else{
            $rrn_number = '';
        }

        if (strstr($resp, 'APPROVAL_CODE:')) {
            $approval_code = explode('APPROVAL_CODE: ', $resp);
            $approval_code = preg_split( '/\r\n|\r|\n/', $approval_code[1] );
            $approval_code = $approval_code[0];
        }else{
            $approval_code = '';
        }

        $return = [
            'result' => $result,
            'result_3dsecure' => $result_3dsecure,
            'result_code' => $result_code,
            'card_number' => $card_number,
            'rrn_number' => $rrn_number,
            'approval_code' => $approval_code,
        ];

        return $return;


    }

    /**
     * checkTimeouts function.
     * Az elmúlt 15 percben elkezdett, de be nem fejezett fizetéseket ellenőrzi.
     *
     * @access public
     * @static
     * @return void
     */
    public static function checkTimeouts() {
        $date = new DateTime();
        $logs = Payment::where('status', 'START')
            ->orderBy('created_at', 'DESC')
            ->where('card_type', 'UNICREDIT')
            ->where('created_at', '<=', $date->modify('-5 minutes')->format('Y-m-d H:i:s'))
            ->get();
        if (!sizeof($logs)) {
            return '';
        }
        foreach ($logs as $log) {
            $result = Unicredit::CheckPayment($log->order_ref);
            $order = Order::where('order_number', $log->order_number)->first();
            $order->sendErrorTransaction($result);
        }
        return '';
    }

    public static function getResultString($string) {
        $string = trim(mb_strtoupper($string));
        switch ($string) {
            case 'OK':
                return 'sikeres tranzakció ('.$string.')';
            break;
            case 'FAILED':
                return 'sikertelen tranzakció ('.$string.')';
            break;
            case 'CREATED':
                return 'a tranzakció még csak létrejött a rendszerben ('.$string.')';
            break;
            case 'PENDING':
                return 'a tranzakció még nem hajtódott végre ('.$string.')';
            break;
            case 'DECLINED':
                return 'a tranzakciót visszautasította az ECOMM, mert az ECI szerepel a
tiltott ECI listán (ECOMM szerver oldali beállítás) REVERSED a transakciót visszavonták ('.$string.')';
            break;
            case 'TIMEOUT':
                return 'a tranzakciónál időtúllépés történt ('.$string.')';
            break;

        }
        return $string;
    }

}
