<?php



class FacebookController extends BaseController
{
    /**
	 * facebookLogin function.
	 * Facebook belépés
	 *
	 * @access public
	 * @return void
	 */
	public function facebookLogin(){

        Session::put('fb_redirect', Input::get('fb_redirect'));


		$fb = \OAuth::consumer('Facebook', action('FacebookController@facebookLoginAuth'));

		$url = $fb->getAuthorizationUri();

		return Redirect::to((string)$url);

	}

	/**
	 * facebookLoginAuth function.
	 * Facebook belépés
	 *
	 * @access public
	 * @return void
	 */
	public function facebookLoginAuth(){
		    // get data from input
		    $code = Input::get( 'code' );

		    // get fb service
		    $fb = OAuth::consumer( 'Facebook' );

		    // check if code is valid

		    // if code is provided get user data and sign in
		    if ( !empty( $code ) ) {

		        // This was a callback request from facebook, get the token
		        $token = $fb->requestAccessToken( $code );

		        // Send a request with it
		        $result = json_decode($fb->request('/me?fields=email,first_name,last_name'),true);

		        $user_exist = User::where('email', $result['email'])->first();

		        if (empty($user_exist)){
		            $user_exist = User::where('facebook_id', $result['id'])->first();
		        }

		        if(!empty($user_exist)){
			        $user_exist->facebook_id = $result['id'];
			        $user_exist->last_login = (new DateTime())->format('Y-m-d H:i:s');
			        $user_exist->save();

					Auth::login($user_exist);

		        }else{
    		        $password = 'A'.rand(100000,999999);
					$user = new User;
					$user->firstname = $result['first_name'];
					$user->lastname = $result['last_name'];
					$user->facebook_id = $result['id'];
					$user->email = strtolower($result['email']);
					$user->password = Hash::make($password);
					$user->shop_id = getShopId();
					$user->last_login = (new DateTime())->format('Y-m-d H:i:s');
					$user->save();

					$user->sendRegMail($password);

					Auth::login($user);
		        }

		        return Redirect::to((Session::has('fb_redirect') ? Session::get('fb_redirect') : action('ProfileController@index')))->with('flash_success', t('login_success'));

		    }
		    // if not ask for permission first
		    else {
		        // get fb authorization
		        $url = $fb->getAuthorizationUri();

		        // return to facebook login url
		         return Redirect::to( (string)$url );
		    }
	}


}
