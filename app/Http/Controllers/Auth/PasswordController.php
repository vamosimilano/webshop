<?php



use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Controllers\Controller;

use Illuminate\View\Factory;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use App\Http\Requests\Auth\EmailPasswordLinkRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;

class PasswordController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         Seo::setTitle(t('Elfelejtett jelszó'));
        $this->subject = t('Segítség új jelszó beállításához');
        $this->redirectTo = '/';
        $this->middleware('guest');
    }

    /**
     * Handle a POST request to reset a user's password.
     *
     * @return Response
     */
    public function postReset()
    {


        $credentials = [
            'email' =>  Input::get('email'),
            'password' =>  Input::get('password'),
            'password_confirmation' =>  Input::get('password_confirmation'),
            'token' =>  Input::get('token'),

        ];

        $response = Password::reset($credentials, function($user, $password)
        {
            $user->password = Hash::make($password);

            $user->save();
            Auth::login($user);
        });

        switch ($response)
        {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                return Redirect::back()->with('flash_error', Lang::get($response));

            case Password::PASSWORD_RESET:
                return Redirect::to(action('ProfileController@index'))->with('flash_success', t('Sikeresen módosította jelszavát!'));


        }
    }

}
