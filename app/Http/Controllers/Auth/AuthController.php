<?php

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends BaseController
{


    /**
     * login function.
     * Belépés form
     *
     * @access public
     * @return void
     */
    public function login()
    {
        if (Auth::id()) {
            return Redirect::to(action('CmsController@home'));
        }
         Seo::setTitle(t('Bejelentkezés'));
        $view = [

        ];
        return View::make('auth.login', $view);
    }

    public function facebookConnect(){
        return 'EZT MEG LE KELL FEJLESZTENI';
    }


    /**
     * register function.
     * Regisztráció form
     *
     * @access public
     * @return void
     */
    public function register()
    {
        if (Auth::id()) {
            return Redirect::to(action('CmsController@home'));
        }
         Seo::setTitle(t('Regisztráció'));
        $view = [

        ];
        return View::make('auth.register', $view);
    }

    /**
     * logout function.
     * Kilépés
     *
     * @access public
     * @return void
     */
    public function logout()
    {
         Auth::logout();
         return Redirect::to(action('AuthController@login'))->with('flash_success', t('logout_success'));
    }

    /**
     * loginSubmit function.
     * Belépés fo
     *
     * @access public
     * @return void
     */
    public function loginSubmit()
    {
        $userdata = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );

        $success = Auth::attempt($userdata);

        if (!$success) {
            $now = new DateTime();
            //Régi adatokkal megpróbáljuk beléptetni
            $old_user = User::where('email', $userdata['email'])
                ->where('old_password', \User::hashOldPassword($userdata['password']))
                ->first();
            if (!empty($old_user)){
                $old_user->password = Hash::make($userdata['password']);
                $old_user->last_login = $now->format('Y-m-d H:i:s');
                $old_user->old_password = '';
                $old_user->save();
                Auth::login($old_user);
				if(isset($_COOKIE["wishlist"]) ){
					DB::table('product_favorites')->where('uid', $_COOKIE["wishlist"])->update(['uid' => $old_user->customer_id]);
					setcookie("wishlist", $old_user->customer_id, time()+(86400*365));
					$_SESSION["wishlist"]=$old_user->customer_id;
				}
                return Redirect::to(action('ProfileController@index'))->with('flash_success', t('login_success'));
            }
        }



        if ($success) {
            $now = new DateTime();
            $user = Auth::user();
            $user->last_login = $now->format('Y-m-d H:i:s');
            $user->save();
			if(isset($_COOKIE["wishlist"]) ){
				DB::table('product_favorites')->where('uid', $_COOKIE["wishlist"])->update(['uid' => $user->customer_id]);
				setcookie("wishlist", $user->customer_id, time()+(86400*365));
				$_SESSION["wishlist"]=$user->customer_id;
			}
            if (Input::get('d')) {
                return Redirect::to(url(Input::get('d')))->with('flash_success', t('login_success'));
            }
            return Redirect::to(action('CmsController@home'))->with('flash_success', t('login_success'));
        } else {



            return Redirect::to(action('AuthController@login'))->withInput()->with('flash_error', t('login_error'));
        }
    }

    /**
	 * regSubmit function.
	 * felhasználó mentése
	 *
	 * @access public
	 * @return void
	 */
	public function regSubmit(){


		$validator = Validator::make(Input::all(), User::$rules);
		$validator->setAttributeNames(User::getFieldNames());
		if ($validator->passes()) {

    		$email = strtolower(Input::get('email'));
			$user = new User;
		    $user->firstname = Input::get('firstname');
		    $user->lastname = Input::get('lastname');
		    $user->email = $email;
		    $user->shop_id = getShopId();
		    $user->password = Hash::make(Input::get('password'));

		    $user->save();

		    if (Input::get('is_subscribed')){
    		    $exist = Newsletter::where('email',$email)->first();
    		    if (empty($exist)){
                    $newsletter = new Newsletter();
                    $newsletter->email = $email;
                    $newsletter->firstname = Input::get('firstname');
                    $newsletter->lastname = Input::get('lastname');
                    $newsletter->save();
                }

		    }
		    $user->sendRegMail(Input::get('password'));


		    Auth::login($user);

			if(isset($_COOKIE["wishlist"]) ){
				DB::table('product_favorites')->where('uid', $_COOKIE["wishlist"])->update(['uid' => $user->customer_id]);
				setcookie("wishlist", $user->customer_id, time()+(86400*365));
				$_SESSION["wishlist"]=$user->customer_id;
			}
            return Redirect::to(action('ProfileController@index'))->with('flash_success', t('Sikeresen regisztrált rendszerünkbe!'));


		}else{
			 return Redirect::to(action('AuthController@register', Input::get('hash')))
	      ->withInput(Input::except('password'))
	      ->withErrors($validator->errors());
		}


	}
}
