<?php


//namespace App\Http\Controllers;

use Order;
use App\PayPal;
use Illuminate\Http\Request;

/**
 * Class PayPalController
 * @package App\Http\Controllers
 */
class PayPalController extends BaseController
{
    /**
     * @param Request $request
     */
    public function form(Request $request, $order_id = null)
    {
		die('form');
		/*
        $order_id = $order_id ?: encrypt(1);

        $order = Order::findOrFail(decrypt($order_id));

        return view('form', compact('order'));
		*/
    }

    /**
     * @param $order_id
     * @param Request $request
     */
    public function checkout($order_id, Request $request)
    {
		$log = Payment::where('payment_id', $order_id)->first();
		if (empty($log)) {
            return Redirect::action('CartController@error', '0')
                ->withErrors([t('Hiba történt a fizetés során, nem találunk ilyen tranzakciót!')]);
        }
        $paypal = new PayPal;

        $response = $paypal->purchase([
            'amount' => $paypal->formatAmount($log->price),
            'transactionId' => $log->order_ref,
            'currency' => 'GBP',
            'cancelUrl' => $paypal->getCancelUrl($order_id),
            'returnUrl' => $paypal->getReturnUrl($order_id),
        ]);

        if ($response->isRedirect()) {
            $response->redirect();
        }

        return redirect()->back()->with([
            'message' => $response->getMessage(),
        ]);
    }

    /**
     * @param $order_id
     * @param Request $request
     * @return mixed
     */
    public function completed($order_id, Request $request)
    {
		$log = Payment::where('payment_id', $order_id)->first();
		if (empty($log)) {
            return Redirect::action('CartController@error', '0')
                ->withErrors([t('Hiba történt a fizetés során, nem találunk ilyen tranzakciót!')]);
        }
		
        $paypal = new PayPal;

        $response = $paypal->complete([
			'amount' => $paypal->formatAmount($log->price),
            'transactionId' => $log->order_ref,
            'currency' => 'GBP',
            'cancelUrl' => $paypal->getCancelUrl($order_id),
            'returnUrl' => $paypal->getReturnUrl($order_id),
			'notifyUrl' => $paypal->getNotifyUrl($order_id),
			
        ]);

        if ($response->isSuccessful()) {
			$log->status='SUCCED';
			//$log->data = serialize($response);
			$log->data = serialize( 
				array(
					"PAYID"=>$response->getTransactionReference(),
					"message"=>$response->getMessage(),
					"status"=>$response->getCode(),
					"price"=>$log->price
					//"cardreference"=>$response->getCardReference()
				)
			);
			$log->save();
			//$response->getTransactionReference()
			
			Seo::setTitle(t('Sikeres megrendelés feladás'));
		
			//outlet eladás ellenőrzése
			//az order itmek között ha van outletes cucc, akkor azt le kell tiltani.
			
			$order = Order::where('order_number', $log->order_number)->first();
			
			$items = $order->items();
			foreach ($items as $item) {
				if ($item->product_id > 0) {
					$product = Product::lang()->active()->find($item->product_id);
					if ($product->isOutlet()) {
						//outletes cucc, letiltani
						DB::table('products_lang')
							->where('product_id', $item->product_id)
							->update(['visible' => 'no']);
						//levél küldése
						$order->sendOutletMail();
						break;
					}
				}
			}
			
			$view = [
				'order' => $order,
				//'payment' =>(Session::has('last_card_trans') ? Session::get('last_card_trans') : []),
				'payment' => $log,

			];
			Session::forget('last_card_trans');
			return View::make('webshop.cart.successbarclay', $view);
        } else {
			$log->status='ERROR';
			//$log->data = serialize($response);
			$log->data = serialize( 
				array(
					"trans_id"=>$response->getTransactionReference(),
					"message"=>$response->getMessage(),
					"status"=>$response->getCode(),
					//"cardreference"=>$response->getCardReference()
				)
			);
			$log->save();
		}

		return Redirect::action('CartController@error', '0')
                ->withErrors([$response->getMessage()]);
    }

    /**
     * @param $order_id
     */
    public function cancelled($order_id, Request $request)
    {
		$log = Payment::where('payment_id', $order_id)->first();
		if (empty($log)) {
            return Redirect::action('CartController@error', '0')
                ->withErrors([t('Hiba történt a fizetés során, nem találunk ilyen tranzakciót!')]);
        }
		
		$result = Input::all();

        $log->data = serialize($result);
		$log->status = 'CANCEL';
		$log->save();

        return Redirect::action('CartController@error', $log->order_number)
                ->withErrors([t('Hiba történt a fizetés során, kérjük próbálja újra!')]);
		
    }

    /**
     * @param $order_id
     * @param $env
     */
    public function webhook($order_id, $env)
    {
		die("webhook");
        // to do with next blog post
    }
}
