<?php

class CmsController extends BaseController
{

    function __construct()
    {
    }

    function home()
    {

        //Seo::setTitle(t('Főoldal')." - ".t('home_title', 'seo'));
		Seo::setDefault();
		Seo::setTitle(t('home_title', 'seo'));
		
		$fep = false; //egyenes
		$flp = false; //L
		$fup = false; //U
		$ffp = false; //Franciaágy
		
		$nap = 700; //Mennyi nap legyen számolva visszafelé a favoritokból.
		
		$t = DB::select("select kl.category_id, p.product_id, count(*) as db from product_categories_lang as kl inner join product_categories as k on (k.deleted_at is null and k.category_id=kl.category_id) inner join products as p on (p.category_id=k.category_id and p.deleted_at is null) inner join products_lang as l on (l.product_id=p.product_id and l.visible='yes' and l.deleted_at is null) inner join product_favorites as f on (f.product_id=p.product_id and f.deleted_at is null and datediff(CURDATE(), f.updated_at)<$nap) where kl.visible='yes' and kl.deleted_at is null and kl.category_id=4 group by kl.category_id, p.product_id ORDER BY `db` DESC LIMIT 1");
		if (isset($t[0])) {
			$fep = Product::where('products.product_id', $t[0]->product_id)->active()->lang()->first();
		}
		$t = DB::select("select kl.category_id, p.product_id, count(*) as db from product_categories_lang as kl inner join product_categories as k on (k.deleted_at is null and k.category_id=kl.category_id) inner join products as p on (p.category_id=k.category_id and p.deleted_at is null) inner join products_lang as l on (l.product_id=p.product_id and l.visible='yes' and l.deleted_at is null) inner join product_favorites as f on (f.product_id=p.product_id and f.deleted_at is null and datediff(CURDATE(), f.updated_at)<$nap) where kl.visible='yes' and kl.deleted_at is null and kl.category_id=12 group by kl.category_id, p.product_id ORDER BY `db` DESC LIMIT 1");
		if (isset($t[0])) {
			$flp = Product::where('products.product_id', $t[0]->product_id)->active()->lang()->first();
		}
		$t = DB::select("select kl.category_id, p.product_id, count(*) as db from product_categories_lang as kl inner join product_categories as k on (k.deleted_at is null and k.category_id=kl.category_id) inner join products as p on (p.category_id=k.category_id and p.deleted_at is null) inner join products_lang as l on (l.product_id=p.product_id and l.visible='yes' and l.deleted_at is null) inner join product_favorites as f on (f.product_id=p.product_id and f.deleted_at is null and datediff(CURDATE(), f.updated_at)<$nap) where kl.visible='yes' and kl.deleted_at is null and kl.category_id=24 group by kl.category_id, p.product_id ORDER BY `db` DESC LIMIT 1");
		if (isset($t[0])) {
			$fup = Product::where('products.product_id', $t[0]->product_id)->active()->lang()->first();
		}
		$t = DB::select("select kl.category_id, p.product_id, count(*) as db from product_categories_lang as kl inner join product_categories as k on (k.deleted_at is null and k.category_id=kl.category_id) inner join products as p on (p.category_id=k.category_id and p.deleted_at is null) inner join products_lang as l on (l.product_id=p.product_id and l.visible='yes' and l.deleted_at is null) inner join product_favorites as f on (f.product_id=p.product_id and f.deleted_at is null and datediff(CURDATE(), f.updated_at)<$nap) where kl.visible='yes' and kl.deleted_at is null and kl.category_id=9 group by kl.category_id, p.product_id ORDER BY `db` DESC LIMIT 1");
		if (isset($t[0])) {
			$ffp = Product::where('products.product_id', $t[0]->product_id)->active()->lang()->first();
		}
		
		$manufacturedcounter = file_get_contents('/var/www/clients/client1/web17/private/CompleteProductNumber');
		$ordercounter = file_get_contents('/var/www/clients/client1/web17/private/OrderNumber');
		
		$p = DB::select("
		SELECT `product_categories_lang`.`category_id`, `products`.`product_id`, `products_lang`.`menu_top` FROM `product_categories_lang`
		INNER JOIN `product_categories` ON (`product_categories`.`deleted_at` IS NULL AND `product_categories`.`category_id`=`product_categories_lang`.`category_id`)
		INNER JOIN `products` ON (`products`.`deleted_at` IS NULL AND `products`.`category_id`=`product_categories`.`category_id`)
		INNER JOIN `products_lang` ON (`products_lang`.`deleted_at` IS NULL AND `products_lang`.`visible`='yes' AND `products_lang`.`product_id`=`products`.`product_id`)
		WHERE `product_categories_lang`.`deleted_at` IS NULL AND `products`.`deleted_at` IS NULL AND `product_categories_lang`.`category_id` IN (4, 9, 12, 24)
		ORDER BY `products_lang`.`menu_top` ASC, if( `product_categories_lang`.`category_id`='9',-1,`product_categories_lang`.`category_id`) 
		LIMIT 4
		");
		if ( isset($p[0]) ) {
			$p0 = Product::where('products.product_id', $p[0]->product_id)->active()->lang()->first();
		}
		if ( isset($p[1]) ) {
			$p1 = Product::where('products.product_id', $p[1]->product_id)->active()->lang()->first();
		}
		if ( isset($p[2]) ) {
			$p2 = Product::where('products.product_id', $p[2]->product_id)->active()->lang()->first();
		}
		if ( isset($p[3]) ) {
			$p3 = Product::where('products.product_id', $p[3]->product_id)->active()->lang()->first();
		}
		$view = [
            'carousel' => Carousels::where('type', 'hometop')->orderBy('weight', 'ASC')->active()->get(),
			'showroom1' => Carousels::where('type', 'home-1')->orderBy('weight', 'ASC')->active()->get(),
			'showroom2' => Carousels::where('type', 'home-2')->orderBy('weight', 'ASC')->active()->get(),
			'showroom3' => Carousels::where('type', 'home-3')->orderBy('weight', 'ASC')->active()->get(),
			'showroom4' => Carousels::where('type', 'home-4')->orderBy('weight', 'ASC')->active()->get(),
            'stores' => Carousels::where('type', 'stores')->orderBy('weight', 'ASC')->active()->get(),
            'designer' => Carousels::where('type', 'designer')->orderBy('weight', 'ASC')->active()->get(),
            'designerbottom' => Carousels::where('type', 'homemiddle')->orderBy('weight', 'ASC')->active()->get(),
            'topproducts' => Carousels::where('type', 'topproducts')->orderBy('weight', 'ASC')->active()->take(4)->get(),
            'bestellers' => Product::getBestellers(),
			'offerproducts' => Product::where('offer', 'yes')->active()->lang()->take(4)->get(),
			'favorit_E' => $fep,
			'favorit_L' => $flp,
			'favorit_U' => $fup,
			'favorit_F' => $ffp,
			'manufacturedcounter' => $manufacturedcounter,
			'ordercounter' => $ordercounter,
			'p0' => $p0,
			'p1' => $p1,
			'p2' => $p2,
			'p3' => $p3,
			'headversion' => 'newhead'
        ];
		
		$seoitem = Carousels::where('type', 'topproducts')->orderBy('weight', 'ASC')->active()->take(1)->get()->first();
		
		if (isset($seoitem)) {
			Seo::setImage($seoitem->getImageUrl());
		}
		
		//Seo::setImage($view['designerbottom']->first()->getImageUrl());

        return View::make('home', $view);
    }

    function index($content = '')
    {

        $cms = Cms::where('url', $content)->active()->first();

        if (empty($cms)){
            App::abort('404');
        }
        $view = [
            'cms' => $cms,
			'headversion' => 'newhead'
        ];
        Seo::setTitle($cms->title);
		//Seo::setDescription($cms->short_description);
		Seo::setDescription($cms->seo_description);

        if ($cms->url == t('hazhozszallitas','url')) {
            $controller = new CartController();
            $default_data = $controller->getCartData();
            $view['default_data'] = $default_data;
            $view['extra_prices'] = ShippingExtra::lang()->where('price', '>', 0)->get();
        }

        return View::make('cms.content', $view);
    }

	function credit() {
		Seo::setTitle(t('Áruhitel'));
		return View::make('cms.credit');
	}
	function blackfriday() {
	Seo::setTitle(t('Black Friday'));
		return View::make('cms.blackfriday');
	}
	function keszletkisopres() {
		Seo::setTitle(t('Készletkisöprés'));
		return View::make('cms.keszletkisopres');
	}
	function tavasziakcio() {
		Seo::setTitle(t('Tavaszi akció'));
		return View::make('cms.tavasziakcio');
	}
	function nyitasiakcio() {
		Seo::setTitle(t('Nyitási akció'));
		return View::make('cms.landing.nyitasiakcio');
	}
	function ajandekagy() {
		Seo::setTitle(t('Matracaink mellé ajándékba adunk egy franciaágyat'));
		return View::make('cms.landing.ajandekagy');
	}
	function szezonzaro() {
		Seo::setTitle(t('Szezonzáró akció - 50-70% kedvezmény, ingyen házhozszállítás'));
		Seo::setDescription(t('Május 17-19: országos szezonzáró akció a Vamosi Milanoban! 50-70%-os kedvezményt adunk bútorainkra!'));
		return View::make('cms.landing.szezonzaro');
	}
	function fizetesimodok() {
		Seo::setTitle(t('Fizetési módok'));
		Seo::setDescription(t('Válasszon az Önnek és pénztárcájának legmegfelelőbb fizetési módot a Vamosinál.'));
		return View::make('cms.landing.fizetes');
	}
	function reszletfizetestajekoztato() {
		$products = DB::select("SELECT p.product_id, l.name, s.size_name, s.price, s.list_price, s.monthly_price, l.menu_top FROM products as p 
			INNER JOIN products_lang as l on (p.product_id=l.product_id and l.visible='yes' and l.shop_id=1 and l.deleted_at is null)
			INNER JOIN product_sizes as s on (p.product_id=s.product_id and s.deleted_at is null and s.shop_id=1)
			WHERE p.deleted_at is null and p.category_id in (4, 9, 12, 24)
			ORDER BY l.name, s.list_price");
		Seo::setTitle(t('Részletfizetés tájékoztató'));
		Seo::setDescription(t('Részletfizetés tájékoztató'));
		$view = [
			"products"=>$products
		];
		return View::make('cms.landing.reszletfizetes', $view);
	}
	function berlettajekoztato() {
		$products = DB::select("SELECT p.product_id, l.name, s.size_name, s.price, s.list_price, s.monthly_price, l.menu_top FROM products as p 
			INNER JOIN products_lang as l on (p.product_id=l.product_id and l.visible='yes' and l.shop_id=1 and l.deleted_at is null)
			INNER JOIN product_sizes as s on (p.product_id=s.product_id and s.deleted_at is null and s.shop_id=1)
			WHERE p.deleted_at is null and p.category_id in (4, 9, 12, 24)
			ORDER BY l.name, s.list_price");
		Seo::setTitle(t('Bérlet tájékoztató'));
		Seo::setDescription(t('Bérlet tájékoztató'));
		$view = [
			"products"=>$products
		];
		return View::make('cms.landing.berlet', $view);
	}
}