<?php



class ApiController extends BaseController
{
    private $token_dev = 'HWXdHD96cRxh23O933wqu9MRVQ67P0r0';
    private $token = '5cN5nx7OYfC17vV3zQDuM7hpj7Q2U8U0';

    public function validateToken($token = ''){

        if (strstr(url('/'), 'local') or strstr(url('/'), 'new.')){
            if ($token == $this->token_dev){
                return true;
            }
        }
        if ($token == $this->token){
            return true;
        }
        return false;
    }

    public function addToCart($session_id, $product_id)
    {
        $data = Input::all();


        $product = Product::lang()->active()->where('products.product_id', $product_id)->first();
        if (empty($product)){
            return $this->errorHandler('not_found');
        }

        $cart = CartHelper::where('session_id', $session_id)->first();
        if (empty($cart)) {
            $cart = new CartHelper();
            $cart->session_id = $session_id;
            $cart->save();
        }
        $item = new CartItem();
        $input = Input::all();
        $input['product_id'] = $product_id;
        unset($input['token']);
        /*
        $input['product_id'] = $product_id;

        $input['mounting'] = 'right';
        $input['sizes'] = [
                'a' => 'S',
                'b' => 'M',
                'c' => 'XL',
        ];
        $input['textile']['also'] = '551';
        $input['textile']['felso'] = '329';
        */

        $item->product_id = $input['product_id'];
        $item->feature_id = 0;
        $item->type = $product->type;
        $item->qty = 1;
        $item->cart_id = $cart->cart_id;
        $item->product_name = $product->getName();
        $item->price = CartHelper::calculateItemPrice($input);
        $item->opt = serialize($input);

        if ($item->save()) {
            $array = [
                'response' => 'SUCCESS',
                'error' => false,
                'status_code' => 200,
                'data' => [
                    'resore_url' => action('CartController@restore',$cart->session_id),
                    'products' => CartItem::where('cart_id', $cart->cart_id)->count('cart_id'),
                ]
            ];
        }else{
            $array = [
                'response' => 'SAVE ERROR',
                'error' => true,
                'status_code' => 200
            ];
        }

        return Response::json($array);
    }

    /**
     * loginUser function.
     * Felhasználó beléptetése
     *
     * @access public
     * @return void
     */
    function loginUser(){
        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
        $userdata = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );

        $success = Auth::attempt($userdata);
        if (!$success) {
            $now = new DateTime();
            //Régi adatokkal megpróbáljuk beléptetni
            $old_user = User::where('email', $userdata['email'])
                ->where('old_password', \User::hashOldPassword($userdata['password']))
                ->first();
            if (!empty($old_user)){
                $old_user->password = Hash::make($userdata['password']);
                $old_user->last_login = $now->format('Y-m-d H:i:s');
                $old_user->old_password = '';
                $old_user->save();
                Auth::login($old_user);
                $success = true;
            }
        }

        if ($success) {
            $user = Auth::user();
            $user->old_password = '';
            $array = [
                'response' => 'SUCCESS',
                'error' => false,
                'status_code' => 200,
                'data' => $user,
            ];
        }else{
            $array = [
                'response' => 'login error',
                'error' => true,
                'status_code' => 200,
                'data' => new stdClass(),
            ];
        }
        return Response::json($array);

    }

    /**
     * getUser function.
     * Felhasználó lekérdezése
     *
     * @access public
     * @param mixed $uid
     * @return void
     */
    function getUser($uid){
        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }

        $user = User::where('customer_id', $uid)->first();
        if (sizeof($user)) {
            $user->old_password = '';
            $array = [
                'response' => 'SUCCESS',
                'error' => false,
                'status_code' => 200,
                'data' => $user,
            ];
        }else{
            $array = [
                'response' => 'not found error',
                'error' => true,
                'status_code' => 200,
                'data' => new stdClass(),
            ];
        }
        return Response::json($array);

    }

    /**
     * getTextiles function.
     * Textilek visszadása, 20-as lapozóval.
     * /api/v1/textiles
     * @access public
     * @return void
     */
    function getTextiles(){

        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }


        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => new stdClass(),
        ];

        $where = [];
        if (Input::get('where')) {
            $where = Input::get('where');
        }
		$orderField = 'product_textiles_lang.name';
		if (Input::get('orderField')) {
            $orderField = Input::get('orderField');
        }
		
		$orderOrder = 'ASC';
		if (Input::get('orderOrder')) {
            $orderOrder = Input::get('orderOrder');
        }

        $data = ProductTextile::getAll(true, $where, $orderField, $orderOrder);
        if (sizeof($data)){
            foreach ($data as $key => $item) {
				if ((isset($item->textile_chain_id)) and (!is_null($item->textile_chain_id)) and ($item->textile_chain_id > 0)) {
					$r = DB::select("SELECT image FROM textile_chain WHERE textile_chain_id = ".$item->textile_chain_id);
					foreach ($r as $t) {
						if ((isset($t)) and (isset($t->image))) {
							$item->image_white_url = "https://static.vamosimilano.hu/chain/".$t->image;
						} else {
							$item->image_white_url = '';
						}
					}
				} else {
					$item->image_white_url = '';
				}
                $item->image_url = url(ProductTextile::getImageUrl($item->textile_id));
                $item->image_thumb_url = url(ProductTextile::getImageUrl($item->textile_id, 40));
				unset($item->textile_lang_id);
				unset($item->shop_id);
				unset($item->url);
				unset($item->visible);
				unset($item->in_planner);
				unset($item->created_at);
				unset($item->updated_at);
				unset($item->deleted_at);
				unset($item->extra_price);
				unset($item->just_pillow);
				unset($item->textile_pattern_id);
				unset($item->textile_catname);
            }
            $array['data'] = $data;
        }else{
            $data = new stdClass();
        }

        return Response::json($array);
    }
    /**
     * getTextile function.
     * Egy adott textile visszadása.
     *
     * @access public
     * @param mixed $id
     * @return void
     */
    function getTextile($id) {
        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => new stdClass(),
        ];


        $data = ProductTextile::lang()->where('product_textiles.textile_id', $id)->first();
        if (!empty($data)){
            $data->image_url = url(ProductTextile::getImageUrl($data->textile_id));
            $data->image_thumb_url = url(ProductTextile::getImageUrl($data->textile_id, 40));

            $array['data'] = $data;
        }
        return Response::json($array);
    }

    /**
     * getProducts function.
     * Termékek listája - akár kategória azonosító alapján
     *
     * @access public
     * @param int $category_id (default: 0)
     * @return void
     */
    function getProducts($category_id = 0){

        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }

        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => new stdClass(),
        ];
		
		if (Input::get('planner')=='yes') {
		
			if ($category_id){
				$category = ProductCategory::active()->lang()->where('product_categories.category_id', $category_id)->first();
				if (!empty($category)){
					$products = Product::active()->lang()->where('planner','yes')->where('category_id', $category->category_id)->orderBy('name','ASC')->get();
				}
			}else{
				$products = Product::active()->lang()->where('planner','yes')->paginate(50);
			}
		} else {
			if ($category_id){
				$category = ProductCategory::active()->lang()->where('product_categories.category_id', $category_id)->first();
				if (!empty($category)){
					$products = Product::active()->lang()->where('category_id', $category->category_id)->orderBy('name','ASC')->get();
				}
			}else{
				$products = Product::active()->lang()->orderBy('name','ASC')->paginate(50);
			}
		}

        if (sizeof($products)){
            foreach ($products as $product) {
                $price = $product->getDefaultPrice();
                $price_full = $product->getDefaultFullPrice();
                $product->price = $price;
                $product->full_price = $price_full;
                $product->image_url = url($product->getDefaultImageUrl(600));
            }
            $array['data'] = $products;
        }else{
            $array['data'] = new stdClass();
        }
        return Response::json($array);
    }

    /**
     * getSize function.
     * A méret adatai
     *
     * @access public
     * @param int $size_id (default: 0)
     * @return void
     */
    function getSize($size_id = 0){

        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }

        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => new stdClass(),
        ];

        $size = ProductSize::lang()->where('size_id', $size_id)->first();
        if (!empty($size)) {
            $array['data'] = $size;
        }

        return Response::json($array);
    }


    /**
     * getCategories function.
     * Kategóriák listázása, szülő kategóriávl vagy anélkül
     *
     * @access public
     * @param int $parent_id (default: 0)
     * @return void
     */
    function getCategories($parent_id = 0) {

        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }

        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => new stdClass(),
        ];
        if (!$parent_id){
            $all_categories = ProductCategory::active()->lang()->get();
        }else{
            $all_categories = ProductCategory::where('parent_id', $parent_id)->active()->lang()->get();
        }

        if (sizeof($all_categories)){
            foreach ($all_categories as $category){
                $category->image_url = ($category->image_url ? url($category->image_url) : NULL);
                $category->banner_url = ($category->banner_url ? url($category->banner_url) : NULL);
            }
            $array['data'] = $all_categories;
        }
        return Response::json($array);
    }

    function getProduct($product_id) {
        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
        $product = Product::lang()->active()->where('products.product_id', $product_id)->first();
        if (empty($product)){
            return $this->errorHandler('not_found');
        }
        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => new stdClass(),
        ];

        $price = $product->getDefaultPrice();
        $price_full = $product->getDefaultFullPrice();
        $product->price = $price;
        $product->full_price = $price_full;
        $product->image_url = url($product->getDefaultImageUrl(600));
        $product->sizes = $product->getSizes();
        $product->textiles = $product->getTextilePreviews();

        $another_textile = false;
        if (sizeof($product->textiles)){
            foreach ($product->textiles as $item) {
                unset($item->image);
                $item->image_url = url(ProductTextile::getImageUrl($item->textile1_id));
                $item->image_thumb_url = url(ProductTextile::getImageUrl($item->textile1_id, 40));
                if ($item->textile2_id) {
                    $another_textile = true;
                    $item->image_url_2 = url(ProductTextile::getImageUrl($item->textile2_id));
                    $item->image_thumb_url_2 = url(ProductTextile::getImageUrl($item->textile2_id, 40));
                }else{
                    $item->image_url_2 = $item->image_url;
                    $item->image_thumb_url_2 = $item->image_thumb_url;
                }
            }
        }
        $product->parts = $product->getParts();
        if (sizeof($product->parts)) {
            foreach ($product->parts as $item) {
                unset($item->size_id);
            }
        }
        $product->all_parts = $product->getAllParts();
        if (sizeof($product->all_parts)) {
            foreach ($product->parts as $item) {
                unset($item->size_id);
            }
        }


        $array['data'] = $product;

        return Response::json($array);

    }

    /**
     * getPrice function.
     * A megadott méretek, és textilek alapján visszaadja a termék árát.
     *
     *
     * @access public
     * @param mixed $product_id
     * @return void
     */
    function getPrice($product_id) {
        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
        $product = Product::lang()->active()->where('products.product_id', $product_id)->first();
        if (empty($product)){
            return $this->errorHandler('not_found');
        }
        $input = Input::all();
        $input['product_id'] = $product_id;
        /*
        $input['product_id'] = $product_id;

        $input['sizes'] = [
                'a' => 'S',
                'b' => 'M',
                'c' => 'XL',
        ];
        $input['textile']['also'] = '551';
        $input['textile']['felso'] = '329';
        */

        $price_array = CartHelper::calculateItemPrice($input, true);

        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => $price_array,
        ];



        return Response::json($array);
    }
    /**
     * updateFavorite function.
     * Kedvenc frissítése
     *
     * @access public
     * @param mixed $favorite_id
     * @return void
     */
    function updateFavorite($favorite_id) {

        $favorite_ = Favorites::find($favorite_id);
        if (empty($favorite_)){
            return $this->errorHandler('not_found');
        }
        return $this->setFavorite($favorite_->product_id, $favorite_->uid, $favorite_id);
    }
    /**
     * deleteFavorite function.
     * Kedvenc törlése
     *
     * @access public
     * @param mixed $favorite_id
     * @return void
     */
    function deleteFavorite($favorite_id) {

        $favorite_ = Favorites::find($favorite_id);
        if (empty($favorite_)){
            return $this->errorHandler('not_found');
        }
        $favorite_->delete();
        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => new stdClass(),
        ];
        return Response::json($array);
    }
    /**
     * setFavorite function.
     * Kedvenc mentése
     *
     * @access public
     * @param mixed $product_id
     * @param mixed $user_id
     * @param int $favorite_id (default: 0)
     * @return void
     */
    function setFavorite($product_id, $user_id, $favorite_id = 0) {
        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
        $product = Product::lang()->active()->where('products.product_id', $product_id)->first();
        if (empty($product)){

            return $this->errorHandler('not_found');
        }
        $input = Input::all();
        $input['product_id'] = $product_id;
        $input['type'] = $product->type;
        unset($input['token']);
        if ($favorite_id == 0) {
            $favorite = new Favorites();
        }else{
            $favorite = Favorites::find($favorite_id);
            if (empty($favorite)) {
                $favorite = new Favorites();
            }
        }
        $favorite->uid = $user_id;
        $favorite->product_id = $product_id;
        $favorite->product_name = $product->getName();
        $favorite->name = Input::get('name');
        $favorite->type = $product->type;
        $favorite->options = serialize(Favorites::setRowOptions($input));
        if ($favorite->save()) {
            $favorite->options = $favorite->getRowOptions();
            $array = [
                'response' => 'SUCCESS',
                'error' => false,
                'status_code' => 200,
                'data' => $favorite,
            ];
        }else{
            $array = [
                'response' => 'save error',
                'error' => false,
                'status_code' => 200,
                'data' => '',
            ];
        }
        return Response::json($array);

    }

    /**
     * getFavorite function.
     * Felhasználó kedvenceinek lekérdezése
     *
     * @access public
     * @param mixed $user_id
     * @return void
     */
    function getFavorite($user_id) {
        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
        $favorites = Favorites::where('uid', $user_id)
            ->orderBy('created_at', 'DESC')
            ->get();

        $data = new stdClass();
        if (sizeof($favorites)){

            foreach ($favorites as $item) {
                $item->options = $item->getRowOptions();
            }
            $data = $favorites;
        }
        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => $data,
        ];
        return Response::json($array);

    }

    function errorHandler($type) {
        if ($type == 'token'){
            $array = [
                'error' => true,
                'response' => 'token error',
                'status_code' => 535
            ];
        }
        if ($type == 'not_found'){
            $array = [
                'error' => true,
                'response' => 'not found',
                'status_code' => 404
            ];
        }

        return Response::json($array);
    }
	
	function getOrders($lastid = 0){

        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }

        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => new stdClass(),
        ];
		
		$orderek = Order::where('order_id', '>', $lastid)->orderBy('order_id','ASC')->paginate(50);

        if (sizeof($orderek)){
            foreach ($orderek as $order) {
				$order->orderitems = $order->items();
            }
            $array['data'] = $orderek;
        }else{
            $array['data'] = new stdClass();
        }
        return Response::json($array);
    }
	
	function getCreditCardPayment($code = 0){

        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }

        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => new stdClass(),
        ];
		
		//$paymentdata = Payment::where('data', 'LIKE', '"%approval_code";s:6:"'.$code.'";%')->orderBy('payment_id','ASC')->paginate(50);
		$paymentdata = Payment::where('data', 'like', '%"approval_code"%"'.$code.'";%')->orderBy('payment_id','ASC')->paginate(50);
		//$paymentdata = Payment::where('payment_id', $code)->orderBy('payment_id','ASC')->paginate(50);

        if (isset($paymentdata)){
            $array['data'] = $paymentdata;
        }else{
            $array['data'] = new stdClass();
        }
        return Response::json($array);
    }
	
	function getProductSizes($product_id) {
        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
        $product = Product::lang()->active()->where('products.product_id', $product_id)->first();
        if (empty($product)){
            return $this->errorHandler('not_found');
        }
        $input = Input::all();
        $input['product_id'] = $product_id;
		
		$t = $product->getSizes();
		foreach($t as $key => $value) {
			unset($value->size_id);
			unset($value->shop_id);
			unset($value->size_x);
			unset($value->size_y);
			unset($value->size_z);
			unset($value->is_standard);
			unset($value->factory_price_old);
			unset($value->factory_price);
			unset($value->price);
			unset($value->list_price);
			unset($value->monthly_price);
			unset($value->factory_price_en);
			unset($value->price_en);
			unset($value->list_price_en);
			unset($value->created_at);
			unset($value->updated_at);
			unset($value->deleted_at);
		}
		
        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => $t,
        ];

        return Response::json($array);
    }
	
	function calculateprice() {
		if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
		$input = Input::all();

        $price_array = CartHelper::calculateItemPrice($input, true);

        $plus_price = 0;
        if (isset($input['related']) and $input['related'] != '0' and $input['related'] != '') {
            $related_ = ProductRelated::find($input['related']);
			if (isset($related)) {
				if ($related_->show_price > 0){
					//$plus_price = $related_->show_price;
				}
			}
        }

    	$data = [
    	    'furniture_price' => money($price_array['furniture_price']) ,
    	    'price' => money($price_array['all_price']+$plus_price),
    	    'textile_price' => money($price_array['textile_price']),
    	    //'debug' => isset($price_array['debug']) ? $price_array['debug'] : [],
			'priceall' => ($price_array['all_price']+$plus_price),
    	    'priceplus' => ($price_array['textile_price']),
    	   ];
		$array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => $data,
        ];

        return Response::json($array);
	}
	
	function getCarousel() {
		$data = [];
		$c = Carousels::where('type', 'appbanner')->orderBy('weight', 'ASC')->active()->get();
		if (isset($c)){
			$b = $c[rand(0,(count($c)-1))];
			if (isset($b)){
				$data = [
					"imageurl" => $b->getImageUrl(),
					"href" => $b->link
				];
			}
		}
		
		$array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => $data,
        ];

        return Response::json($array);
	}
	
	function imagerender1() {
		if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
		$q = DB::select("SELECT t.* FROM product_textiles as t inner join product_textiles_lang as l on (t.textile_id = l.textile_id and l.deleted_at is null and l.visible='yes') WHERE t.colorcode is not null and t.deleted_at is null and (t.on_egesz='yes' or t.on_felso='yes' or t.on_also='yes') and ((t.textile_id in (select also_textile_id from product_images_gen)) or (t.textile_id in (select felso_textile_id from product_images_gen)) )");
		$t = array();
		foreach($q as $p) {
			$t["textile"][] = array(
				'TextileID' => $p->textile_id,
				'Root' => 'https://static.vamosimilano.hu/textiles/'.$p->image
			);
		}
		
		$q = DB::select("SELECT DISTINCT product_id FROM product_images_gen WHERE imageurl1 is null");
		$s = array();
		foreach($q as $p) {
			$s["Sofa"][] = array(
				'SofaID' => $p->product_id
			);
		}
		
		$data = array(
			'Init' => array(
				'PicturesRoot' => 'renderimages',
				'UpdateTime' => 1,
				'PictureBackgroundColor' => '000000',
				'Textiles' => $t,
				'Sofas' => $s,
			)
		);
		
		if (isset($data)) {
			$array = [
				'response' => 'SUCCESS',
				'error' => false,
				'status_code' => 200,
				'data' => $data,
			];
		} else {
			$array = [
				'response' => 'NOT FOUND',
				'error' => false,
				'status_code' => 404,
				'data' => $data,
			];
		}

        return Response::json($array);
	}
	
	
	function imagerender2() {
		if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
		$q = DB::select('SELECT * FROM product_images_gen WHERE imageurl1 is null limit 100');
		if (isset($q)) {
			foreach ($q as $p) {
				$data['Products']['Product'][] = array(
					'RequestID' => $p->product_images_gen_id,
					'SofaID' => $p->product_id,
					'Elements' => array(
						'Element' => array(
							array(
								'ElementID' => 'also',
								'TextileID' => $p->also_textile_id,
								'ColorID' => $p->also_colorcode
							),
							array(
								'ElementID' => 'felso',
								'TextileID' => $p->felso_textile_id,
								'ColorID' => $p->felso_colorcode
							),
							array(
								'ElementID' => 'egesz',
								'TextileID' => $p->egesz_textile_id,
								'ColorID' => $p->egesz_colorcode
							)
						)
					)
				);
			}
		}
		
		if (isset($data)) {
			$array = [
				'response' => 'SUCCESS',
				'error' => false,
				'status_code' => 200,
				'data' => $data,
			];
		} else {
			$array = [
				'response' => 'NOT FOUND',
				'error' => false,
				'status_code' => 404,
				'data' => $data,
			];
		}

        return Response::json($array);
	}
	
	function imagerender3() {
		if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
		
		$array = [
			'response' => 'SUCCESS',
			'error' => false,
			'status_code' => 200,
			'data' => ''
		];

        return Response::json($array);
	}
	
	function getRender($product_id){
		if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
        $render = ProductRender::where('product_id', $product_id)->first();
        if (empty($render)){
			$product = Product::lang()->active()->where('products.product_id', $product_id)->first();
			if (empty($product)){
				return $this->errorHandler('not_found');
			}
			$render = new ProductRender();
			$render->product_id = $product_id;
			$render->save();
			$render = ProductRender::where('product_id', $product_id)->first();
			if (empty($render)){
				return $this->errorHandler('not_found');
			}
        }
		
        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => $render,
        ];
		
        return Response::json($array);
    }
	
	function setRender($product_id) {
        if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }
        $render = ProductRender::where('product_id', $product_id)->first();
        if (empty($render)){

            return $this->errorHandler('not_found');
        }
		
        $render->position1_x = Input::get('position1_x');
		$render->position1_y = Input::get('position1_y');
		$render->position1_z = Input::get('position1_z');
		$render->rotation1_x = Input::get('rotation1_x');
		$render->rotation1_y = Input::get('rotation1_y');
		$render->rotation1_z = Input::get('rotation1_z');
		
		$render->position2_x = Input::get('position2_x');
		$render->position2_y = Input::get('position2_y');
		$render->position2_z = Input::get('position2_z');
		$render->rotation2_x = Input::get('rotation2_x');
		$render->rotation2_y = Input::get('rotation2_y');
		$render->rotation2_z = Input::get('rotation2_z');
		
		$render->position3_x = Input::get('position3_x');
		$render->position3_y = Input::get('position3_y');
		$render->position3_z = Input::get('position3_z');
		$render->rotation3_x = Input::get('rotation3_x');
		$render->rotation3_y = Input::get('rotation3_y');
		$render->rotation3_z = Input::get('rotation3_z');
		
		$render->position4_x = Input::get('position4_x');
		$render->position4_y = Input::get('position4_y');
		$render->position4_z = Input::get('position4_z');
		$render->rotation4_x = Input::get('rotation4_x');
		$render->rotation4_y = Input::get('rotation4_y');
		$render->rotation4_z = Input::get('rotation4_z');
		
		$render->position5_x = Input::get('position5_x');
		$render->position5_y = Input::get('position5_y');
		$render->position5_z = Input::get('position5_z');
		$render->rotation5_x = Input::get('rotation5_x');
		$render->rotation5_y = Input::get('rotation5_y');
		$render->rotation5_z = Input::get('rotation5_z');
		
		$render->position6_x = Input::get('position6_x');
		$render->position6_y = Input::get('position6_y');
		$render->position6_z = Input::get('position6_z');
		$render->rotation6_x = Input::get('rotation6_x');
		$render->rotation6_y = Input::get('rotation6_y');
		$render->rotation6_z = Input::get('rotation6_z');
		
		$render->tiling_x = Input::get('tiling_x');
		$render->tiling_y = Input::get('tiling_y');
		
        if ($render->save()) {
            $array = [
                'response' => 'SUCCESS',
                'error' => false,
                'status_code' => 200,
                'data' => $render,
            ];
        }else{
            $array = [
                'response' => 'save error',
                'error' => false,
                'status_code' => 200,
                'data' => '',
            ];
        }
        return Response::json($array);
    }
}
