<?php



class WebshopController extends BaseController
{
	
	
	/**
     * updateFavorite function.
     * Kedvenc frissítése
     *
     * @access public
     * @param mixed $favorite_id
     * @return void
     */
    function updateFavorite($favorite_id) {
        $favorite_ = Favorites::find($favorite_id);
        if (empty($favorite_)){
            return $this->errorHandler('not_found');
        }
        return $this->setFavorite($favorite_->product_id, $favorite_->uid, $favorite_id);
    }
    /**
     * deleteFavorite function.
     * Kedvenc törlése
     *
     * @access public
     * @param mixed $favorite_id
     * @return void
     */
    function deleteFavorite($favorite_id) {
        $favorite_ = Favorites::find($favorite_id);
        if (empty($favorite_)){
            return $this->errorHandler('not_found');
        }
        $favorite_->delete();
        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => new stdClass(),
        ];
        return Response::json($array);
    }
    /**
     * setFavorite function.
     * Kedvenc mentése
     *
     * @access public
     * @param mixed $product_id
     * @param mixed $user_id
     * @param int $favorite_id (default: 0)
     * @return void
     */
    function setFavorite($product_id, $user_id, $favorite_id = 0) {
        /*if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }*/
        $product = Product::lang()->active()->where('products.product_id', $product_id)->first();
        if (empty($product)){
            return $this->errorHandler('not_found');
        }
        $input = Input::all();
        $input['product_id'] = $product_id;
        $input['type'] = $product->type;
        unset($input['token']);
        if ($favorite_id == 0) {
            $favorite = new Favorites();
        }else{
            $favorite = Favorites::find($favorite_id);
            if (empty($favorite)) {
                $favorite = new Favorites();
            }
        }
        $favorite->uid = $user_id;
        $favorite->product_id = $product_id;
        $favorite->product_name = $product->getName();
        $favorite->name = Input::get('name');
        $favorite->type = $product->type;
        $favorite->options = serialize(Favorites::setRowOptions($input));
        if ($favorite->save()) {
            $favorite->options = $favorite->getRowOptions();
            $array = [
                'response' => 'SUCCESS',
                'error' => false,
                'status_code' => 200,
                'data' => $favorite,
            ];
        }else{
            $array = [
                'response' => 'save error',
                'error' => false,
                'status_code' => 200,
                'data' => '',
            ];
        }
        return Response::json($array);
    }
    /**
     * getFavorite function.
     * Felhasználó kedvenceinek lekérdezése
     *
     * @access public
     * @param mixed $user_id
     * @return void
     */
    function getFavorite($user_id) {
        /*if (!$this->validateToken((Input::get('token') ? Input::get('token') : ''))) {
            return $this->errorHandler('token');
        }*/
        $favorites = Favorites::where('uid', $user_id)
            ->orderBy('created_at', 'DESC')
            ->get();
        $data = new stdClass();
        if (sizeof($favorites)){
            foreach ($favorites as $item) {
                $item->options = $item->getRowOptions();
            }
            $data = $favorites;
        }
        $array = [
            'response' => 'SUCCESS',
            'error' => false,
            'status_code' => 200,
            'data' => $data,
        ];
        return Response::json($array);
    }

    /**
     * oldRoute function.
     * Régi URL-ek lekezelése
     *
     * @access public
     * @param mixed $slug
     * @return void
     */
    function oldRoute($slug) {
        $tmp = explode("/", $slug);
        $end = end($tmp);

        if (is_numeric($end) and $end < 100) {
            $end = $tmp[count($tmp)-2];
        }
        $product = Product::where('url', $end)->active()->lang()->first();
        if (!empty($product)) {
            return Redirect::to($product->getUrl(), 301);
        } else {
			//return $this->product($slug);
			$category = ProductCategory::where('category_url',$end)->active()->lang()->first();
			if (!empty($category)) {
				return Redirect::to($category->getUrl(), 301);
			} else {
				return Redirect::to("/termekek/".$slug, 301);
			}
		}

    }

    /**
     * sales function.
     * A Hónap bútorai
     *
     * @access public
     * @return void
     */
    function sales() {
        Seo::setTitle(t('A hónap Bútorai'));

        $products = $filters = $filter_selected = $all_categories = [];

        $products = Product::where('menu_top', 'yes')->active()->lang()->get();



        $view = [
            'products' => $products,
            'filters' => $filters,
            'all_categories' => $all_categories,
            'filter_selected' => $filter_selected,
            'body_class' => 'page-with-filter page-products categorypath-gear category-gear catalog-category-view page-layout-2columns-left layout-1170 wide',
			'headversion' => 'newhead',
    	];
		if (Config::get('app.debug')) {
			return View::make('webshop.sales', $view); //dev
		} else {
			return View::make('webshop.sales', $view);
		}
    }

    function product($slug = ''){
        $products = $filters = $filter_selected = $all_categories = [];
		
		if ($slug=='favorit') {
			
			$products = Product::active()->lang()->favorit()->distinct()->get(['products.*','products_lang.*']);
			
			$category = $first_category = [];
			
			$view = [
				'products' => $products,
				'filters' => $filters,
				'all_categories' => $all_categories,
				'body_class' => 'page-with-filter page-products categorypath-gear category-gear catalog-category-view page-layout-2columns-left layout-1170 wide',
				'headversion' => 'newhead'
			];

			return View::make('webshop.category', $view);
		}
    	if ($slug == '') {
        	$category = $first_category = [];
        	Seo::setTitle(t('Termékeink'));
        	$all_categories = ProductCategory::where('parent_id', 0)->active()->lang()->get();

        	$view = [
                'all_categories' => $all_categories,
                'fist_all' => true,
                'body_class' => 'page-with-filter page-products categorypath-gear category-gear catalog-category-view page-layout-2columns-left layout-1170 wide',
				
				'headversion' => 'newhead'
            ];

            return View::make('webshop.category_all', $view);

    	}else{
    	    $tmp = explode("/", $slug);
			
			$tmp1 = end($tmp);
			
			if (strpos($tmp1,'gazzone') === false) {
				//ok
			} else {
				$tmp1 = str_replace('gazzone','gadzone',$tmp1);
				$product = Product::where('url', $tmp1)->active()->lang()->first();
				if (isset($product)) {
					return Redirect::to($product->getUrl(), 301);
				} else {
					App::abort(404);
					return;
				}
			}

    	    $product = $this->getProduct($tmp);
			
            if(sizeof($product)) {
                //echo strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]) .'.' .strtolower($product->getUrl());
                //die();
				
				$nu=$_SERVER["REQUEST_URI"];
				if (strpos($nu, "?")>0) {
					$nu = substr($nu, 0, strpos($nu, "?"));
				}
	
				if (strtolower(env('APP_URL').$nu) != strtolower($product->getUrl())) {
					if (
						(strpos(strtolower(env('APP_URL').$nu), '/kanap')>0) or
						(strpos(strtolower(env('APP_URL').$nu), '/egyenes-kanape')>0) or
						(strpos(strtolower(env('APP_URL').$nu), '/l-alaku-kanape')>0) or
						(strpos(strtolower(env('APP_URL').$nu), '/u-alaku-kanape')>0) or
						(strpos(strtolower(env('APP_URL').$nu), '/franciaagy')>0) or
						(strpos(strtolower(env('APP_URL').$nu), '/matrac')>0)
					){
						$ni = $_SERVER["REQUEST_URI"];
						if (strpos($ni, "?")==0) {
							return Redirect::to($product->getUrl(), 301);
						} else {
							$kjel = substr($ni, strpos($ni, "?"), strlen($ni));
							return Redirect::to($product->getUrl().$kjel, 301);
							//App::abort(404);
							//return;
						}
					} else {
						App::abort(404);
						return;
					}	
				}
                Seo::setTitle($product->getName());
				Seo::setDescription($product->meta_desc);
				Seo::setImage($product->getDefaultImageUrl(600));
                $category = ProductCategory::active()->lang()->where('product_categories.category_id', $product->category_id)->first();
				$familyproducts = Product::active()->lang()->where('family_id', $product->family_id)->get();

                $view = [
                    'all_categories' => [],
                    'products' => [],
                    'product' => $product,
                    'photos' => $product->getPhotos(),
                    'current_category' => $category,
                    'all_textiles' => ProductTextile::getAll(),
                    'textiles' => $product->getTextilePreviews(),
                    'custom_parts' => $product->getPartsCustom(),
                    'parts' => $product->getParts(),
                    'sizes' => $product->getSizes(),
                    /*'parts' => $product->getPartsByType('random'),
                    'half_parts' => $product->getPartsByType('half'),
                    'full_parts' => $product->getPartsByType('full'),*/
                    'related_products' => $product->getRelations(),
					'garancia_products' => $product->getGarancia(),
                    'body_class' => 'page-product-configurable catalog-product-view product- categorypath-fashion-women category-women page-layout-2columns-right layout-1170 wide',
					'familyproducts' => $familyproducts,
                ];

				Seo::setTitle($product->getName());
                Seo::setImage($product->getDefaultImageUrl(600));

				$view = $this->getProductView($product);

                return View::make('webshop.product', $view);

            }else{
                $category = ProductCategory::active()->lang()->where('category_url', end($tmp))->first();

				if (!sizeof($category)) {
					$category = ProductCategory::active()->lang()->where('category_url', prev($tmp))->first();
				}
				if (!sizeof($category)) {
					$category = ProductCategory::active()->lang()->where('category_url', prev($tmp))->first();
				}

				if (!sizeof($category)) {
					if (
						(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/kanap')>0) or
						(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/franciaagy')>0)
						
					){
						$nu = $_SERVER["REQUEST_URI"];
						if (strpos($nu, '/kanape')==0) {
							$nu = str_replace('/kanap','/kanape/',$nu);
						}
						if (strpos($nu, '/franciaagyak')==0) {
							$nu = str_replace('/franciaagy','/franciaagyak/',$nu);
						}
						if (strpos($nu, '/egyenes-kanapek')==0) {
							$nu = str_replace('/egyenes-kanape','/egyenes-kanapek/',$nu);
						}
						if (strpos($nu, '/l-alaku-kanapek')==0) {
							$nu = str_replace('/l-alaku-kanape','/l-alaku-kanapek/',$nu);
						}
						if (strpos($nu, '/u-alaku-kanapek')==0) {
							$nu = str_replace('/u-alaku-kanape','/u-alaku-kanapek/',$nu);
						}
						$nu = str_replace('//','/',$nu);
						if ((strpos($nu, '/kanape/')==0) and (strpos($nu, '/franciaagyak/')==0) and (strpos($nu, '/egyenes-kanapek/')==0) and (strpos($nu, '/l-alaku-kanapek/')==0) and (strpos($nu, '/u-alaku-kanapek/')==0)) {
							App::abort(404);
							return;
						} else {
							return Redirect::to(env('APP_URL').$nu, 301);
						}
					} else {
						App::abort(404);
						return;
					}
				}
				if (strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]) != strtolower($category->getUrl())) {
					if (
						(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/kanap')>0) or
						(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/egyenes-kanape')>0) or
						(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/l-alaku-kanape')>0) or
						(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/u-alaku-kanape')>0) or
						(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/franciaagy')>0) or
						(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/matrac')>0)
					){
						if (
							(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/kanap')>0) or
							(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/egyenes-kanape')>0) or
							(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/l-alaku-kanape')>0) or
							(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/u-alaku-kanape')>0) or
							(strpos(strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]), '/franciaagy')>0)
						) {
							$nu = $_SERVER["REQUEST_URI"];
							
							if (strpos($nu, "?")>0) {
								$nu = substr($nu, 0, strpos($nu, "?"));
							}
							
							if (strpos($nu, '/kanape')==0) {
								$nu = str_replace('/kanap','/kanape/',$nu);
							}
							if (strpos($nu, '/franciaagyak')==0) {
								$nu = str_replace('/franciaagy','/franciaagyak/',$nu);
							}
							if (strpos($nu, '/egyenes-kanapek')==0) {
								$nu = str_replace('/egyenes-kanape','/egyenes-kanapek/',$nu);
							}
							if (strpos($nu, '/l-alaku-kanapek')==0) {
								$nu = str_replace('/l-alaku-kanape','/l-alaku-kanapek/',$nu);
							}
							if (strpos($nu, '/u-alaku-kanapek')==0) {
								$nu = str_replace('/u-alaku-kanape','/u-alaku-kanapek/',$nu);
							}
							$nu = env('APP_URL').str_replace('//','/',$nu);
						} else {
							$nu = $category->getUrl();
						}
						if (strtolower($nu) != strtolower($category->getUrl())) {
							App::abort(404);
							return;
						} else {
							$ni = env('APP_URL').$_SERVER["REQUEST_URI"];
							if (strpos($ni, "?")==0) {
								return Redirect::to($nu, 301);
							} else {
								$kjel = substr($ni, strpos($ni, "?"), strlen($ni));
								if ($nu.$kjel != $ni) {
									return Redirect::to($nu.$kjel, 301);
								}
							}
						}	
					} else {
						$ni = $_SERVER["REQUEST_URI"];
						if (strpos($ni, "?")==0) {
							App::abort(404);
							return;
						}
					}
				}
                $all_categories = ProductCategory::active()->lang()->where('parent_id', $category->category_id)->get();
				
				if (($category->category_id >= 139) and ($category->category_id <= 142)) {
					$products = Product::active()
						->lang()->where('category_id', $category->category_id)
						->orderBy('menu_top', 'ASC')
						->orderBy('price', 'ASC')->get();
				} else {
					$products = Product::active()
						->lang()->where('category_id', $category->category_id)
						->orderBy('menu_top', 'ASC')
						->orderBy('name', 'ASC')->get();
				}
            }

            if (!sizeof($category)) {
				App::abort(404);
				return;
            }

            //Ha csak ennyit irnak be: /termekek/termek

            if (sizeof($tmp) == 1 and sizeof($product)) {

                $url = str_replace(Request::root(), '',$category->getUrl());
                $tmp = explode("/", $url);
                $first_category =  ProductCategory::active()->lang()->where('category_url', $tmp[0])->first();
            }else{
                $first_category =  ProductCategory::active()->lang()->where('category_url', $tmp[0])->first();
            }

            //$products = $products->paginate(1000);

            Seo::setTitle($category->name);
            if ($category->meta_description) {
                Seo::setDescription($category->meta_description);
            }
        }
        if (sizeof($products) == 1) {
            return Redirect::to($products[0]->getUrl());
        }


        $view = [
            'current_category' => $category,
            'first_category' => $first_category,
            'products' => $products,
            'filters' => $filters,
            'all_categories' => $all_categories,
            'filter_selected' => $filter_selected,
            'body_class' => 'page-with-filter page-products categorypath-gear category-gear catalog-category-view page-layout-2columns-left layout-1170 wide',
			'headversion' => 'newhead'
    	];

        return View::make('webshop.category', $view);
    }

    function search() {

        Seo::setTitle(t('Keresés'));
        $q = Input::get('q');
        $products = Product::active()->lang()
            ->where(function($query) use($q){
                $query->where('name', 'like', '%'.$q.'%')
                    ->orWhere('description', 'like', '%'.$q.'%');

            })->get();


        $view = [
            'all_categories' => [],
            'products' => $products,
            'search' => Input::get('q'),
            'body_class' => 'page-with-filter page-products categorypath-gear category-gear catalog-category-view page-layout-2columns-left layout-1170 wide',
			'headversion' => 'newhead'
        ];

        return View::make('webshop.category', $view);

    }

    function productAddToCart($slug) {
        $tmp = explode("/", $slug);

        $product = Product::where('url', end($tmp))->active()->lang()->first();

        if (empty($product)) {
            App::abort(404);
        }

        Seo::setTitle($product->getName());

        $category = ProductCategory::active()->lang()->where('product_categories.category_id', $product->category_id)->first();


        $view = [
            'product' => $product,
            'current_category' => $category,
        ];
        return View::make('webshop.addtocart', $view);
    }

    function productSelectTextile($slug) {
        $slug = str_replace('/' . t('selecttextile', 'url'), '', $slug);
        $tmp = explode("/", $slug);
        $product = $this->getProduct($tmp);
		
		if (strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]) != strtolower($product->getUrl().'/'.t('selecttextile', 'url'))) {
			//return Redirect::to($product->getUrl().'/'.t('selecttextile', 'url'), 301);
			App::abort(404);
		}

        if (empty($product)) {
            App::abort(404);
        }

        Seo::setTitle($product->getName());

        $view = $this->getProductView($product);

        $view['textileditor'] = true;

        return View::make('webshop.changetextile', $view);
    }
	
	function productModalTextile($slug) {
		//die(print_r($_SERVER));
		$t = explode("/",$_SERVER["REQUEST_URI"]);
		$partname=end($t);
		$slug = str_replace('/' . t('selecttextile', 'url'), '', $slug);
        $tmp = explode("/", $slug);
        $product = $this->getProduct($tmp);
		
		/*
		if (strtolower(env('APP_URL').$_SERVER["REQUEST_URI"]) != strtolower($product->getUrl().'/'.t('selecttextile', 'url'))) {
			//return Redirect::to($product->getUrl().'/'.t('selecttextile', 'url'), 301);
			App::abort(404);
		}
		*/
        if (empty($product)) {
            App::abort(404);
        }
				
        $view = $this->getProductModalTextileView($product);
		$view['partname'] = $partname;
        return View::make('webshop.modaltextile', $view);
    }


    /**
     * getProduct function.
     * A slug alapján visszatér a termékekl
     *
     * @access public
     * @param mixed $tmp
     * @return void
     */
    function getProduct($tmp) {
        $product = Product::where('url', end($tmp))->active()->lang()->first();
        if(!sizeof($product)){
			$product = Product::where('url', prev($tmp))->active()->lang()->first();
		}
        return $product;
    }
	
	function getProductModalTextileView($product) {
        return [
            'product' => $product,
            'all_textiles' => ProductTextile::getAll(),
            'textiles' => $product->getTextilePreviews(),
            'custom_parts' => $product->getPartsCustom(),
            'parts' => $product->getParts(),
            'sizes' => $product->getSizes()
        ];
    }

    /**
     * getProductView function.
     * A product objektum alaján visszadja a view-hez szükséges adatokat.
     *
     * @access public
     * @param mixed $product
     * @return void
     */
    function getProductView($product) {
        $category = ProductCategory::active()->lang()->where('product_categories.category_id', $product->category_id)->first();
		$familyproducts = Product::active()->lang()->where('family_id', $product->family_id)->get();

        return [
            'all_categories' => [],
            'products' => [],
            'product' => $product,
            'photos' => $product->getPhotos(),
            'current_category' => $category,
            'all_textiles' => ProductTextile::getAll(),
            'textiles' => $product->getTextilePreviews(),
            'custom_parts' => $product->getPartsCustom(),
            'parts' => $product->getParts(),
            'sizes' => $product->getSizes(),
            /*'parts' => $product->getPartsByType('random'),
            'half_parts' => $product->getPartsByType('half'),
            'full_parts' => $product->getPartsByType('full'),*/
            'related_products' => $product->getRelations(),
			'garancia_products' => $product->getGarancia(),
            'body_class' => 'page-product-configurable catalog-product-view product- categorypath-fashion-women category-women page-layout-2columns-right layout-1170 wide',
            'familyproducts' => $familyproducts,
			
			'headversion' => 'newhead', //newhead az új adatlap
        ];

    }
	
	function cookieaccept() {
		//$cookie = Input::get('check');
		
		//$cookie_accepted = Input::get('cookie_accepted');
		
		$stat=Input::get('stat_cookie');
		$mark=Input::get('marketing_cookie');
		
		//echo "stat: ".$stat;
		//echo " marketing: ".$mark;
		if(Input::get('stat_cookie')):
			$stat_cookie = "yes";
		else:
			$stat_cookie = "no";
		endif;
		
		if(Input::get('marketing_cookie')):
			$marketing_cookie = "yes";
		else:
			$marketing_cookie = "no";
		endif;
		
		
		//minden cookiet 90 napra teszek le: time()+60*60*24*90
		setcookie("stat_cookie", $stat_cookie, time()+60*60*24*90);
		setcookie("marketing_cookie", $marketing_cookie, time()+60*60*24*90);
		
			
		return Redirect::to($_SERVER['HTTP_REFERER'], 301);
	}
	
function ajaxtest() {
	$content = Input::get('ujszoveg');
	$hide = Input::get('rejtett');
	$view = array('content'=>$content, 'rejtett'=>$hide);
	//return View::make('webshop.block.ajaxtest', $view);
	return view('webshop.block.ajaxtest', $view);
}//ajaxtest()
}
