#!/bin/bash

#
# Minden fájlon futassa le:
#   sudo ./.psr.sh go all
# 1 adott fájlon futassa le:
#   sudo ./.psr.sh app/Models/User/UserGroups fix
#
#

if [ $2 == 'fix' ]; then
   ./vendor/bin/phpcbf --standard=PSR2 $1.php
   chmod 777 $1.php
    ./vendor/bin/phpcs --standard=PSR2 $1.php
fi




if [ $2 == 'all' ]; then
    for line in $(find app/Models/* -name '*.php'); do
        echo "$line"
         ./vendor/bin/phpcbf --standard=PSR2 $line
         chmod 777 $line

    done
    for line in $(find app/Http/Controllers/* -name '*.php'); do
        echo "$line"
         ./vendor/bin/phpcbf --standard=PSR2 $line
         chmod 777 $line

    done
    for line in $(find resources/lang/* -name '*.php'); do
        echo "$line"
         ./vendor/bin/phpcbf --standard=PSR2 $line
         chmod 777 $line

    done
fi
