<?php

return [
    'home_title' => 'Handmade Sofas & Beds',
    'default_title' => 'Handmade Sofas & Beds',
    'default_description' => 'Vamosi Milan - an original, Italian-style furniture maker offering 100% handmade and tailor-made sofas, beds and accessories with a wide range of custom fabrics!'
];
