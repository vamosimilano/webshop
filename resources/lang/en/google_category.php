<?

	return [
		"u-shaped-sofas" => "Furniture > Sofas",
		"corner-sofas" => "Furniture > Sofas",
		"armchairs" => "Furniture > Chairs > Arm Chairs, Recliners & Sleeper Chairs",
		"double-beds" => "Furniture > Beds & Accessories > Beds & Bed Frames",
		"straight-sofas" => "Furniture > Sofas",
		"footstools" => "Furniture > Ottomans",
		"bed-mattress" => "Furniture > Beds & Accessories > Beds & Bed Frames",
	]

?>