<?php

return [
    'fb_link' => 'https://www.facebook.com/vamosimilan',
    'login' => 'login',
    'rolunk' => 'about-us',
    'bemutatotermeink' => 'contact',
    'karrier' => 'http://karrier.vamosimilano.hu',
    'bankkartyas-fizetes-tajekoztato' => 'credit-card-informations',
    'aruhitel' => 'comercial-credit',
    'hazhozszallitas' => 'shipping',
    'felhasznalasi-feltetelek' => 'terms-and-conditions',
    'search' => 'search',
    'contact' => 'contact',
    'register' => 'register',
    'blog' => 'http://blog.vamosimilano.hu',
    'password/email' => 'login/forgot-password',
    'products' => 'products',
    'cart' => 'cart',
    'cart/personal' => 'cart/personal',
    'cart/store' => 'cart/store',
    'cart/success' => 'cart/success',
    'cart/error' => 'cart/error',
    'subscribe' => 'subscribe',
    'addtocart' => 'addtocart',
    'termek-leiras' => 'product-description',
    'jellemzok' => 'features',
    'leiratkozas' => 'unsubscribe',
    'a-honap-butorai' => 'highlighted-products',
    'subscribe/success' => 'subscribe/success',
	'hasznalati-utmutato' => 'user-guide',
    'cart/overview' => 'cart/overview',
    'cart/savepersonal' => 'cart/savepersonal',
    'butorok-old' => 'butor',
    'lampahaz-old' => 'lampahaz',
    'aszf' => 'terms-and-conditions',
	'kiemelt-akciok' => 'sale-discounts',
	'adatvedelem' => 'privacy',
	'impresszum' => 'legal-notice',
];
