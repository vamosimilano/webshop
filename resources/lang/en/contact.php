<?php

return [
    'Londoni üzletünk' => 'Our furniture store in London',
    '27 Cricklewood Broadway, NW2 3JX' => '27 Cricklewood Broadway, NW2 3JX',
    'Telefonszám:' => 'Phone number:', //Phone number:
	'london-phone' => '+44 (0) 208 2083808', //+44 (0) 7511 054224 +36-1-353-9410
    'london-tel_href' => 'tel:00442082083808', //00447511054224 tel:003613539410
    'E-mail:' => 'Email:',
    'london-email' => 'sales@vamosi.co.uk',
    'london-mail_href' => 'mailto:sales@vamosi.co.uk',
    'Nyitva tartás:' => 'Opening hours:',
    'Hétfő-Szombat: 10:00 - 19:00' => 'Monday-Saturday: 10:00 AM - 7:00 PM',
    'Szombat: 10:00 - 16:30' => 'Szombat: 10:00 - 16:30',
    'Vasárnap: 12:00 - 18:00' => 'Sunday: 12:00 PM - 6:00 PM',
    'Ügyfélszolgálat: 9:00-17:00' => 'Ügyfélszolgálat: 9:00-17:00',
	'factory_name' => 'Sofa Art London Ltd.'
];
