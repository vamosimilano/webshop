<?php

return [
    'home_title' => 'Prémium bútorok, design kanapék',
    'default_title' => 'Modern, olasz formatervezés',
    'default_description' => 'A Vamosi Milano modern, olasz formatervezési hagyományok alapján, kézzel készít design kanapékat és franciaágyakat - melyek teljesen személyre szabhatóak!'

];
