<?php

 return array(

    'user'                  => 'Nem található felhasználó a megadott e-mail címmel.',
    'token'                     => 'Az új jelszó generálásához tartozó kód érvénytelen.',
    'sent'                      => 'A jelszó emlékeztetőt kiküldtük a megadott e-mail címre!',
    'reset'                     => 'Jelszavát sikeresen módosította!',
    'reminder_subject'              => 'Új jelszó igény',
    'Sikeres jelszó beállítás, lépjen be az új jelszavával!' => 'Sikeres jelszó beállítás, lépjen be az új jelszavával!',

 );
