<?php
$headversion = "newhead";
?>
@extends('layouts.default')
@section('content')
<?php
if( isset($headversion) && $headversion === "newhead" ) {
?>
<div class="container">
	<div class="row my-5">
		<div class="col-lg-3 col-md-4">
			@include('profile.sidebar', ['aktiv'=>'index'])
		</div>
		<div class="col-lg-9 col-md-8">
			<h1 class="h3-responsive title-center text-uppercase">{{ t('Saját adatok kezelése') }}</h1>
			<form class="form" action="{{ action('ProfileController@update') }}" method="post" id="form-validate" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-6">
						<div class="md-form md-outline mb-0">
							<input id="lastname" name="lastname" required value="{{$user->lastname}}" title="{{ t('Vezetéknév') }}" class="form-control" type="text">
							<label for="lastname">{{ t('Vezetéknév') }}<strong class="ml-1">*</strong></label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="md-form md-outline mb-0">
							<input id="firstname" name="firstname" required value="{{$user->firstname}}" title="{{ t('Keresztnév') }}" class="form-control" type="text">
							<label for="firstname">{{ t('Keresztnév') }}<strong class="ml-1">*</strong></label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="md-form md-outline mb-0">
							<input id="phone" name="phone" value="{{$user->phone}}" title="{{ t('Telefonszám') }}" class="form-control" type="text">
							<label for="phone">{{ t('Telefonszám') }}</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="md-form md-outline mb-0">
							<input id="email" name="email" required value="{{$user->email}}" title="{{ t('E-mail cím') }}" class="form-control" type="email">
							<label for="email">{{ t('E-mail cím') }}<strong class="ml-1">*</strong></label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="md-form md-outline">
							<input id="password" name="password" autocomplete="off" title="{{ t('Jelenlegi jelszó') }}" class="form-control" type="password">
							<label for="password">{{ t('Jelenlegi jelszó') }}</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="md-form md-outline">
							<input id="password_new" name="password_new" autocomplete="off" title="{{ t('Új jelszó') }}" class="form-control" type="password">
							<label for="password_new">{{ t('Új jelszó') }}</label>
						</div>
					</div>
					<div class="col-md-6">
						<p class="">{{ t('Jelszava megváltoztatásához írja be jelenlegi jelszavát, majd adja meg az újat!') }}</p>
					</div>
					<div class="col-md-6">
						<button type="submit" class="btn btn-block btn-primary">{{ t('Profil frissítése') }}</button>
					</div>
				</div>
				{{ csrf_field() }}
			</form>
		</div>
	</div>
</div>
<?php
}
else {
?>
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Saját adatok kezelése') }}</strong>
                        </li>
            </ul>
</div>
<main class="page-main" id="maincontent">
        <div class="columns">
    <div class="column main">


<form class="form form-edit-account" action="{{ action('ProfileController@update') }}" method="post" id="form-validate" enctype="multipart/form-data">
    <fieldset class="fieldset info">
            <legend class="legend"><span>{{ t('Saját adatok kezelése') }}</span></legend><br>

        <div class="field field-name-lastname required">
            <label class="label" for="lastname">
                <span>{{ t('Vezetéknév') }}</span>
            </label>

            <div class="control">
                <input id="lastname" name="lastname" required="" value="{{$user->lastname}}" title="{{ t('Vezetéknév') }}" class="input-text required-entry"  type="text">
            </div>
        </div>
        <div class="field field-name-firstname required">
            <label class="label" for="firstname">
                <span>{{ t('Keresztnév') }}</span>
            </label>
            <div class="control">
                <input id="firstname" name="firstname" required="" value="{{$user->firstname}}" title="{{ t('Keresztnév') }}" class="input-text required-entry" type="text">
            </div>
        </div>

        <div class="field field-name-phone">
            <label class="label" for="phone">
                <span>{{ t('Telefonszám') }}</span>
            </label>
            <div class="control">
                <input id="phone" name="phone" value="{{$user->phone}}" title="{{ t('Telefonszám') }}" class="input-text required-entry" type="text">
            </div>
        </div>
        <div class="field field-name-email">
            <label class="label" for="email">
                <span>{{ t('E-mail cím') }}</span>
            </label>
            <div class="control">
                <input id="email" name="email" required="" value="{{$user->email}}" title="{{ t('E-mail cím') }}" class="input-text required-entry" type="email">
            </div>
        </div>
        <legend class="legend"><span>{{ t('Jelszó változtatása') }}</span></legend>
        <p>{{ t('Jelszava megváltoztatásához írja be jelenlegi jelszavát, majd adja meg az újat!') }}</p>
        <div class="field field-name-passwords">
            <label class="label" for="passwords">
                <span>{{ t('Jelenlegi jelszó') }}</span>
            </label>
            <div class="control">
                <input id="password" name="password" value="" autocomplete="off" title="{{ t('Jelenlegi jelszó') }}" class="input-text required-entry" type="password">
            </div>
        </div>
        <div class="field field-name-passwords">
            <label class="label" for="password_new">
                <span>{{ t('Új jelszó') }}</span>
            </label>
            <div class="control">
                <input id="password_new" name="password_new" value="" autocomplete="off" title="{{ t('Új jelszó') }}" class="input-text required-entry" type="password">
            </div>
        </div>




    </fieldset>
    <div class="clearfix"></div>

    <div class="actions-toolbar">
        <div class="primary">
            <button type="submit" class="action save primary" title="{{ t('Profil frissítése') }}"><span>{{ t('Profil frissítése') }}</span></button>
        </div>
    </div>
    {{ csrf_field() }}
	<?php
	/*
	if ($user->customer_id == 1499) {
		
		$aprice = CartHelper::getAdvPriceTeszt();
		
		echo '<div id="rejtett"><pre>'.var_export($aprice,true).'</pre></div>';
	}
	*/
	?>
	
</form>

</div>

    @include('profile.sidebar')

    </div>
</main>
<?php
}
?>
@stop