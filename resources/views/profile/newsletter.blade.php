<?php
$headversion = "newhead";
?>
@extends('layouts.default')
@section('content')
<?php
if( isset($headversion) && $headversion === "newhead" ) {
?>
<div class="container">
	<div class="row my-5">
		<div class="col-lg-3 col-md-4">
			@include('profile.sidebar', ['aktiv'=>'newsletter'])
		</div>
		<div class="col-lg-9 col-md-8">
			<h1 class="h3-responsive title-center text-uppercase">{{ t('Hírlevél feliratkozás') }}</h1>
			<p class="text-center">{{ t('Értesüljön elsőként a meghírdetett akcióinkról') }}</p>
			<form id="subForm" class="js-cm-form" action="https://www.createsend.com/t/subscribeerror?description=" method="post" data-id="{{t('newlettersubscribeid')}}">
				<div class="md-form md-outline">
					<input id="fieldName" class="form-control" name="cm-name" type="text" value="{{Auth::user()->lastname}} {{Auth::user()->firstname}}" required >
					<label for="fieldName active">{{ t('Név') }}<strong class="ml-1">*</strong></label>
				</div>
				<div class="md-form md-outline">
					<input id="fieldEmail" class="js-cm-email-input form-control" name="{{t('newlettersubscribename')}}" type="email" value="{{Auth::user()->email}}" required />
					<label for="fieldEmail">{{ t('E-mail') }}<strong class="ml-1">*</strong></label>
				</div>
				<div class="form-check md-form fcl-except">
					<input id="fieldok" class="form-check-input js-cm-email-input" type="checkbox" required />
					<label for="fieldok" class="form-check-label">{{t('Feliratkozasengedélyszöveg')}}</label>
				</div>
				<div class="form-group text-right">
					<button class="action subscribe primary btn btn-primary" type="submit">{{t('Feliratkozás')}}</button> 
				</div>
			</form>
			
		</div>
	</div>
</div>
<?php
}
else {
?>
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Hírlevél feliratkozás') }}</strong>
                        </li>
            </ul>
</div>
<main class="page-main" id="maincontent">
        <div class="columns">
    <div class="column main">
		<legend class="legend"><span>{{ t('Hírlevél feliratkozás') }}</span></legend><br>
        <p>{{ t('Értesüljön elsőként a meghírdetett akcióinkról') }}</p>
	<?/*
    <form class="form form-edit-account"  method="post" id="subscribe-footer-profile" action="{{ action('SubscribeController@subscribe') }}" enctype="multipart/form-data">

    <fieldset class="fieldset info" style="width: 100%; padding: 0px;margin:0px;">
        <legend class="legend"><span>{{ t('Hírlevél feliratkozás') }}</span></legend><br>
        <p>{{ t('Értesüljön elsőként a meghírdetett akcióinkról') }}</p>

        @if (empty($unsub))


                                            <input type="hidden" name="lastname" value="{{ $user->lastname }}"  />
                                            <input type="hidden" name="firstname" value="{{ $user->firstname }}"  />
                                            <input type="hidden" name="email" value="{{ $user->email }}"  />
                                            <input type="hidden" name="key" value="1"  />


                                            {{ csrf_field() }}
                                            <div class="clearfix"></div>

                                        <div class="clearfix"></div>
                                        <a class="btn btn-primary white-text" href="javascript:$('#subscribe-footer-profile').submit();">{{ t('Feliratkozás') }}</a>

        @elseif ($unsub->unsubscribe_time != null)
            <p>{{ t('Ön leiratkozott hírleveleinkről, mi ezt tiszteletben tartjuk, mert mi is utáljuk a SPAM-et! Amennyiben felszeretne iratkozni újra, keresse ügyfélszolgálatunkat!') }}</p>
        @else
            <p>{{ t('Ön jelenleg felvan iratkozva hírlevelünkre!') }}</p>
            <a class="btn btn-primary white-text" href="{{ action('SubscribeController@unSubscribe', $unsub->unsub_url) }}">{{ t('Leiratkozás') }}</a>
        @endif


        <div class="clearfix"></div>
    </fieldset>
    <div class="clearfix"></div>
    </form>
	*/?>
	@include('cms.subscribeGDPR')
    <div class="clearfix"></div>

</div>

    @include('profile.sidebar')

    </div>
</main>
<?php
}
?>
@stop