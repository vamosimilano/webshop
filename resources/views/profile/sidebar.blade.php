<?php
$headversion = "newhead";
if( isset($headversion) && $headversion === "newhead" ) {
	$index = "";
	$orders = "";
	$newsletter = "";
	if($aktiv=="index"){
		$index = "active";
	}
	elseif ($aktiv == "orders") {
		$orders = "active";
	}
	elseif ($aktiv == "newsletter") {
		$newsletter = "active";
	}
?>
<div class="card mb-3">
	<div class="view overlay bg-secondary text-center">
		<i class="fal fa-user fa-4x text-center text-white my-4"></i>
	</div>
	<a class="btn-floating btn-action ml-auto mr-4 btn-primary" title="{{ t('Kilépés') }}" href="{{ action('AuthController@logout') }}"><i class="fal fa-sign-out"></i></a>
	<div class="card-body">
		<ul class="nav md-pills pills-gold mt-3">
			<li class="nav-item w-100">
				<a href="{{ action('ProfileController@index') }}" class="nav-link {{$index}}">{{ t('Saját adatok') }}</a>
			</li>
			<li class="nav-item w-100">
				<a href="{{ action('ProfileController@orders') }}" class="nav-link {{$orders}}">{{ t('Rendelések') }}</a>
			</li>
			<li class="nav-item w-100">
				<a href="{{ action('ProfileController@newsletter') }}" class="nav-link {{$newsletter}}">{{ t('Hírlevél') }}</a>
			</li>
		</ul>
	</div>
</div>
<?php
}
else {
?>
<div class="sidebar sidebar-main"><div class="block account-nav">
    <div class="title">
        <strong>{{ t('hello_user', 'global', ['firstname' => Auth::user()->firstname]) }}</strong>
    </div>
    <div class="content">
        <nav class="account-nav">
            <ul class="nav items">
                <li class="nav item">
                    <a href="{{ action('ProfileController@index') }}">{{ t('Saját adatok') }}</a>
                </li>
                <li class="nav item">
                    <a href="{{ action('ProfileController@orders') }}">{{ t('Rendelések') }}</a>
                </li>
                <li class="nav item">
                    <a href="{{ action('ProfileController@newsletter') }}">{{ t('Hírlevél') }}</a>
                </li>
                <li class="nav item">
                    <a href="{{ action('AuthController@logout') }}">{{ t('Kilépés') }}</a>
                </li>
            </ul>
        </nav>
    </div>
</div>
</div>
<?php
}
?>