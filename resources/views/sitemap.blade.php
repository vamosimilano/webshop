<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
        xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
    @foreach( $categories as $category )
    <url>
        <loc>{{$category->getUrl()}}</loc>
        <lastmod>{{(new DateTime())->format('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>1</priority>
    </url>
    @endforeach
    @foreach( $products as $product )
	<?
	if ($product->visible!='yes') {
		continue;
	}
	?>
    <url>
        <loc>{{$product->getUrl()}}</loc>
        <lastmod>{{(new DateTime())->format('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>
    @endforeach
    @foreach( $cms as $item )
    <url>
        <loc>{{$item->getUrl()}}</loc>
        <lastmod>{{(new DateTime())->format('Y-m-d')}}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    @endforeach
    <url>
        <loc>{{ action('ContactController@index') }}</loc>
        <lastmod>{{(new DateTime())->format('Y-m-d')}}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.7</priority>
    </url>
</urlset>
