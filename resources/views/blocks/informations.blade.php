@if (sizeof($informations))
    @if (!isMobile())
    <div class="slidercontainer" id="home-informations">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12">
                    <h2 class="filterproduct-title customerreview-title" ><span class="content"><strong>{{ t('Hasznos információk') }}</strong></span></h2>

    <div class="owl-top-narrow info-container">
        <div id="category-slider-demo-7" class="owl-carousel owl-theme">
            @foreach ($informations as $info)
            <div class="item"><a class="single-image" href="{{$info->link}}">
                <?php /*<img src="{{ $info->getImageUrl() }}" class="lazyOwl" alt="" /><p width="100%"><span width="100%">{{ $info->title . ($info->title2 ? '<br><small>' . $info->title2 . '</small>' : '') }}</span></p> */ ?>
				<img src="{{ $info->getImageUrl() }}" class="lazyOwl" alt="{{ $info->title }}" /><p><span>{{ $info->title }}</span></p>
                </a></div>
            @endforeach

            		</div>
    </div>
    <script type="text/javascript">
    require([
                            'jquery',
                            'owl.carousel/owl.carousel.min'
                        ], function ($) {

        $("#category-slider-demo-7").owlCarousel({
            lazyLoad: true,
            itemsCustom: [ [0, 1], [320, 1], [480, 2], [768, 3], [992, 4], [1170, 4] ],
            responsiveRefreshRate: 50,
            slideSpeed: 200,
            paginationSpeed: 500,
            scrollPerPage: false,
            stopOnHover: true,
            rewindNav: true,
            rewindSpeed: 600,
            pagination: false,
            navigation: true,
            autoPlay: true,
            items: {{ sizeof($informations) }},
            navigationText:["<i class='fa fa-arrow-left'></i>","<i class='fa fa-arrow-right'></i>"]
        });
    });
    </script>
    </div>
    </div>
    </div>
    </div>
    </div>
    @else
    <div class="slidercontainer" id="home-informations">

    <h2 class="filterproduct-title customerreview-title" ><span class="content"><strong>{{ t('Hasznos információk') }}</strong></span></h2>
    @foreach ($informations as $info)
            <div class="col-sm-12 marginbottom10px">
            <div class="item"><a class="single-image" href="{{$info->link}}">
                <img src="{{ $info->getImageUrl() }}" class="lazyOwl" alt="{{ $info->title }}" /><p><span>{{ $info->title . ($info->title2 ? '<br><small>' . $info->title2 . '</small>' : '') }}</span></p>
                </a></div>
            </div>
    @endforeach

</div>

    @endif
@endif

