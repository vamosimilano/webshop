@if (sizeof($designer))
<div class="col-sm-12">
	<h2 class="filterproduct-title"><span class="content"><strong>{{ t('Bútortervező') }}</strong></span></h2>
	<div class="homepage-grid-banner">
		<div class="container">
			@if (isMobile())
			<div class="row mobile-view">
				@foreach ($designer as $k => $item)
					@if ($k == 0)
							<a href="{{ $item->link }}"><img src="{{ $item->getImageUrl() }}" alt="{{ $item->title }}" /></a><br><br><br>
					@endif
				@endforeach
			</div>
			@else
			<div class="row desktop-view">
				@foreach ($designer as $k => $item)
					@if ($k == 0)
							<a href="{{ $item->link }}-desktop"><img src="{{ $item->getImageUrl() }}" alt="{{ $item->title }}" /></a><br><br><br>
					@endif
				@endforeach
			</div>
			@endif
		</div>
	</div>
</div>
@endif