<div class="single-images paddingtop30">
	<div class="row">
		@foreach (ProductCategory::menuHighLight() as $top_category)
		<div class="col-sm-3 paddingbottom15">
			<a class="image-link" href="{{ $top_category->getUrl() }}">
				<img src="{{ $top_category->getImageUrl(370) }}" title="{{ $top_category->name }}" width="370" height="254" alt="{{ $top_category->name }}" />
				<span class="category-title">{{ $top_category->name }}</span>
			</a>
		</div>
		@endforeach
	</div>
</div>