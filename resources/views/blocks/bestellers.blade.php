@if (sizeof($bestellers))
<div class="single-images border-radius">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
            <h2 class="filterproduct-title"><span class="content"><strong>{{ t('Bestsellers') }}</strong></span></h2>
            </div>
            @foreach ($bestellers as $item_product)
            <div class="col-sm-3 col-xs-6" >
                <a class="image-link" href="{{$item_product->getUrl()}}">
                    <img src="{{$item_product->getDefaultImageUrl(600)}}" alt="{{$item_product->getName()}}" />

                </a><br>{{$item_product->getName()}} {{ t('akár már') }} {{money($item_product->getDefaultPrice(), true)}}
            </div>
            @endforeach

         </div>
    </div>
</div>
@endif
