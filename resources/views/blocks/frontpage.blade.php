<?php
$textile_count = DB::select("select count(*) as db FROM product_textiles as t INNER JOIN product_textiles_lang as l on (t.textile_id=l.textile_id and l.visible='yes' and l.deleted_at is null) WHERE t.deleted_at is null");
//print_r($textile_count);
//echo $textile_count[0]->db;
?>
<div class="container-fluid px-0">
@if(ismobile())
	@include('layouts.fp_showroom_mobile', ['showroom'=>$showroom4, 'sid'=>1])
@else
	@include('layouts.fp_showroom', ['showroom'=>$showroom4, 'sid'=>1])
@endif
</div>
@include('layouts.fp_aboutus')
@include('layouts.fp_usp', ['textile_count'=>$textile_count[0]->db])
<div class="container-fluid my-4">
	<header class="row">
		<hgroup class="col-12">
			<h2 class="h3-responsive text-uppercase title-center my-4">Vásárlóink kedvenc termékei</h2>
		</hgroup>
	</header>
	<section class="row product-listing">
		@include('webshop.product_card', ['product'=>$favorit_L])
		@include('webshop.product_card', ['product'=>$favorit_U])
		@include('webshop.product_card', ['product'=>$favorit_E])
		@include('webshop.product_card', ['product'=>$favorit_F])
	</section>
</div>
@include('layouts.fp_video')
<div class="container-fluid my-4">
	<header class="row">
		<hgroup class="col-12">
			<h2 class="h3-responsive text-uppercase title-center my-4">Kiemelt akcióink</h2>
		</hgroup>
	</header>
	<section class="row product-listing">
		@include('webshop.product_card', ['product'=>$p0, 'metacount'=>1, 'product_i'=>1])
		@include('webshop.product_card', ['product'=>$p1, 'metacount'=>1, 'product_i'=>1])
		@include('webshop.product_card', ['product'=>$p2, 'metacount'=>1, 'product_i'=>1])
		@include('webshop.product_card', ['product'=>$p3, 'metacount'=>1, 'product_i'=>1])
	</section>
	<footer class="row">
		<div class="col-12 text-right">
			<a href="{{ url( 'kiemelt-akciok' ) }}" class="btn btn-primary">{{t('Tovább az összes akciós termékhez')}}</a>
		</div>
	</footer>
</div>
@include('layouts.fp_product_counter', ['manufacturedcounter'=>$manufacturedcounter,'textile_count'=>$textile_count[0]->db])
@include('layouts.fp_facebook_counter')
