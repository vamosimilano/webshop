@if (sizeof($topproducts))
<div class="container">
	<h2 class="filterproduct-title"><span class="content"><strong>{{ t('Kiemelt akciók') }}</strong></span></h2>
	<div id="slideshow">
		<div class="homepage-grid-banner">
			<div class="container">
				<div class="row">
					@foreach ($topproducts as $k => $item)
					<a href="{{ $item->link }}"><img src="{{ $item->getImageUrl() }}" alt="{{ $item->title }}" /></a><br><br><br>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endif