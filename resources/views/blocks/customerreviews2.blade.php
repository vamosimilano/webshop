<?php $customer_reviews = Cms::getCustomerReviews(12); ?>
<div class="container my-5">
	<div class="row mt-4">
		<div class="col-12 line-title">
			<span>{{ t('Vásárlóink küldték') }}</span>
		</div>
		<div id="reviews" class="carousel slide w-100" data-ride="carousel">
			<ol class="carousel-indicators" id="review-indicator">
				<?php if(ismobile()): ?>
				<li data-target="#reviews" data-slide-to="0" class="active"><i class="fa fa-circle" aria-hidden="true"></i></li>
				<li data-target="#reviews" data-slide-to="1"><i class="fa fa-circle" aria-hidden="true"></i></li>
				<li data-target="#reviews" data-slide-to="2"><i class="fa fa-circle" aria-hidden="true"></i></li>
				<li data-target="#reviews" data-slide-to="3"><i class="fa fa-circle" aria-hidden="true"></i></li>
				<li data-target="#reviews" data-slide-to="4"><i class="fa fa-circle" aria-hidden="true"></i></li>
				<li data-target="#reviews" data-slide-to="5"><i class="fa fa-circle" aria-hidden="true"></i></li>
				<?php else: ?>
				<li data-target="#reviews" data-slide-to="0" class="active"><i class="fa fa-circle" aria-hidden="true"></i></li>
				<li data-target="#reviews" data-slide-to="1"><i class="fa fa-circle" aria-hidden="true"></i></li>
				<?php endif; ?>
			</ol>
			<div class="carousel-inner w-100">
				<div class="carousel-item active">
					<div class="row mx-0 px-0">
					<?php
					$i=0;
					foreach($customer_reviews as $review): 
						if(ismobile()):
							$counter=2;
						else:
							$counter=6;
						endif;
						if($i===$counter):
						$i=0;
					?>
					</div>
				</div>
				<div class="carousel-item">
					<div class="row mx-0 px-0">
					<?php	
						endif;
					?>
						<div class="col my-3">
							<a href="{{ $review->getImageUrl(1024) }}" title="{{ $review->title }}" data-toggle="modal" data-target="#review-<?php echo $i; ?>"><img src="{{ $review->getImageUrl() }}" alt="{{ $review->title }}" class="img-fluid" /><?php //echo $i; ?></a>
						</div>
					<?php 
					$i++;
					endforeach; ?>
					</div>
				</div>
			</div>
		</div><?php //carousel vége ?>
		<?php
		/*
			Carouselből megnyíló képek modal ablakai
		*/
		$i=0;
		foreach($customer_reviews as $review): ?>
		<div class="modal fade" id="review-<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<img src="{{ $review->getImageUrl(1024) }}" class="img-fluid" alt="{{ $review->title }}" data-dismiss="modal" aria-label="Close">
					</div>
				</div>
			</div>
		</div>
		<?php $i++;
		endforeach; ?>
	</div>
</div>