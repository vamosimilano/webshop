@if (sizeof($stores))
<div class="col-sm-12">
	<h2 class="filterproduct-title"><span class="content"><strong>{{ t('Bemutatótermünk') }}</strong></span></h2>
</div>
<div class="clearfix"></div>
<div class="col-lg-12" id="store-slider-container">
	<div class="stores-stab desktop-view">
	@foreach ($stores as $k=>$item)
	<div data-target="{{ ($k+1) }}" id="store_tab_{{ ($k+1) }}" {{ ($k==0 ? 'class="active animated zoomInDown"' : 'class="animated zoomInDown"') }} onclick="gotoParalaxStore({{ ($k) }})">{{$item->title}}</div>
	@endforeach
	</div>
	<div id="banner-slider-stores" class="owl-carousel owl-banner-carousel owl-middle-narrow">
		@foreach ($stores as $k=>$item)
		<div class="item carousel-bella-item" id="store_{{ ($k+1) }}" style="background:url({{ (isMobile() ? $item->getMobilImageUrl() :  $item->getImageUrl()) }});">
			<div class="container pos-relative">
				<a href="{{ $item->link }}">
				<img src="{{asset('images/slider_layer.png')}}" alt="" style="max-height: 400px">
				</a>
				<div class="store-details">{{ $item->title.($item->title2 ? "<br>".$item->title2 : '').($item->title3 ? "<br>".$item->title3 : '') }}</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
	var paralax_store;
	require(['jquery', 'owl.carousel/owl.carousel.min' ], function($) {
		paralax_store = $("#banner-slider-stores").owlCarousel({
			items: 1,
			/*autoplay: true,
			autoplayTimeout: 10000,
			autoplayHoverPause: true,*/
			dots: false,
			nav: false,
			navRewind: true,
			//loop: true,
			height: 400
		});
		paralax_store.on('change.owl.carousel', function(e) {
			var current = (e.item.index);
			$('.stores-stab div').removeClass('active');
			if (!$("#store_tab_"+(current)).length){
				current = 1;
			}
			$("#store_tab_"+(current)).addClass('active');
		});
	});
	function gotoParalaxStore(step){
		if (step > 2) {
			//step = step - 1;
		}
		paralax_store.trigger("to.owl.carousel", [step, 500, true]);
	}
</script>
@endif