<div class="clearfix"><br><br></div>
<div class="col-sm-12">
	<h2 class="filterproduct-title"><span class="content"><strong>{{ t('Vamosi a Facebookon') }}</strong></span></h2>
</div>
<div class="col-md-2 text-center">
	<a href="https://www.facebook.com/vamosimilano" title="Kövessen a Facebookon is!" target="_blank">
		<p><br><i class="fa fa-5x fa-thumbs-o-up text-dark" aria-hidden="true"></i></p>
		<p class="lead text-bold">540502</p>
	</a>
</div>
<div class="col-md-10">
	<h3 class="text-dark text-uppercase text-bold">{{t('Kövesse a Vamosi-t és legyen részese Magyarország egyik legnagyobb Facebook közösségének!')}}</h3>
	<p>A <a href="https://www.facebook.com/vamosimilano" title="Kövessen a Facebookon is!" target="_blank">Vamosi Milano Facebook oldalán</a> mindig értesül fantasztikus kanapé és franciaágy akcióinkról, folyamatos kedvezményeinkről és újdonságainkról.</p>
	<p>Ne feledje, havonta hatalmas nyereményjátékokat hirdetünk követőink, lájkolóink és megosztóink számára – vesse bele magát a játék izgalmába és szerezze meg álmai bútorát!</p>
</div>