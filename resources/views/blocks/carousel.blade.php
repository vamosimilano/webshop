@if (sizeof($carousel))
		<div id="banner-slider-demo-11" class="owl-carousel owl-banner-carousel owl-middle-narrow">
            @foreach($carousel as $item)
            <div class="item carousel-bella-item" style="background:url({{ (isMobile() ? $item->getMobilImageUrl() :  $item->getImageUrl()) }});">
                <div class="pos-relative">
                    <a href="{{ $item->link }}"><img src="{{ ( isMobile() ? asset('images/600x600_mobile_slider.png') : asset('images/slider_layer.png') ) }}" alt="Vamosi Milano - egyedi bútorok"></a>
                </div>
            </div>
            @endforeach

        </div>
        <script type="text/javascript">
            require([
                'jquery',
                'owl.carousel/owl.carousel.min'
            ], function($) {
                $("#banner-slider-demo-11").owlCarousel({
                    items: 1,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    dots: false,
                    nav: true,
                    navRewind: true,
                    loop: true,
                    navText: ["<em class='fa fa-arrow-left'></em>", "<em class='fa fa-arrow-right'></em>"]
                });
            });
        </script>
@endif
