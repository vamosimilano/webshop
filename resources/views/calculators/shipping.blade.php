
        <form action="" id="carform">
        <div id="checkout" class="checkout-container">
        <div class="opc-wrapper">

        <div class="block block-dashboard-info">
                <div class="col-lg-12 nopadding">
                    <div class="block-content">
                        <div class="box">
                            <strong class="box-title">
                                <span>{{ t('Szállítási mód') }}</span>
                            </strong>
                            <div class="box-content">
                                <p>{{ t('Kérjük válasszon szállítási módot, hogy hol veszi át a rendelését!') }}</p>
								<?php
									if (!isset($default_data['delivery_mode']))  {
										$modok=getConfig('shipping_mode');
										$default_data['delivery_mode'] = $modok[0];
									}
								?>
                                @foreach (getConfig('shipping_mode') as $mode)
                                    <div class="col-lg-12 margin-bottom-rem">
                                        <label><input type="radio" required="" onchange="loadOverViewBlock()" name="delivery_mode" {{ ($default_data['delivery_mode'] == $mode ? 'checked' : '') }} value="{{ $mode }}"> {{ t('shipping_mode_'.$mode) }}</label>
                                    </div>
                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="clearfix"></div>
                @if (sizeof($extra_prices))
                <div class="block-content">
                    <div class="box">
                        <strong class="box-title">
                            <span>{{ t('További szolgáltatások') }}</span>
                        </strong>
                        <div class="box-content no-min-height">
                            @foreach ($extra_prices as $extra)
                            <div class="col-lg-12">
                                <div class="form-group required">
                                    <label><input type="checkbox" name="extra_prices[]" onchange="loadOverViewBlock()"  {{ ((isset($default_data['extra_prices']) and in_array($extra->id, $default_data['extra_prices'])) ? 'checked' : '') }} value="{{ $extra->id }}"> {{ $extra->name }} (+{{money($extra->price)}})</label>
                                </div>
                            </div>
                            @endforeach
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                @endif
                <div class="block-content">
                    <div class="box">
                        <strong class="box-title">
                            <span>{{ t('Szállítási adatok') }}</span>
                        </strong>
                        <div class="box-content">
                            <div class="col-lg-12">
                                <div class="form-group addressGroup">
                                    <label for="" class="control-label">{{ t('Cím') }}*:</label>
                                    <div class="input-group">
                                    <div class="col-sm-2 mobil-margin-bottom" >
                                        <input name="zip" id="zip" onblur="searchCity(this.value)" data-target="billing_zip" placeholder="{{ t('Irsz.') }}" required="" class="form-control input-sm billing-copy"  value="{{ (isset($default_data['zip']) ? $default_data['zip'] : '') }}" type="{{Config::get('shop.'.getShopCode().'.ziptype')}}">
                                    </div>
                                    <div class="col-sm-2 mobil-margin-bottom" >
                                        <input name="city" id="city"  data-target="billing_city" placeholder="{{ t('Város') }}" required="" class="form-control input-sm billing-copy"  value="{{ (isset($default_data['city']) ? $default_data['city'] : '') }}" type="text">
                                    </div>
                                    <div class="col-sm-6 mobil-margin-bottom" >
                                        <input name="address" id="address" data-target="billing_address" onblur="loadOverViewBlock()" placeholder="{{ t('Utca, Hsz.') }}" required="" class="form-control input-sm billing-copy"  value="{{ (isset($default_data['address']) ? $default_data['address'] : '') }}" type="text">
                                    </div>
									<? /*
                                    <div class="col-sm-2 mobil-margin-bottom" >
                                        <input name="other_addr" id="other_addr" data-target="billing_other_addr" placeholder="{{ t('Ajtó') }}" class="form-control input-sm billing-copy" value="{{ (isset($default_data['other_addr']) ? $default_data['other_addr'] : '') }}" type="text">
                                    </div>
									*/ ?>
                                    <div class="col-sm-2 mobil-margin-bottom" >
                                        <select name="floor" required="" onchange="loadOverViewBlock()">
                                            <option value="">{{ t('Emelet') }}</option>
                                            @for ($i = 0; $i < 20; $i++)

                                            <option value="{{ $i }}" {{ ((isset($default_data['floor']) and $default_data['floor'] == $i) ? 'selected' : '') }}>{{ ($i == '0' ? t('Fsz') : $i.' '.t('Emelet')) }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>






        </div>





        </div>
            <aside class="modal-custom opc-sidebar opc-summary-wrapper custom-slide" id="cart-right-sidebar" data-role="modal" data-type="custom" tabindex="0">

            @include('webshop.cart.blocks.shipping_right')
            </aside>

            {{ csrf_field() }}


        </div>
        <input type="hidden" name="country" value="{{$default_data['country']}}">
        <input type="hidden" name="coupon" value="{{(isset($default_data['coupon']) ? $default_data['coupon'] : '')}}">
        <input type="hidden" name="billing_country" value="{{$default_data['billing_country']}}">
        <input type="hidden" name="description" value="{{(isset($default_data['description']) ? $default_data['description'] : '')}}">
        <input type="hidden" name="billing_zip" value="{{(isset($default_data['description']) ? $default_data['billing_zip'] : '')}}">
        <input type="hidden" name="billing_city" value="{{(isset($default_data['billing_city']) ? $default_data['billing_city'] : '')}}">
        <input type="hidden" name="billing_address" value="{{(isset($default_data['billing_address']) ? $default_data['billing_address'] : '')}}">
        <input type="hidden" name="billing_other_addr" value="{{(isset($default_data['billing_other_addr']) ? $default_data['billing_other_addr'] : '')}}">
        <input type="hidden" name="billing_tax_number" value="{{(isset($default_data['billing_tax_number']) ? $default_data['billing_tax_number'] : '')}}">
        <input type="hidden" name="billing_name" value="{{(isset($default_data['billing_name']) ? $default_data['billing_name'] : '')}}">
        <input type="hidden" name="lastname" value="{{(isset($default_data['lastname']) ? $default_data['lastname'] : '')}}">
        <input type="hidden" name="firstname" value="{{(isset($default_data['firstname']) ? $default_data['firstname'] : '')}}">
        <input type="hidden" name="phone" value="{{(isset($default_data['phone']) ? $default_data['phone'] : '')}}">
        <input type="hidden" name="email" value="{{(isset($default_data['email']) ? $default_data['email'] : '')}}">
        <input type="hidden" name="user_firstname" value="{{(isset($default_data['user_firstname']) ? $default_data['user_firstname'] : '')}}">
        <input type="hidden" name="user_lastname" value="{{(isset($default_data['user_lastname']) ? $default_data['user_lastname'] : '')}}">
        <input type="hidden" name="payment" value="{{(isset($default_data['payment']) ? $default_data['payment'] : '')}}">
        <input type="hidden" name="block" value="shipping_right">
        </form>
