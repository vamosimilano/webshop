<script>
	function computeLoan(){
		var amount = document.getElementById('amount').value;
		var interest_rate = document.getElementById('interest_rate').value;
		var months = document.getElementById('months').value;
		var interest = (amount * (interest_rate * .01)) / months;
		var payment = Math.round(((amount / months) + interest).toFixed(2));
		payment = payment.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		var onero = Math.round(amount*0.2);
		onero = onero.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		document.getElementById('payment').innerHTML = "Havonta fizetendo = "+payment+" Ft";
		if (+amount > 1000000 || +amount < 29999) {
			document.getElementById('hitelosszeg_error').style.display = 'block';
			document.getElementById('hitelosszeg_error').innerHTML = "A hitelösszeg minimum 30 000 Ft és maximum 1 000 000 Ft.";
		} else {
			document.getElementById('hitelosszeg_error').style.display = 'none';
			if (+amount > 300000) {
				document.getElementById('figyelmeztetes').style.display = 'none';
				document.getElementById('figyelmeztetes').innerHTML = "300 000 Forint hitelösszeg felett 20% önero szükséges, ami jelen esetben "+onero+" Ft. ";
				document.getElementById('onero').innerHTML = "Önero: "+onero;
				var onero = Math.round(amount*0.2);
				var hitelosszegOnerovel = amount-onero;
				var torleszto = Math.round(((hitelosszegOnerovel / months) + interest).toFixed(2));
				torleszto = torleszto.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
				document.getElementById('payment').innerHTML = "Havonta fizetendo = "+torleszto+" Ft";
			} else {
				document.getElementById('figyelmeztetes').style.display = 'none';
				document.getElementById('onero').innerHTML = "Önero: 0 Ft.";
			}
		}
	}
</script>
<?php $oldal = "235"; ?>
<div class="row">
	<div class="col-md-12">
		<h1 class="h1-responsive title-center text-uppercase my-4">Hitel 0% önerővel</h1>
		<p><strong>Frissítve:</strong> 2019. május 24.</p>
		<div id="figyelmeztetes" style="display:none" class="alert alert-info"></div>
		<div class="md-form md-outline">
			<label for="amount">Hitelösszeg</label>
			<div id="hitelosszeg_error" style="display:none" class="alert alert-danger"></div>
			<input class="form-control" id="amount" type="number" min="30000" max="1000000" placeholder="30 000 Ft - 1 000 000 Ft" onkeyup="computeLoan()">
		</div>
		<div class="md-form md-outline">
			<label for="interest_rate">THM (%)</label>
			<input class="form-control" id="interest_rate" type="number" min="0" max="100" value="38.9" disabled>
		</div>
		<div class="select-outline">
			<label for="months">Futamidő (hónap)</label>
			<select class="mdb-select md-form md-outline" id="months" onchange="computeLoan()">
				<option value="12">12 hónap</option>
				<option value="13">13 hónap</</option>
				<option value="14">14 hónap</</option>
				<option value="15">15 hónap</</option>
				<option value="16">16 hónap</</option>
				<option value="17">17 hónap</</option>
				<option value="18">18 hónap</</option>
				<option value="19">19 hónap</</option>
				<option value="20">20 hónap</</option>
				<option value="21">21 hónap</</option>
				<option value="22">22 hónap</</option>
				<option value="23">23 hónap</</option>
				<option value="24">24 hónap</</option>
				<option value="25">25 hónap</</option>
				<option value="26">26 hónap</</option>
				<option value="27">27 hónap</</option>
				<option value="28">28 hónap</</option>
				<option value="29">29 hónap</</option>
				<option value="30">30 hónap</</option>
				<option value="31">31 hónap</</option>
				<option value="32">32 hónap</</option>
				<option value="33">33 hónap</</option>
				<option value="34">34 hónap</</option>
				<option value="35">35 hónap</</option>
				<option value="36">36 hónap</</option>
				<option value="37">37 hónap</</option>
				<option value="38">38 hónap</</option>
				<option value="39">39 hónap</</option>
				<option value="40">40 hónap</</option>
				<option value="41">41 hónap</</option>
				<option value="42">42 hónap</</option>
				<option value="43">43 hónap</</option>
				<option value="44">44 hónap</</option>
				<option value="45">45 hónap</</option>
				<option value="46">46 hónap</</option>
				<option value="47">47 hónap</</option>
				<option value="48">48 hónap</option>
			</select>
		</div>
		<h2 id="payment"></h2>
		<h2 id="onero"></h2>
		<br>
		<div class="alert alert-warning">
			<strong>Hitelösszeg:</strong> 30 000 Ft és 1 000 000 Ft között.<br>
			<strong>Futamido</strong>: 12 hónap. <br><strong>Önerő</strong>: 300.000 Ft-ig minimum 0%, 300.000 Ft felett minimum 20%. <br><strong>Éves ügyleti kamat</strong>: 37,45%. <br>
		</div>
	</div>
	<div class="col-md-6">
		<p>
			<a href="https://static.vamosimilano.hu/loancalculator/hirdetmenyek/akcio_235_20190524.pdf" target="_blank" class="btn btn-outline-primary"><i class="fal fa-file-pdf fa-fw mr-2"></i> Hirdetmény letöltése</a>
		</p>
	</div>
	<div class="col-md-6">
		<p class="mt-2">
			<a href="https://www.otpbank.hu/portal/hu/SzabadFelhasznalasuHitelek/Aruhitel" target="_blank">Áruhitelekkel kapcsolatban bővebb tájékoztatást az OTP weboldalán talál</a>
		</p>
	</div>
</div>