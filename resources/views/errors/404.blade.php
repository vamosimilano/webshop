<?php $headversion = "newhead"; ?>
@extends('layouts.default')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<p class="lead text-center my-4 py-5 text-primary"><i class="fal fa-bug fa-5x"></i><br>404</p>
			<h1 class="h1-responsive title-center text-uppercase my-4">A keresett oldal nem található</h1>
			<p>Ezen a webcímen nem található semmilyen tartalom. Sajnáljuk. Előfordulhat, hogy olyan régi hivatkozásról érkezett, ami közben máshová költözött vagy megszűnt. A lenti keresőmező segítségével ellenőrizheti, hogy esetleg az oldal más területén megtalálja-e amit keresett.</p>
		</div>
	</div>
	<form method="get" action="{{ action('WebshopController@search') }}">
		<div class="row">
			<div class="col-md-3 mx-auto">
				<div class="md-form md-outline">
					<input type="text" id="sf_salespage" class="form-control" value="" maxlength="128" name="q" required>
					<label for="sf_salespage">{{ t('Írja be a keresőszót') }}</label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 mx-auto">
				<div class="md-form">
					<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search mr-3" aria-hidden="true"></i> Új Keresés</button>
				</div>
			</div>
		</div>
	</form>
</div>
@stop
@section('footer_js')
@append