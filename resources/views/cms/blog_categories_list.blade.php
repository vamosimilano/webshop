<div class="col-12 blog_categories mb-4">
	<h3 class="h4-responsive title text-uppercase">Blog kategóriák</h3>
	<ul class="list-unstyled">
<?php
$categories = DB::select("SELECT count(*) as db, category FROM `cms_content` WHERE cms_type='blog' and is_active=1 and deleted_at is null GROUP BY category ORDER BY category");
foreach ($categories as $category) {
?>
		<li class="mt-2"><a href="/blog/{{ $category->category }}"><i class="fal fa-caret-right fa-fw mt-3"></i><span class="text-dark">{{ t('cms_blogcategory_'.$category->category) }}</span> <span class="badge badge-primary">{{$category->db}}</span></a></li>
<?php
}
?>
	</ul>
</div>