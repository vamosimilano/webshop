<?php
$headversion = "newhead";
if($headversion === "newhead") {
?>
@extends('layouts.default')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div id="map" style="width:100%;height:500px;"></div>
	</div>
</div>
<div class="container-fluid">
<?php
$sunday = $monday = $tuesday = $wednesday = $thursday = $friday = $saturday = "";
$openhour = date("Hi");
$sr1_open="";
$sr2_open="";
if( date("w")==0){
	$sunday = "text-primary";
	//budapesti üzlet
	if( ("1000" < $openhour) && ($openhour < "1800") ){
		$sr1_open = "Nyitva";
	}
	else {
		$sr1_open = "Zárva";
	}
	//csepeli üzlet
	if( ("1000" < $openhour) && ($openhour < "1800") ){
		$sr2_open = "Nyitva";
	}
	else {
		$sr2_open = "Zárva";
	}
}
if( date("w")==1){
	$monday = "text-primary";
	//budapesti üzlet
	if( ("1000" < $openhour) && ($openhour < "1830") ){
		$sr1_open = "Nyitva";
	}
	else {
		$sr1_open = "Zárva";
	}
	//csepeli üzlet
	if( ("1000" < $openhour) && ($openhour < "1830") ){
		$sr2_open = "Nyitva";
	}
	else {
		$sr2_open = "Zárva";
	}
}
if( date("w")==2){
	$tuesday = "text-primary";
	//budapesti üzlet
	if( ("1000" < $openhour) && ($openhour < "1830") ){
		$sr1_open = "Nyitva";
	}
	else {
		$sr1_open = "Zárva";
	}
	//csepeli üzlet
	if( ("1000" < $openhour) && ($openhour < "1830") ){
		$sr2_open = "Nyitva";
	}
	else {
		$sr2_open = "Zárva";
	}
}
if( date("w")==3){
	$wednesday = "text-primary";
	//budapesti üzlet
	if( ("1000" < $openhour) && ($openhour < "1830") ){
		$sr1_open = "Nyitva";
	}
	else {
		$sr1_open = "Zárva";
	}
	//csepeli üzlet
	if( ("1000" < $openhour) && ($openhour < "1830") ){
		$sr2_open = "Nyitva";
	}
	else {
		$sr2_open = "Zárva";
	}
}
if( date("w")==4){
	$thursday = "text-primary";
	//budapesti üzlet
	if( ("1000" < $openhour) && ($openhour < "1830") ){
		$sr1_open = "Nyitva";
	}
	else {
		$sr1_open = "Zárva";
	}
	//csepeli üzlet
	if( ("1000" < $openhour) && ($openhour < "1830") ){
		$sr2_open = "Nyitva";
	}
	else {
		$sr2_open = "Zárva";
	}
}
if( date("w")==5){
	$friday = "text-primary";
	//budapesti üzlet
	if( ("1000" < $openhour) && ($openhour < "1830") ){
		$sr1_open = "Nyitva";
	}
	else {
		$sr1_open = "Zárva";
	}
	//csepeli üzlet
	if( ("1000" < $openhour) && ($openhour < "1830") ){
		$sr2_open = "Nyitva";
	}
	else {
		$sr2_open = "Zárva";
	}
}
if( date("w")==6){
	$saturday = "text-primary";
	//budapesti üzlet
	if( ("1000" < $openhour) && ($openhour < "1830") ){
		$sr1_open = "Nyitva";
	}
	else {
		$sr1_open = "Zárva";
	}
	//csepeli üzlet
	if( ("1000" < $openhour) && ($openhour < "1830") ){
		$sr2_open = "Nyitva";
	}
	else {
		$sr2_open = "Zárva";
	}
}
/* Mivel Szeged ugyanúgy van nyitva, mint budapest egyenlővé teszem őket: */
if(date("Ymd") > "20190425") {
	$sr3_open=$sr1_open;
	//kedd szerda zárva
	if ((date("w")==2) or (date("w")==3)) {
		$sr3_open = "Zárva";
	}
} else {
	$sr3_open = "Zárva, Nyitás: 2019.04.26 10:00";
}
if(ismobile()) {
	$br="<br>";
}
else {
	$br=" ";
}
?>
	<div class="row my-4">
		<div class="col-md-9">
			<ul class="nav nav-tabs" id="showrooms" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="budapest-tab-md" data-toggle="tab" href="#budapest-md" role="tab" aria-controls="budapest-md" aria-selected="true">{{ t( 'showroom1' ) }}{{$br}}<span class="badge badge-primary">{{$sr1_open}}</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="csepel-tab-md" data-toggle="tab" href="#csepel-md" role="tab" aria-controls="csepel-md" aria-selected="false">{{ t( 'showroom2' ) }}{{$br}}<span class="badge badge-primary">{{$sr2_open}}</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="szeged-tab-md" data-toggle="tab" href="#szeged-md" role="tab" aria-controls="szeged-md" aria-selected="false">{{ t( 'showroom3' ) }}{{$br}}<span class="badge badge-primary">{{$sr3_open}}</span></a>
				</li>
			</ul>
			<div class="tab-content" id="showroomcontent">
				<div class="tab-pane fade show active" id="budapest-md" role="tabpanel" aria-labelledby="budapest-md">
					<?php
					if( ismobile() ) {
					?>
					<h3 class="h4-responsive title text-uppercase">Üzlet nyitvatartás</h3>
					<ul class="list-unstyled mt-3">
						<li>
							<p class="{{$monday}}">Hétfő: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$tuesday}}">Kedd: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$wednesday}}">Szerda: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$thursday}}">Csütörtök: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$friday}}">Péntek: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$saturday}}">Szombat: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$sunday}}">Vasárnap: <span class="float-right">11:00 - 18:00</span></p>
						</li>
					</ul>
					<h3 class="h4-responsive title text-uppercase">Ügyfélszolgálati órák</h3>
					<ul class="list-unstyled mt-3">
						<li>
							<p class="{{$monday}}">Hétfő: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$tuesday}}">Kedd: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$wednesday}}">Szerda: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$thursday}}">Csütörtök: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$friday}}">Péntek: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$saturday}}">Szombat: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$sunday}}">Vasárnap: <span class="float-right">11:00 - 18:00</span></p>
						</li>
					</ul>
					<h3 class="h4-responsive title text-uppercase">Szolgáltatások</h3>
					<ul class="list-unstyled mt-3">
						<li>
							<p><i class="fal fa-trophy-alt fa-fw text-primary mr-3"></i>Szakképzett értékesítők</p>
						</li>
						<li>
							<p><i class="fal fa-coffee fa-fw text-primary mr-3"></i>Ajándék kávé</p>
						</li>
						<!--<li>
							<p><i class="fal fa-parking-circle fa-fw text-primary mr-3"></i>Ingyen parkoló</p>
						</li>-->
						<li>
							<p><i class="fal fa-child fa-fw text-primary mr-3"></i>Gyereksarok játékokkal</p>
						</li>
						<li>
							<p><i class="fal fa-wifi fa-fw text-primary mr-3"></i>Ingyen WiFi elérés</p>
						</li>
						<li>
							<p><a href="https://goo.gl/maps/qdrKhemQ1n92" target="_blank"><i class="fal fa-route fa-fw mr-3"></i><span class="text-dark">Útvonaltervezés</span></a></p>
						</li>
					</ul>
					<h2 class="h4-responsive title text-uppercase">Kapcsolatfelvételi lehetőségek</h2>
					<ul class="list-unstyled mt-3" id="content-bp">
						<li>
							<p><a href="https://www.google.com/maps/search/?api=1&query=47.4646476,19.0417743&query_place_id=ChIJ3-yHp7zdQUcRuD52t49Ce9E" target="_blank"><i class="fal fa-map-marker fa-fw mr-2"></i><span class="text-dark">1119 Budapest Petzvál József utca 46-48.</span></a></p>
						</li>
						<li>
							<p><a href="tel:+3613539410"><i class="fal fa-phone-office fa-fw mr-2"></i><span class="text-dark">+36-1-353-9410</span></a></p>
						</li>
						<li>
							<p><a href="mailto:ertekesites@vamosimilano.hu"><i class="fal fa-at fa-fw mr-2"></i><span class="text-dark">ertekesites@vamosimilano.hu</span></a></p>
						</li>
						<li>
							<p><a href="https://m.me/vamosimilano" target="_blank"><i class="fab fa-facebook-messenger fa-fw mr-2"></i><span class="text-dark">m.me/vamosimilano</span></a></p>
						</li>
						<li>
							<p><a href="https://facebook.com/vamosimilano"><i class="fab fa-facebook fa-fw mr-2"></i><span class="text-dark">facebook.com/vamosimilano</span></a></p>
						</li>
						<li>
							<p><a href="https://instagram.com/vamosimilano/"><i class="fab fa-instagram fa-fw mr-2"></i><span class="text-dark">instagram.com/vamosimilano/</span></a></p>
						</li>
					</ul>
					<?php
					} //mobil
					else { //desktop
					?>
					<div class="row mb-md-3">
						<div class="col-md-2 text-center">
							<p><i class="fal fa-trophy-alt fa-2x fa-fw text-primary"></i></p>
							<p>Szakképzett értékesítők</p>
						</div>
						<div class="col-md-2 text-center">
							<p><i class="fal fa-coffee fa-2x fa-fw text-primary"></i></p>
							<p>Ajándék kávé</p>
						</div>
						<!--<div class="col-md-2 text-center">
							<p><i class="fal fa-parking-circle fa-fw fa-2x text-primary"></i></p>
							<p>Ingyen parkoló</p>
						</div>-->
						<div class="col-md-2 text-center">
							<p><i class="fal fa-child fa-fw fa-2x text-primary"></i></p>
							<p>Gyereksarok játékokkal</p>
						</div>
						<div class="col-md-2 text-center">
							<p><i class="fal fa-wifi fa-fw fa-2x text-primary"></i></p>
							<p>Nyílt WiFi elérés</p>
						</div>
						<div class="col-md-2 text-center">
							<a href="https://goo.gl/maps/qdrKhemQ1n92" target="_blank"><p><i class="fal fa-route fa-fw fa-2x"></i></p>
							<p class="text-dark">Útvonaltervezés</p></a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<h2 class="h4-responsive title text-uppercase">Bemutatóterem nyitvatartás</h2>
							<table class="table-striped w-100 mb-4">
								<tbody>
									<tr class="{{$monday}}">
										<th class="px-2" scope="row">
											Hétfő
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$tuesday}}">
										<th class="px-2" scope="row">
											Kedd
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$wednesday}}">
										<th class="px-2" scope="row">
											Szerda
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$thursday}}">
										<th class="px-2" scope="row">
											Csütörtök
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$friday}}">
										<th class="px-2" scope="row">
											Péntek
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$saturday}}">
										<th class="px-2" scope="row">
											Szombat
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$sunday}}">
										<th class="px-2" scope="row">
											Vasárnap
										</th>
										<td class="px-2 text-right">
											11:00 - 18:00
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-4">
							<h2 class="h4-responsive title text-uppercase">Ügyfélszolgálati nyitvatartás</h2>
							<table class="table-striped w-100 mb-4">
								<tbody>
									<tr class="{{$monday}}">
										<th class="px-2" scope="row">
											Hétfő
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$tuesday}}">
										<th class="px-2" scope="row">
											Kedd
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$wednesday}}">
										<th class="px-2" scope="row">
											Szerda
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$thursday}}">
										<th class="px-2" scope="row">
											Csütörtök
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$friday}}">
										<th class="px-2" scope="row">
											Péntek
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$saturday}}">
										<th class="px-2" scope="row">
											Szombat
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$sunday}}">
										<th class="px-2" scope="row">
											Vasárnap
										</th>
										<td class="px-2 text-right">
											11:00 - 18:00
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-4" id="content-bp">
							<h2 class="h4-responsive title text-uppercase">Kapcsolatfelvételi lehetőségek</h2>
							<ul class="list-unstyled">
								<li class="pb-2">
									<a href="https://www.google.com/maps/search/?api=1&query=47.4646476,19.0417743&query_place_id=ChIJ3-yHp7zdQUcRuD52t49Ce9E" target="_blank"><i class="fal fa-map-marker fa-fw mr-2"></i><span class="text-dark">1119 Budapest Petzvál József utca 46-48.</span></a>
								</li>
								<li class="pb-2">
									<a href="tel:+3613539410"><i class="fal fa-phone-office fa-fw mr-2"></i><span class="text-dark">+36-1-353-9410</span></a>
								</li>
								<li class="pb-2">
									<a href="mailto:ertekesites@vamosimilano.hu"><i class="fal fa-at fa-fw mr-2"></i><span class="text-dark">ertekesites@vamosimilano.hu</span></a>
								</li>
								<li class="pb-2">
									<a href="https://m.me/vamosimilano" target="_blank"><i class="fab fa-facebook-messenger fa-fw mr-2"></i><span class="text-dark">m.me/vamosimilano</span></a>
								</li>
								<li class="pb-2">
									<a href="https://facebook.com/vamosimilano"><i class="fab fa-facebook fa-fw mr-2"></i><span class="text-dark">facebook.com/vamosimilano</span></a>
								</li>
								<li>
									<a href="https://instagram.com/vamosimilano/"><i class="fab fa-instagram fa-fw mr-2"></i><span class="text-dark">instagram.com/vamosimilano/</span></a>
								</li>
							</ul>
						</div>
					</div>
					<?php
					}
					?>
				</div>
				<div class="tab-pane fade" id="csepel-md" role="tabpanel" aria-labelledby="csepel-md">
				<?php 
				if ( ismobile() ) {
				?>
					<h3 class="h4-responsive title text-uppercase">Üzlet nyitvatartás</h3>
					<ul class="list-unstyled mt-3">
						<li>
							<p class="{{$monday}}">Hétfő: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$tuesday}}">Kedd: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$wednesday}}">Szerda: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$thursday}}">Csütörtök: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$friday}}">Péntek: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$saturday}}">Szombat: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$sunday}}">Vasárnap: <span class="float-right">11:00 - 18:00</span></p>
						</li>
					</ul>
					<h3 class="h4-responsive title text-uppercase">Ügyfélszolgálati órák</h3>
					<ul class="list-unstyled mt-3">
						<li>
							<p class="{{$monday}}">Hétfő: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$tuesday}}">Kedd: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$wednesday}}">Szerda: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$thursday}}">Csütörtök: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$friday}}">Péntek: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$saturday}}">Szombat: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$sunday}}">Vasárnap: <span class="float-right">11:00 - 18:00</span></p>
						</li>
					</ul>
					<h3 class="h4-responsive title text-uppercase">Szolgáltatások</h3>
					<ul class="list-unstyled mt-3">
						<li>
							<p><i class="fal fa-trophy-alt fa-fw text-primary mr-3"></i>Szakképzett értékesítők</p>
						</li>
						<li>
							<p><i class="fal fa-parking-circle fa-fw text-primary mr-3"></i>Ingyen parkoló</p>
						</li>
						<li>
							<p><a href="https://goo.gl/maps/1PCTU7jrATp" target="_blank"><i class="fal fa-route fa-fw mr-3"></i><span class="text-dark">Útvonaltervezés</span></a></p>
						</li>
					</ul>
					<h2 class="h4-responsive title text-uppercase">Kapcsolatfelvételi lehetőségek</h2>
					<ul class="list-unstyled mt-3" id="content-csepel">
						<li>
							<p><a href="https://goo.gl/maps/BXMHaMYwaL62" target="_blank"><i class="fal fa-map-marker fa-fw mr-2"></i><span class="text-dark">1211 Budapest, II. Rákóczi Ferenc út 107-115 (Bejárat a lakópark Karácsony Sándor és Teller Ede úti sarkánál)</span></a></p>
						</li>
						<li>
							<p><a href="tel:+3613539410"><i class="fal fa-phone-office fa-fw mr-2"></i><span class="text-dark">+36-1-353-9410</span></a></p>
						</li>
						<li>
							<p><a href="mailto:ertekesites@vamosimilano.hu"><i class="fal fa-at fa-fw mr-2"></i><span class="text-dark">ertekesites@vamosimilano.hu</span></a></p>
						</li>
						<li>
							<p><a href="https://m.me/vamosimilano" target="_blank"><i class="fab fa-facebook-messenger fa-fw mr-2"></i><span class="text-dark">m.me/vamosimilano</span></a></p>
						</li>
						<li>
							<p><a href="https://facebook.com/vamosimilano"><i class="fab fa-facebook fa-fw mr-2"></i><span class="text-dark">facebook.com/vamosimilano</span></a></p>
						</li>
						<li>
							<p><a href="https://instagram.com/vamosimilano/"><i class="fab fa-instagram fa-fw mr-2"></i><span class="text-dark">instagram.com/vamosimilano/</span></a></p>
						</li>
					</ul>
				<?php
				}
				else {
				?>
					<div class="row mb-md-3">
						<div class="col-md-2 text-center">
							<p><i class="fal fa-trophy-alt fa-2x fa-fw text-primary"></i></p>
							<p>Szakképzett értékesítők</p>
						</div>
						<div class="col-md-2 text-center">
							<p><i class="fal fa-parking-circle fa-fw fa-2x text-primary"></i></p>
							<p>Ingyen parkoló</p>
						</div>
						<div class="col-md-2 text-center">
							<p><i class="fal fa-wifi fa-fw fa-2x text-primary"></i></p>
							<p>Nyílt WiFi elérés</p>
						</div>
						<div class="col-md-2 text-center">
							<a href="https://goo.gl/maps/1PCTU7jrATp" target="_blank"><p><i class="fal fa-route fa-fw fa-2x"></i></p>
							<p class="text-dark">Útvonaltervezés</p></a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<h2 class="h4-responsive title text-uppercase">Bemutatóterem nyitvatartás</h2>
							<table class="table-striped w-100 mb-4">
								<tbody>
									<tr class="{{$monday}}">
										<th class="px-2" scope="row">
											Hétfő
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$tuesday}}">
										<th class="px-2" scope="row">
											Kedd
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$wednesday}}">
										<th class="px-2" scope="row">
											Szerda
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$thursday}}">
										<th class="px-2" scope="row">
											Csütörtök
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$friday}}">
										<th class="px-2" scope="row">
											Péntek
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$saturday}}">
										<th class="px-2" scope="row">
											Szombat
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$sunday}}">
										<th class="px-2" scope="row">
											Vasárnap
										</th>
										<td class="px-2 text-right">
											11:00 - 18:00
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-4">
							<h2 class="h4-responsive title text-uppercase">Ügyfélszolgálati nyitvatartás</h2>
							<table class="table-striped w-100 mb-4">
								<tbody>
									<tr class="{{$monday}}">
										<th class="px-2" scope="row">
											Hétfő
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$tuesday}}">
										<th class="px-2" scope="row">
											Kedd
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$wednesday}}">
										<th class="px-2" scope="row">
											Szerda
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$thursday}}">
										<th class="px-2" scope="row">
											Csütörtök
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$friday}}">
										<th class="px-2" scope="row">
											Péntek
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$saturday}}">
										<th class="px-2" scope="row">
											Szombat
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$sunday}}">
										<th class="px-2" scope="row">
											Vasárnap
										</th>
										<td class="px-2 text-right">
											11:00 - 18:00
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-4" id="content-csepel">
							<h2 class="h4-responsive title text-uppercase">Kapcsolatfelvételi lehetőségek</h2>
							<ul class="list-unstyled">
								<li class="pb-2">
									<a href="https://goo.gl/maps/BXMHaMYwaL62" target="_blank"><i class="fal fa-map-marker fa-fw mr-2"></i><span class="text-dark">1211 Budapest, II. Rákóczi Ferenc út 107-115 (Bejárat a lakópark Karácsony Sándor és Teller Ede úti sarkánál)</span></a>
								</li>
								<li class="pb-2">
									<a href="tel:+3613539410"><i class="fal fa-phone-office fa-fw mr-2"></i><span class="text-dark">+36-1-353-9410</span></a>
								</li>
								<li class="pb-2">
									<a href="mailto:ertekesites@vamosimilano.hu"><i class="fal fa-at fa-fw mr-2"></i><span class="text-dark">ertekesites@vamosimilano.hu</span></a>
								</li>
								<li class="pb-2">
									<a href="https://m.me/vamosimilano" target="_blank"><i class="fab fa-facebook-messenger fa-fw mr-2"></i><span class="text-dark">m.me/vamosimilano</span></a>
								</li>
								<li class="pb-2">
									<a href="https://facebook.com/vamosimilano"><i class="fab fa-facebook fa-fw mr-2"></i><span class="text-dark">facebook.com/vamosimilano</span></a>
								</li>
								<li>
									<a href="https://instagram.com/vamosimilano/"><i class="fab fa-instagram fa-fw mr-2"></i><span class="text-dark">instagram.com/vamosimilano/</span></a>
								</li>
							</ul>
						</div>
					</div>
				<?php
				} ?>
				</div>
				<div class="tab-pane fade show" id="szeged-md" role="tabpanel" aria-labelledby="szeged-md">
					<?php
					if( ismobile() ) {
					?>
					<h3 class="h4-responsive title text-uppercase">Üzlet nyitvatartás</h3>
					<ul class="list-unstyled mt-3">
						<li>
							<p class="{{$monday}}">Hétfő: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$tuesday}}">Kedd: <span class="float-right">Zárva</span></p>
						</li>
						<li>
							<p class="{{$wednesday}}">Szerda: <span class="float-right">Zárva</span></p>
						</li>
						<li>
							<p class="{{$thursday}}">Csütörtök: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$friday}}">Péntek: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$saturday}}">Szombat: <span class="float-right">10:00 - 18:30</span></p>
						</li>
						<li>
							<p class="{{$sunday}}">Vasárnap: <span class="float-right">11:00 - 18:00</span></p>
						</li>
					</ul>
					<h3 class="h4-responsive title text-uppercase">Ügyfélszolgálati órák</h3>
					<ul class="list-unstyled mt-3">
						<li>
							<p class="{{$monday}}">Hétfő: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$tuesday}}">Kedd: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$wednesday}}">Szerda: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$thursday}}">Csütörtök: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$friday}}">Péntek: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$saturday}}">Szombat: <span class="float-right">09:00 - 17:00</span></p>
						</li>
						<li>
							<p class="{{$sunday}}">Vasárnap: <span class="float-right">11:00 - 18:00</span></p>
						</li>
					</ul>
					<h3 class="h4-responsive title text-uppercase">Szolgáltatások</h3>
					<ul class="list-unstyled mt-3">
						<li>
							<p><i class="fal fa-trophy-alt fa-fw text-primary mr-3"></i>Szakképzett értékesítők</p>
						</li>
						<li>
							<p><i class="fal fa-parking-circle fa-fw text-primary mr-3"></i>Ingyen parkoló</p>
						</li>
						<li>
							<p><i class="fal fa-wifi fa-fw text-primary mr-3"></i>Ingyen WiFi elérés</p>
						</li>
						<li>
							<p><a href="https://www.google.com/maps/dir//Fon%C3%B3gy%C3%A1ri+%C3%BAt+8,+Szeged,+6728/@46.2643342,20.1097395,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x474487958c24b4d3:0xb707edfd26a8a00!2m2!1d20.109877!2d46.2642782" target="_blank"><i class="fal fa-route fa-fw mr-3"></i><span class="text-dark">Útvonaltervezés</span></a></p>
						</li>
					</ul>
					<h2 class="h4-responsive title text-uppercase">Kapcsolatfelvételi lehetőségek</h2>
					<ul class="list-unstyled mt-3" id="content-szeged">
						<li>
							<p><a href="https://www.google.com/maps/place/Szeged,+Fon%C3%B3gy%C3%A1ri+%C3%BAt+8,+6728/@46.2642819,20.1076883,17z/data=!3m1!4b1!4m5!3m4!1s0x474487958c24b4d3:0xb707edfd26a8a00!8m2!3d46.2642782!4d20.109877" target="_blank"><i class="fal fa-map-marker fa-fw mr-2"></i><span class="text-dark">6728 Szeged Fonógyári út 8.</span></a></p>
						</li>
						<li>
							<p><a href="tel:+3613539410"><i class="fal fa-phone-office fa-fw mr-2"></i><span class="text-dark">+36-1-353-9410</span></a></p>
						</li>
						<li>
							<p><a href="mailto:ertekesitessz@vamosimilano.hu"><i class="fal fa-at fa-fw mr-2"></i><span class="text-dark">ertekesitessz@vamosimilano.hu</span></a></p>
						</li>
						<li>
							<p><a href="https://m.me/vamosimilano" target="_blank"><i class="fab fa-facebook-messenger fa-fw mr-2"></i><span class="text-dark">m.me/vamosimilano</span></a></p>
						</li>
						<li>
							<p><a href="https://facebook.com/vamosimilano"><i class="fab fa-facebook fa-fw mr-2"></i><span class="text-dark">facebook.com/vamosimilano</span></a></p>
						</li>
						<li>
							<p><a href="https://instagram.com/vamosimilano/"><i class="fab fa-instagram fa-fw mr-2"></i><span class="text-dark">instagram.com/vamosimilano/</span></a></p>
						</li>
					</ul>
					<?php
					} //mobil
					else { //desktop
					?>
					<div class="row mb-md-3">
						<div class="col-md-2 text-center">
							<p><i class="fal fa-trophy-alt fa-2x fa-fw text-primary"></i></p>
							<p>Szakképzett értékesítők</p>
						</div>
						<div class="col-md-2 text-center">
							<p><i class="fal fa-parking-circle fa-fw fa-2x text-primary"></i></p>
							<p>Ingyen parkoló</p>
						</div>
						<div class="col-md-2 text-center">
							<p><i class="fal fa-wifi fa-fw fa-2x text-primary"></i></p>
							<p>Nyílt WiFi elérés</p>
						</div>
						<div class="col-md-2 text-center">
							<a href="https://www.google.com/maps/dir//Fon%C3%B3gy%C3%A1ri+%C3%BAt+8,+Szeged,+6728/@46.2643342,20.1097395,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x474487958c24b4d3:0xb707edfd26a8a00!2m2!1d20.109877!2d46.2642782" target="_blank"><p><i class="fal fa-route fa-fw fa-2x"></i></p>
							<p class="text-dark">Útvonaltervezés</p></a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<h2 class="h4-responsive title text-uppercase">Bemutatóterem nyitvatartás</h2>
							<table class="table-striped w-100 mb-4">
								<tbody>
									<tr class="{{$monday}}">
										<th class="px-2" scope="row">
											Hétfő
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$tuesday}}">
										<th class="px-2" scope="row">
											Kedd
										</th>
										<td class="px-2 text-right">
											Zárva
										</td>
									</tr>
									<tr class="{{$wednesday}}">
										<th class="px-2" scope="row">
											Szerda
										</th>
										<td class="px-2 text-right">
											Zárva
										</td>
									</tr>
									<tr class="{{$thursday}}">
										<th class="px-2" scope="row">
											Csütörtök
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$friday}}">
										<th class="px-2" scope="row">
											Péntek
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$saturday}}">
										<th class="px-2" scope="row">
											Szombat
										</th>
										<td class="px-2 text-right">
											10:00 - 18:30
										</td>
									</tr>
									<tr class="{{$sunday}}">
										<th class="px-2" scope="row">
											Vasárnap
										</th>
										<td class="px-2 text-right">
											11:00 - 18:00
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-4">
							<h2 class="h4-responsive title text-uppercase">Ügyfélszolgálati nyitvatartás</h2>
							<table class="table-striped w-100 mb-4">
								<tbody>
									<tr class="{{$monday}}">
										<th class="px-2" scope="row">
											Hétfő
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$tuesday}}">
										<th class="px-2" scope="row">
											Kedd
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$wednesday}}">
										<th class="px-2" scope="row">
											Szerda
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$thursday}}">
										<th class="px-2" scope="row">
											Csütörtök
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$friday}}">
										<th class="px-2" scope="row">
											Péntek
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$saturday}}">
										<th class="px-2" scope="row">
											Szombat
										</th>
										<td class="px-2 text-right">
											09:00 - 17:00
										</td>
									</tr>
									<tr class="{{$sunday}}">
										<th class="px-2" scope="row">
											Vasárnap
										</th>
										<td class="px-2 text-right">
											11:00 - 18:00
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-4" id="content-szeged">
							<h2 class="h4-responsive title text-uppercase">Kapcsolatfelvételi lehetőségek</h2>
							<ul class="list-unstyled">
								<li class="pb-2">
									<a href="https://www.google.com/maps/place/Szeged,+Fon%C3%B3gy%C3%A1ri+%C3%BAt+8,+6728/@46.2642819,20.1076883,17z/data=!3m1!4b1!4m5!3m4!1s0x474487958c24b4d3:0xb707edfd26a8a00!8m2!3d46.2642782!4d20.109877" target="_blank"><i class="fal fa-map-marker fa-fw mr-2"></i><span class="text-dark">6728 Szeged Fonógyári út 8.</span></a>
								</li>
								<li class="pb-2">
									<a href="tel:+3613539410"><i class="fal fa-phone-office fa-fw mr-2"></i><span class="text-dark">+36-1-353-9410</span></a>
								</li>
								<li class="pb-2">
									<a href="mailto:ertekesitessz@vamosimilano.hu"><i class="fal fa-at fa-fw mr-2"></i><span class="text-dark">ertekesitessz@vamosimilano.hu</span></a>
								</li>
								<li class="pb-2">
									<a href="https://m.me/vamosimilano" target="_blank"><i class="fab fa-facebook-messenger fa-fw mr-2"></i><span class="text-dark">m.me/vamosimilano</span></a>
								</li>
								<li class="pb-2">
									<a href="https://facebook.com/vamosimilano"><i class="fab fa-facebook fa-fw mr-2"></i><span class="text-dark">facebook.com/vamosimilano</span></a>
								</li>
								<li>
									<a href="https://instagram.com/vamosimilano/"><i class="fab fa-instagram fa-fw mr-2"></i><span class="text-dark">instagram.com/vamosimilano/</span></a>
								</li>
							</ul>
						</div>
					</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card">
				<div class="card-body pb-0">
					<form method="post" id="contact-form" action="{{ action('ContactController@contactSend') }}">
					<h4 class="h4-responsive title text-uppercase card-title">Írjon nekünk üzenetet</h4>
						<select class="mdb-select md-form mt-0 mb-4 colorful-select dropdown-dark" name="subject" id="subject" required>
							<option value="">Az üzenet tárgya: *</option>
							<option value="1" {{(Input::old('subject') == 1 ? 'selected' : '' )}}>{{ t('subject_1') }}</option>
							<option value="2" {{(Input::old('subject') == 2 ? 'selected' : '' )}}>{{ t('subject_2') }}</option>
							<option value="3" {{(Input::old('subject') == 3 ? 'selected' : '' )}}>{{ t('subject_3') }}</option>
							<option value="4" {{(Input::old('subject') == 4 ? 'selected' : '' )}}>{{ t('subject_4') }}</option>
							<option value="5" {{(Input::old('subject') == 5 ? 'selected' : '' )}}>{{ t('subject_5') }}</option>
							<option value="6" {{(Input::old('subject') == 6 ? 'selected' : '' )}}>{{ t('subject_6') }}</option>
							<option value="7" {{(Input::old('subject') == 7 ? 'selected' : '' )}}>{{ t('subject_7') }}</option>
							<option value="8" {{(Input::old('subject') == 8 ? 'selected' : '' )}}>{{ t('subject_8') }}</option>
						</select>
						<select class="mdb-select md-form mt-0 mb-4 colorful-select dropdown-dark" name="toaddress" id="toaddress" required>
							<option value="">Címzett: *</option>
							<option value="1" {{(Input::old('toaddress') == 1 ? 'selected' : '' )}}>{{ t('Budapest') }}</option>
							<?php if(date("Ymd") > "20190425") { ?>
							<option value="1" {{(Input::old('toaddress') == 2 ? 'selected' : '' )}}>{{ t('Szeged') }}</option>
							<?php } ?>
						</select>
						<div class="md-form active-primary">
							<input type="text" value="{{ Input::old('name') }}" id="name" name="name" class="form-control" required>
							<label for="name">{{ t('Az Ön neve') }}: *</label>
						</div>
						<div class="md-form active-primary">
							<input type="text" value="{{ Input::old('email') }}" id="email" name="email" class="form-control" required>
							<label for="name">{{ t('E-mail címe') }}: *</label>
						</div>
						<div class="md-form active-primary">
							<input type="text" value="{{ Input::old('telephone') }}" id="telephone" name="telephone" class="form-control">
							<label for="name">{{ t('Telefonszám') }}:</label>
						</div>
						<div class="md-form active-primary">
							<textarea id="comment" name="comment" class="md-textarea form-control" rows="2">{{ Input::old('comment') }}</textarea>
							<label for="comment">{{ t('Üzenete') }}:</label>
						</div>
						<div class="md-form">
							<button class="btn btn-block btn-sm btn-primary" title="{{ t('Elküldés') }}" type="submit">{{ t('Elküldés') }}</button>
						</div>
					</form>
				</div>
			</div>
			<h2 class="h4-responsive title text-uppercase mt-3">Kapcsolat</h2>
			<p><strong>WORE HUNGARY Kft.</strong></p>
			<p>Franchise jogok tulajdonosa:</p>
			<p>Vamosi SRL,<br>VIA LORENZO MASCHERONI, 31, 20145, MILANO</p>
			<p><a href="http://vamosi.it" target="_blank">http://vamosi.it</a></p>
		</div>
	</div>
</div>
@stop
@section('footer_js')
<script src="//maps.googleapis.com/maps/api/js?key={{getConfig('maps_api_key')}}&callback=initMap" async defer></script>
<script>
$(document).ready(function(){
	$('#contact-form').append('<input type="hidden" name="key" value="AHH">');

})
var map;
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
	  center: {lat: 47.4646476, lng: 19.0417743},
	  zoom: 7
	});
	initMarkerBp();
	initMarkerCsepel();
	initMarkerSzeged();
	
}
function initMarkerBp(){
	var pos = new google.maps.LatLng(47.4646476,19.0417743);
	var infowindow = new google.maps.InfoWindow({
		content: $("#content-bp").html()
	});
	var marker_bp = new google.maps.Marker({
		position: pos,
		map: map,
		title: 'Budapesti Üzlet'
	});
	marker_bp.addListener('click', function() {
		infowindow.open(map, marker_bp);
	});
}
function initMarkerCsepel(){
	var pos = new google.maps.LatLng(47.427789, 19.066879);
	var infowindow = new google.maps.InfoWindow({
		content: $("#content-csepel").html()
	});
	var marker_csepel = new google.maps.Marker({
		position: pos,
		map: map,
		title: 'Csepeli Outlet Áruház'
	});
	marker_csepel.addListener('click', function() {
		infowindow.open(map, marker_csepel);
	});
}
function initMarkerSzeged(){
	var pos = new google.maps.LatLng(46.2642782, 20.1076883);
	var infowindow = new google.maps.InfoWindow({
		content: $("#content-szeged").html()
	});
	var marker_szeged = new google.maps.Marker({
		position: pos,
		map: map,
		title: 'Szegedi üzletünk'
	});
	marker_szeged.addListener('click', function() {
		infowindow.open(map, marker_szeged);
	});
}
</script>
@append
<?php
}
else { //régi design
?>
@include('cms.contact.'.App::getLocale())
<?php
}
?>