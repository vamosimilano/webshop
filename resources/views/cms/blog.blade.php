@extends('layouts.default')
@section('content')
<?php $headversion = "newhead"; 
if( isset($headversion) && $headversion === "newhead" ) {
?>

<div class="container">
	<header class="row mt-5">
		<div class="col-md-12">
			<hgroup>
				<h1 class="h2-responsive text-center text-uppercase"><a href="{{url('/blog')}}" class="text-dark">{{t('blogtitle')}}</a></h1>
			</hgroup>
		</div>
	</header>
	@include('cms.blog_categories_nav')
	<div class="row">
		<section class="col-md-12 my-5">
			<div class="row d-flex">
		<?php
		if ( sizeof($blogs) ) {
			foreach ( $blogs as $blog ) {
		?>
				<article class="col-lg-4 mb-4 align-self-stretch">
					<div class="card card-product h-100">
						<div class="view overlay">
						<?php
						$headers = @get_headers($blog->getImageUrl('1920'));
						//print_r($headers);
						if( $headers[0] == 'HTTP/1.1 200 OK' ) {
							$img_url = $blog->getImageUrl('1920');
						}
						else {
							$img_url = "https://static.vamosimilano.hu/carousel/full/147-morbi-eu-sem-at-est-egestas-venenatis-a-convallis-nulla-5600.jpg";
						}
						?>
							<img src="{{$img_url}}" alt="{{$blog->title}}" class="card-img-top img-fluid">
							<a href="{{$blog->getUrl()}}">
								<div class="mask rgba-white-slight"></div>
							</a>
						</div>
						<div class="card-body text-center">
							<p class="text-uppercase small">{{ t('cms_blogcategory_'.$blog->category) }}</p>
							<h2 class="h4-responsive text-uppercase"><a href="{{$blog->getUrl()}}" class="text-dark">{{$blog->title}}</a></h2>
							<p class="text-left">{{strip_tags($blog->short_description)}}</p>
						</div>
						<div class="card-footer mx-3 px-0 text-left">
							<a href="{{$blog->getUrl()}}" class="text-uppercase">Elolvasom<i class="fal fa-chevron-right ml-2"></i></a>
							<span class="float-right"><i class="fal fa-calendar-alt fa-fw mr-2"></i>{{t((new DateTime($blog->published_at))->format('Y'))}}. <span class="text-lowercase">{{t((new DateTime($blog->published_at))->format('F'))}}</span> {{(new DateTime($blog->published_at))->format('d')}}.</span>
						</div>
					</div>
				</article>
			<?php
			}//foreach
		}//if
		else {
			echo "Nincs megjeleníthető blogbejegyzés.";
		}
		?>
				<div class="col-12 text-center">
					<nav class="mdb-pagination d-flex">
						{{ $blogs->appends(Input::all())->render() }}
					</nav>
				</div>
			</div>
		</section>
		<?php /* include('cms.sidebar', ['headversion' => 'newhead']) */ ?>
	</div>
</div>
@section ("footer_js")
<script>
$(".mdb-pagination ul.pagination").addClass("pg-dark mx-auto");
$(".mdb-pagination ul.pagination li").addClass("page-item");
$(".mdb-pagination ul.pagination li a").addClass("page-link");
var disabled = $(".mdb-pagination ul.pagination li.disabled span").html();
var active = $(".mdb-pagination ul.pagination li.active span").html();
$(".mdb-pagination ul.pagination li.disabled").html('<a class="page-link disabled" href="javascript:void(0)">'+disabled+'</a>');
$(".mdb-pagination ul.pagination li.active").html('<a class="page-link active waves-effect" href="javascript:void(0)">'+active+'</a>');
</script>
@append
<?php	
}

else {
?>

<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Blog').($search ? " -  ".t('Keresés')." - ".$search : '') }}</strong>
                        </li>
            </ul>
</div>

<main id="maincontent" class="page-main">
<div class="page-title-wrapper">
    <h1 class="page-title">
        <span class="base" data-ui-id="page-title-wrapper">{{ t('Blog').($search ? " -  ".t('Keresés')." - ".$search : '') }}</span>    </h1>
    </div>
    <div class="columns">
        <div class="column main">
            <div class="post-list-wrapper">
                @if (sizeof($blogs))
                    <ol class="post-list">
                    @foreach ($blogs as $blog)
                        <li class="post-holder post-holder-3">
                            <div class="post-banner">
                                <div id="post_image_gallery_banner">
                                    <a href="{{$blog->getUrl()}}" title="{{$blog->title}}"><img src="{{$blog->getImageUrl(1280)}}" alt="{{$blog->title}}"></a>
                                </div>
                            </div>
                            <div class="post-date">
                                <span class="day">{{(new DateTime($blog->published_at))->format('d')}}</span>
                                <span class="month">{{t('shortmonth_'.(new DateTime($blog->published_at))->format('m'))}}</span>
                            </div>
                            <div class="post-header">

                                <div class="post-title-holder">


                                    <h2 class="post-title">
                                        <a class="post-item-link" href="{{$blog->getUrl()}}" title="{{$blog->title}}">
                                            {{$blog->title}}                </a>

                                            <? /*<div class="addthis_inline_share_toolbox addthis_toolbox addthis_default_style" style="width: 100px;" addthis:url="{{$blog->getUrl()}}"><div class="atclear"></div></div>*/?>

                                    </h2>
                                </div>
                            </div>

                            <div class="post-content">
                                <div class="post-description clearfix">
                                    <div class="post-text-hld clearfix">
                                        <p>{{strip_tags($blog->short_description)}} </p>
                                    </div>
                                    <a class="post-read-more" href="{{$blog->getUrl()}}" title="{{$blog->title}}">
                                       {{ t('Tovább olvasás') }} »            </a>
                                </div>
                            </div>
                            <div class="post-footer">
                                <div class="post-info clear">
                                    <div class="item post-posed-date">
                                        <span class="label"><em class="porto-icon-calendar"></em></span>
                                        <span class="value">{{(new DateTime($blog->published_at))->format('Y-m-d')}}</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    </ol>
                    <div class="paginator-container">
                    {{ $blogs->appends(Input::all())->render() }}
                    </div>
                @elseif ($search)
                    <br><br>
                    <p>{{ t('Nincs eredménye a keresésnek, próbáljon más kulcsszót!') }}</p>

                @else
                    <br><br>
                    <p>{{ t('Még nem töltöttünk fel blog cikket, jöjjön vissza később!') }}</p>
                @endif

            </div>

        </div>
        @include('cms.sidebar')

    </div>

</main>

@section('footer_js')


@append
<?php } ?>
@stop