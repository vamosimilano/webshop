<?php $headversion = "newhead"; 
if( isset($headversion) && $headversion === "newhead" ) {
?>
@extends('layouts.default')
@section('content')
<?php
$uri_parts = explode('/', $_SERVER['REQUEST_URI'], 2);
$uri = 'https://' . $_SERVER['HTTP_HOST'].$uri_parts[0];
//$uri = $_SERVER['REQUEST_URI'];
//@include('cms.karrier_categories_nav')
?>
<div class="container">
	<div class="row">
		<section class="col-md-12">
			<header class="px-0 px-lg-5 mb-4">
				<hgroup class="view">
					<img src="{{$blog->getImageUrl(1920)}}" alt="{{$blog->title}}" class="img-fluid">
					<div class="mask rgba-black-light d-flex align-items-center">
						<h1 class="h2-responsive text-uppercase text-white mx-auto">{{$blog->title}}</h1>
					</div>
				</hgroup>
			</header>
			<div class="mt-4 px-2 px-lg-5">
				<hr>
				<p>
					<i class="fal fa-calendar-alt fa-fw mr-2"></i>{{t((new DateTime($blog->published_at))->format('Y'))}}. <span class="text-lowercase">{{t((new DateTime($blog->published_at))->format('F'))}}</span> {{(new DateTime($blog->published_at))->format('d')}}.</span>
					<span class="float-right text-uppercase"><i class="fal fa-folder mr-2"></i>{{ t('cms_blogcategory_'.$blog->category) }}</span>
				</p>
				<hr>
				<p class="lead">{{strip_tags($blog->short_description)}}</p>
				{{$blog->description}}
			</div>
			<div class="mt-4 px-2 px-lg-5">
				<h3 class="h4-responsive title text-uppercase">Cégünkről</h3>
				<p>Cégünk fő profilja a kárpitozott kanapék, franciaágyak olyan magyarországi gyártással, ahol ismerik és értik a bútorokat. Exkluzív, elegáns termékeink igazodnak a változó igényekhez és változó prioritásokhoz, figyelembe véve, hogy minden családnak más a tökéletes bútor. Ezért innovatív módon egyedi tervezési igényeket is kielégítünk, mind méretet, mind szövetválasztékot tekintve. Ehhez megfizethető ár és házhozszállítás is tartozik.</p>
			</div>
			<div class="mt-4 px-2 px-lg-5">
				<h3 class="h4-responsive title text-uppercase">Jelentkezés módja</h3>
				<p>A meghírdetett <strong>{{$blog->title}}</strong> álláslehetőségre jelentkezni a <a href="mailto:karrier@vamosimilano.hu" target="_blank">karrier@vamosimilano.hu</a> email címen lehet, <em>fényképes önéletrajzzal és a pozíció megnevezésével</em>.</p>
			</div>
			<footer class="my-4 px-lg-5">
				<hr>
				<div class="row pt-3">
					<div class="col-md-12">
						<p class="text-right">
							<a href="https://www.facebook.com/sharer/sharer.php?u={{$uri}}" class="btn btn-fb" target="_blank"><i class="fab fa-facebook mr-2"></i>Facebook</a>
							<a href="https://www.linkedin.com/shareArticle?mini=true&url={{$uri}}&title={{$blog->title}}" class="btn btn-li" target="_blank"><i class="fab fa-linkedin mr-2"></i>LinkedIn</a>
							<a href="https://twitter.com/home?status={{$uri}}" class="btn btn-tw" target="_blank"><i class="fab fa-twitter mr-2"></i>Twitter</a>
						</p>
					</div>
				</div>
			</footer>
		</section>
	</div>
</div>
<input type="hidden" name="current_uri" value="{{$uri}}" id="catch_the_uri">
<?php }
else {  ?>

<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                        <li class="item cms_page">
                            <a href="{{ action('BlogController@karrier') }}">{{ t('Karrier') }}</a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ $blog->title }}</strong>
                        </li>
            </ul>
</div>

<main id="maincontent" class="page-main">

    <div class="columns">
        <div class="column main">
            <div class="post-view">
    <div class="post-holder post-holder-2">
        <div class="post-banner">
            <div id="post_image_gallery_banner">


                                    <img src="{{$blog->getImageUrl(1280)}}" alt="{{$blog->title}}">

            </div>
        </div>
        <div class="post-date">
            <span class="day">{{(new DateTime($blog->published_at))->format('d')}}</span>
                                <span class="month">{{t('shortmonth_'.(new DateTime($blog->published_at))->format('m'))}}</span>
        </div>
        <div class="post-header">

            <h2 class="post-title theme-color">
                {{$blog->title}}            </h2>


<div class="post-info clear">
    <div class="item post-posed-date">
        <span class="label"><em class="porto-icon-calendar"></em></span>
        <span class="value">{{(new DateTime($blog->published_at))->format('Y-m-d')}}</span>
    </div>

    </div>        </div>
        <div class="post-content">
            <div class="post-description clearfix">
                <div class="post-text-hld">
                    {{$blog->description}}
                <div class="clear"></div>
                <div class="fb-like" data-href="{{ $blog->getUrl() }}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>

                </div>
            </div>
        </div>

    </div>
</div>





        </div>
        @include('cms.sidebar_karrier')

    </div>

</main>

<?php } ?>
@stop
@section('footer_js')
@append