<?php $headversion = 'newhead'; ?>
@extends('layouts.default')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="h1-responsive text-uppercase title-center my-4">Bérlet tájékoztató</h1>
		</div>
		<div class="col-md-6">
			<p class="text-justify">
				Ha nem szeretne vásárolni, nem akar egy összegben kifizetni egy bútort? Előre láthatóan 1-2 éven belül költözik, vagy panziója, vendégháza van, és nem szeretne beruházni? Szeretne mindig a legújabb trendek szerinti, vadonatúj kanapét otthonába, vagy egyszerűen csak vágyik a változatosságra? Fontos önnek, hogy választott bútora folyamatos garanciában legyen, vagy esetleges javítás esetén cserebútorra tarthasson igényt?
			</p>
		</div>
		<div class="col-md-6">
			<p class="text-justify">
				Érezni szeretné azt a kényelmet, mikor a Vamosi Milano akár minden évben vadonatúj kanapét szállít otthonába, és lecseréli a régit? Ha a válasz igen, akkor Ön jó helyen jár. A bútorbérleti programunkat Önnek találtak ki. 
			</p>
		</div>
		<div class="table-responsive-md mx-auto" style="min-width:100%!important;">
			<table id="dtBasicExample" class="table table-striped w-80" cellspacing="0">
				<thead class="black white-text">
					<tr>
						<th scope="col" width="25%">Bútor</td>
						<th scope="col" class="text-center">Méret</th>
						<!--<th scope="col" class="text-right">Alapár</th>-->
						<th scope="col" class="text-right">Kaució</th>
						<th scope="col" class="text-right">Bérleti díj</th>
					</tr>
				</thead>
				<tbody>
				<? foreach($products as $p) { 
					$ar = $p->list_price;
				?>
					<tr>
						<th scope="row">{{ $p->name }}</th>
						<td scope="col" class="text-center">{{ $p->size_name }}</td>
						<!--<td scope="col" class="text-right">{{ money($ar) }}</td>-->
						<td scope="col" class="text-right">{{ money(round($ar*0.33,0)) }}</td>
						<td scope="col" class="text-right">{{ money(round($ar*0.035,0)) }}</td>
					</tr>
				<?	} ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- MDBootstrap Datatables  -->
<link href="/css2/addons/datatables.min.css" rel="stylesheet">
<style>
table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_desc_disabled:before,
table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_desc_disabled:after {
	font-family: "Font Awesome 5 Pro" !important;
	}
</style>
@stop
@section('footer_js')
	<!-- MDBootstrap Datatables  -->
	<script type="text/javascript" src="/js2/addons/datatables.min.js"></script>
	<script>
	$(document).ready(function () {
		$('#dtBasicExample').DataTable({
			"language": {
				"sSearch": "Keresés:",
			    "sLengthMenu": "Mutasson _MENU_ sort",
				"sInfo": "_START_-től _END_-ig. Összesen _TOTAL_ elem.",
				"sInfoEmpty": "0 találat",
				"sInfoFiltered": "(_MAX_ elemből)",
				"sInfoPostFix": "",
				"sLoadingRecords": "Pillanat, töltődnek az adatok...",
				"sZeroRecords": "Nincs egyező találat",
				"sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
				"oPaginate": {
					"sPrevious": "Előző",
					"sNext": "Következő",
					"sFirst": "Első",
					"sLast": "Utolsó"
				}
			}
		});
		$('.dataTables_length').addClass('bs-select');
	});
	</script>
@append