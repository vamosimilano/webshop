<?php $headversion = 'newhead'; ?>
@extends('layouts.default')
@section('content')
<?php
$cover_title	=	"Ajándék ágy akció május 15.-től!<br>Megajándékozzuk egy franciaággyal!";
$cover_text		=	"Rendeljen Dr. Balm prémium matracaink közül az Ön igénye szerint és ingyen adjuk választott Franciaágyát!";
$cover_cta		=	"Az akció részletekért, kérjük kattintanak ide";
if ( ismobile() ) {
?>
<div class="container-fluid px-0">
	<div class="view mt-0 white-text">
		<img src="{{ asset('/media/landing/agymatrac/drbalm-m-cover.jpg?v1') }}" class="img-fluid animated fadeInLeft" alt="Matracához ingyen franciaágyat ajándékozunk Önnek">
		<div class="mask flex-center rgba-black-light animated fadeInRight">
			<div>
				<h3 class="h3-responsive text-uppercase text-center font-weight-bold mb-4">{{$cover_title}}</h3>
				<p class="text-center">{{$cover_text}}</p>
				<p class="text-center smooth-scroll mt-3">
					<a href="#franciaagy" class="btn btn-primary btn-sm">Franciaágy választék</a>
				</p>
				<p class="text-center smooth-scroll mt-3">
					<a href="#matracok" class="btn btn-primary btn-sm">Dr. Balm Matrac választék</a>
				</p>
			</div>
		</div>
	</div>
</div>
<?php }//if
else {
?>
<div class="container-fluid px-0">
	<div class="view">
		<img src="{{ asset('/media/landing/agymatrac/drbalm_cover.jpg?v1') }}" class="img-fluid animated fadeInLeft" alt="Matracához ingyen franciaágyat ajándékozunk Önnek" style="width:100%;">
		<div class="mask flex-center rgba-black-light animated fadeInDown">
			<div class="row">
				<div class="col-10 mx-auto p-3 white-text">
					<h1 class="h2-responsive text-uppercase text-center font-weight-bold">{{$cover_title}}</h1>
					<p class="lead text-center">{{$cover_text}}</p>
				</div>
				<div class="col-md-3 ml-auto mt-2">
					<p class="text-center smooth-scroll">
						<a href="#franciaagy" class="btn btn-primary">Franciaágy választék</a>
					</p>
				</div>
				<div class="col-md-3 mr-auto mt-2">
					<p class="text-center smooth-scroll">
						<a href="#matracok" class="btn btn-primary">Dr. Balm Matrac választék</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}//else
?>
<div class="container mt-3" id="franciaagy">
	<div class="row">
		<div class="col-12">
			<h1 class="h2-responsive title-center text-uppercase">Válasszon az alábbi Franciaágyaink közül személyre szabottan!</h1>
			<p class="text-center">Vásároljon egyet matracaink közül és válasszon mellé ingyen egy franciaágyat az alábbi kínálatunkból.</p>
		</div>
		<div class="col-md-4 my-4 text-center wow tada">
			<img src="{{asset('/media/landing/nyitasiakcio/szovetek-1x1.jpg')}}" alt="Prémium szövetek a Vamosi Milanonál" class="img-fluid z-depth-1 rounded-circle w-50 my-3 mx-auto">
			<h2 class="h4-responsive text-uppercase mb-3">Válasszon Franciaágyához prémium minőségű Szövetet!</h2>
			<p class="text-center">Csúcstechnológiás kopás, folt és tűzálló szöveteinket keresse több száz színben.</p>
		</div>
		<div class="col-md-4 my-4 text-center wow tada">
			<img src="{{asset('/media/landing/nyitasiakcio/garancia-1x1.jpg')}}" alt="1 év plusz garancia" class="img-fluid z-depth-1 rounded-circle w-50 my-3 mx-auto">
			<h2 class="h4-responsive text-uppercase mb-3">Igényelje 10 év Garanciával Franciaágyát!</h2>
			<p class="text-center">Önköltségi áron akár 5 alkalommal újrakárpitozzuk bútorát.</p>
		</div>
		<div class="col-md-4 my-4 text-center wow tada">
			<img src="{{asset('/media/landing/nyitasiakcio/kiszallitas-1x1.jpg')}}" alt="Országos házhozszállítás" class="img-fluid z-depth-1 rounded-circle w-50 my-3 mx-auto">
			<h2 class="h4-responsive text-uppercase mb-3">Összeszerelés és Ingyenes házhozszállítás</h2>
			<p class="text-center">Kérjen ingyenes házhozszállítást és igény szerint válassza mellé összeszerelési szolgáltatásunkat és élvezze az előnyeit! <a href="{{ url('/hazhozszallitas') }}">Feltételekről itt olvashat.</a></p>
		</div>
	</div>
	<div class="row productgrid">
		<?php
		$metacount=$product_i=0;
		$pl1 = Product::where('products.product_id', 1222)->active()->lang()->first();
		$pl2 = Product::where('products.product_id', 1228)->active()->lang()->first();
		$pl3 = Product::where('products.product_id', 2412)->active()->lang()->first();
		$pl4 = Product::where('products.product_id', 1231)->active()->lang()->first();
		$pl5 = Product::where('products.product_id', 1234)->active()->lang()->first();
		$pl6 = Product::where('products.product_id', 1581)->active()->lang()->first();
		$pl7 = Product::where('products.product_id', 1240)->active()->lang()->first();
		?>
		@include('webshop.product_card', ['product'=>$pl1, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('webshop.product_card', ['product'=>$pl2, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('webshop.product_card', ['product'=>$pl3, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('webshop.product_card', ['product'=>$pl4, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('webshop.product_card', ['product'=>$pl5, 'metacount'=>$metacount, 'product_i'=>$product_i, 'col'=>3])
		@include('webshop.product_card', ['product'=>$pl6, 'metacount'=>$metacount, 'product_i'=>$product_i, 'col'=>3])
		@include('webshop.product_card', ['product'=>$pl7, 'metacount'=>$metacount, 'product_i'=>$product_i, 'col'=>3])
	</div>
</div>
<?php
$textalign = "";
$hsize = " h2-responsive";
if ( ismobile() ) {
	$textalign = " text-center";
	$hsize=" h4-responsive";
}
?>
<div class="container" id="matracok">
	<div class="row">
		<div class="view">
			<img src="{{ asset('/media/landing/agymatrac/drbalm_sleep.jpg') }}" alt="Dr.Balm a pihentető alvás szakértője" class="img-fluid wow fadeInDown">
			<div class="mask flex-center rgba-black-light">
				<h2 class="text-white text-uppercase px-3{{$hsize}}{{$textalign}}">DR. BALM MATRACOK A KÉNYELMES ALVÁSÉRT!</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12{{$textalign}}">
			<h2 class="h2-responsive title-center text-uppercase my-4">Válasszon az alábbi Dr. Balm matracaink közül személyre szabottan!</h2>
			<p class="lead">A Dr. Balm Sleeping System matracokat az egész világon a gerincbántalmak enyhítésének és megelőzésének szakértőjeként, és az éjjeli alvás legpihentetőbb (NREM) szakaszának meghosszabbításában legjobb eredményeket elérő márkaként ismerik.</p>
		</div>
		<div class="col-md-4 lead teal-text text-center text-sm-right">
			<p>Speciális matracmag és zónakiosztás</p>
			<p>Antibakteriális</p>
			<p>Antiallergén</p>
			<p>Memory hab</p>
			<p>Kókuszréteg</p>
		</div>
		<div class="col-md-4 lead teal-text text-center">
			<img src="{{ asset('/media/landing/agymatrac/garancia-200x200.png') }}" alt="Dr.Balm a pihentető alvás szakértője" class="img-fluid wow tada">
			<p class="lead">10 év Garancia</p>
		</div>
		<div class="col-md-4 lead teal-text text-center text-sm-left">
			<p>Szellőző és légcsatornák</p>
			<p>Extra alátámasztás</p>
			<p>Téli és Nyári oldal</p>
			<p>Német acélrugók</p>
			<p>Személyre szabott gyártás</p>
		</div>
	</div>
	<div class="row productgrid">
		<?php
		$metacount=$product_i=0;
		$pl1 = Product::where('products.product_id', 2410)->active()->lang()->first();
		$pl2 = Product::where('products.product_id', 2411)->active()->lang()->first();
		$pl3 = Product::where('products.product_id', 2493)->active()->lang()->first();
		$pl4 = Product::where('products.product_id', 2406)->active()->lang()->first();
		$pl5 = Product::where('products.product_id', 2407)->active()->lang()->first();
		$pl6 = Product::where('products.product_id', 2408)->active()->lang()->first();
		$pl7 = Product::where('products.product_id', 2409)->active()->lang()->first();
		$pl8 = Product::where('products.product_id', 2404)->active()->lang()->first();
		$pl9 = Product::where('products.product_id', 2405)->active()->lang()->first();
		?>
		@include('webshop.product_card', ['product'=>$pl1, 'metacount'=>$metacount, 'product_i'=>$product_i, 'col'=>3])
		@include('webshop.product_card', ['product'=>$pl2, 'metacount'=>$metacount, 'product_i'=>$product_i, 'col'=>3])
		@include('webshop.product_card', ['product'=>$pl3, 'metacount'=>$metacount, 'product_i'=>$product_i, 'col'=>3])
		@include('webshop.product_card', ['product'=>$pl4, 'metacount'=>$metacount, 'product_i'=>$product_i, 'col'=>3])
		@include('webshop.product_card', ['product'=>$pl5, 'metacount'=>$metacount, 'product_i'=>$product_i, 'col'=>3])
		@include('webshop.product_card', ['product'=>$pl6, 'metacount'=>$metacount, 'product_i'=>$product_i, 'col'=>3])
		@include('webshop.product_card', ['product'=>$pl7, 'metacount'=>$metacount, 'product_i'=>$product_i, 'col'=>3])
		@include('webshop.product_card', ['product'=>$pl8, 'metacount'=>$metacount, 'product_i'=>$product_i, 'col'=>3])
		@include('webshop.product_card', ['product'=>$pl9, 'metacount'=>$metacount, 'product_i'=>$product_i, 'col'=>3])
	</div>
	<div class="row">
		<div class="col-12{{$textalign}} animated tada">
			<p><small class="text-muted">Akcióink nem összevonhatók! Képeink illusztrációk! A tájékoztatás nem teljes körű. Az általunk forgalmazott termékek nem minősülnek gyógyászati segédeszköznek.</small></p>
		</div>
	</div>
</div>
@stop
@section('footer_js')
@append