<?php $headversion = 'newhead'; ?>
@extends('layouts.default')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<h1 class="h1-responsive text-uppercase title-center my-4">Fizetési módok</h1>
		</div>
		<div class="col-md-6 mx-auto">
			<p class="text-center text-md-left">
				Válasszon a Vamosi Milano 5 féle fizetési módozata közül, és keresse meg az Ön számára legideálisabbat. Szeretné az akciós bútorainkat meg olcsóbban? Vagy fél év részletre? Alacsony kezdőbefizetessel? Áruhitelünk segítségével akár önerő nélkül? Vagy nem akar vásárolni, de szívesen bérelne kanapét akár csak 1 évre? Válasszon lenti listánk segítségével, és élvezze a Vamosi Milano kényelmi szolgáltatásait.
			</p>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col mb-4">
			<div class="hoverable grey lighten-4 pb-2 h-100">
				<div class="orange white-text">
					<h4 class="text-center py-4 text-uppercase lead font-weight-bold">Egyösszegű</h4>
				</div>
				<div class="px-4">
					<p class="text-justify">Megrendeléskor, amennyiben Ön egy összegben fizeti meg megrendelése ellenértékét, <strong>plusz 2 % kedvezmény</strong>t kap maximum 50% engedménnyel kínált ágyaink, kanapéink árából.</p>
					<p class="text-justify">Bemutatótermünkben, weboldalunkon és szállítópartnerünknél is fizethet készpénzben, bankkártyával, de választhatja a banki átutalást is.</p>
				</div>
			</div>
		</div>
		<div class="col mb-4">
			<div class="hoverable grey lighten-4 pb-2 h-100">
				<div class="orange darken-1 white-text">
					<h4 class="text-center py-4 text-uppercase lead font-weight-bold">Klasszikus</h4>
				</div>
				<div class="px-4">
					<p class="text-justify">Megrendeléskor <strong>elegendő a bútor alapárának 30%-át kifizetnie</strong>, a maradék összeg kifizetése csak a házhoz szállítással egyidejűleg esedékes.</p>
				</div>
			</div>
		</div>
		<div class="col mb-4">
			<div class="hoverable grey lighten-4 pb-2 h-100">
				<div class="light-green darken-1 white-text">
					<h4 class="text-center py-4 text-uppercase lead font-weight-bold">Áruhitel</h4>
				</div>
				<div class="px-4">
					<p class="text-justify">Igénybe veheti kedvezményes áruhiteleinket, mely segítségével akár önerő nélkül, 20 havi futamidő mellett is vásárolhat tőlünk.</p>
					<p class="text-center">
						<a href="{{ url('/aruhitel') }}" class="btn btn-light-green white-text mt-5">Áruhitel kalkulátor</a>
					</p>
				</div>
			</div>
		</div>
		<div class="col mb-4">
			<div class="hoverable grey lighten-4 pb-2 h-100">
				<div class="orange darken-3 white-text">
					<h4 class="text-center py-4 text-uppercase lead font-weight-bold">Részletfizetési program</h4>
				</div>
				<div class="px-4">
					<p class="text-justify">Vásárolja meg bútorát 6 havi részletre. Nincs kamat, nincs bírálat, nincsenek benyújtandó dokumentumok, csak végtelen kedvezmények.</p>
					<p class="text-center">
						<a href="{{ url('/reszletfizetes-tajekoztato') }}" class="btn btn-orange darken-3 white-text mt-5">Kondíciók</a>
						<a href="{{ url('/aszf') }}" class="btn btn-outline-orange darken-3 mt-5">ÁSZF</a>
					</p>
				</div>
			</div>
		</div>
		<div class="col mb-4">
			<div class="hoverable grey lighten-4 pb-2 h-100">
				<div class="red white-text">
					<h4 class="text-center py-4 text-uppercase lead font-weight-bold">Bérleti program</h4>
				</div>
				<div class="px-4">
					<p class="text-justify">Ha nem szeretne vásárolni, minden évben valami újat, tökéleteset szeretne lakásába, vagy cége van, és nem szeretne beruházni, Önnek tökéletes megoldás a minimum 12 hónapra elérhető bérleti programunk.</p>
					<p class="text-center">
						<a href="{{ url('/berlet-tajekoztato') }}" class="btn btn-red white-text mt-3">Kondíciók</a>
						<a href="{{ url('/aszf') }}" class="btn btn-outline-red mt-3">ÁSZF</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<p class="text-center">Részletfizetési, bérleti programunkban való részvételt, egyösszegű kifizetés után járó kedvezményt és áruhitelt jelenleg csak bemutatótermi rendelés esetén tudunk biztosítani.</p>
			<p class="text-center">Akcióink nem összevonhatók! A képek illusztrációk. Az adatok tájékoztató jellegűek. A hirdetés nem minősül konkrét ajánlattételnek.</p>
		</div>
	</div>
</div>
@stop
@section('footer_js')
@append