<?php $headversion = 'newhead'; ?>
@extends('layouts.default')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="h1-responsive text-uppercase title-center my-4">Részletfizetési tájékoztató</h1>
		</div>
		<div class="col-md-6">
			<p class="text-justify">
				A Vamosi Milanonál bárki választhatja az azonnali kifizetés helyett a 6 havi részletben történő vásárlást is, így könnyebben tervezhetővé válik a családi költségvetés. A részletfizetés esetén megrendeléskor 30% kezdőbefizetést kérünk, hogy az Önök elképzelése szerinti méretezésű és kárpitozású bútor gyártása biztonságosan elkezdődhessen, a maradék összeget pedig elegendő 6 hónap alatt kifizetni részünkre.
			</p>
		</div>
		<div class="col-md-6">
			<p class="text-justify">
				A részletfizetési konstrukció nem vehető igénybe az 50%, vagy magasabb kedvezménnyel kínált bútorok esetén, illetve outlet bútorainkra.
			</p>
		</div>
		<div class="table-responsive-md mx-auto" style="min-width:100%!important;">
			<table id="dtBasicExample" class="table table-striped w-80" cellspacing="0">
				<thead class="black white-text">
					<tr>
						<th scope="col">Bútor név</th>
						<th scope="col" class="text-center">Méret</th>
						<th scope="col" class="text-right">Alapár</th>
						<th scope="col" class="text-right">Előleg</th>
						<th scope="col" class="text-right">1-5. havi részlet</th>
						<?php /*<th scope="col" class="text-right">2. részlet</th>
						<th scope="col" class="text-right">3. részlet</th>
						<th scope="col" class="text-right">4. részlet</th>
						<th scope="col" class="text-right">5. részlet</th> */ ?>
						<th scope="col" class="text-right">6. havi részlet</th>
					</tr>
				</thead>
				<tbody>
			<? foreach($products as $p) { 
				$ar = $p->list_price;
				if ($p->price<$ar) $ar = $p->price;
				//if ($p->menu_top == 'yes') $ar = $p->monthly_price;
			?>
					<tr>
						<th scope="row">{{ $p->name }}</th>
						<td class="text-center">{{ $p->size_name }}</td>
						<td class="text-right">{{ money($ar) }}</td>
						<td class="text-right">{{ money(round($ar*0.3,0)) }}</td>
						<td class="text-right">{{ money(round($ar*0.12,0)) }}</td>
						<?php /*<td class="text-right">{{ money(round($ar*0.12,0)) }}</td>
						<td class="text-right">{{ money(round($ar*0.12,0)) }}</td>
						<td class="text-right">{{ money(round($ar*0.12,0)) }}</td>
						<td class="text-right">{{ money(round($ar*0.12,0)) }}</td>*/ ?>
						<td class="text-right">{{ money(round($ar*0.1,0)) }}</td>
					</tr>
			<?	} ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- MDBootstrap Datatables  -->
<link href="/css2/addons/datatables.min.css" rel="stylesheet">
<style>
table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_desc_disabled:before,
table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_desc_disabled:after {
	font-family: "Font Awesome 5 Pro" !important;
	}
</style>
@stop
@section('footer_js')
	<!-- MDBootstrap Datatables  -->
	<script type="text/javascript" src="/js2/addons/datatables.min.js"></script>
	<script>
	$(document).ready(function () {
		$('#dtBasicExample').DataTable({
			"language": {
				"sSearch": "Keresés:",
			    "sLengthMenu": "Mutasson _MENU_ sort",
				"sInfo": "_START_-től _END_-ig. Összesen _TOTAL_ elem.",
				"sInfoEmpty": "0 találat",
				"sInfoFiltered": "(_MAX_ elemből)",
				"sInfoPostFix": "",
				"sLoadingRecords": "Pillanat, töltődnek az adatok...",
				"sZeroRecords": "Nincs egyező találat",
				"sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
				"oPaginate": {
					"sPrevious": "Előző",
					"sNext": "Következő",
					"sFirst": "Első",
					"sLast": "Utolsó"
				}
			}
		});
		$('.dataTables_length').addClass('bs-select');
	});
	</script>
@append