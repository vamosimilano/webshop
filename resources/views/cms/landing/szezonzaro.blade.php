<?php $headversion = 'newhead'; ?>
@extends('layouts.default')
@section('content')
<style>
	@font-face {font-family:"5AM Gender";src:url("{{url('/media/landing/szezonzaro/Gender.eot?')}}") format("eot"),url("{{url('/media/landing/szezonzaro/Gender.woff')}}") format("woff"),url("{{url('/media/landing/szezonzaro/Gender.ttf')}}") format("truetype"),url("{{url('/media/landing/szezonzaro/Gender.svg#5AMGender')}}") format("svg");font-weight:normal;font-style:normal;}
	.gender {
		font-family: "5AM Gender" !important;
	}
	.linediv div {
		height: 2rem;
	}
</style>
<div class="container-fluid border border-left-0 border-right-0 border-white">
	<div class="row linediv">
		<div class="col-6 pink accent-3 border border-top-0 border-left-0 border-white"></div>
		<div class="col-3 teal lighten-2 border border-top-0 border-left-0 border-white"></div>
		<div class="col-3 pink lighten-3 border border-top-0 border-left-0 border-right-0 border-white"></div>
	</div>
	<div class="row">
		<div class="col-12">
			<h1 class="display-2 text-uppercase text-center pink-text my-3 gender">Szezonzáró akció</h1>
			<p class="text-center lead"><strong>Budapesti és szegedi üzletünkben! Egyedi, Olaszországban tervezett, prémium minőségű bútorok az Ön igényeire szabva.</strong></p>
		</div>
	</div>
	<div class="row">
		<div class="col-12 pink accent-3">
			<h2 class="h2-responsive white-text text-uppercase text-center gender mt-2">-50-70% bútorainkra - május 17-19.</h2>
		</div>
	</div>
</div>
<div class="container-fluid teal lighten-2 white-text">
	<div class="container">
		<div class="row">
			<div class="col-3 text-center">
				<p id="day" class="display-4 gender mb-0">00</p>
				<p>nap</p>
			</div>
			<div class="col-3 text-center">
				<p id="hour" class="display-4 gender mb-0">00</p>
				<p>óra</p>
			</div>
			<div class="col-3 text-center">
				<p id="minute" class="display-4 gender mb-0">00</p>
				<p>perc</p>
			</div>
			<div class="col-3 text-center">
				<p id="second" class="display-4 gender mb-0">00</p>
				<p>
					<?php if( ismobile() ) { ?>
					<span class="mp">mp</span>
					<?php } else { ?>
					<span class="masodperc">másodperc</span>
					<?php } ?>
				</p>
			</div>
			<?php /*
			<div class="col-12">
				<p class="countdown text-center lead mt-2"><strong id="countdown"></strong></p>
			</div>
			*/ ?>
			<script>
			function n(n){
				return n > 9 ? "" + n: "0" + n;
			}
			// Set the date we're counting down to
			<?php if( date('Ymd') < "20190517" ) { ?>
			var countDownDate = new Date("May 17, 2019 00:00:00").getTime();
			var countertext = "indul az akció";
			<?php } else { ?>
			var countDownDate = new Date("May 20, 2019 00:00:00").getTime();
			var countertext = "ér véget akció";
			<?php } ?>

			// Update the count down every 1 second
			var x = setInterval(function() {

				// Get todays date and time
				var now = new Date().getTime();

				// Find the distance between now and the count down date
				var distance = countDownDate - now;

				// Time calculations for days, hours, minutes and seconds
				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);

				// Output the result in an element with id="demo"
				//document.getElementById("countdown").innerHTML = n(days) + " nap " + n(hours) + " óra "  + n(minutes) + " perc " + n(seconds) + " másodperc ";
				document.getElementById("day").innerHTML = n(days);
				document.getElementById("hour").innerHTML = n(hours);
				document.getElementById("minute").innerHTML = n(minutes);
				document.getElementById("second").innerHTML = n(seconds);
				//document.getElementById("countdown").innerHTML = countertext;

				// If the count down is over, write some text 
				if (distance < 0) {
					clearInterval(x);
					//document.getElementById("countdown").innerHTML = "A Black Friday akció elindult!";
				document.getElementById("day").innerHTML = n(00);
				document.getElementById("hour").innerHTML = n(00);
				document.getElementById("minute").innerHTML = n(00);
				document.getElementById("second").innerHTML = n(00);
				}
			}, 1000);
			</script>
		</div>
	</div>
</div>
<div class="container pb-3 border border-white border-left-0 border-right-0">
	<div class="row">
		<div class="col-md-3 text-center">
			<a href="{{ url('/termekek/kanape/l-alaku-kanapek') }}" class="">
				<img src="{{ asset('/media/landing/szezonzaro/marelli.jpg?v2') }}" alt="Kanapék" class="img-fluid my-3 z-depth-1 wow fadeIn">
			</a>
			<a href="{{ url('/termekek/kanape/l-alaku-kanapek') }}" class="btn btn-pink pink accent-3 white-text">Sarokkanapék</a>
		</div>
		<div class="col-md-3 text-center">
			<a href="{{ url('/termekek/kanape/u-alaku-kanapek') }}" class="">
				<img src="{{ asset('/media/landing/szezonzaro/grandioso.jpg') }}" alt="Kanapék" class="img-fluid my-3 z-depth-1 wow fadeIn">
			</a>
			<a href="{{ url('/termekek/kanape/u-alaku-kanapek') }}" class="btn btn-pink pink accent-3 white-text">U-alakú kanapék</a>
		</div>
		<div class="col-md-3 text-center">
			<a href="{{ url('/termekek/kanape/egyenes-kanapek') }}" class="">
				<img src="{{ asset('/media/landing/szezonzaro/insiamo.jpg') }}" alt="Kanapék" class="img-fluid my-3 z-depth-1 wow fadeIn">
			</a>
			<a href="{{ url('/termekek/kanape/egyenes-kanapek') }}" class="btn btn-pink pink accent-3 white-text">Egyenes kanapék</a>
		</div>
		<div class="col-md-3 text-center">
			<a href="{{ url('/termekek/kanape/franciaagyak') }}" class="">
				<img src="{{ asset('/media/landing/szezonzaro/erizzo_03.jpg') }}" alt="Kanapék" class="img-fluid my-3 z-depth-1 wow fadeIn">
			</a>
			<a href="{{ url('/termekek/kanape/franciaagyak') }}" class="btn btn-pink pink accent-3 white-text">Franciaágyak és heverők</a>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h2 class="display-5 gender mt-3 text-center text-md-left">9 prémium textil felár nélkül</h2>
				<p>Válasszon Prémium Szövetet Felár nélkül bútorához! Csúcstechnológiás, magas kopásállóságú, foltálló, tűzálló, lélegző, Reach Oeko Tex, bolyhosodás gátolt egyedi tulajdonságokkal. Kattintson a képekre a finom részletekért!</p>
				<p class="text-center text-md-left">
					<img src="{{asset('/media/landing/nyitasiakcio/szovetek-1x1.jpg')}}" alt="Prémium szövetek a Vamosi Milanonál" class="img-fluid z-depth-1  my-3">
				</p>
				<!-- Uruguay  -->
				<h3 class="display-5 text-center text-md-left gender mt-3">Uruguay </h3>
				<p>finoman barázdált, puha műbőr</p>
				<ul class="list-unstyled">
					<li><strong>Kopásállóság:</strong> 100.000+ [Martindale skála]</li>
					<li><strong>Tűzálló:</strong> BS 5852 I. Cigaretta teszt & EN 1021-1 Cigaretta teszt</li>
					<li><strong>Könnyen tisztítható:</strong> Easy to clean szövet</li>
					<li><strong>Erős színtartóság</strong></li>
					<li><strong>Reach oeko tex</strong> környezetvédelmi szabványnak megfelelő</li>
					<li><strong>Vízálló és antibakteriális</strong></li>
				</ul>
				<div class="row">
					<div class="col-md-3 col-sm-6 text-center text-md-left">
						<a href="https://static.vamosimilano.hu/textiles/uruguay_07_white_1240.jpg" target="_blank">
							<img src="https://static.vamosimilano.hu/textiles/uruguay_07_white_1240.jpg" alt="Uruguay 07 White műbőr" class="img-fluid mb-3 border border-white z-depth-1 w-75" id="popover-show" data-toggle="popover" title="Ismerje meg a szövetet!" data-content="A szövetek képeire kattintva új ablakban nyílnak meg nagy felbontású textil fotóink, ahol a szövetek struktúráját, színét és jellegzetességeit is megismerheti. Üzleteinkben a szöveteket meg is tapinthatja!" data-placement="bottom" data-trigger="focus">
						</a>
						<p>Uruguay 07 White</p>
					</div>
					<div class="col-md-3 col-sm-6 text-center text-md-left">
						<a href="https://static.vamosimilano.hu/textiles/uruguay_17_black_1250.jpg" target="_blank">
							<img src="https://static.vamosimilano.hu/textiles/uruguay_17_black_1250.jpg" alt="Uruguay 17 Black műbőr" class="img-fluid mb-3 border border-white z-depth-1 w-75">
						</a>
						<p>Uruguay 17 Black</p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<!-- Dubai -->
				<h3 class="display-5 text-center text-md-left gender mt-3">Dubai</h3>
				<p>puha, finoman struktúrált felüleltű szövet</p>
				<ul class="list-unstyled">
					<li><strong>Kopásállóság:</strong> 65.000+ [Martindale skála]</li>
					<li><strong>Tűzálló:</strong> BS 5852 I. Cigaretta teszt & EN 1021-1 Cigaretta teszt</li>
					<li><strong>Lélegző szövet</strong></li>
					<li><strong>Reach oeko tex</strong> környezetvédelmi szabványnak megfelelő</li>
					<li><strong>Bolyhosodás:</strong> 4-5 (5 fokú skálán, ahol az 5 a legjobb)</li>
				</ul>
				<div class="row">
					<div class="col-md-3 col-sm-6 text-center text-md-left">
						<a href="https://static.vamosimilano.hu/textiles/dubai_02_beige_1548.jpg" target="_blank">
							<img src="https://static.vamosimilano.hu/textiles/dubai_02_beige_1548.jpg" alt="Dubai 02 beige szövet" class="img-fluid mb-3 border border-white z-depth-1 w-75">
						</a>
						<p>Dubai 02 Beige</p>
					</div>
					<div class="col-md-3 col-sm-6 text-center text-md-left">
						<a href="https://static.vamosimilano.hu/textiles/dubai_03_tabac_1549.jpg" target="_blank">
							<img src="https://static.vamosimilano.hu/textiles/dubai_03_tabac_1549.jpg" alt="Dubai 03 tabac szövet" class="img-fluid mb-3 border border-white z-depth-1 w-75">
						</a>
						<p>Dubai 03 Tabac</p>
					</div>
					<div class="col-md-3 col-sm-6 text-center text-md-left">
						<a href="https://static.vamosimilano.hu/textiles/dubai_20_antracite_1566.jpg" target="_blank">
							<img src="https://static.vamosimilano.hu/textiles/dubai_20_antracite_1566.jpg" alt="Dubai 20 Anthracite szövet" class="img-fluid mb-3 border border-white z-depth-1 w-75">
						</a>
						<p>Dubai 20 Anthracite</p>
					</div>
				</div>
				<!-- Lisbon -->
				<h3 class="display-5 text-center text-md-left gender mt-3">Lisbon</h3>
				<p>erősebb, keményebb, közepesen struktúrált szövet fajta</p>
				<ul class="list-unstyled">
					<li><strong>Kopásállóság:</strong> 80.000+ [Martindale skála]</li>
					<li><strong>Tűzálló:</strong> BS 5852 I. Cigaretta teszt & EN 1021-1 Cigaretta teszt</li>
					<li><strong>Foltálló szövet</strong></li>
					<li><strong>Reach oeko tex</strong> környezetvédelmi szabványnak megfelelő</li>
					<li><strong>Bolyhosodás:</strong> 4-5 (5 fokú skálán, ahol az 5 a legjobb)</li>
				</ul>
				<div class="row">
					<div class="col-md-3 col-sm-6 text-center text-md-left">
						<a href="https://static.vamosimilano.hu/textiles/lisbon_16_emerald_2320.jpg" target="_blank">
							<img src="https://static.vamosimilano.hu/textiles/lisbon_16_emerald_2320.jpg" alt="Lisbon 16 Emerald szövet" class="img-fluid mb-3 border border-white z-depth-1 w-75">
						</a>
						<p>Lisbon 16 Emerald</p>
					</div>
					<div class="col-md-3 col-sm-6 text-center text-md-left">
						<a href="https://static.vamosimilano.hu/textiles/lisbon_20_light-grey_2323.jpg" target="_blank">
							<img src="https://static.vamosimilano.hu/textiles/lisbon_20_light-grey_2323.jpg" alt="Lisbon 20 Light Grey szövet" class="img-fluid mb-3 border border-white z-depth-1 w-75">
						</a>
						<p>Lisbon 20 Light Grey</p>
					</div>
				</div>
				<!-- Madeira -->
				<h3 class="display-5 text-center text-md-left gender mt-3">Madeira</h3>
				<p>zsákszövet, erősen struktúrált szövet fajta</p>
				<ul class="list-unstyled">
					<li><strong>Kopásállóság:</strong> 50.000+ [Martindale skála]</li>
					<li><strong>Tűzálló:</strong> BS 5852 I. Cigaretta teszt & EN 1021-1 Cigaretta teszt</li>
					<li><strong>Lélegző szövet</strong></li>
					<li><strong>Elszíneződés védelem:</strong> kiemelkedően jó</li>
					<li><strong>Reach oeko tex</strong> környezetvédelmi szabványnak megfelelő</li>
					<li><strong>Bolyhosodás:</strong> 5 (5 fokú skálán, ahol az 5 a legjobb)</li>
				</ul>
				<div class="row">
					<div class="col-md-3 col-sm-6 text-center text-md-left">
						<a href="https://static.vamosimilano.hu/textiles/madeira_02_ash_1435.jpg" target="_blank">
							<img src="https://static.vamosimilano.hu/textiles/madeira_02_ash_1435.jpg" alt="Madeira 02 Ash szövet" class="img-fluid mb-3 border border-white z-depth-1 w-75">
						</a>
						<p>Madeira 02 Ash</p>
					</div>
					<div class="col-md-3 col-sm-6 text-center text-md-left">
						<a href="https://static.vamosimilano.hu/textiles/madeira_13_elephant_1446.jpg" target="_blank">
							<img src="https://static.vamosimilano.hu/textiles/madeira_13_elephant_1446.jpg" alt="Madeira 13 Elephant szövet" class="img-fluid mb-3 border border-white z-depth-1 w-75">
						</a>
						<p>Madeira 13 Elephant</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$cover_title	=	"Ajándék ágy akció május 15.-től!<br>Megajándékozzuk egy franciaággyal!";
$cover_text		=	"Rendeljen Dr. Balm prémium matracaink közül az Ön igénye szerint és ingyen adjuk választott Franciaágyát!";
$cover_cta		=	"Kattintson ide az akció részleteiért";
if ( ismobile() ) {
?>
<div class="container-fluid px-0">
	<div class="view mt-0 white-text">
		<img src="{{ asset('/media/landing/agymatrac/drbalm-m-cover.jpg?v1') }}" class="img-fluid animated fadeInLeft" alt="Matracához ingyen franciaágyat ajándékozunk Önnek">
		<div class="mask flex-center rgba-black-light animated fadeInRight">
			<div>
				<h3 class="h3-responsive text-uppercase text-center font-weight-bold mb-4">{{$cover_title}}</h3>
				<p class="text-center">{{$cover_text}}</p>
				<p class="text-center smooth-scroll mt-3">
					<a href="{{url('/ajandekagy-akcio')}}" class="btn btn-primary btn-sm">{{$cover_cta}}</a>
				</p>
			</div>
		</div>
	</div>
</div>
<?php }//if
else {
?>
<div class="container-fluid px-0">
	<div class="view">
		<img src="{{ asset('/media/landing/agymatrac/drbalm_cover.jpg?v1') }}" class="img-fluid animated fadeInLeft" alt="Matracához ingyen franciaágyat ajándékozunk Önnek" style="width:100%;">
		<div class="mask flex-center rgba-black-light animated fadeInDown">
			<div class="row">
				<div class="col-10 mx-auto p-3 white-text">
					<h1 class="h2-responsive text-uppercase text-center font-weight-bold">{{$cover_title}}</h1>
					<p class="lead text-center">{{$cover_text}}</p>
				</div>
				<div class="col-10 mx-auto mt-2">
					<p class="text-center smooth-scroll mt-3">
						<a href="{{url('/ajandekagy-akcio')}}" class="btn btn-primary">{{$cover_cta}}</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}//else
?>
<div class="container-fluid">
	<div class="row white-text">
		<div class="col-md-6 pink accent-3">
			<div class="row">
				<div class="col-md-8 ml-auto mr-md-3 text-center">
					<img src="{{asset('/media/landing/nyitasiakcio/garancia-1x1.jpg')}}" alt="1 év plusz garancia" class="img-fluid z-depth-1 my-3 mx-auto">
					<h2 class="display-5 gender mb-3">Ajándékba adunk +1 év kiterjesztett garanciát</h2>
					<p class="text-md-left">Vásároljon új bútora mellé kiterjesztett garanciát, amelyből egy évet ezen a hétvégén ajándékba adunk, hogy az Ön igényeihez igazodva, hosszútávon is élvezhesse a Vamosi Milano nyújtotta biztonságot és kényelmet. Amennyiben a maximális, 10 év időtartamra igényli kiterjesztett garanciákat, akkor most önköltségi áron, akár öt alkalommal is újrakárpitoztathatja velünk bútorát.</p>
				</div>
			</div>
		</div>
		<div class="col-md-6 teal lighten-2">
			<div class="row">
				<div class="col-md-8 mr-auto ml-md-3 text-center">
					<img src="{{asset('/media/landing/nyitasiakcio/kiszallitas-1x1.jpg')}}" alt="Országos házhozszállítás" class="img-fluid z-depth-1 my-3 mx-auto">
					<h2 class="display-5 gender mb-3">Ingyenes kiszállítás az ország egész területén</h2>
					<p class="text-md-left">Rendeljen kanapéja mellé most akciós áron puffot, fotelt, dohányzóasztalt vagy előszobafalat, és élvezze országosan ingyenes házhozszállítási szolgáltatásunkat!</p>
					<p class="text-md-left">Rendeljen összeszerelést a házhozsszállított bútora mellé. Használja ki komfort szolgáltatásaink teljes palettáját.</p>
				</div>
			</div>
		</div>
	</div>
</div>
@append