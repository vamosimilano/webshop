@extends('layouts.default')

@section('content')

<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Hírlevél feliratkozás') }}</strong>
                        </li>
            </ul>
</div>
<main class="page-main" id="maincontent">
        <div class="columns">
    <div class="column main">
		<legend class="legend"><span>{{ t('Hírlevél feliratkozás') }}</span></legend><br>
        <p>{{ t('Értesüljön elsőként a meghírdetett akcióinkról') }}</p>
	<?/*
    <form class="form form-edit-account"  method="post" id="subscribe-footer-profile" action="{{ action('SubscribeController@subscribe') }}" enctype="multipart/form-data">

    <fieldset class="fieldset info" style="width: 100%; padding: 0px;margin:0px;">
        <legend class="legend"><span>{{ t('Hírlevél feliratkozás') }}</span></legend><br>
        <p>{{ t('Értesüljön elsőként a meghírdetett akcióinkról') }}</p>

        @if (empty($unsub))


                                            <input type="hidden" name="lastname" value="{{ $user->lastname }}"  />
                                            <input type="hidden" name="firstname" value="{{ $user->firstname }}"  />
                                            <input type="hidden" name="email" value="{{ $user->email }}"  />
                                            <input type="hidden" name="key" value="1"  />


                                            {{ csrf_field() }}
                                            <div class="clearfix"></div>

                                        <div class="clearfix"></div>
                                        <a class="btn btn-primary white-text" href="javascript:$('#subscribe-footer-profile').submit();">{{ t('Feliratkozás') }}</a>

        @elseif ($unsub->unsubscribe_time != null)
            <p>{{ t('Ön leiratkozott hírleveleinkről, mi ezt tiszteletben tartjuk, mert mi is utáljuk a SPAM-et! Amennyiben felszeretne iratkozni újra, keresse ügyfélszolgálatunkat!') }}</p>
        @else
            <p>{{ t('Ön jelenleg felvan iratkozva hírlevelünkre!') }}</p>
            <a class="btn btn-primary white-text" href="{{ action('SubscribeController@unSubscribe', $unsub->unsub_url) }}">{{ t('Leiratkozás') }}</a>
        @endif


        <div class="clearfix"></div>
    </fieldset>
    <div class="clearfix"></div>
    </form>
	*/?>
	@include('cms.subscribeGDPR')
    <div class="clearfix"></div>

</div>


    </div>
</main>




@stop
