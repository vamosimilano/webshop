@extends('layouts.default')

@section('content')
<style>
	body {
		overflow-x: hidden;
		width: 100vw !important;
	}
	.text-red {
		color: #FF4B43;
	}
	.text-white {
		color: #e3e3e3;
	}
	.circle-red {
		border: solid 1px #FF4B43;
		border-radius: 50%;
		width: 8rem;
		height: 8rem;
		line-height: 8rem;
		background-color: #FF4B43;
		color: white;
	}
	.counter-num p {
		border: 3px solid #FF4B43;
		/*background: white;*/
		color: white;
		margin: 0 auto;
		width: 8rem;
		font-size: 5rem !important;
		
	}
	.mp {
		display: none !important;
	}
	.masodperc {
		display: inline !important;
	}
	@media screen and (max-width: 768px) {
		.showroom img, .showroom p, .showroom h2 {
			text-align: center !important;
		}
		.showroom .img-responsive {
			margin-left: auto !important;
			margin-right: auto !important;
		}
	}
	@media screen and (max-width: 600px) {
		.counter-num p {
			width: 4rem;
			font-size: 2rem !important;
		}
		.mp {
			display: inline !important;
		}
		.masodperc {
			display: none !important;
		}
	}
	@media screen and (max-width: 420px) {
		h2.text-white {
			font-size: 2rem;
		}
	}
	.slidercontainer, .footer-top {
		display: none;
	}
	.img-responsive + h2 {
		margin-top: 8px;
	}
	.logo2 {
		margin: 0px auto !important;
		z-index: 2 !important;
	}
	.social-icons.new li a {
		display: inline-block;
		width: 60px;
		height: 60px;
		line-height: 60px !important;
		background: #FF4B43;
		text-align: center;
		border-radius: 50%;
		color: #fff;
		font-size: 30px !important;
		margin: 1rem 2rem;
		transition: .3s background-color;
	}
	.social-icons.new li {
		margin-bottom: 0;
	}
</style>
<div class="clearfix"></div>
<div class="breadcrumbs">
	<ul class="items">
		<li class="item home">
			<a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">{{ t('Főoldal') }} </a>
		</li>
		<li class="item item-active">
			Készletkisöprés
		</li>
	</ul>
</div>
<div class="container-fluid" style="background:#282828!important;margin: 0;">
	<div class="row" style="background:#282828!important;">
		<div class="col-md-12 text-center" style="background:#282828!important;">
			<img src="https://vamosimilano.hu/images/black2.png" alt="Black Friday 50-70% kedvezmény - Vamosi Milano" class="img-responsive logo2">
		</div>
	</div>
</div>
<main class="container-fluid" style="background:#282828!important;margin: 0;">
<div class="container">
	<div class="row text-center">
		<div class="col-md-12">
			<h2 class="text-white"><strong>2019. március 22-24.</strong></h2>
			<p class="lead text-white">A Vamosi Milano Black Friday akciójában most fantasztikus 50-70% kedvezménnyel vásárolhatja meg új kanapéját, franciaágyát vagy választhat azonnal elvihető, outlet bútoraink közül is! Amint a számláló lejár, máris leadhatja rendelését online, bemutatótermünkben vagy outlet áruházunkban.</p>
		</div>
	</div>
	<div class="row">
		<div class="col col-md-12 text-center0">
			<p class="countdown text-center lead" style="margin: 5rem auto;"><strong id="countdown" style="color:#FF4B43;font-size: 4rem;"></strong></p>
			<table class="text-white text-center lead" style="width:50%;margin: 3rem auto;">
				<tr>
					<td class="counter-num">
						<p id="day"></p>
					</td>
					<td class="counter-num">
						<p id="hour"></p>
					</td>
					<td class="counter-num">
						<p id="minute"></p>
					</td>
					<td class="counter-num">
						<p id="second"></p>
					</td>
				</tr>
				<tr>
					<td style="width:25%;">NAP</td>
					<td style="width:25%;">ÓRA</td>
					<td style="width:25%;">PERC</td>
					<td style="width:25%;"><span class="mp">MP</span> <span class="masodperc">MÁSODPERC</span></td>
				</tr>
			</table>
			<script>
			function n(n){
				return n > 9 ? "" + n: "0" + n;
			}
			// Set the date we're counting down to
			var countDownDate = new Date("Marc 22, 2019 00:00:01").getTime();

			// Update the count down every 1 second
			var x = setInterval(function() {

				// Get todays date and time
				var now = new Date().getTime();

				// Find the distance between now and the count down date
				var distance = countDownDate - now;

				// Time calculations for days, hours, minutes and seconds
				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);

				// Output the result in an element with id="demo"
				//document.getElementById("countdown").innerHTML = n(days) + " nap " + n(hours) + " óra "  + n(minutes) + " perc " + n(seconds) + " másodperc ";
				document.getElementById("day").innerHTML = n(days);
				document.getElementById("hour").innerHTML = n(hours);
				document.getElementById("minute").innerHTML = n(minutes);
				document.getElementById("second").innerHTML = n(seconds);

				// If the count down is over, write some text 
				if (distance < 0) {
					clearInterval(x);
					document.getElementById("countdown").innerHTML = "A Black Friday akció elindult!";
				document.getElementById("day").innerHTML = n(00);
				document.getElementById("hour").innerHTML = n(00);
				document.getElementById("minute").innerHTML = n(00);
				document.getElementById("second").innerHTML = n(00);
				}
			}, 1000);
			</script>
		</div>
	</div>
	<div class="jumbotron" style="background-color: #282828; color: white;background-image:url('https://vamosimilano.hu/images/bemutatoterem.png');background-position: center -300px;">
		<h2 class="text-center" style="margin-top:80px;font-size: 2rem;">Nyugodtan tervezgessen, álmodozzon, játsszon a színekkel és méretekkel, majd válassza ki legújabb kedvenc bútorát nappalijába vagy hálószobájába!</h2>
		<p class="text-center" style="margin-top: 4rem;"><a href="{{ url('/termekek') }}" class="btn btn-lg" style="border: 1px solid white; color: white;">Választok</a></p>
	</div>
	<hr style="margin: 8rem 0px; background-color:#FF4B43; border: none;">
	<div class="row">
		<div class="col col-sm-3 text-center">
			<p><i class="fa fa-truck fa-3x circle-red fa-flip-horizontal" aria-hidden="true"></i></p>
			<p class="text-red lead">Ingyenes házhozszállítás</p>
			<p class="text-white">Vásároljon legalább 200.000 forint értékben* és rendelését teljesen ingyen házhoz szállítjuk az egész ország területén. A házhozszállítást követően kérésre össze is szereljük új bútorát!</p>
		</div>
		<div class="col col-sm-3 text-center">
			<p><i class="fa fa-crop fa-3x circle-red" aria-hidden="true"></i></p>
			<p class="text-red lead">Teljesen egyedi bútorok</p>
			<p class="text-white">Bútorainkat több méretkombinációban és több száz féle kárpitozással rendelheti – ezzel is feldobva otthona stílusos nappaliját vagy hálószobáját. Használja szövetválasztónkat a termékeknél!</p>
		</div>
		<div class="col col-sm-3 text-center">
			<p><i class="fa fa-certificate fa-3x circle-red" aria-hidden="true"></i></p>
			<p class="text-red lead">10 év vázgarancia</p>
			<p class="text-white">Megrendelésére akár 10 év kiterjesztett vázgaranciát adunk, melynek keretében bútorát bármikor önköltségen újrakárpitozzuk – így évek múltán is stílusos kiegészítője lehet otthonának!</p>
		</div>
		<div class="col col-sm-3 text-center">
			<p><i class="fa fa-money fa-3x circle-red" aria-hidden="true"></i></p>
			<p class="text-red lead">Áruhitel önerő nélkül</p>
			<p class="text-white">Vegye igénybe kedvező feltételekkel áruhitelünket és legyen öné már most álmai bútora. Áruhitelünket akár önerő nélkül, gyorsan igényelheti és már le is adhatja megrendelését!</p>
		</div>
	</div>
	<hr style="margin: 8rem 0px; background-color:#FF4B43; border: none;">
	<div class="row showroom" style="margin: 2rem 0;">
		<div class="col-sm-4">
			<a href="{{ url('https://vamosimilano.hu/termekek/kanape/egyenes-kanapek') }}"><img src="https://static.vamosimilano.hu/category/370/4_egyenes-kanapek3503.jpg" class="img-responsive" alt="Egyenes kanapék"></a>
			<h2 class="text-white">Egyenes kanapék</h2>
			<p class="text-white">Válassza olasz stílusú, eredeti olasz design alapján készült egyenes kanapéinkat és ülőgarnitúráinkat - válasszon egyenes kanapét nappalijába és varázsolja egyedivé otthonát helytakarékos, gyönyörű és praktikus bútorainkkal!</p>
			<p class="" style="margin-bottom: 4rem;"><a href="{{ url('https://vamosimilano.hu/termekek/kanape/egyenes-kanapek') }}" class="btn" style="border: 1px solid #FF4B43; color: white;background-color:#FF4B43;">Tovább a termékekre</a></p>
		</div>
		<div class="col-sm-4">
			<a href="{{ url('https://vamosimilano.hu/termekek/kanape/l-alaku-kanapek') }}"><img src="https://static.vamosimilano.hu/category/370/12_l-kanapek9924.jpg" class="img-responsive" alt="Sarok kanapék"></a>
			<h2 class="text-white">Sarok kanapék</h2>
			<p class="text-white">Stílusosan dekoratív sarokkanapéink tökéletes harmóniát teremtenek nappalijában. Eredeti olasz tervezés alapján, kézzel készült L-alakú kanapéink és ülőgarnitúráink vendégággyá alakíthatóak, egyedi méretben és kárpitozással is rendelhetőek!</p>
			<p class="" style="margin-bottom: 4rem;"><a href="{{ url('https://vamosimilano.hu/termekek/kanape/l-alaku-kanapek') }}" class="btn" style="border: 1px solid #FF4B43; color: white;background-color:#FF4B43;">Tovább a termékekre</a></p>
		</div>
		<div class="col-sm-4">
			<a href="{{ url('https://vamosimilano.hu/termekek/kanape/u-alaku-kanapek') }}"><img src="https://static.vamosimilano.hu/category/370/24_u-kanapek8030.jpg" class="img-responsive" alt="U-alakú kanapék"></a>
			<h2 class="text-white">U-alakú kanapék</h2>
			<p class="text-white">U-alakú kanapé választékunk darabjai a hamisítatlan itáliai életérzés megtestesítői - a stílust ötvözik a praktikummal, hiszen teljesen testre szabhatóak, egyedi méretben és színben rendelhetőek, valamint vendégággyá is alakíthatóak!</p>
			<p class="" style="margin-bottom: 4rem;"><a href="{{ url('https://vamosimilano.hu/termekek/kanape/u-alaku-kanapek') }}" class="btn" style="border: 1px solid #FF4B43; color: white;background-color:#FF4B43;">Tovább a termékekre</a></p>
		</div>
		<div class="col-sm-4">
			<a href="{{ url('https://vamosimilano.hu/termekek/kanape/franciaagyak') }}"><img src="https://static.vamosimilano.hu/category/370/9_franciaagyak4491.jpg" class="img-responsive" alt="Franciaágyak"></a>
			<h2 class="text-white">Franciaágyak</h2>
			<p class="text-white">Válassza extravagánsan impozáns franciaágyainkat hálószobájába és édesítse meg a pihenéssel töltött órákat! Az olasz bútortervezési hagyományok alapján, kézzel készült ágykeretek egyedi méretben és szövettel is rendelhetőek.</p>
			<p class="" style="margin-bottom: 4rem;"><a href="{{ url('https://vamosimilano.hu/termekek/kanape/franciaagyak') }}" class="btn" style="border: 1px solid #FF4B43; color: white;background-color:#FF4B43;">Tovább a termékekre</a></p>
		</div>
		<div class="col-sm-4">
			<a href="{{ url('https://vamosimilano.hu/termekek/outlet') }}"><img src="https://www.vamosimilano.hu/images/outlet.jpg" class="img-responsive" alt="Outletes bútorok"></a>
			<h2 class="text-white">Outletes bútorok</h2>
			<p class="text-white">Válogasson a Vamosi Milano outlet választékából, jusson hozzá nagyon kedvező áron álmai bútorához és varázsolja egyedivé otthonát!<br><br><br></p>
			<p class="" style="margin-bottom: 4rem;"><a href="{{ url('https://vamosimilano.hu/termekek/outlet') }}" class="btn" style="border: 1px solid #FF4B43; color: white;background-color:#FF4B43;">Tovább a termékekre</a></p>
		</div>
	</div>
	<hr style="margin: 2rem 0px; background-color:#FF4B43; border: none;">
	<div class="row">
		<div class="col-sm-12 text-center">
			<h2 class="text-white" style="margin-bottom:3rem;"><strong>Kövessen minket!</strong></h2>
			<ul class="social-icons new" style="padding: 0px 0px 2rem 0px;">
				<li><a class="facebook-link" href="https://www.facebook.com/vamosimilano" target="_blank"><em class="fa fa-facebook"></em></a></li>
				<li><a class="twitter-link" href="https://twitter.com/vamosimilano" target="_blank"><em class="fa fa-twitter"></em></a></li>
				<li><a class="linkedin-link" href="https://www.linkedin.com/company/vamosimilano" target="_blank"><em class="fa fa-linkedin"></em></a></li>
				<li><a class="youtube-link" href="https://www.youtube.com/channel/UC6ckSCubYM-M4fdOmXkfb1g" target="_blank"><em class="fa fa-youtube-play"></em></a></li>
				<li><a class="instagram-link" href="https://instagram.com/vamosimilano/" target="_blank"><em class="fa fa-instagram"></em></a></li>
				<li><a class="pinterest-link" href="https://www.pinterest.com/vamosimilano/" target="_blank"><em class="fa fa-pinterest"></em></a></li>
			</ul>
		</div>
	</div>
	<hr style="margin: 2rem 0px; background-color:#FF4B43; border: none;">
	<div class="row" style="margin-bottom: 3rem;">
		<div class="col-sm-6">
			<h2 class="text-white"><strong>Vamosi Milano bemutatóterem</strong></h2>
			<ul class="list-unstyled text-white">
				<li class="lead"><i class="fa fa-phone-square" aria-hidden="true" style="margin-right: 1rem;"></i> +36-1-353-9410</li>
				<li class="lead"><a href="mailto:ertekesites@vamosimilano.hu" class="text-white"><i class="fa fa-envelope-o" aria-hidden="true" style="margin-right: 1rem;"></i> ertekesites@vamosimilano.hu</a></li>
				<li class="lead"><i class="fa fa-clock-o" aria-hidden="true" style="margin-right: 1rem;"></i> H-Sz: 9:00 - 17:00<br><span style="margin-left: 3.5rem;">V: 11:00 - 18:00</span></li>
				<li class="lead"><i class="fa fa-map-marker" aria-hidden="true" style="margin-right: 1rem;"></i> Budapest, Petzvál József utca 46-48., 1119</li>
			</ul>
		</div>
		<div class="col-sm-6">
			<h2 class="text-white"><strong>Csepeli outlet áruház</strong></h2>
			<ul class="list-unstyled text-white">
				<li class="lead"><i class="fa fa-phone-square" aria-hidden="true" style="margin-right: 1rem;"></i> +36-1-353-9410</li>
				<li class="lead"><a href="mailto:ertekesites@vamosimilano.hu" class="text-white"><i class="fa fa-envelope-o" aria-hidden="true" style="margin-right: 1rem;"></i> ertekesites@vamosimilano.hu</a></li>
				<li class="lead"><i class="fa fa-clock-o" aria-hidden="true" style="margin-right: 1rem;"></i> H, Cs-Va: 9:00 - 18:00<br><span style="margin-left: 3.5rem;">K - Sze: ZÁRVA</span></li>
				<li class="lead"><i class="fa fa-map-marker" aria-hidden="true" style="margin-right: 1rem;"></i> Budapest, II. Rákóczi Ferenc út 107-115., 1211</li>
			</ul>
		</div>
	</div>
</div>
</main>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div id="big_map" style="width:100vw!important;height: 40vh;"></div>
		</div>
	</div>
</div>
@stop
@section('footer_js')
<script src="//maps.googleapis.com/maps/api/js?v=3.17&key=AIzaSyD-2e09J6RRvWiyp_p0PR_zpiQuzM0mWqk" type="text/javascript"></script>
<script src="https://vamosimilano.hu/js/contact_v9.js"></script>
<script type="text/javascript">google.maps.event.addDomListener(window, 'load', initBigMap);</script>
@append

