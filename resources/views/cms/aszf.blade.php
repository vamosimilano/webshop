<div class="row">
	<div class="col-lg-8 text-justify">
		<h2 class="h4-responsive text-uppercase title">{{$subtitle}}</h2>
		<ol class="my-4">
			<li>
				<p>A jelen szerződéses feltételek kerülnek alkalmazásra valamennyi, a Vamosi Milano / tulajdonos: WORE HUNGARY Kft (cégjegyzékszám: 01-09-944100, adószám: 22905192-2-43 székhely: 1119 Bp., Petzvál J. u. 46-48., <?php /* bankszámlaszám: 11712004-22460127-00000000 */ ?>; továbbiakban: Eladó) által értékesített bútor vonatkozásában. Vevő aláírásával elfogadja az általános szerződési feltételeket.</p>
			</li>
			<li>
				<p>Vevő a bútort katalógusból, online felületekről / weboldal, közösségi portál /, illetve a Vamosi Milano bemutatótermében kiállított mintadarabok alapján rendeli meg. Vevőnek lehetősége van valamennyi rendelhető és a gyártás során felhasználható szövetminta személyes megtekintésére a Vamosi Milano bemutatótermében. Vevő tudomásul veszi, hogy a megrendelőlap benyújtását követően az általa kiválasztott szövetek színével, típusával, minőségével kapcsolatban kifogást nem emelhet. A weboldalon és a hirdetési felületeken látható képek illusztrációk.</p>
				<p>Vevő aláírásával elfogadja, hogy az elektronikus felületen megjelenő szín, ábra eltérhet a gyártáshoz ténylegesen felhasznált és a bemutatóteremben kiállított mintadarabtól. Ezen eltérések az elektronikus képmegjelenítő eszközök fajtájától is függhetnek és a megrendelni kívánt bútor kárpitozását ( színárnyalatait ), valamint a bútor arányait érinthetik ( a képen vagy bemutatóteremben feltűntetett daraboktól eltérő méretben történő megrendelés esetén ). A párnák darabszáma a megrendelt bútor méretétől függően változhat.</p>
				<p>Vevő tudomásul veszi, hogy az outletes termékként megjelölt bútorok kiállított - és ennek megfelelően bizonyos mértékig használt – darabok. Ebből kifolyólag vevő ezen termékeket kedvezményes áron vásárolhatja meg, azonban további kedvezményre vagy árcsökkentésre – a vásárláskor már fennálló és felismerhető esetleges hibák tekintetében - nem jogosult, továbbá ezen hibák vonatkozásában javítási vagy csere igénnyel nem léphet fel. Webshopos vásárlás esetén vevőnek lehetősége van megtekinteni az outletes terméket eladó bemutatótermében. Ennek elmulasztásából eredő minden felelősség vevőt terheli. Így az értékesítéskor már fennálló és felismerhető hibákkal (például de nem kizárólagosan kopás, fakulás) összefüggésben nem hivatkozhat a megtekintés elmaradására.</p>
				<p>Tekintettel arra, hogy bútoraink egyedi igényekre készülnek, egyes, a megrendelő által kiválasztott kárpitok vastagsága, valamint kárpitozási technikája és az ehhez szükséges gyártási technológia – figyelembe véve a bútor elemeinek számát is - eltérhet, így vevő aláírásával elfogadja, hogy a bútor mérete ezen eltérésekből adódóan, a megrendelőlapon szereplő mérettől összesen 1-10 cm-t eltérhet. Vevő jelen szerződés aláírásával igazolja, hogy a bútor típusát, színét, méretét, továbbá a kárpitot ellenőrizte, elfogadja, és a jelen szerződés mellékletét képező megrendelőlapon feltüntetett tulajdonságokkal kívánja a bútort megvásárolni. Vevő tudomásul veszi, hogy az általa meghatározott és a megrendelőlapon rögzített tulajdonságoktól a későbbiekben nem térhet el és az annak megfelelően előállított bútort köteles átvenni és a vételárat kiegyenlíteni.</p>
				<p>Vevő a megrendelőlap benyújtása előtt köteles ellenőrizni minden azon szereplő és a bútor gyártásához szükséges adatot / elérhetőség, számlázási cím, a bútor valamennyi tulajdonsága, ideértve típus, méretek, szövet, egyedi igények /. Vevő tudomásul veszi, hogy egyedi megrendelés esetén amennyiben nem határozza meg egyértelműen a bútor kiegészítő részeinek tulajdonságait, úgy Eladó jogosult azt saját hatáskörben – a termék elkészítéséhez szükséges mértékben – meghatározni.Vevő tudomásul veszi, hogy a megrendelőlap aláírásával és a 4. pontban részletezett vételárrészlet megfizetésével a megrendelés véglegessé válik, és azon ezt követően utólagos módosításra nincs lehetőség. A megrendelőlap benyújtását követően a Vevő részéről történt bármely tévedés vagy elírás miatti felelősséget Eladó kifejezetten kizárja.</p>
			</li>
			<li>
				<p>Amennyiben az Eladó minden gondossága ellenére hibás ár kerül a webáruház felületére, különös tekintettel a nyilvánvalóan téves, pl. a termék közismert, általánosan elfogadott vagy becsült árától jelentősen eltérő, esetleg rendszerhiba miatt megjelenő téves árra, akkor az Eladó nem köteles a terméket hibás áron szállítani, hanem felajánlhatja a helyes áron történő szállítást, amelynek ismeretében a Vevő elállhat vásárlási szándékától.</p>
			</li>
			<li>
				<p>Vevő tudomásul veszi, hogy a mindenkor változó akciókban meghirdetett ajándékokra kizárólag abban az esetben jogosult, amennyiben eleget tett az abban szereplő feltételeknek, továbbá, az ajándékra vonatkozó igényét a vásárlással egyidejűleg bejelentette és az a megrendelőlapon rögzítésre került. Az Eladó által havonta meghirdetett „Hónap bútora” elnevezésű akció keretében adott kedvezmény az adott hónap első napjától a hónap utolsó napjáig leadott, hatályos megrendeléssel történő vásárlás esetében érvényes. A hatályos megrendelés feltételeit az 5. pont tartalmazza.</p>
				<p>A mindenkor hatályos akciókban szereplő kedvezményes vételár és/vagy ajándékok és/vagy nyeremények igénybevételéhez szükséges feltételeket a meghirdetett akció szövege tartalmazza. Vevő kijelenti, hogy az akcióban szereplő feltételeket megismerte és azoknak hiánytalanul eleget tett.</p>
				<p><a id="utazasitamogatas" name="utazasitamogatas"></a>Az a vásárló, aki Budapesten, illetve Pest Megyén kívüli lakcímmel rendelkezik, a megvásárolt termék kiszállítási címe is ezen területeken kívülre esik, valamint a megrendelése összege meghaladja a 200.000.-Ft-ot, 12.000.-Ft kedvezményt kap megrendelése végösszegéből. A kedvezmény utólagosan nem igényelhető, kizárólag a megrendeléssel egyidejűleg, mindhárom feltétel együttes fennállása esetén. A lakcímet a vásárlónak lakcimkártyája bemutatásával kell igazolnia. Ezen akció 2018.11.25-től visszavonásig érvényes. Az akció online vásárlásokra nem vonatkozik.</p>
			</li>
			<li>
				<p>A megrendelés akkor hatályos, ha Vevő a bútor vételárának 30%-át, valamint az egyéb szolgáltatások - szövet, illetve méret felárak, házhozszállítás díja - 100 %-át kifizette, továbbá a megrendelőlapot aláírta. Ezen feltételtől való eltérésre kizárólag egyedi megállapodás alapján, továbbá bruttó 1.500.000,-Ft-ot elérő értékű megrendelés esetén van lehetőség. A jelen pontban rögzített, fenti előírás alól kivételt képeznek az áruhitel igénybevételével kezdeményezett megrendelések is. Áruhitel igénybevételével történő megrendelés esetén a megrendelés a hitelkérelem bank általi elfogadásával egyidejűleg válik hatályossá, és ezen időponttól kezdődően alkalmazandó valamennyi ehhez kapcsolódó és jelen ÁSZF-ben rögzített jogkövetkezmény /módosítási tilalom, kártérítési – és kötbérfizetési kötelezettség, stb./</p>
				<p>A vételárhátralékot Vevő a bútor átvételével egyidejűleg köteles megfizetni. Vevő a vételár hiánytalan megfizetésével teljesíti a szerződést. Eladó a vételárhátralék teljes megfizetéséig tulajdonjogát fenntartja.</p>
				<p>Vevő az Általános Szerződési Feltételek elfogadásával beleegyezik, hogy Eladó elektronikus számlát bocsát ki. Késedelmes fizetés esetén Vevő köteles megfizetni Eladó részére a Polgári Törvénykönyvről szóló 2013. évi V. törvény (a továbbiakban: Ptk.) szerinti késedelmi kamatot, továbbá a 8. pontban foglalt tárolási díjat.</p>
			</li>
			<li>
				<p><strong>Elállási jog, kötbér</strong></p>
				<p>Tekintettel arra, hogy Eladó a megrendelt terméket Vevő kifejezett kérésére és utasításai alapján állítja elő, így Vevő a fogyasztó és a vállalkozás közötti szerződések részletes szabályairól szóló 45/2014. (II.26.) Korm. rendelet 20. §-ban foglalt elállási jogot nem gyakorolhatja a rendelet 29. § (1) bekezdés c. pontja értelmében.</p>
				<p>Amennyiben Vevő a megrendelést követően olyan okból áll el a vásárlástól, mely Eladónak nem felróható, Eladó – a megkezdett egyedi gyártás sajátosságaira tekintettel - kártérítésre tarthat igényt, melynek mértéke a megrendelt bútor vételárának 30%-a.</p>
				<p>Amennyiben Vevő a megrendelt bútort nem veszi át, vagy a vételár fennmaradó részét nem egyenlíti ki, az Eladó a 9. pontban rögzített módon jogosult eljárni. Vevő az Eladó – 9. pontban foglaltak szerinti - elállása esetén nemteljesítési kötbér fizetésére köteles, melynek mértéke a megrendelt bútor vételárának 30%-a, valamint az egyéb szolgáltatások 100 %-a. Eladó a kötbér összegének erejéig jogosult beszámítani a Vevő által teljesített vételárrészletet. Az Eladó a 7. pontban rögzített gyártási határidő be nem tartása esetén, a Vevő felé a megrendelt bútor alapára 1%-ának megfelelő összegű késedelmi kötbér fizetésére köteles a késedelemmel érintett naptári naponként, a késedelembe esés 5. munkanapjától kezdődően addig az időpontig, amikor Eladó a bútor elkészültéről igazolhatóan értesíti Vevőt. Az Eladó által fizetendő kötbér összege azonban nem haladhatja meg a bútor vételárának 30%-át. Eladó késedelmi kötbér fizetési kötelezettsége nem vonatkozik az egyedi, az Eladó által megadott általános tulajdonságokkal rendelkező bútoroktól eltérő vevői megrendelésekre.</p>
			</li>
			<li>
				<p>A gyártási idő a megrendelés 5. pont szerinti hatályossá válását követően kezdődik. Vevő tudomásul veszi, hogy a megrendelőlapon feltüntetett, várható gyártási idő általános, becsült intervallumot jelent, melytől eltérések előfordulhatnak.</p>
				<p>Eladó a végleges megrendelés benyújtásától és az 5. pontban részletezett vételárrészlet beérkezésétől számított 90 napon belül köteles a megrendelt termék elkészítésére. Vevő tudomásul veszi, hogy amennyiben a megrendelt bútor kárpitjaként egy magas kategóriájú szövetet ( 2-7 kategória ) választ, ez az egyedi megrendelésre tekintettel hosszabb gyártási időt eredményezhet. Eladó ezen esetekben fenntartja a jogot arra, hogy a jelen pontban rögzített gyártási határidőt további 60 nappal meghosszabbítsa. Ebben az esetben eladó késedelmi kötbér fizetésére vonatkozó kötelezettsége is a további 60 nap eltelte után ( 90+60 nap ) kezdődik.</p>
				<p>Eladó a Vevő által megadott elektronikus levelezési címre küldött e-maillel tájékoztatja Vevőt a megrendelt termék elkészültéről és ezáltal a megrendelés teljesítéséről. Kizárólag ezen e-mail minősül Eladó részéről értesítésnek. Vevő tudomásul veszi, hogy az értesítéstől a kiszállításig eltelt idő ezen határidőbe nem számít bele. Nem minősül a gyártási határidő Eladó részéről történő megszegésének, ha a megrendelésben, a megrendelés hatályosulását követően bármilyen változtatás történik, ez esetben a változtatás elfogadásától számítva a gyártási időtartam újrakezdődik.</p>
			</li>
			<li>
				<p>Eladó részéről a szerződés teljesítettnek minősül, amikor a bútort az előírásoknak és a megrendelésnek megfelelően előállította, azt átadásra előkészítette és erről a Vevőt értesítette. Az értesítés kizárólag a vevő által megrendeléskor megadott e-mail címére küldött elektronikus levél formájában minősül szabályosan megtettnek. Eladó a teljesítés során alvállalkozót vesz igénybe.</p>
				<p>Eladó kizárja felelősségét minden olyan kár vonatkozásában, amely a Vevő által helytelenül megadott értesítési címből/elérhetőségből származik.</p>
			</li>
			<li>
				<p>Az átadás-átvétel az értesítés kézhezvételétől számított 8 napon belül történik vagy Eladó telephelyén, vagy pedig Vevő kérése esetén házhozszállítással. A termék személyes átvételekor – az azonosítás érekében - ;Vevő köteles közölni a megrendelés számát és bemutatni az eredeti megrendelőlapot. A házhozszállítás külön díj ellenében történik Amennyiben vevő nem veszi igénybe a kiszállítási szolgáltatást, úgy személyes átvétel esetén vevő köteles gondoskodni a bútor elszállításáról és az ehhez szükséges rakodási munkák elvégzéséről. Eladó kizárólag budapesti átvétel esetén, bútoronként 10.000,-Ft+ÁFA rakodási díj megfizetése ellenében biztosítja az ehhez szükséges feltételeket, melynek díja a helyszínen fizetendő.</p>
				<p>Eladó – országosan - ingyenes házhozszállítást biztosít, amennyiben a vevő</p>
				<ul class="mb-2">
					<li>legalább 2 darab új megrendelésű bútort vásárol (kanapét és/vagy franciaágyat )</li>
					<li>legalább 1 új megrendelésű bútort ( kanapé vagy franciaágy ) és 1 darab új megrendelésű kiegészítő terméket ( puff, fotel, dohányzóasztal, előszobafal ) vásárol. A lámpák és a matracok nem minősülnek kiegészítő terméknek. Az igénybevétel további feltétele, hogy vevő – fentieknek megfelelő – megrendelésének összértéke elérje a 200.000.-Ft-ot.</li>
				</ul>
				<p>A kiszállítás igénybevétele esetén a szállítók a megadott címen lévő első zárható helyiségig kötelesek szállítani a terméket. Amennyiben a bútort emeletre kell szállítani, vevő erre irányuló – előzetes vagy helyszíni –kérése alapján 1000.-Ft/emelet összegű díjazás ellenében van lehetőség. A kiszállítás összeszerelési szolgáltatást nem tartalmaz, annak megrendelésére és igénybevételére külön díjazás alapján van lehetőség. Az összeszerelési szolgáltatás kizárólag az Eladó által kiszállított bútorok vonatkozásában igényelhető, díja 16.900.-Ft.</p>
				<p>A bútor átadásának feltétele a Vevő részéről az 5. pontban részletezett vételárrészlet megfizetését igazoló bizonylat bemutatása, továbbá a vételárhátralék hiánytalan megfizetése. Amennyiben Vevő az értesítéstől számított 8 napon belül a bútort nem veszi át, úgy Eladó a késedelem idejére jogosult tárolási költséget felszámítani, melynek összege a bútor teljes vételárának 1%-a/nap. Vevő erre irányuló kötelezettsége abban az esetben is fennáll, amennyiben eladó a megrendelőlapon feltűntetett, becsült gyártási határidőt megelőzően értesíti vevőt a termék elkészültéről. Eladó a bútort legfeljebb az értesítés Vevő általi kézhezvételétől számított 30 napig köteles tárolni. Ezen határidő elteltét követően Eladó jogosult a szerződéstől elállni és a bútort értékesíteni. Ebben az esetben Vevő által befizetett, 5. pontban részletezett vételárrész összegét Eladó jogosult a 6. pont szerinti kötbér összegébe beszámítani, ezáltal Vevő kötbérfizetési kötelezettsége teljesítettnek tekintendő. A bútor átvételénél vevő köteles megvizsgálni, hogy megfelel-e a megrendelésnek. Amennyiben megfelel, úgy köteles ezt az átvételkor írásban igazolni. A Vevő köteles továbbá a bútor felismerhető hibáit azonnal kifogásolni.</p>
			</li>
			<li>
				<p>A bútor Vevő részére történő átadásával, továbbá az értesítésétől számított 8 nap elteltével a bútorhoz fűződő kárveszély Vevőre száll át.</p>
			</li>
			<li>
				<p>Kiterjesztett garancia: a vásárlóknak lehetőségük van – az 1 éves jótálláson felüli – a bútor vázára vonatkozó, kiterjesztett garanciát vásárolni bútoruk mellé, melynek díja a megvásárolni kívánt garancia időtartamától függően az alábbiak szerint alakul:</p>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<td width="30%">Vázgarancia időtartama (években)</td>
							<td width="40%">Ár</td>
							<td width="30%">Szövet csere</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1 - Alap garancia</td>
							<td>díjmentes</td>
							<td>-</td>
						</tr>
						<tr>
							<td>2</td>
							<td>(Bútor ár + szövet felár) *1,5%</td>
							<td>-</td>
						</tr>
						<tr>
							<td>3</td>
							<td>(Bútor ár + szövet felár) *3%</td>
							<td>-</td>
						</tr>
						<tr>
							<td>4</td>
							<td>(Bútor ár + szövet felár) *4,5%</td>
							<td>-</td>
						</tr>
						<tr>
							<td>5</td>
							<td>(Bútor ár + szövet felár) *6%</td>
							<td>-</td>
						</tr>
						<tr>
							<td>6</td>
							<td>(Bútor ár + szövet felár) *7%</td>
							<td>-</td>
						</tr>
						<tr>
							<td>7</td>
							<td>(Bútor ár + szövet felár) *8%</td>
							<td>-</td>
						</tr>
						<tr>
							<td>8</td>
							<td>(Bútor ár + szövet felár) *9%</td>
							<td>-</td>
						</tr>
						<tr>
							<td>9</td>
							<td>(Bútor ár + szövet felár) *10%</td>
							<td>-</td>
						</tr>
						<tr>
							<td>10</td>
							<td>(Bútor ár + szövet felár) *11%</td>
							<td>ingyenes átkárpitozás max. 5 alkalommal, a szövet ára térítendő</td>
						</tr>
					</tbody>
				</table>
				<p>Ez a többletszolgáltatás kizárólag azon esetekre vonatkozik, amennyiben a bútor rendeltetésszerű használatából eredően következik be a vázszerkezet törése. A garanciákból kizárt esetek ( példaszerűen, nem kizárólagosan ) : nem rendeltetésszerű  használatból eredő sérülések, külső fizikai hatásból eredő sérülések, láb leszakadása, csavar kiszakadása vagy meglazulása folytán bekövetkezett károsodás, helytelen vagy többszöri összeszerelésből eredő sérülések, bútor elázása, stb.</p>
				<p>A szolgáltatásra vonatkozó igényt megrendeléskor jelezni kell és az kizárólag a díj  megfizetését követően vehető igénybe.</p>
				<p>10 év vázgarancia megvásárlása esetén a szolgáltatás tartalmaz továbbá a vásárlástól számított 10 éven belül maximum 5 alkalommal igénybe vehető átkárpitozási lehetőséget az alábbiak szerint:</p>
				<p>Vevő a szolgáltatás megvásárlásával jogosulttá válik arra, hogy igény szerinti időközzel – a maximális 5 alkalmat nem átlépve – az eladó által meghatározott önköltségi áron igényelje eladótól megvásárolt bútora átkárpitozását az igény bejelentésekor aktuálisan elérhető szövetválasztékból. A pontos díjat a minden esetben egyedileg készített árajánlat tartalmazza. Ezen szolgáltatás kizárólag kanapék és franciaágyak esetén vehető igénybe és csak a bútor teljes felülete tekintetében.</p>
				<p>A bútor el- és visszaszállításának költségeit a szolgáltatás díja nem tartalmazza.</p>
			</li>
			<li>
				<p>Ha a kiszállított bútor hibás vagy hiányos, abban az esetben Vevő ezt a helyszínen jelezni köteles, melyről a szállítók jegyzőkönyvet vesznek fel. Utólagos, jegyzőkönyv nélküli reklamációt Eladónak nem áll módjában elfogadni. A Vevőt az Eladó hibás teljesítése esetén megilletik mindazok a szavatossági, illetve / amennyiben a Vevő a Ptk. vonatkozó rendelkezései alapján fogyasztónak minősül / jótállási jogok, amelyeket számára a Ptk., illetve bruttó 10.000,- Ft-ot meghaladó bútorvásárlás esetén az egyes tartós fogyasztási cikkekre vonatkozó kötelező jótállásról szóló 151/2003. (IX.22.) Korm. rendelet lehetővé tesznek. A Dr. BALM matracok belső szerkezetére, illetve a matrac belső magjára és alakjára Eladó az átvételtől számított 6 éves garanciát vállal. A matrac külső burkolatára / huzatára / 1 éves jótállás vonatkozik. Vevő szavatossági, illetve jótállási igényeit a bútor átvételekor átadott jótállási jeggyel, ennek hiányában az ellenérték megfizetését igazoló bizonylattal ( számla) érvényesítheti. A Vevő panaszát&nbsp; személyesen vagy írásban nyújthatja be. Írásban benyújtottnak minősül az ajánlott postai küldeményben megküldött és kézbesített panaszlevél, a bemutatótermekben kihelyezett, kereskedelmi hatóság által hitelesített vásárlók könyvébe írt bejegyzés, továbbá az <a href="mailto:ugyintezes@vamosimilano.hu">ugyintezes@vamosimilano.hu</a> címre megküldött elektronikus levél. A szavatosság, illetve a jótállás nem terjed ki olyan hibákra, melyek az alábbiakra vezethetők vissza:</p>
				<ul class="mb-2">
					<li>ha a károsodás rendeltetésellenes (használati kezelési útmutatótól eltérő) használat következménye</li>
					<li>valamely alkatrész /szövet stb./ a természetes kopás folytán tönkremegy/elhasználódik, vagy</li>
					<li>a vevő nem tett eleget a kárenyhítési kötelezettségének / pl. a felismert hibákat időben nem kifogásolta /</li>
					<li>ha a hiba káresemény következménye</li>
					<li>ha a terméken a vásárló, harmadik személy, illetve nem a jótállásra kötelezett által megjelölt javítószolgálat átalakítást, javítást végzett</li>
					<li>olyan hibákra, amelyet a vásárló már az eladáskor ismert, vagy amelyre árengedményt kapott.</li>
				</ul>
				<p>Vevő tudomásul veszi, hogy annak előzetes elbírálásához, hogy a bejelentett hiba a jótállás körébe tartozik-e, eladó jogosult fényképfelvételt kérni a kifogásolt termékről, illetve annak egy részéről és vállalja ennek csatolását a jótállási igénybejelentéshez. Vevő tudomásul veszi továbbá, hogy amennyiben panasza nem tartozik a jótállás körébe, úgy az esetlegesen felmerült szállítási költségeket köteles megfizetni Eladó részére. Ezen költségekbe nem számítanak bele a felmerülő hiba okának megállapításával járó, azzal kapcsolatban felmerülő többletkiadások. Eladó köteles a garanciális problémákat megfelelő határidőn belül orvosolni. A megfelelő határidőre a Polgári Törvénykönyv rendelkezései irányadóak. Vevő tudomásul veszi, és jelen ÁSZF aláírásával elfogadja, hogy a megrendelt bútor egyedi termék, ami a vevő által igényelt paramétereknek megfelelően kerül legyártásra. Az esetleges javítás/csere teljesítését eladó – figyelembe véve a termék fenti tulajdonságait - törekszik a Ptk. 6:159.§ (4) bekezdésében foglalt megfelelő határidőn belül elvégezni.</p>
			</li>
			<li>
				<p>Amennyiben Vevő a bútor átvétele előtt az értesítési címét megváltoztatja, köteles erről Eladót tájékoztatni. Eladó jognyilatkozatai a Vevővel szemben hatályosak, amennyiben azokat Eladó az utoljára megadott értesítési címre küldte ki.</p>
			</li>
			<li>
				<p>Jelen ÁSZF-ben nem szabályozott kérdésekben a Polgári Törvénykönyv és a Magyar Jog vonatkozó rendelkezéseit kell alkalmazni. Vevő jelen ÁSZF aláírásával kifejezetten kijelenti, hogy az általános szerződési feltételeket teljes terjedelmében megismerte, arról részletes tájékoztatást kapott, és az abban foglaltakat elfogadta, különös tekintettel a megrendelés hatályosulásával kapcsolatos 5. pontban rögzített rendelkezésekre, továbbá a szerződésszegés vonatkozásában a 6. és 9. pontban rögzített jogkövetkezmények (kötbér, tárolási díj) alkalmazására.</p>
			</li>
		</ol>
	</div>
	<div class="col-lg-4">
		<h3 class="h4-responsive text-uppercase title">Korábbi ÁSZF verziók</h3>
		<p>A jelenlegi és a korábbi ÁSZF verziók letölthetőek PDF formátumban:</p>
		<ul class="list-unstyled">
			<li>
				<a href="{{ asset('/media/pdf/aszf_20190409.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2019. 04. 09.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20190308.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2019. 03. 08.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20190213.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2019. 02. 13.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20181126.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2018. 11. 26.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20180823.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2018. 08. 23.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20180815.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2018. 08. 15.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20180807.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2018. 08. 07.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20180628.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2018. 06. 28.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20180503.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2018. 05. 03.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20180323.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2018. 03. 23.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20180307.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2018. 03. 07.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20180122.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2018. 01. 22.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20180111.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2018. 01. 11.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20170720.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2017. 07. 20.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20170705.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2017. 07. 05.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20170629.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2017. 06. 29.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20170619.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2017. 06. 19.</a>
			</li>
			<li>
				<a href="{{ asset('/media/pdf/aszf_20170601.pdf') }}"><i class="fal fa-file-pdf fa-fw mr-2"></i>2017. 06. 01.</a>
			</li>
		</ul>
	</div>
</div>