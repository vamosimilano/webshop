@extends('layouts.default')
@section('content')
<?php
if( isset($headversion) && $headversion == "newhead" ){
?>
<div class="container">
	<section class="row">
		<article class="col-12 my-5">
			<header>
				<hgroup>
					<h1 class="h2-responsive title-center text-uppercase mb-5">{{ $cms->title }}</h1>
				</hgroup>
			</header>
			<div class="content">
			<?php
			/*
			if ( $cms->url == t('hazhozszallitas','url') ) {
			?>
				{{ $cms->description }}
				@include('calculators.shipping')
			<?php
			}
			*/
			if( $cms->url == t('aszf','url') ) {
			?>
				@include('cms.aszf', ['subtitle'=>$cms->subtitle])
			<?php
			}
			elseif( $cms->url == t('adatvedelem','url') ) {
			?>
				@include('cms.adatvedelem', ['subtitle'=>$cms->subtitle])
			<?php
			}
			else {
			?>
				{{ $cms->description }}
			<?php
			}
			?>
			</div>
		</article>
	</section>
</div>

<?php
}//if
else {
?>
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ $cms->title }}</strong>
                        </li>
            </ul>
</div>

<main class="page-main" id="maincontent">

<div class="entry-content min-height">


    <div class="row">
        <div class="row-wrapper container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">{{ $cms->title }}</h3>
                    {{ $cms->description }}
                </div>
            </div>
        </div>
    </div>
    @if ($cms->url == t('hazhozszallitas','url'))
        @include('calculators.shipping')
    @endif



</div>
</div></div>
</main>
<?php
}//else
?>
@stop
@section('footer_js')
@append