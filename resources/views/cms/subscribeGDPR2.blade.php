<form id="subForm" class="js-cm-form" action="https://www.createsend.com/t/subscribeerror?description=" method="post" data-id="{{t('newlettersubscribeid')}}">
	<div class="form-group">
		<input id="fieldName" class="form-control" name="cm-name" type="text" placeholder="{{ t('Név') }}" />
	</div>
	<div class="form-group">
		<input id="fieldEmail" class="js-cm-email-input form-control" name="{{t('newlettersubscribename')}}" type="email" placeholder="{{ t('E-mail') }}" required /> 
	</div>
	<div class="form-check">
		<label class="form-check-label"><input id="fieldok" class="form-check-input js-cm-email-input" type="checkbox" required />{{t('Feliratkozasengedélyszöveg')}}</label>
	</div>
	<div class="form-group text-right">
		<button class="action subscribe primary btn btn-primary" type="submit">{{t('Feliratkozás')}}</button> 
	</div>
</form>
<script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>