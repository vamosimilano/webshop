@extends('layouts.default')



@section('content')









<main class="page-main" id="maincontent">

<div class="page-title-wrapper">

    <h1 class="page-title">

        <span data-ui-id="page-title-wrapper" class="base">{{ t('Kapcsolat') }}</span>    </h1>

</div>



<div class="columns">

<div class="column main">

    <div class="row">

        <div class="col-sm-8">

        <form  method="post" id="contact-form" action="{{ action('ContactController@contactSend') }}" class="form contact" >

        <fieldset class="fieldset">

            <legend class="legend"><span>{{ t('Írjon Nekünk üzenetet') }}</span></legend><br>

            <fieldset class="fieldset row">

                <div class="fields col-sm-6">

                    <div class="field subject required">

                        <label for="subject" class="label"><span>{{ t('Tárgy') }}</span></label>

                        <div class="control">

                            <select name="subject" id="subject" class="input-select" required="">

                                <option value="">{{ t('Kérem válasszon!') }}</option>

                                <option value="1" {{(Input::old('subject') == 1 ? 'selected' : '' )}}>{{ t('subject_1') }}</option>

                                <option value="2" {{(Input::old('subject') == 2 ? 'selected' : '' )}}>{{ t('subject_2') }}</option>

                                <option value="3" {{(Input::old('subject') == 3 ? 'selected' : '' )}}>{{ t('subject_3') }}</option>

                                <option value="4" {{(Input::old('subject') == 4 ? 'selected' : '' )}}>{{ t('subject_4') }}</option>

                                <option value="5" {{(Input::old('subject') == 5 ? 'selected' : '' )}}>{{ t('subject_5') }}</option>

                                <option value="6" {{(Input::old('subject') == 6 ? 'selected' : '' )}}>{{ t('subject_6') }}</option>

                                <option value="7" {{(Input::old('subject') == 7 ? 'selected' : '' )}}>{{ t('subject_7') }}</option>

                                <option value="8" {{(Input::old('subject') == 8 ? 'selected' : '' )}}>{{ t('subject_8') }}</option>

                            </select>



                        </div>

                    </div>



                    <div class="field name required">

                        <label for="name" class="label"><span>{{ t('Az Ön neve') }}</span></label>

                        <div class="control">

                            <input type="text"  class="input-text" value="{{ Input::old('name') }}"  id="name" name="name"  aria-required="true" required="">

                        </div>

                    </div>



                    <div class="field telephone">

                        <label for="telephone" class="label"><span>{{ t('Telefonszám') }}</span></label>

                        <div class="control">

                            <input type="text" class="input-text" value="{{ Input::old('telephone') }}"  id="telephone" name="telephone">

                        </div>

                    </div>

             </div>

             <div class="fields col-sm-6">



                    <div class="field toaddress required">

                        <label for="toaddress" class="label"><span>{{ t('Címzett') }}</span></label>

                        <div class="control">

                            <select name="toaddress" id="toaddress" class="input-select" required="">

                                <option value="">{{ t('Kérem válasszon!') }}</option>

                                <option value="1" {{(Input::old('toaddress') == 1 ? 'selected' : '' )}}>{{ t('London') }}</option>

                            </select>



                        </div>

                    </div>

                    <div class="field email required">

                        <label for="email" class="label"><span>{{ t('E-mail címe') }}</span></label>

                        <div class="control">

                            <input type="email"  class="input-text" value="{{ Input::old('email') }}"  id="email" name="email" aria-required="true" required="">

                        </div>

                    </div>

            </div>

            <div class="clearfix"></div>

            <br>

            <div class="col-lg-12">

            <div class="field comment required">

                <label for="comment" class="label"><span>{{ t('Üzenete') }}</span></label>

                <div class="control">

                    <textarea rows="7" cols="3" class="input-text"  id="comment" name="comment" aria-required="true">{{ Input::old('comment') }}</textarea>

                </div>

                <br>

            </div>

            <div class="col-lg-12 text-center">

                <button class="action submit primary margin-top2" title="{{ t('Elküldés') }}" type="submit">

                    <span>{{ t('Elküldés') }}</span>

                </button>



            </div>

            {{ csrf_field() }}

                        </div>

            </fieldset>

        </fieldset>



    </form>

    </div>

		<div class="col-sm-4 contact-info">



			<fieldset class="fieldset">



            <legend class="legend"><span>{{ t('Londoni üzletünk', 'contact') }}</span></legend><br>



            <div>{{ t('27 Cricklewood Broadway, NW2 3JX', 'contact') }}<br>

            {{ t('Telefonszám:', 'contact') }} <a href="{{ t('london-tel_href', 'contact') }}" title="{{ t('Hívás most!') }}">{{ t('london-phone', 'contact') }}</a><br>

            {{ t('E-mail:', 'contact') }} <a href="{{ t('london-mail_href', 'contact') }}" title="{{ t('Írjon Nekünk üzenetet') }}">{{ t('london-email', 'contact') }}</a><br>

            {{ t('Nyitva tartás:', 'contact') }}<br>

            {{ t('Hétfő-Szombat: 10:00 - 19:00', 'contact') }}<br>

            {{ t('Vasárnap: 12:00 - 18:00', 'contact') }}<br>

<?php
	if(date("YmdHi")<201805290000):
?>

			<p style="color:#CD7126"><strong>

			7th May, Monday (Early May bank holiday) : closed<br>

			28th may, Monday (Spring bank holiday) : closed<br>

			</strong></p>
<?php 
	endif;
?>	

            <a href="{{ t('fb_link', 'url') }}" target="_fb">{{ t('fb_link', 'url') }}</a><br>

			<br><br>

            </div>

            </fieldset>

			

        </div>

		

        <div class="col-sm-4 contact-info">



            <fieldset class="fieldset">



            <legend class="legend"><span>{{ t('Kapcsolat adatok') }}</span></legend><br>



            <div>{{ t('factory_name', 'contact') }}<br>

			<br><b>

			Owner of Franchise Rights:<br>

			Vamosi SRL,<br>

			VIA LORENZO MASCHERONI, 31, 20145, MILANO<br>

			<a href="http://vamosi.it" target="_blank">http://vamosi.it</a><br>

			</b><br>

            {{ t('Központi telefonszám:')." ".t('phone_number') }}

            </div>

            </fieldset>



        </div>

		

        

    </div>

</div>

</div>



<div class="container">

    

    

</div>



</main>

<div id="big_map">

</div>











@stop



@section('footer_js')

<script src="//maps.googleapis.com/maps/api/js?v=3.17&key={{getConfig('maps_api_key')}}" type="text/javascript"></script>

<script src="{{asset('js/contact_en.js')}}"></script>

<script type="text/javascript">

    google.maps.event.addDomListener(window, 'load', initBigMap);

    /*

    google.maps.event.addDomListener(window, 'load', initialize_bp);

    google.maps.event.addDomListener(window, 'load', initialize_deb);

    google.maps.event.addDomListener(window, 'load', initialize_vesz);

    */

</script>



@append

