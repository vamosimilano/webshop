@extends('layouts.default')



@section('content')









<main class="page-main" id="maincontent">

<div class="page-title-wrapper">

    <h1 class="page-title">

        <span data-ui-id="page-title-wrapper" class="base">{{ t('Kapcsolat') }}</span>    </h1>

</div>



<div class="columns">

<div class="column main">

    <div class="row">

        <div class="col-sm-8">

        <form  method="post" id="contact-form" action="{{ action('ContactController@contactSend') }}" class="form contact" >

        <fieldset class="fieldset">

            <legend class="legend"><span>{{ t('Írjon Nekünk üzenetet') }}</span></legend><br>

            <fieldset class="fieldset row">

                <div class="fields col-sm-6">

                    <div class="field subject required">

                        <label for="subject" class="label"><span>{{ t('Tárgy') }}</span></label>

                        <div class="control">

                            <select name="subject" id="subject" class="input-select" required="">

                                <option value="">{{ t('Kérem válasszon!') }}</option>

                                <option value="1" {{(Input::old('subject') == 1 ? 'selected' : '' )}}>{{ t('subject_1') }}</option>

                                <option value="2" {{(Input::old('subject') == 2 ? 'selected' : '' )}}>{{ t('subject_2') }}</option>

                                <option value="3" {{(Input::old('subject') == 3 ? 'selected' : '' )}}>{{ t('subject_3') }}</option>

                                <option value="4" {{(Input::old('subject') == 4 ? 'selected' : '' )}}>{{ t('subject_4') }}</option>

                                <option value="5" {{(Input::old('subject') == 5 ? 'selected' : '' )}}>{{ t('subject_5') }}</option>

                                <option value="6" {{(Input::old('subject') == 6 ? 'selected' : '' )}}>{{ t('subject_6') }}</option>

                                <option value="7" {{(Input::old('subject') == 7 ? 'selected' : '' )}}>{{ t('subject_7') }}</option>

                                <option value="8" {{(Input::old('subject') == 8 ? 'selected' : '' )}}>{{ t('subject_8') }}</option>

                            </select>



                        </div>

                    </div>



                    <div class="field name required">

                        <label for="name" class="label"><span>{{ t('Az Ön neve') }}</span></label>

                        <div class="control">

                            <input type="text"  class="input-text" value="{{ Input::old('name') }}"  id="name" name="name"  aria-required="true" required="">

                        </div>

                    </div>



                    <div class="field telephone">

                        <label for="telephone" class="label"><span>{{ t('Telefonszám') }}</span></label>

                        <div class="control">

                            <input type="text" class="input-text" value="{{ Input::old('telephone') }}"  id="telephone" name="telephone">

                        </div>

                    </div>

             </div>

             <div class="fields col-sm-6">



                    <div class="field toaddress required">

                        <label for="toaddress" class="label"><span>{{ t('Címzett') }}</span></label>

                        <div class="control">

                            <select name="toaddress" id="toaddress" class="input-select" required="">

                                <option value="">{{ t('Kérem válasszon!') }}</option>

                                <option value="1" {{(Input::old('toaddress') == 1 ? 'selected' : '' )}}>{{ t('Budapest') }}</option>

                                <? /*

								<optin value="2" {{(Input::old('toaddress') == 2 ? 'selected' : '' )}}>{{ t('Szeged') }}</option>

								*/ ?>

                                <option value="4" {{(Input::old('toaddress') == 4 ? 'selected' : '' )}}>{{ t('Debrecen') }}</option>

								@if (date("YmdHi")<201803311830)

								<option value="5" {{(Input::old('toaddress') == 5 ? 'selected' : '' )}}>{{ t('Miskolc') }}</option>

								@endif

                            </select>



                        </div>

                    </div>

                    <div class="field email required">

                        <label for="email" class="label"><span>{{ t('E-mail címe') }}</span></label>

                        <div class="control">

                            <input type="email"  class="input-text" value="{{ Input::old('email') }}"  id="email" name="email" aria-required="true" required="">

                        </div>

                    </div>

            </div>

            <div class="clearfix"></div>

            <br>

            <div class="col-lg-12">

            <div class="field comment required">

                <label for="comment" class="label"><span>{{ t('Üzenete') }}</span></label>

                <div class="control">

                    <textarea rows="7" cols="3" class="input-text"  id="comment" name="comment" aria-required="true">{{ Input::old('comment') }}</textarea>

                </div>

                <br>

            </div>

            <div class="col-lg-12 text-center">

                <button class="action submit primary margin-top2" title="{{ t('Elküldés') }}" type="submit">

                    <span>{{ t('Elküldés') }}</span>

                </button>



            </div>

            {{ csrf_field() }}

                        </div>

            </fieldset>

        </fieldset>



    </form>

    </div>

        <div class="col-sm-4 contact-info">



            <fieldset class="fieldset">



            <legend class="legend"><span>{{ t('Kapcsolat adatok') }}</span></legend><br>



            <div>{{ t('factory_name', 'contact') }}<br>

			<br><b>

			Franchise jogok tulajdonosa:<br>

			Vamosi SRL,<br>

			VIA LORENZO MASCHERONI, 31, 20145, MILANO<br>

			<a href="http://vamosi.it" target="_blank">http://vamosi.it</a><br>

			</b><br>

            {{ t('Központi telefonszám:')." ".t('phone_number') }}

            </div>

            </fieldset>



        </div>

    </div>

</div>

</div>



<div class="container">

<?php
	if(date("YmdHi")<201805220000):
?>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6 text-center" style="background-color:#CD7126; color:#fff;border-radius: 5px;">
			<h3>Májusi nyitvatartás</h3>
			<p class="lead">Pünkösdi hosszú hétvége</p>
			<p>Május 20., vasárnap - ZÁRVA</p>
			<p>Május 21., hétfő - ZÁRVA</p>
		</div>
	</div>
<?php 
	endif;
?>	
	
	
    <div class="row">

        <div class="col-lg-6">



            <div class="text-center" id="content-bp">

            <h3>{{ t('Budapesti üzletünk', 'contact') }}:</h3>

            <p>{{ t('1119 Budapest Petzvál József utca 46-48.', 'contact') }}<br>

            {{ t('Telefonszám: +36-1-353-9410', 'contact') }}<br>

            {{ t('E-mail: ertekesites@vamosimilano.hu', 'contact') }}<br>

            {{ t('Nyitva tartás:', 'contact') }}<br>

			Hétfő-Szombat: 10:00 - 18:30<br>

			Vasárnap: 11:00 - 18:00 <br>
			<strong>{{ t('Ügyfélszolgálat nyitvatartása') }}:</strong><br>
			{{ t('Hétfő-Szombat: 9:00-17:00') }}<br>
			{{ t('Vasárnap: 11:00-18:00') }}<br>

            <a href="{{ t('fb_link', 'url') }}" target="_fb">{{ t('fb_link', 'url') }}</a><br>

            </p>

            </div>

            <br >

        </div>
		
		<div class="col-lg-6">



            <div class="text-center" id="content-csepel">

            <h3>{{ t('Csepeli outlet áruház', 'contact') }}:</h3>

            <p>{{ t('1211 Budapest, II. Rákóczi Ferenc út 107-115', 'contact') }}<br>
			{{t('(Bejárat a lakópark Karácsony Sándor és Teller Ede úti sarkánál)')}}
            {{ t('Telefonszám: +36-1-353-9410', 'contact') }}<br>

            {{ t('E-mail: ertekesites@vamosimilano.hu', 'contact') }}<br>

            {{ t('Nyitva tartás:', 'contact') }}<br>

			Hétfő 09:00 - 18:00<br>
			Kedd-Szerda : ZÁRVA<br>
			Csütörtök-Vasárnap: 09:00 - 18:00<br>

			<strong>{{ t('Ügyfélszolgálat nyitvatartása') }}:</strong><br>
			{{ t('Hétfő-Péntek: 9:00-17:00') }}<br>


            </p>

            </div>

            <br >

        </div>
<?php /*
        <div class="col-lg-6">



			<div class="text-center" id="content-deb">
			<h3>{{ t('Debreceni üzletünk', 'contact') }}:</h3>
			<p>{{ t('4024 Debrecen Egyetem Sugárút 11/A', 'contact') }}<br>
			{{ t('Telefonszám: +36-1-353-9410', 'contact') }}<br>
			{{ t('E-mail: ertekesitesdb@vamosimilano.hu', 'contact') }}<br>
			{{ t('Nyitva tartás:', 'contact') }}<br>
			Hétfő-Kedd: 10:00 - 18:30<br>
			Szerda: ZÁRVA<br>
			Csütörtök-Szombat: 10:00 - 18:30<br>
			Vasárnap: 11:00 - 18:00 <br>
			{{ t('Ügyfélszolgálat: 9:00-17:00', 'contact') }}<br>

            <a href="{{ t('fb_link', 'url') }}" target="_fb">{{ t('fb_link', 'url') }}</a><br>

            </p>

            </div>

            <br>

        </div>
*/ ?>
    </div>

    <div class="clearfix"></div>

    <div class="row">

	

	@if (date("YmdHi")<201803311830)

	

	<div class="col-lg-6">

		

            <div class="text-center" id="content-miskolc">

            <h3>{{ t('Miskolci üzletünk', 'contact') }}:</h3>

            <p>{{ t('3526 Miskolc, Zsolcai kapu 1.', 'contact') }}<br>

            {{ t('Telefonszám: +36-1-353-9410', 'contact') }}<br>

            {{ t('E-mail: ertekesitesm@vamosimilano.hu ', 'contact') }}<br>

            {{ t('Nyitva tartás', 'contact') }}:<br>

			Hétfő-Szombat: 10:00 - 18:30<br>

			Vasárnap: 11:00 - 18:00 <br>

			{{ t('Ügyfélszolgálat: 9:00-17:00', 'contact') }}<br>

            <a href="{{ t('fb_link', 'url') }}" target="_fb">{{ t('fb_link', 'url') }}</a><br>

            </p>

            </div>



    </div>

	

	@endif

	

	<div class="col-lg-6">



            <div class="text-center" id="content-raktar">

            <h3>{{ t('Központi raktár', 'contact') }}:</h3>

            <p>{{ t('1211 Budapest, Weiss Manfréd út 14.', 'contact') }}<br>

			A raktárból a vevők hétköznapokon 8:00 és 16:00 között vehetik át a bútorokat.<br>

			</p>

            </div>



    </div>
	
	<div class="col-lg-6">
		<div class="text-center" id="content-szeged">
			<h3>Szeged bútorátvételi pont:</h3>
			<p>6728, Szeged, Vásár utca 16.<br>
			(régebben 6728, Szeged, Cserje sor 9.)<br>
			Zoll Platz Kft. telephelye<br>
			A raktár GPS koordinátái: 46.268548, 20.118107<br>
			Nyitvatartás: Hétköznap, 8:30-15:30</p>
			<?php if(date("YmdHi")<201805091200): ?>
			<p style="color: #CD7126">A szegedi átvételi pont 2018 május 9.-én, szerdán <br>10:00 és 12:00 között <strong>ZÁRVA</strong> tart!</p>
			<?php endif; ?>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="text-center" id="content-miskolc">
			<h3>Miskolc bútorátvételi pont:</h3>
			<p>3561, Felsőzsolca, Epres utca 1.<br>
			(Road 66 Kft. telephelye)<br>
			Nyitvatartás: Hétköznap, 8:00-15:30</p>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="text-center" id="content-gyor">
			<h3>Győr bútorátvételi pont:</h3>
			<p>9027 Győr, Hűtőház út 25.<br>
			(Intertranscoop Nemzetközi szállítmányozó és Kereskedelmi Kft. telephelye)<br>
			Nyitva tartás: Hétköznap, 8:00-16:00</p>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="text-center" id="content-gyor">
			<h3>Debrecen bútorátvételi pont:</h3>
			<p>4031 Debrecen, Vadvirág utca 19. (17261/25 hrsz.)<br>
			(Centrum Raktár Telephely Zrt. telephelye)<br>
			Nyitva tartás: Hétköznap, 8:00-16:00</p>
		</div>
	</div>	
</div>



</main>

<div id="big_map">

</div>











@stop



@section('footer_js')

<script src="//maps.googleapis.com/maps/api/js?v=3.17&key={{getConfig('maps_api_key')}}" type="text/javascript"></script>

@if (date("YmdHi")<201803311830)

<script src="{{asset('js/contact_v2.js')}}"></script>

@else

<script src="{{asset('js/contact_v6.js')}}"></script>

@endif

<script type="text/javascript">

    google.maps.event.addDomListener(window, 'load', initBigMap);

    /*

    google.maps.event.addDomListener(window, 'load', initialize_bp);

    google.maps.event.addDomListener(window, 'load', initialize_deb);

    google.maps.event.addDomListener(window, 'load', initialize_vesz);

    */

</script>



@append

