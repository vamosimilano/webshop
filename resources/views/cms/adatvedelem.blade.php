<div class="row">
	<div class="col-md-10">
		<p>A <strong>WORE HUNGARY Korlátolt Felelősségű Társaság</strong> (székhely: 1119 Budapest, Petzvál József utca 46-48. cégjegyzékszám: 01-09-944100; adószám: 22905192-2-43; elektronikus elérhetőség: <a href="mailto:info@vamosimilano.hu">info@vamosimilano.hu</a>; honlap címe: <a href="https://vamosimilano.hu">https://vamosimilano.hu</a> (a továbbiakban: <strong>honlap</strong>); telefonszám: +36-1-353-9410; a továbbiakban: <strong>Adatkezelő</strong>) a vonatkozó, személyes adatok védelmét meghatározó jogszabályi rendelkezésekkel, így különösen a GDPR-ral  összhangban az alábbiakban tájékoztatja a  szolgáltatásait igénybe vevőket az általa alkalmazott adatkezelésről.</p>
		<ol class="my-4" data-spy="scroll" data-target="#mdb-scrollspy-ex">
			<li id="adatkezeles_bemutatasa">
				<p><strong>Adatkezelés bemutatása</strong></p>
				<p>Az Adatkezelő kiemelt figyelmet fordít a személyes adatok védelmére és mindenkor ügyel a tisztességes és átlátható adatkezelés biztosítására, melynek alapvető követelménye az adatok kezelésére vonatkozó megfelelő tájékoztatás nyújtása. Jelen Adatkezelési Tájékoztató többek között információt nyújt az Adatkezelő szolgáltatásának nyújtása során történő adatkezeléséről: a kezelt adatok megismerésének forrásairól, köréről, az adatkezelés jogalapjáról, céljáról és időtartamáról, a személyes adatokkal kapcsolatos jogokról és az azok közötti választási lehetőségekről, illetve tartalmazza azokat az elérhetőségeket is, amelyeken választ kaphat a személyes adatok kezelésével érintett az Adatkezelő adatvédelmi folyamataival kapcsolatos kérdéseire.</p>
				<p>Az Adatkezelő eredeti olasz bútorok tervezésével, forgalmazásával és magyarországi kereskedelmével foglalkozó vállalat. Adatkezelő adatkezelési folyamatai az alábbiak: honlapon történő regisztráció, honlapon webshop üzemeltetése és az Adatkezelő termékei megrendelésének teljesítése, ideértve az annak során felmerülő igényérvényesítéssel kapcsolatos adatkezelési folyamatokat, az Adatkezelő működésében felmerülő munkaerő-hiány betöltése, bútortervező mobil applikáción (a továbbiakban: <strong>applikáció</strong>) letöltésének és használatának a személyes adatok kezelésével érintettek részére történő lehetővé tétele, a velük való kapcsolattartás, számukra hírlevél küldése, valamint sütik honlapon történő alkalmazása, továbbá Adatkezelő jogi kötelezettségei körében a személyes adatok kezelésével érintettek panaszainak kivizsgálása és a bizonylatmegőrzési kötelezettség teljesítése.</p>
				<p>Az Adatkezelő adatkezelésének könnyebb áttekintése céljából az alábbiakban összefoglaló táblázatokat tesz közzé az egyes adatkezelési folyamatokról. Részletes tájékoztatást a teljes Adatkezelési Tájékoztató nyújt a személyes adatok kezelésével érintett számára.</p>
				<table class="table table-striped" id="tablazat_regisztracio">
					<thead class="elegant-color white-text">
						<tr>
							<th colspan="2" scope="row">
								Honlapon történő regisztrációval kapcsolatos adatkezelés összefoglalása
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">Adatkezelés célja</td>
							<td scope="col">Honlapon történő regisztráció, felhasználói profil létrehozása</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés jogalapja</td>
							<td scope="col">Érintett hozzájárulása [GDPR 6. cikk (1) bekezdés a) pont]; a hozzájárulás bármikor visszavonható <sup>1</sup></td>
						</tr>
						<tr>
							<td scope="row">Kezelt adatok köre</td>
							<td scope="col">
								<ul>
									<li>Érintett vezetékneve</li>
									<li>Érintett keresztneve</li>
									<li>Érintett e-mail címe</li>
									<li>Érintett jelszava</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés időtartama</td>
							<td scope="col">Érintett hozzájárulásának visszavonása</td>
						</tr>
						<tr>
							<td scope="row">Adatfeldolgozók</td>
							<td scope="col">
								<p>
									<strong>Érték-Rendszerház Kft.</strong> (székhely: 1115 Ballagi M. u. 4.)
									<br>adatfeldolgozás célja: informatikai feltételek biztosítása, webshop és applikáció üzemeltetése
								</p>
							</td>
						</tr>
					</tbody>
				</table>
				<p><sup>1</sup> <small>A természetes személyeknek a személyes adatok kezelése tekintetében történő védelméről és az ilyen adatok szabad áramlásáról, valamint a 95/46/EK rendelet hatályon kívül helyezéséről szóló európai parlament és a tanács (EU) 2016/679 rendelete - általános adatvédelmi rendelet (GDPR); A GDPR szövege ezen a <a href="http://eur-lex.europa.eu/legal-content/HU/TXT/ELI/?eliuri=eli:reg:2016:679:oj" target="_blank">linken</a> keresztül elérhető az Európai Unió valamennyi hivatalos nyelvén</small></p>
				<table class="table table-striped" id="tablazat_szolg_megrendel">
					<thead class="elegant-color white-text">
						<tr>
							<th colspan="2" scope="row">
								Adatkezelő által nyújtott szoltáltatással, termékeinek megrendelésével kapcsolatos adatkezelés összefoglalása
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">Adatkezelés célja</td>
							<td scope="col">
								<p>Adatkezelő termékei Érintett általi megrendelésének teljesítése, az ezzel kapcsolatban felmerülő valamennyi körülmény:</p>
								<ul>
									<li>a honlapon az Érintett általi regisztráció,</li>
									<li>felhasználói fiókba belépés,</li>
									<li>megrendelés,</li>
									<li>vételár megfizetésének teljesítése,</li>
									<li>termék szállítása,</li>
									<li>Érintett fogyasztói igényeinek érvényesítése</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés jogalapja</td>
							<td scope="col">Adatkezelő és az Érintett (vásárló) közötti szerződés teljesítése [GDPR 6. cikk (1) bekezdés b) pont]</td>
						</tr>
						<tr>
							<td scope="row">Kezelt adatok köre</td>
							<td scope="col">
								<p>Megrendelés teljesítése során kezelt adatok:</p>
								<ul>
									<li>Érintett vezetékneve</li>
									<li>Érintett keresztneve</li>
									<li>Érintett telefonszáma</li>
									<li>Érintett rendeléshez rögzített megjegyzése (amennyiben azt az Érintett rögzít megjegyzést; számla igénylése esetén: számlázási név, adószám, cím (irányítószám, város, utca, házszám)</li>
									<li>házhozszállítás esetén az Érintett vezetékneve és utóneve, lakcíme és a lakcímen lift igénybevételi lehetőségének ténye</li>
									<li>kuponkód</li>
									<li>Facebook profil importálása esetén szolgáltatott adatok</li>
								</ul>
								<p class="mt-2">Érintett fogyasztói jogainak érvényesítése esetén kezelt adatok:</p>
								<ul>
									<li>Elállás, jótállási igény vagy felmondás egyedi azonosítószáma</li>
									<li>Elállás, jótállási igény, vagy felmondás előterjesztésének helye és ideje</li>
									<li>Elállás, jótállási igény vagy felmondás előterjesztésének módja</li>
									<li>Érintett által beküldött iratok, dokumentumok, egyéb bizonyítékok jegyzéke</li>
									<li>Elállás, jótállási igény vagy felmondás tartalma</li>
									<li>Jegyzőkönyv tartalma</li>
									<li>Elállásra, jótállási igényre vagy felmondásra adott válasz</li>
									<li>Termék lényeges adatai (méret, cikkszám, ár, megnevezés, mennyiség)</li>
									<li>Érintett neve</li>
									<li>Érintett címe</li>
									<li>Érintett telefonszáma</li>
									<li>Számlaszám</li>
									<li>Szállítólevél száma</li>
									<li>Rendelésszám</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés időtartama</td>
							<td scope="col">Szolgáltatás teljes időtartama és azt követő fogyasztóvédelmi, valamint általános polgári jogi igények érvényesíthetőségének ideje: 5 év</td>
						</tr>
						<tr>
							<td scope="row">Adatfeldolgozók</td>
							<td scope="col">
								<p>
									<strong>Érték-Rendszerház Kft.</strong> (székhely: 1115 Ballagi M. u. 4.)
									<br>adatfeldolgozás célja: informatikai feltételek biztosítása, webshop és applikáció üzemeltetése
								</p>
								<p>
									<strong>Kocka-Trans Kft.</strong> (székhely: 1201 Vágóhíd u. 14.)
									<br>adatfeldolgozás célja: megvásárolt termékek kiszállítása
								</p>
								<p>
									<strong>IFFM Technology Kft.</strong> (székhely: 1136 Tátra u. 5. A ép.)
									<br>adatfeldolgozás célja: megrendelt termékek gyártása, raktározása, garanciális igények kezelése
								</p>
								<p>
									<strong>Your Accountant Kft.</strong> (székhely: 1119 Petzvál J. u. 46-48.)
									<br>adatfeldolgozás célja: pénzügyi-számviteli feladatok ellátása
								</p>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-striped" id="tablazat_hirlevel">
					<thead class="elegant-color white-text">
						<tr>
							<th colspan="2" scope="row">
								Adatkezelő és partnerei által reklámot, akciót, promóciót, ajánlatot tartalmazó hírlevél küldéssel kapcsolatos adatkezelés összefoglalása
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">Adatkezelés célja</td>
							<td scope="col">
								<p>Adatkezelő reklámjait tartalmazó hírlevelek küldése</p>
							</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés jogalapja</td>
							<td scope="col">Érintett hozzájárulása [GDPR 6. cikk (1) bekezdés a) pont és a gazdasági reklámtevékenység alapvető feltételeiről és egyes korlátairól 2008. évi XLVIII. törvény 6. § (1) bekezdés]; a hozzájárulás bármikor visszavonható</td>
						</tr>
						<tr>
							<td scope="row">Kezelt adatok köre</td>
							<td scope="col">
								<ul>
									<li>Érintett vezetékneve</li>
									<li>Érintett keresztneve</li>
									<li>Érintett e-mail címe</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés időtartama</td>
							<td scope="col">Hozzájárulás visszavonásáig</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-striped" id="tablazat_karrier">
					<thead class="elegant-color white-text">
						<tr>
							<th colspan="2" scope="row">
								Adatkezelő által toborzási, munkaerő-hiány betöltése céljával kapcsolatos adatkezelés összefoglalása
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">Adatkezelés célja</td>
							<td scope="col">
								<p>Adatkezelő működésében felmerülő munkaerő-hiány betöltése vagy az állásajánlatra jelentkező pályázatának elutasítása esetén, illetve a pályázat pozitív elbírálása ellenére amennyiben a munkakört nem az Adatkezelő által kiválasztott pályázó tölti be, vagy az Adatkezelő által meghirdetett állásajánlat hiányában Adatkezelő részére történő megküldésekor a(z) (ismételt) kapcsolatfelvétel</p>
							</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés jogalapja</td>
							<td scope="col">Érintett hozzájárulása [GDPR 6. cikk (1) bekezdés a) pont]; a <em>hozzájárulás bármikor visszavonható</em></td>
						</tr>
						<tr>
							<td scope="row">Kezelt adatok köre</td>
							<td scope="col">
								<ul>
									<li>Érintett vezetékneve</li>
									<li>Érintett keresztneve</li>
									<li>Érintett e-mail címe</li>
									<li>Érintett telefonszáma</li>
									<li>adott esetben, az Érintett fizetési igénye</li>
									<li>valamint az Érintett által az állásajánlatra jelentkezés céljából önéletrajzában rendelkezésre bocsátott egyéb személyes adatok</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés időtartama</td>
							<td scope="col">
								<ol type="a">
									<li>Munkakör betöltéséig, vagy</li>
									<li>A pályázati elutasítása esetén, illetve a pályázat pozitív elbírálása ellenére amennyiben a munkakört nem az Adatkezelő által kiválasztott pályázó tölti be, az ismételt kapcsolatfelvétel céljából további kettő évig; vagy</li>
									<li>Az Érintett önéletrajzának Adatkezelő által meghirdetett állásajánlat hiányában Adatkezelő részére történő megküldése esetén az Adatkezelő működésében jelentkező munkaerő-hiány megszüntetése érdekében, az ismételt kapcsolatfelvétel céljából kettő évig</li>
								</ol>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-striped" id="tablazat_app">
					<thead class="elegant-color white-text">
						<tr>
							<th colspan="2" scope="row">
								Adatkezelő által üzemeltetett applikáció használatával kapcsolatos adatkezelés összefoglalása
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">Adatkezelés célja</td>
							<td scope="col">Adatkezelő által üzemeltetett applikáció Érintett általi letöltése, használata az Érintett egyedi igénye szerint kialakított bútor megtervezése érdekében</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés jogalapja</td>
							<td scope="col">Érintett hozzájárulása [GDPR 6. cikk (1) bekezdés a) pont]; a <em>hozzájárulás bármikor visszavonható</em></td>
						</tr>
						<tr>
							<td scope="row">Kezelt adatok köre</td>
							<td scope="col">
								<ul>
									<li>Érintett vezetékneve</li>
									<li>Érintett keresztneve</li>
									<li>Érintett e-mail címe</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés időtartama</td>
							<td scope="col">Érintett hozzájárulásának visszavonása</td>
						</tr>
						<tr>
							<td scope="row">Adatfeldolgozók</td>
							<td scope="col">
								<p>
									<strong>Érték-Rendszerház Kft.</strong> (székhely: 1115 Ballagi M. u. 4.)
									<br>adatfeldolgozás célja: informatikai feltételek biztosítása, webshop és applikáció üzemeltetése
								</p>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-striped" id="tablazat_cookie">
					<thead class="elegant-color white-text">
						<tr>
							<th colspan="2" scope="row">
								Adatkezelő sütikre vonatkozó adatkezelésének összefoglalása
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">Adatkezelés célja</td>
							<td scope="col">Az Érintett honlapon folytatott böngészése preferenciáira, személyes beállításaira és azonosítására vonatkozó információk összegyűjtése és tárolása révén az Adatkezelő szolgáltatásainak az Érintett igényeihez igazítása</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés jogalapja</td>
							<td scope="col">Érintett hozzájárulása [GDPR 6. cikk (1) bekezdés a) pont]; a <em>hozzájárulás bármikor visszavonható</em></td>
						</tr>
						<tr>
							<td scope="row">Kezelt adatok köre</td>
							<td scope="col">Az adatkezelés tájékoztató 8. pontjában található</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés időtartama</td>
							<td scope="col">Amennyiben hozzájárult az Érintett, úgy a hozzájárulás visszavonásáig</td>
						</tr>
						<tr>
							<td scope="row">Adatfeldolgozók</td>
							<td scope="col">
								<p>
									<strong>Érték-Rendszerház Kft.</strong> (székhely: 1115 Ballagi M. u. 4.)
									<br>adatfeldolgozás célja: informatikai feltételek biztosítása, webshop és applikáció üzemeltetése
								</p>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-striped" id="tablazat_contact">
					<thead class="elegant-color white-text">
						<tr>
							<th colspan="2" scope="row">
								Adatkezelő és az Érintett közötti kapcsolattartásra irányuló adatkezelés összefoglalása
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">Adatkezelés célja</td>
							<td scope="col">Adatkezelő és az Érintett közötti kapcsolattartás</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés jogalapja</td>
							<td scope="col">Érintett hozzájárulása [GDPR 6. cikk (1) bekezdés a) pont]; a <em>hozzájárulás bármikor visszavonható</em></td>
						</tr>
						<tr>
							<td scope="row">Kezelt adatok köre</td>
							<td scope="col">
								<ul>
									<li>Érintett vezetékneve</li>
									<li>Érintett keresztneve</li>
									<li>Érintett e-mail címe</li>
									<li>Érintett telefonszáma</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés időtartama</td>
							<td scope="col">Érintett hozzájárulásának visszavonásáig, vagy a hozzájárulás visszavonás hiányában az Adatkezelő szolgáltatásának az Érintett tekintetében történő nyújtásának teljes időtartama alatt, valamint azt követően az általános polgári jogi igények elévülésének általános ideje alapján öt év</td>
						</tr>
						<tr>
							<td scope="row">Adatfeldolgozók</td>
							<td scope="col">
								<p>
									<strong>Érték-Rendszerház Kft.</strong> (székhely: 1115 Ballagi M. u. 4.)
									<br>adatfeldolgozás célja: informatikai feltételek biztosítása, webshop és applikáció üzemeltetése
								</p>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-striped" id="tablazat_panasz">
					<thead class="elegant-color white-text">
						<tr>
							<th colspan="2" scope="row">
								Adatkezelő panaszkezelési célú adatkezelésének összefoglalása
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">Adatkezelés célja</td>
							<td scope="col">Adatkezelő által az Érintett részéről benyújtott panasz kivizsgálása</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés jogalapja</td>
							<td scope="col">Adatkezelőre vonatkozó jogi kötelezettség teljesítése [GDPR 6. cikk (1) bekezdés c) pont]</td>
						</tr>
						<tr>
							<td scope="row">Kezelt adatok köre</td>
							<td scope="col">
								<ul>
									<li>Panasz egyedi azonosítószáma</li>
									<li>Panasz előterjesztésének helye és ideje</li>
									<li>Panasz előterjesztésének módja</li>
									<li>Érintett által beküldött iratok, dokumentumok, egyéb bizonyítékok jegyzéke</li>
									<li>Panasz tartalma</li>
									<li>Jegyzőkönyv tartalma</li>
									<li>Panaszra adott válasz</li>
									<li>Termék lényeges adatai</li>
									<li>Érintett vezetékneve</li>
									<li>Érintett keresztneve</li>
									<li>Érintett e-mail címe</li>
									<li>Érintett telefonszáma</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés időtartama</td>
							<td scope="col">A kezelt adatokat - különösen az Érintett által benyújtott panaszról felvett jegyzőkönyvet és a válasz másolati példányát - az Adatkezelő öt évig [Fgytv. 17/A. § (7) bekezdés] köteles megőrizni</td>
						</tr>
						<tr>
							<td scope="row">Adatfeldolgozók</td>
							<td scope="col">
								<p>
									<strong>Kocka-Trans Kft.</strong> (székhely: 1201 Vágóhíd u. 14.)
									<br>adatfeldolgozás célja: helyszíni jegyzőkönyvek felvétele
								</p>
								<p>
									<strong>IFFM Technology Kft.</strong> (székhely: 1136 Tátra u. 5. A ép.)
									<br>adatfeldolgozás célja: adatfeldolgozás célja: garanciális igények kezelése
								</p>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-striped" id="tablazat_bizonylatmegorzes">
					<thead class="elegant-color white-text">
						<tr>
							<th colspan="2" scope="row">
								Adatkezelő bizonylatmegőrzési kötelezettségének teljesítése célú adatkezelésének összefoglalása
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">Adatkezelés célja</td>
							<td scope="col">Adatkezelő bizonylatmegőrzési kötelezettségének teljesítése</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés jogalapja</td>
							<td scope="col">Adatkezelőre vonatkozó jogi kötelezettség teljesítése [GDPR 6. cikk (1) bekezdés c) pont]</td>
						</tr>
						<tr>
							<td scope="row">Kezelt adatok köre</td>
							<td scope="col">Könyvviteli elszámolást alátámasztó számviteli bizonylatok</td>
						</tr>
						<tr>
							<td scope="row">Adatkezelés időtartama</td>
							<td scope="col">Az Adatkezelő termékeinek értékesítésével szorosan összefüggően a könyvviteli elszámolást közvetlenül és közvetetten alátámasztó számviteli bizonylatokat, így az Adatkezelő bizonylatmegőrzési kötelezettségének teljesítésével kapcsolatos személyes adatokat nyolc évig köteles megőrizni [Sztv. 169. § (2) bekezdés]</td>
						</tr>
						<tr>
							<td scope="row">Adatfeldolgozók</td>
							<td scope="col">
								<p>
									<strong>Your Accountant Kft.</strong> (székhely: 1119 Petzvál J. u. 46-48.)
									<br>adatfeldolgozás célja: pénzügyi-számviteli feladatok ellátása
								</p>
							</td>
						</tr>
					</tbody>
				</table>
			</li>
			<li id="erintett">
				<p><strong>Érintett</strong></p>
				<p>Adatkezelő által végzett, személyes adatok kezelésével kapcsolatos folyamatok által érintettek közé tartoznak</p>
				<ul>
					<li>a honlapon regisztrálók</li>
					<li>az Adatkezelő termékét megrendelők</li>
					<li>az Adatkezelő és partnerei által reklám, akció, promóció, ajánlat célból küldött hírlevélre feliratkozók</li>
					<li>az Adatkezelő által a karrier lehetőség keretében meghirdetett állásajánlatra pályázók</li>
					<li>az önéletrajzukat Adatkezelő által meghirdetett állásajánlat hiányában Adatkezelő részére megküldők</li>
					<li>az Adatkezelő által alkalmazott sütik címzettjei</li>
					<li>az Adatkezelő által üzemeltetett applikációt letöltők, majd azon regisztrálók</li>
					<li>az Adatkezelővel kapcsolatot tartók</li>
					<li>Adatkezelő részére panaszt benyújtók</li>
				</ul>
				<p class="mt-2">(a továbbiakban együtt: <strong>Érintett</strong>).</p>
			</li>
			<li id="szemelyes_adatok_forrasai">
				<p><strong>A személyes adatok Adatkezelő általi megismerésének forrásai</strong></p>
				<p>Az Adatkezelő az Érintett személyes adatait az Érintettől</p>
				<ul>
					<li>honlapon történő regisztráció</li>
					<li>a termék megrendelése</li>
					<li>a hírlevélre feliratkozás</li>
					<li>az Adatkezelő által biztosított karrierlehetőség keretében meghirdetett állásajánlatra pályázás</li>
					<li>az önéletrajz Adatkezelő által meghirdetett állásajánlat hiányában Adatkezelő részére megküldése</li>
					<li>a sütik elfogadása</li>
					<li>az applikáció letöltése és az applikáción történő regisztrálás</li>
					<li>a kapcsolattartásra irányuló megkeresés</li>
					<li>a panasz benyújtása</li>
				</ul>
				<p class="mt-2">során kapja meg.</p>
			</li>
			<li id="adatkezeles_jogalapja">
				<p><strong>Adatkezelés jogalapja, célja, a kezelt adatok köre és az adatkezelés időtartama</strong></p>
				<ul class="list-unstyled">
					<li id="negypontegy">
						<p>4.1. <strong>Az Adatkezelő honlapján történő regisztráció, felhasználói profil létrehozása céljából kezelt adatok, az adatkezelés jogalapja és időtartama</strong></p>
						<ul class="list-unstyled ml-3">
							<li>
								<p>4.1.1. Adatkezelő által a honlapon történő <strong>regisztrálás, felhasználói profil létrehozása céljából</strong> megvalósított adatkezelésének jogalapja az <strong>Érintett hozzájárulása</strong> [GDPR 6. cikk (1) bekezdés a) pont]. A hozzájárulás bármikor visszavonható.</p>
							</li>
							<li>
								<p>4.1.2. Az Adatkezelő honlapján történő <strong>regisztráció, felhasználói profil létrehozása</strong> céljából az alábbi adatokat kezeli:</p>
								<ul>
									<li>Érintett vezetékneve</li>
									<li>Érintett keresztneve</li>
									<li>Érintett e-mail címe</li>
									<li>Érintett jelszava.</li>
								</ul>
							</li>
							<li>
								<p class="mt-2">4.1.3. Az Adatkezelő honlapján történő regisztráció, felhasználói profil létrehozása céljából kezelt adatokat az <strong>Érintett hozzájárulásának visszavonásáig kezeli</strong>.</p>
							</li>
						</ul>
					</li>
				</ul>
				<ul class="list-unstyled">
					<li id="negypontketto">
						<p class="mt-2">4.2. <strong>Az Adatkezelő által nyújtott szoltáltatással, termékeinek - regisztrációval/regisztráció nélkül történő - megrendelése céljából kezelt adatok, az adatkezelés jogalapja és időtartama</strong></p>
						<ul class="list-unstyled ml-3">
							<li>
								<p>4.2.1. Adatkezelő termékei Érintett általi megrendelésének teljesítése céljából megvalósított adatkezelésének jogalapja az Adatkezelő és az Érintett között létrejött <strong>szerződés teljesítése</strong> [GDPR 6. cikk (1) bekezdés b) pont].</p>
							</li>
							<li>
								<p>4.2.2. Az Adatkezelő <strong>termékeinek - honlapon regisztrációval/regisztráció nélkül - történő megrendelésének teljesítése</strong> és az ezzel kapcsolatban felmerülő valamennyi körülmény kezelése, így a honlapon az Érintett általi regisztráció, megrendelés, vételár megfizetésének teljesítése, termék szállítása, Érintett fogyasztói igényeinek érvényesítése, panaszának kivizsgálása <strong>céljából az alábbi adatokat kezeli</strong>:</p>
							<p class="ml-3">Megrendelés teljesítése során kezelt adatok:</p>
							<ul class="ml-3">
									<li>Érintett vezetékneve</li>
									<li>Érintett keresztneve</li>
									<li>Érintett e-mail címe</li>
									<li>Érintett telefonszáma</li>
									<li>Érintett rendeléshez rögzített megjegyzése (amennyiben azt az Érintett rögzít megjegyzést)</li>
									<li>számla igénylése esetén: számlázási név, adószám, cím (irányítószám, város, utca, házszám)</li>
									<li>házhozszállítás esetén az Érintett vezetékneve és utóneve, lakcíme és a lakcímen lift igénybevételi lehetőségének ténye</li>
									<li>kuponkód</li>
									<li>Facebook profil importálása esetén szolgáltatott adatok</li>
								</ul>
							<p class="mt-2 ml-3">Érintett fogyasztói jogainak érvényesítése esetén kezelt adatok:</p>
							<ul class="ml-3">
									<li>Elállás, jótállási igény vagy felmondás egyedi azonosítószáma</li>
									<li>Elállás, jótállási igény, vagy felmondás előterjesztésének helye és ideje</li>
									<li>Elállás, jótállási igény vagy felmondás előterjesztésének módja</li>
									<li>Érintett által beküldött iratok, dokumentumok, egyéb bizonyítékok jegyzéke</li>
									<li>Elállás, jótállási igény vagy felmondás tartalma</li>
									<li>Jegyzőkönyv tartalma</li>
									<li>Elállásra, jótállási igényre vagy felmondásra adott válasz</li>
									<li>Termék lényeges adatai (méret, cikkszám, ár, megnevezés, mennyiség)</li>
									<li>Érintett neve</li>
									<li>Érintett címe</li>
									<li>Érintett telefonszáma</li>
									<li>Számlaszám</li>
									<li>Szállítólevél száma</li>
									<li>Rendelésszám.</li>
								</ul>
							</li>
							<li>
								<p class="mt-2">4.2.3. Az Adatkezelő a <strong>termékeinek megrendelése céljából</strong> kezelt adatokat az általa az Érintett irányában nyújtott szolgáltatás teljes időtartama alatt és azt követően a fogyasztóvédelmi, valamint általános polgári jogi igények érvényesíthetőségi idő tekintetében öt évig kezeli.</p>
							</li>
						</ul>
					</li>
					<li id="negypontharom">
						<p class="mt-2">4.3 <strong>Adatkezelő és partnerei által reklámot, akciót, promóciót, ajánlatot tartalmazó hírlevél küldéssel kapcsolatos adatkezelés összefoglalása</strong></p>
						<ul class="list-unstyled ml-3">
							<li>
								<p>4.3.1 Adatkezelő és partnerei által kifejtett reklám tevékenység keretében akciót, promóciót, ajánlatot tartalmazó hírlevek küldése céljából megvalósított adatkezelés jogalapja az Érintett hozzájárulása [GDPR 6. cikk (1) bekezdés a) pont; a gazdasági reklámtevékenység alapvető feltételeiről és egyes korlátairól 2008. évi XLVIII. törvény 6. § (1) bekezdés is].</p>
							</li>
							<li>
								<p>4.3.2 Az Adatkezelő és partnerei által reklámot, akciót, promóciót, ajánlatot tartalmazó hírlevelek küldése érdekében az alábbi adatokat kezeli:</p>
								<ul>
									<li>Érintett vezetékneve</li>
									<li>Érintett utóneve</li>
									<li>Érintett e-mail címe</li>
									<li>Érintett telefonszáma</li>
								</ul>
							</li>
							<li>
								<p class="mt-2">4.3.3 Az Adatkezelő a reklám küldése cél érdekében kezelt adatokat az Érintett hozzájárulásának visszavonásáig kezeli, azt követően törli. A hozzájárulás bármikor ingyenesen visszavonható, a hírlevélről az Érintett bármikor leiratkozhat, melyről Adatkezelő külön tájékoztatást nyújt valamennyi hírlevélben.</p>
							</li>
							<li>
								<p>4.3.4 Érintett az Adatkezelő és partnerei által az Adatkezelő reklám tevékenysége keretében továbbított, akciót, promóciót, ajánlatot tartalmazó hírlevek küldéséhez adott hozzájárulását a honlapon az <em>"Értesüljön akcióinkról"-"Feliratkozás"</em> felületre kattintást követően a <em>"Hozzájárulok személyes adataim részemre reklám - akciókat, promóciót, ajánlatot tartalmazó hírlevek - küldése céljából történő kezeléséhez."</em> szövegezésű jelölő négyzetre kattintással adhatja meg.</p>
							</li>
						</ul>
					</li>
					<li id="negypontnegy">
						<p class="mt-2">4.4. <strong>Toborzási célból kezelt adatok, az adatkezelés jogalapja és időtartama</strong></p>
						<ul class="list-unstyled ml-3">
							<li>
								<p>4.4.1 Adatkezelő az általa biztosított karrierlehetőség keretében a toborzás, munkaerő-hiány betöltése, valamint a meghirdetett állásajánlatra jelentkező Érintett pályázatának elutasítása esetén, vagy a pályázat pozitív elbírálása ellenére, amennyiben nem az Adatkezelő által kiválasztott Érintett pályázó tölti be a munkakört, e pályázóval való ismételt kapcsolatfelvétel céljából megvalósított adatkezelés jogalapja az Érintett hozzájárulása [GDPR 6. cikk (1) bekezdés a) pont]. A hozzájárulás bármikor visszavonható.</p>
							</li>
							<li>
								<p>4.4.2 Az Adatkezelő a toborzás, munkaerő-hiány betöltése érdekében az alábbi adatokat kezeli:</p>
								<ul>
									<li>Érintett vezetékneve</li>
									<li>Érintett utóneve</li>
									<li>Érintett e-mail címe</li>
									<li>Érintett telefonszáma</li>
									<li>adott esetben, az Érintett fizetési igénye</li>
									<li> valamint az Érintett által az állásajánlatra jelentkezés céljából önéletrajzában rendelkezésre bocsátott egyéb személyes adatok.</li>
								</ul>
							</li>
							<li>
								<p class="mt-2">4.4.3 Adatkezelő a pályázat pozitív elbírálása esetén a felvételt nyert pályázó Érintett esetében a munkakör betöltéséig kezeli e célból az Érintett személyes adatait.</p>
							</li>
							<li>
								<p>4.4.4 Adatkezelő</p>
								<ul>
									<li>a pályázat negatív elbírálása esetén, a felvételt nem nyert pályázó Érintett tekintetében</li>
									<li>pályázat pozitív elbírálása ellenére amennyiben a munkakört nem az Adatkezelő által kiválasztott pályázó Érintett tölti be,</li>
								</ul>
								<p class="mt-2">az adatokat haladéktalanul törli, vagy az ismételt kapcsolatfelvétel céljából, az Érintett külön hozzájárulása alapján további kettő évig kezeli. A hozzájárulás bármikor visszavonható.</p>
							</li>
							<li>
								<p>4.4.5  Az Érintett önéletrajzának Adatkezelő által meghirdetett állásajánlat hiányában Adatkezelő részére történő megküldésével, így személyes adatainak rendelkezésre bocsátásával hozzájárul ahhoz, hogy a személyes adatait <strong>kettő évig</strong> kezelje az Adatkezelő a működésében jelentkező munkaerő-hiány megszüntetése érdekében, az <strong>ismételt kapcsolatfelvétel céljából</strong>.</p>
							</li>
						</ul>
					</li>
					<li id="negypontot">
						<p>4.5 <strong>Adatkezelő által üzemeltetett applikáció Érintett általi használatának lehetővé tétele céljából kezelt adatok, az adatkezelés jogalapja és időtartama</strong></p>
						<ul class="list-unstyled ml-3">
							<li>
								<p>4.5.1 Adatkezelő által <strong>üzemeltetett applikáció Érintett általi használatának lehetővé tétele</strong> céljából megvalósított adatkezelésének jogalapja az <strong>Érintett hozzájárulása</strong> [GDPR 6. cikk (1) bekezdés a) pont]. A hozzájárulás bármikor visszavonható.</p>
							</li>
							<li>
								<p>4.5.2 Adatkezelő által <strong>üzemeltetett applikáció Érintett általi használatának lehetővé tétele</strong>, azaz az applikáció letöltése, használata az Érintett egyedi igénye szerint kialakított bútor megtervezése <strong>céljából</strong> az alábbi adatokat kezeli:</p>
								<ul>
									<li>Érintett vezetékneve</li>
									<li>Érintett utóneve</li>
									<li>Érintett e-mail címe</li>
								</ul>
							</li>
							<li>
								<p class="mt-2">4.5.3 Adatkezelő <strong>az applikáció Érintett általi használatának lehetővé tétele</strong> céljából kezelt adatokat az <strong>Érintett hozzájárulásának visszavonásáig</strong> kezeli, azt követően törli. A hozzájárulás bármikor ingyenesen visszavonható</p>
							</li>
						</ul>
					</li>
					<li id="negyponthat">
						<p>4.6 <strong>Adatkezelő és az Érintett közötti kapcsolattartás céljából kezelt adatok, az adatkezelés jogalapja és időtartama</strong></p>
						<ul class="list-unstyled ml-3">
							<li>
								<p>4.6.1 Adatkezelő által az Érintettel történő <strong>kapcsolattartás</strong> céljából megvalósított adatkezelésének jogalapja az <strong>Érintett hozzájárulása</strong> [GDPR 6. cikk (1) bekezdés a) pont]. A hozzájárulás bármikor visszavonható.</p>
							</li>
							<li>
								<p>4.6.2 Adatkezelő az <strong>Érintett és ő közötte történő kapcsolattartás céljából</strong> az alábbi adatokat kezeli:</p>
								<ul>
									<li>Érintett vezetékneve</li>
									<li>Érintett utóneve</li>
									<li>Érintett e-mail címe</li>
									<li>Érintett telefonszáma</li>
								</ul>
							</li>
							<li>
								<p class="mt-2">4.6.3 Adatkezelő a honlapon külön felületet biztosít az Érintett számára a kapcsolattartás megkönnyítése, hatékonyabbá tétele érdekében. </p>
							</li>
							<li>
								<p>4.6.4 Adatkezelő az Érintett való kapcsolattartás céljából kezelt adatokat az <strong>Érintett hozzájárulásának visszavonásáig</strong>, vagy a hozzájárulás visszavonás hiányában <strong>az Adatkezelő szolgáltatásának az Érintett tekintetében történő nyújtásának teljes időtartama alatt</strong>, valamint azt követően az általános polgári jogi igények elévülésének általános ideje alapján öt évszolgáltatásának az Érintett tekintetében történő nyújtásának teljes időtartama alatt, valamint azt követően az általános polgári jogi igények elévülésének általános ideje alapján öt évig kezeli.</p>
							</li>
						</ul>
					</li>
					<li id="negyponthet">
						<p>4.7 <strong>Adatkezelő által az Érintett által benyújtott panasz kivizsgálása céljából kezelt adatok, az adatkezelés jogalapja és időtartama</strong></p>
						<ul class="list-unstyled ml-3">
							<li>
								<p>4.7.1 Adatkezelő által az Érintetti panasz kivizsgálása céljából az alábbi adatokat kezeli:</p>
								<ul>
									<li>Panasz egyedi azonosítószáma</li>
									<li>Panasz előterjesztésének helye és ideje</li>
									<li>Panasz előterjesztésének módja</li>
									<li>Érintett által beküldött iratok, dokumentumok, egyéb bizonyítékok jegyzéke</li>
									<li>Panasz tartalma</li>
									<li>Jegyzőkönyv tartalma</li>
									<li>Panaszra adott válasz</li>
									<li>Termék lényeges adatai</li>
									<li>Érintett vezetékneve</li>
									<li>Érintett utóneve</li>
									<li>Érintett e-mail címe</li>
									<li>Érintett telefonszáma</li>
								</ul>
							</li>
							<li>
								<p class="mt-2">4.7.2 Adatkezelőnek az Érintett által benyújtott panasz kivizsgálása céljából történő adatkezelése során az Érintett által benyújtott panaszról felvett jegyzőkönyvet és a válasz másolati példányát <strong>öt évig</strong> kezeli a fogyasztóvédelemről szóló 1997. évi CLV. törvény 17/A. § (7) bekezdése alapján.</p>
							</li>
						</ul>
					</li>
					<li id="negypontnyolc">
						<p>4.8 <strong>Az Adatkezelő által bizonylatmegőrzési kötelezettségének teljesítése céljából kezelt adatok, az adatkezelés jogalapja és időtartama</strong></p>
						<ul class="list-unstyled ml-3">
							<li>
								<p>4.8.1 Adatkezelő által <strong>bizonylatmegőrzési kötelezettségének teljesítése céljából</strong> az alábbi adatokat kezeli:</p>
								<ul>
									<li>termékeinek értékesítésével szorosan összefüggő könyvviteli elszámolást közvetlenül és közvetetten alátámasztó számviteli bizonylatok.</li>
								</ul>
							</li>
							<li>
								<p class="mt-2">4.8.2 Adatkezelő <strong>bizonylatmegőrzési kötelezettségének teljesítése céljából</strong> történő adatkezelése során a termékeinek értékesítésével szorosan összefüggő könyvviteli elszámolást közvetlenül és közvetetten alátámasztó számviteli bizonylatokat nyolc évig köteles megőrizni a számvitelről szóló 2000. évi C. törvény 169. § (2) bekezdése alapján.</p>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<li id="ot">
				<p><strong>Adatfeldolgozás, további adatkezelők</strong></p>
				<ul class="list-unstyled">
					<li>
						<p>5.1. Az Adatkezelő az Érintett adatait a szolgáltatás nyújtása során, annak teljesítése érdekében, és ahhoz szükséges mértékben, szerződött partnerei  részére átadja adatfeldolgozásra.</p>
					</li>
					<li>
						<p>5.2. A szolgáltatásnyújtás teljesítése érdekében szükségesen igénybe vett, Adatkezelő szerződött partnerei közé tartozó adatfeldolgozók, akik által az Érintett adatai feldolgozásra kerülnek:</p>
						<ul>
							<li>
								<p><strong>Érték-Rendszerház Kft. </strong>(székhely: 1115 Ballagi M. u. 4.)</p>
								<p>adatfeldolgozás célja: informatikai feltételek biztosítása, webshop és applikáció üzemeltetése</p>
							</li>
							<li>
								<p><strong>Kocka-Trans Kft. </strong>(székhely: 1201 Vágóhíd u. 14.)</p>
								<p>adatfeldolgozás célja: megvásárolt termékek kiszállítása, helyszíni jegyzőkönyvek felvétele</p>
							</li>
							<li>
								<p><strong>IFFM Technology Kft. </strong>(székhely: 1136 Tátra u. 5. A ép.)</p>
								<p>adatfeldolgozás célja: megrendelt termékek gyártása, raktározása, garanciális igények kezelése</p>
							</li>
							<li>
								<p><strong>Your Accountant Kft. </strong>(székhely: 1119 Petzvál J. u. 46-48.)</p>
								<p>adatfeldolgozás célja: pénzügyi-számviteli feladatok ellátása</p>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<li id="hat">
				<p><strong>Adatbiztonság, az adatok megismerésére jogosultak</strong></p>
				<ul class="list-unstyled">
				<li>
					<p>6.1. Az adatkezelési időtartam lejártakor az Adatkezelő törli az Érintett adatait.</p>
				</li>
				<li>
					<p>6.2. Adatkezelő gondoskodik a kezelt adatok biztonságáról, és minden intézkedést megtesz a jogosulatlan hozzáférés, megváltoztatás, továbbítás, nyilvánosságra hozatal, törlés vagy megsemmisítés, valamint a véletlen megsemmisülés és sérülés, továbbá az alkalmazott technika megváltozásából fakadó hozzáférhetetlenné válás, azaz annak érdekében, hogy az Érintettek személyes adatai a jogszabályoknak megfelelő védelemben részesüljenek.</p>
				</li>
				<li>
					<p>6.3. Az Adatkezelő az adatbiztonság követelményének megtartásához szükséges intézkedések körében számítógépes adatbázisban, automatikus és manuális módon kezeli az Érintett adatait és intézkedett arról, hogy az Érintett adatainak kezelése zárt, és minden esetben jelszóval védett rendszerben, merevlemezre mentve történjen és e rendszereket csak a szolgáltatás nyújtásával összefüggésben, az ahhoz elengedhetetlenül szükséges mértékben használják az adatok megismerésére jogosultak.</p>
				</li>
				<li>
					<p>6.4. A számítógépes rendszerek tűzfallal és megfelelő vírusvédelemmel vannak ellátva.</p>
				</li>
				<li>
					<p>6.5. Adatkezelő elvégzi a rendszer műszaki ellenőrzését és hiba észlelése vagy jelzése esetén intézkedik.</p>
				</li>
				<li>
					<p>6.6. Adatkezelő biztosítja, hogy az adatokhoz hozzáférésére jogosultak teljeskörű, adatvédelmi szabályokról történő tájékoztatását. Adatbiztonsági garanciaként Adatkezelő vezetői tisztségviselőit és munkavállalóit titoktartási kötelezettség és jogi felelősség terheli az e tevékenységük során megismert személyes adatok tekintetében.</p>
				</li>
				<li>
					<p>6.7. A honlapra látogatók, valamint az Érintett számítógépének használatával az Adatkezelő részére hozzáférhetővé váló információk és hálózati azonosítói (IP cím) a honlap látogatottsági adatainak generálása, valamint az esetlegesen felmerülő hibák és támadási kísérletek detektálása céljából naplózásra kerülnek.  A hálózati azonosítókat az Adatkezelő nem köti semmilyen olyan más adathoz, amely alapján a honlapra látogató, vagy az Érintett személye azonosítható lenne. A honlap látogatásával egyidejűleg jelen Adatkezelési Tájékoztatóban rögzítetteken túl egyéb sütik, illetve webbug-ok letöltésére nem kerül sor.</p>
				</li>
				<li>
					<p>6.8. Az Adatkezelő részéről a kezelt adatok megismerésére jogosultak köre az adott adatkezelési cél érdekében kezelt adatkezelési időtartamhoz igazodóan differenciált:</p>
					<ul>
						<li>
							<p>a honlapon történő regisztráció, a hírlevelek küldése, az applikáció használatának Érintett részére történő lehetővé tétele, valamint a kapcsolattartás céljából az Érintett hozzájárulásának visszavonásáig, ezek hiányában öt éves időtartamú adatkezelése során az Érintett személyes adatainak megismerésére jogosultak: marketing vezető, marketing asszisztens, Your Accountant Kft</p>
						</li>
						<li>
							<p>Adatkezelő működésében felmerülő munkaerő-hiány betöltése céljából, vagy az állásajánlatra jelentkező pályázatának elutasítása esetén, illetve a pályázat pozitív elbírálása ellenére amennyiben a munkakört nem az Adatkezelő által kiválasztott pályázó tölti be, az ismételt kapcsolatfelvétel céljából, vagy az  Érintett önéletrajzának Adatkezelő által meghirdetett állásajánlat hiányában Adatkezelő részére történő megküldése esetén a kapcsolatfelvétel céljából folytatott, a munkaerőhiány betöltéséig, vagy ismételt kapcsolatfelvétel esetén további kettő évig tartó adatkezelés során az Érintett személyes adatainak megismerésére jogosultak: ügyvezető, Your Accountant Kft</p>
						</li>
						<li>
							<p>az Adatkezelő a termékei Érintett általi megrendelésének teljesítése, az Érintett által benyújtott panasz kivizsgálása céljából kezelt személyes adatok öt éves adatkezelési időtartama alatt az Érintett személyes adatainak megismerésére jogosultak: ügyvezető, jogtanácsos, pénzügyi asszisztens, megbízott ügyvéd</p>
						</li>
						<li>
							<p>az Adatkezelő bizonylatmegőrzési kötelezettségének teljesítésével kapcsolatos személyes adatokat tekintetében előírt nyolc éves adatkezelési időtartam alatt az Érintett személyes adatainak megismerésére jogosultak: ügyvezető, pénzügyi asszisztens, Your Accountant Kft</p>
						</li>
					</ul>
				</li>
				<li>
					<p>6.9. Adatkezelő a reklámot, akciót, promóciót, ajánlatot tartalmazó hírlevél küldéséhez hozzájáruló nyilatkozatot tevő Érintettek személyes adatairól nyilvántartást vezet.</p>
				</li>
				</ul>
			</li>
			<li id="het">
				<p><strong>Érintetti jogok, jogorvoslati lehetőségek</strong></p>
				<ul class="list-unstyled">
				<li>
					<p>7.1. Érintett a jelen pontban rögzített jogait elektronikus úton, az <a href="mailto:info@vamosimilano.hu">info@vamosimilano.hu</a> e-mail címre, vagy postai úton az Adatkezelő székhelyére az Adatkezelőnek címzett levélben gyakorolhatja.</p>
				</li>
				<li>
					<p>7.2. Az Érintett kérelmére írásban vagy elektronikus úton az Adatkezelő tájékoztatást ad [GDPR 15. cikk (1) bekezdés; tájékoztatáshoz való jog]:</p>
					<ul>
						<li>arról, hogy milyen adatokat kezel az Érintettről</li>
						<li>az adatkezelés céljairól</li>
						<li>azon címzettek kategóriáról, akik számára a személyes adatokat átadhatja</li>
						<li>az adatkezelés időtartamáról</li>
						<li>az Érintett jogairól és a jogorvoslati lehetőségeiről</li>
					</ul>
				</li>
				<li>
					<p class="mt-2">7.3. Az Érintett kérelmére Adatkezelő a személyes adatok másolatát széles körben használt elektronikus formátumban vagy az Érintett által választott más formátumban rendelkezésére bocsátja. [GDPR 15. cikk (3) bekezdés; hozzáférési jog, másolatkiadás iránti jog]</p>
				</li>
				<li>
					<p>7.4. Az Adatkezelő az Érintett kérésének megfelelően módosítja, pontosítja (helyesbíti) az Érintett személyes adatát, valamint erre a felhasználói profilon keresztül is lehetőséget biztosít [GDPR 16. cikk; helyesbítéshez való jog].</p>
				</li>
				<li>
					<p>7.5. Az Érintett kérelmére Adatkezelő törli az Érintett személyes adatát. Az Adatkezelő a kérelem teljesítését a GDPR 17. cikk (3) bekezdésében szereplő okokból tagadhatja meg, így például abban az esetben, ha a személyes adatok jogi igény előterjesztéséhez, érvényesítéséhez szükségesek, vagy a személyes adatok kezelését előíró, az Adatkezelőre alkalmazandó uniós vagy tagállami jog szerinti kötelezettség teljesítése, illetve közérdekből, vagy a véleménynyilvánítás szabadságához és a tájékozódáshoz való jog gyakorlása céljából. [GDPR 17. cikk; törléshez való jog].</p>
				</li>
				<li>
					<p>7.6. Az Érintett a regisztráció, a hírlevél részére történő küldése, a toborzás, a hozzájárulást igénylő sütik alkalmazása, a kapcsolattartás céljából történő adatkezeléshez adott hozzájáruló nyilatkozatát bármikor korlátozás és indokolás nélkül, ingyenesen visszavonhatja, illetve bejelentheti a reklám küldésének megtiltása iránti igényét. Ebben az esetben az Érintett valamennyi személyes adatát Adatkezelő haladéktalanul törli a nyilvántartásból és az Érintett részére a továbbiakban nem küld hírlevelet [hozzájárulás visszavonásához való jog]. A visszavonás nem érinti a visszavonás előtt a hozzájárulás alapján végrehajtott adatkezelés jogszerűségét.</p>
				</li>
				<li>
					<p>7.7. Az Érintett jogosult kérelmezni a személyes adatok kezelésének korlátozását (zárolását)</p>
					<ul>
						<li>
							<p>ha vitatja a személyes adatok pontosságát, akkor azon időtartamig kérheti az adatok zárolását, amíg Adatkezelő ellenőrizi a személyes adatok pontosságát;</p>
						</li>
						<li>
							<p>ha az adatkezelés jogellenes, és az Érintett ellenzi a személyes adatok törlését, és ehelyett azok felhasználásának korlátozását kéri;</p>
						</li>
						<li>
							<p>Adatkezelőnek már nincs szüksége a személyes adatokra, de az Érintett kéri az adatok zárolását jogi igények előterjesztéséhez, érvényesítéséhez vagy védelméhez [GDPR 18. cikk; adatkezelés korlátozásához való jog (zárolás)]</p>
						</li>
					</ul>
					<p>Adatkezelő zárolási kérelmét úgy teljesíti, hogy a személyes adatokat minden más személyes adattól elkülönítetten tárolja. Így például elektronikus adatállományok esetében külső adathordozóra kimenti, vagy a papír alapon tárolt személyes adatokat külön mappába helyezi át. Adatkezelő a tárolás kivételével csak az Érintett hozzájárulásával, vagy jogi igények előterjesztéséhez, érvényesítéséhez vagy védelméhez, vagy más természetes vagy jogi személy jogainak védelme érdekében, vagy az Unió, illetve valamely tagállam fontos közérdekéből kezeli. Az adatkezelés korlátozásának e feloldásáról előzetesen tájékoztatni fogjuk.</p>
				</li>
				<li>
					<p>7.8. Az Érintett jogosult arra, hogy személyes adatait széles körben használt, géppel olvasható, tagolt formátumban megkapja, továbbá jogosult arra, hogy ezeket az adatokat egy másik adatkezelőnek továbbítsa. Ezen túlmenően Adatkezelő biztosítja, hogy erre irányuló kifejezett kérelme esetén közvetlenül továbbítja Érintett adatait az Érintett által meghatározott másik adatkezelőnek. [GDPR 20. cikk (1) és (2) bekezdése; adathordozhatósághoz való jog].</p>
				</li>
				<li>
					<p>7.9. Adatkezelő az Érintett kérelmének beérkezését követő egy hónapon belül tájékoztatja az Érintettet a hozott intézkedésekről. A kérelem megtagadása esetén Adatkezelő a kérelem beérkezésétől számított egy hónapon belül tájékoztatja Önt a megtagadás indokairól, valamint arról, hogy panaszt nyújthat be a Hatóságnál, és élhet bírósági jogorvoslati jogával.</p>
				</li>
				<li>
					<p>7.10. A joggyakorlás díjmentes. Bizonyos esetekben Adatkezelő az adminisztratív költségeken alapuló, díjat számíthat fel, vagy megtagadhatja a kérelem alapján történő intézkedést, ha az Érintett az adatairól másolatot kér, vagy ha az Érintett kérelme egyértelműen megalapozatlan vagy – különösen ismétlődő jellege miatt – túlzó).</p>
				</li>
				<li>
					<p>7.11. Adatkezelő fenntartja magának azt a jogot, hogy amennyiben megalapozott kétségei vannak a kérelmet benyújtó személy kilétét illetően, akkor az Érintett személyazonosságának megerősítéséhez szükséges információk nyújtását kérje. Ilyen esetnek tekinthető különösen az, ha az Érintett a másolat kéréséhez való jogával él, amely esetben indokolt, hogy Adatkezelő meggyőződjön arról, hogy a kérelem a jogosult személytől származik.</p>
				</li>
				<li>
					<p>7.12. A jelen pontban szabályozott eljárások kezdeményezése előtt az Érintett jogosult - elektronikus úton benyújtható - panaszával az Adatkezelőhöz fordulni az adatkezeléssel kapcsolatos aggályainak kiküszöbölése, valamint a jogszerű állapot helyreállítása érdekében. Adatkezelő a panaszt egy hónapon belül megvizsgálja, arról jegyzőkönyvet vesz fel és annak megalapozottsága kérdésében döntést hoz, és döntéséről az Érintettet írásban, elektronikus úton tájékoztatja. Ha az Adatkezelő az Érintett panaszának megalapozottságát megállapítja, az adatkezelés jogszerű állapotát helyreállítja, vagy az adatkezelést – beleértve a további adatfelvételt és adattovábbítást is – megszünteti. Ekkor az Adatkezelő az Érintett adatait nem kezelheti tovább, kivéve, ha az Adatkezelő bizonyítja, hogy az adatkezelést olyan kényszerítő erejű jogos okok indokolják, amelyek elsőbbséget élveznek az Érintett érdekeivel, jogaival és szabadságaival szemben, vagy amelyek jogi igények előterjesztéséhez, érvényesítéséhez vagy védelméhez kapcsolódnak. Az Adatkezelő a panaszról és az annak alapján tett intézkedésekről értesíti azokat, akik részére a panasszal Érintett adatot továbbította. Jelen pontban szabályozott panaszeljárás az Érintettel történő kapcsolattartás keretében az adatkezeléssel kapcsolatos aggályok kiküszöbölésére szolgál és nem vonatkozik Adatkezelő termékeivel kapcsolatos, fogyasztói jogok (szavatossági igények, jótállás, elállás, felmondás) érvényesítése érdekében tett jognyilatkozatokra.</p>
				</li>
				<li>
					<p>7.13. Amennyiben az Érintett megítélése szerint az Adatkezelő megsértette a személyes adatok védelméhez fűződő jogát, vagy az Adatkezelő jogellenes adatkezelést végez, akkor a Hatóság eljárását kezdeményezheti. A Hatóság elérhetőségei: postacím: 1530 Budapest, Pf.: 5.; e-mail cím: <a href="mailto:ugyfelszolgalat@naih.hu">ugyfelszolgalat@naih.hu</a>; telefonszám: +36 (1) 391-1400; honlap címe: <a href="https://http://www.naih.hu/" target="_blank">www.naih.hu</a>.</p>
				</li>
				<li>
					<p>7.14.  Amennyiben az Érintett megítélése szerint a Adatkezelő megsértette a személyes adatok védelméhez fűződő jogát, akkor a bírósági eljárást is kezdeményezhet és követelheti az adatai jogellenes kezelésével vagy adatbiztonság megszegésével az Érintettnek okozott kár megtérítését, személyiségi jogsérelem esetén sérelemdíj megfizetését. Bírósági jogérvényesítés esetén az Érintett a pert lakóhelye vagy tartózkodási helye szerinti törvényszék előtt is megindíthatja.</p>
				</li>
				</ul>
			</li>
			<li id="nyolc">
				<p><strong>Sütik</strong></p>
				<ul class="list-unstyled">
					<li>
						<p>8.1. Amikor az Érintett meglátogatja a honlapot, számítógépe sütiket tárol. A sütik kisméretű szövegfájlok, amelyeket a böngészője tárol az Érintett eszközén bizonyos információk mentése érdekében. Amikor az Érintett a következő alkalommal ugyanazon eszköz igénybevételével látogatja meg honlapot, a sütikben mentett információ vagy a honlapra („Első Fél sütije”) továbbítódik vagy egy másik olyan honlapra, amelyhez a süti tartozik („Harmadik Fél sütije”).</p>
					</li>
					<li>
						<p>8.2. A mentett és visszaküldött információn keresztül a honlap felismeri, hogy az Érintett korábban már belépett oda és meglátogatta azt az eszközön használt böngészővel. Ezt az információt arra használja az Adatkezelő, hogy a honlapot az Érintett preferenciáival összhangban optimális módon tervezze és jelenítse meg. Ebben a vonatkozásban kizárólag maga a süti kerül azonosításra az Érintett eszközén. Ezen a mértéken túl az Érintett személyes adatait csak a kifejezett hozzájárulásával, vagy abban az esetben menti az Adatkezelő, ha az feltétlenül szükséges az Érintett részére felkínált és az Érintett által elért szolgáltatás használatához.</p>
					</li>
					<li>
						<p>8.3. Adatkezelő sütik alkalmazása, azaz a honlapon nyújtott szolgáltatásainak az Érintett igényeihez igazítása céljából megvalósított adatkezelésének jogalapja az Érintett hozzájárulása [GDPR 6. cikk (1) bekezdés a) pont]. Az Érintett hozzájárulását igénylő sütik az alábbiak:</p>
						<ul>
							<li>Doubleclick (DSID) - reklám süti</li>
							<li>Doubleclick (IDE) - reklám süti</li>
							<li>Google Adservices (AID) - reklám süti</li>
						</ul>
						<p class="mt-2">Érintett a sütik alkalmazásához adott hozzájárulását a honlapon az adott süti jelölő négyzetére kattintással adhatja meg. Az Érintett általi hozzájárulást nem igénylő sütikről Adatkezelő az alábbiakban tájékoztatást nyújt.</p>
					</li>
					<li>
						<p>8.4. A honlap a sütik alábbi típusait használja, amely sütik terjedelmére és funkciójára vonatkozó magyarázat alább található:</p>
						<ul>
							<li>Feltétlenül szükséges sütik</li>
							<li>Funkció és Teljesítmény sütik</li>
							<li>Hozzájáruláson alapuló sütik</li>
						</ul>
					</li>
					<li>
						<p class="mt-2">8.5. A honlap az alábbi sütiket használja:</p>
						<table class="table table-striped" id="tablazat_regisztracio">
						<thead class="elegant-color white-text">
							<tr>
								<th scope="col">
									Süti <br>neve
								</td>
								<th scope="col">
									Süti <br>típusa
								</td>
								<th scope="col">
									Süti <br>Szolgáltató
								</td>
								<th scope="col">
									Süti által <br>kezelt adatok
								</td>
								<th scope="col">
									Süti <br>célja
								</td>
								<th scope="col">
									Süti <br>élettartama
								</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">laravel session</th>
								<td>Feltétlenül szükséges</td>
								<td>vamosimilano.hu</td>
								<td></td>
								<td>a weboldal alapvető folyamatait segíti</td>
								<td>3 nap</td>
							</tr>
							<tr>
								<th scope="row">XSRF-TOKEN</th>
								<td>Feltétlenül szükséges</td>
								<td>vamosimilano.hu</td>
								<td></td>
								<td>egyedi azonosító kulcs a látogatóknak</td>
								<td>3 nap</td>
							</tr>
							<tr>
								<th scope="row">lastusedmounting</th>
								<td>Feltétlenül szükséges</td>
								<td>vamosimilano.hu</td>
								<td>left. right</td>
								<td>oldalválasztási adatot tárol</td>
								<td>10 nap</td>
							</tr>
							<tr>
								<th scope="row">(id)-(oldaltípus)</th>
								<td>Feltétlenül szükséges</td>
								<td>vamosimilano.hu</td>
								<td>S, M, L vagy XL</td>
								<td>Méreteket tárol</td>
								<td>3 nap</td>
							</tr>
							<tr>
								<th scope="row">selected(id)</th>
								<td>Feltétlenül szükséges</td>
								<td>vamosimilano.hu</td>
								<td>szövettípus</td>
								<td>Szövet választás</td>
								<td>3 nap</td>
							</tr>
							<tr>
								<th scope="row">lastusedtextil (típus)</th>
								<td>Feltétlenül szükséges</td>
								<td>vamosimilano.hu</td>
								<td>szövet</td>
								<td>Szövettípusok részenként</td>
								<td>10 nap</td>
							</tr>
							<tr>
								<th scope="row">DSID</th>
								<td>Reklám süti</td>
								<td>Doubleclick</td>
								<td></td>
								<td></td>
								<td>14 nap</td>
							</tr>
							<tr>
								<th scope="row">IDE</th>
								<td>Reklám süti</td>
								<td>Doubleclick</td>
								<td></td>
								<td></td>
								<td>24 nap</td>
							</tr>
							<tr>
								<th scope="row">fr</th>
								<td></td>
								<td>Facebook</td>
								<td></td>
								<td></td>
								<td>3 hónap</td>
							</tr>
							<tr>
								<th scope="row">nincs neve</th>
								<td></td>
								<td>Facebook</td>
								<td></td>
								<td></td>
								<td>A böngészési folyamat végéig él</td>
							</tr>
							<tr>
								<th scope="row">1P_JAR</th>
								<td></td>
								<td>Google</td>
								<td></td>
								<td></td>
								<td>1 hónap</td>
							</tr>
							<tr>
								<th scope="row">AID</th>
								<td></td>
								<td>Google</td>
								<td></td>
								<td></td>
								<td>16 hónap</td>
							</tr>
							<tr>
								<th scope="row">APISID</th>
								<td></td>
								<td>Google</td>
								<td></td>
								<td></td>
								<td>2 év</td>
							</tr>
							<tr>
								<th scope="row">CONSENT</th>
								<td></td>
								<td>Google</td>
								<td></td>
								<td></td>
								<td>20 év</td>
							</tr>
							<tr>
								<th scope="row">HSID</th>
								<td></td>
								<td>Google</td>
								<td></td>
								<td></td>
								<td>2 év</td>
							</tr>
							<tr>
								<th scope="row">NID</th>
								<td></td>
								<td>Google</td>
								<td></td>
								<td></td>
								<td>6 hónap</td>
							</tr>
							<tr>
								<th scope="row">SAPISID</th>
								<td></td>
								<td>Google</td>
								<td></td>
								<td></td>
								<td>2 év</td>
							</tr>
							<tr>
								<th scope="row">SID</th>
								<td></td>
								<td>Google</td>
								<td></td>
								<td></td>
								<td>2 év</td>
							</tr>
							<tr>
								<th scope="row">SIDCC</th>
								<td></td>
								<td>Google</td>
								<td></td>
								<td></td>
								<td>3 hónap</td>
							</tr>
							<tr>
								<th scope="row">SSID</th>
								<td></td>
								<td>Google</td>
								<td></td>
								<td></td>
								<td>2 év</td>
							</tr>
							<tr>
								<th scope="row">intercom-session-kf1dzÜpp</th>
								<td></td>
								<td>Google</td>
								<td></td>
								<td></td>
								<td>7 nap</td>
							</tr>
							<tr>
								<th scope="row">UULE</th>
								<td></td>
								<td>Google</td>
								<td></td>
								<td></td>
								<td>1 nap</td>
							</tr>
							<tr>
								<th scope="row">AID</th>
								<td>Reklám süti</td>
								<td>Google Adservices</td>
								<td></td>
								<td></td>
								<td>16 hónap</td>
							</tr>
							<tr>
								<th scope="row">PHPSESSID</th>
								<td>Feltétlenül szükséges</td>
								<td>vamosimilano.hu</td>
								<td></td>
								<td></td>
								<td>A böngészési folyamat végéig él</td>
							</tr>
							<tr>
								<th scope="row">_ga</th>
								<td>Funkció és teljesítmény</td>
								<td>Google Analytics</td>
								<td></td>
								<td></td>
								<td>2 év</td>
							</tr>
							<tr>
								<th scope="row">_gat</th>
								<td>Funkció és teljesítmény</td>
								<td>Google Analytics</td>
								<td></td>
								<td></td>
								<td>1 nap</td>
							</tr>
							<tr>
								<th scope="row">_gat_UA-65687144-9</th>
								<td>Funkció és teljesítmény</td>
								<td>Google Analytics</td>
								<td></td>
								<td></td>
								<td>1 nap</td>
							</tr>
							<tr>
								<th scope="row">_gid</th>
								<td>Funkció és teljesítmény</td>
								<td>Google Analytics</td>
								<td></td>
								<td></td>
								<td>1 nap</td>
							</tr>
						</tbody>
						</table>
					</li>
					<li>
						<p>8.6. <strong>Feltétlenül szükséges sütik</strong></p>
						<ul class="list-unstyled ml-3">
							<li>
								<p>8.6.1. A feltétlenül szükséges sütik olyan funkciókat biztosítanak, amelyek nélkül a honlap nem használható rendeltetésszerűen. Ezen sütiket kizárólag az Adatkezelő használja, ezért ezek az Első Fél sütijei. Ez azt jelenti, hogy a sütikben tárolt valamennyi információ visszakerül a honlapra.</p>
							</li>
							<li>
								<p>8.6.2. A feltétlenül szükséges sütik például arra szolgálnak, hogy Érintett, mint regisztrált felhasználó mindig bejelentkezve maradjon, amikor a honlap különböző aloldalaira belép, és így ne kelljen minden alkalommal újra beírnia a belépési adatait egy új oldalra történő belépéskor.</p>
							</li>
							<li>
								<p>8.6.3. A feltétlenül szükséges sütik használata a honlapon az Érintett hozzájárulása nélkül lehetséges. A feltétlenül szükséges sütik ezért egyénileg nem aktiválhatók és deaktiválhatók. Az Érintett ugyanakkor bármikor kikapcsolhatja a sütiket a böngészőjében.</p>
							</li>
							<li>
								<p>8.6.4. Jogalap: GDPR 6.cikk (1) bekezdés b) pont.</p>
							</li>
						</ul>
					</li>
					<li>
						<p>8.7. <strong>Funkció és Teljesítmény sütik</strong></p>
						<ul class="list-unstyled ml-3">
							<li>
								<p>8.7.1. A funkció sütik lehetővé teszik, hogy a honlap már megadott adatokat tároljon (mint például a regisztrált név vagy nyelvválasztás), és ezen adatok alapján tökéletesített és személyre szabottabb funkciókat kínáljon. Ezen sütik csak anonim adatokat gyűjtenek és tárolnak, hogy ne tudják követni az Érintett egyéb honlapokon való mozgását.</p>
							</li>
							<li>
								<p>8.7.2. A teljesítmény sütik arról gyűjtenek adatokat, hogy hogyan használják a honlapot, annak érdekében, hogy a honlap vonzerejét, tartalmát és funkcióit javítsa az Adatkezelő. Ezen sütik segítenek például meghatározni, hogy a honlap aloldalai látogatottak-e, illetve, hogy mely aloldalak a látogatottak, valamint, hogy a felhasználókat különösen mely tartalmak érdeklik. A teljesítmény süt arra is szolgálnak, hogy</p>
								<ul>
									<li>rögzítsék különösen egy oldal látogatásainak a számát, az aloldalakra való belépések számát, a honlapon eltöltött időt, a látogatott oldalak sorrendjét, azt, hogy melyik keresési kifejezések vezették el az Érintettet az Adatkezelőhöz, az országot, a régiót és adott esetben a várost, ahonnan belép az Érintett, valamint a honlapra belépő mobil eszközök arányát;</li>
									<li>rögzítsék a számítógép egerének mozgását, az egérrel végrehajtott kattintásokat és görgetést is, hogy az Adatkezelő megértse, hogy a honlap mely területei érdeklik különösen a felhasználókat.</li>
								</ul>
							</li>
							<li>
								<p class="mt-2">8.7.3. Ezeket az adatokat nem az Érintett nevéhez vagy felhasználói profiljához kapcsolódóan használja fel az Adatkezelő, hanem statisztikai kimutatás részeként dolgozza azokat fel. Ennek eredményeként a felhasználók igényeinek megfelelően tudja alakítani a honlap tartalmát és optimalizálni a kínálatát. Az Érintett számítógépének technikai okok miatt továbbított IP címe automatikusan anonimizált és nem enged következtetéseket levonni az egyéni felhasználóra nézve.</p>
							</li>
							<li>
								<p>8.7.4.  Jogalap: GDPR 6. cikk (1) bekezdés f) pont.</p>
							</li>
						</ul>
					</li>
					<li>
						<p>8.8. <strong>Hozzájáruláson alapuló sütik</strong></p>
						<ul class="list-unstyled ml-2">
							<li>
								<p>8.8.1. Azon sütiket, amelyek nem feltétlenül szükségesek és nem funkció vagy teljesítmény sütik, így például a reklám / marketing sütiket, csak a kifejezett hozzájárulásával használja az Adatkezelő.</p>
							</li>
							<li>
								<p>8.8.2. Az Adatkezelő fenntartja továbbá a jogot, hogy azon adatokat, amelyekhez a honlap látogatói szokásainak az anonim elemzéséből a sütik által jut hozzá, felhasználja annak érdekében, hogy a honlapon bizonyos termékeinek egyes reklámjait megjelenítse. Mindez az Érintett előnyére válik, mert olyan reklámot vagy tartalmat jelenít meg az Adatkezelő ezáltal, amelyről úgy gondolja, hogy érdekli az Érintettet a böngészési szokásai alapján, ezáltal kevesebb olyan véletlenszerűen megjelenő reklámot vagy tartalmat fog látni, amely kevésbé esik egybe az Érintett igényeivel.</p>
							</li>
							<li>
								<p>8.8.3. A marketing sütik külső reklámtársaságoktól érkeznek (Harmadik Fél sütijei) és az Érintett által látogatott honlapokról gyűjt információt annak érdekében, hogy az Érintettnek célcsoportra irányuló reklámot készítsen.</p>
							</li>
							<li>
								<p>8.8.4. Jogalap: a GDPR 6. cikk (1) bekezdése a) pont.</p>
							</li>
							<li>
								<p>8.8.5. Az online reklámozáshoz használt sütiket letilthatja az Érintett önszabályozó programok keretében számos országban létrehozott olyan eszközök révén, amelyek biztosítják az Érintett választási lehetőségének az érvényesülését, mint amilyen például az amerikai <a href="https://www.aboutads.info/choices/" target="_blank">https://www.aboutads.info/choices/</a> vagy az európai <a href="http://www.youronlinechoices.com/hu/" target="_blank">http://www.youronlinechoices.com/hu/</a>. A sütik beállítási lehetőségei a böngésző eszköztárától függően találhatóak meg (Internet Explorer <a href="http://windows.microsoft.com/hu-hu/windows-vista/block-or-allow-cookies" target="_blank">itt</a>, Google Chrome <a href="https://support.google.com/chrome/answer/95647?hl=hu" target="_blank">itt</a>, Mozilla Firefox <a href="https://support.mozilla.org/hu/kb/sutik-engedelyezese-es-tiltasa-amit-weboldak-haszn" target="_blank">itt</a>, Safari <a href="http://support.apple.com/kb/ph5042" target="_blank">itt</a>). Ezeken a linkeken állíthatja be, milyen követési funkciókat engedélyez/tilt meg a számítógépén. A hozzájáruláson alapuló sütik használatához való hozzájárulását egyénileg, a jövőre nézve tehát bármikor visszavonhatja az Érintett a süti beállításainak megfelelő módosításával.</p>
							</li>
							<li>
								<p>8.8.6. Azok az Érintettek, akik nem szeretnék, hogy a Google Analytics jelentést készítsen a látogatásukról, telepíthetik a Google Analytics letiltó böngészőbővítményt. Ez a kiegészítő arra utasítja a Google Analytics JavaScript-szkriptjeit (ga.js, analytics.js, and dc.js), hogy ne küldjenek látogatási információt a Google számára. Emellett azok az Érintettek, akik telepítették a letiltó böngésző bővítményt, a tartalmi kísérletekben sem vesznek részt. Amennyiben az Érintett Analytics webes tevékenységének letiltási szándéka esetén felkeresheti a <a href="http://tools.google.com/dlpage/gaoptout." target="_blank">Google Analytics letiltó oldal</a>át, és telepítheti a bővítményt böngészőjéhez. A bővítmény telepítéséről és eltávolításáról további tájékoztatásért tekintse meg az adott böngészőhöz tartozó súgót.</p>
							</li>
						</ul>
					</li>
					<li>
						<p>8.9. <strong>A sütik kezelése és törlése</strong></p>
						<p>Az Érintett a böngészőjét beállíthatja (Internet Explorer <a href="http://windows.microsoft.com/hu-hu/windows-vista/block-or-allow-cookies" target="_blank">itt</a>, Google Chrome <a href="https://support.google.com/chrome/answer/95647?hl=hu" target="_blank">itt</a>, Mozilla Firefox <a href="https://support.mozilla.org/hu/kb/sutik-engedelyezese-es-tiltasa-amit-weboldak-haszn" target="_blank">itt</a>, Safari <a href="http://support.apple.com/kb/ph5042" target="_blank">itt</a>) úgy, hogy az a sütik mentését főszabályként ne engedje meg, és/vagy úgy, hogy a böngésző minden alkalommal rákérdezzen arra, hogy az Érintett egyetért-e a sütik bekapcsolásával. Az Érintett bármikor törölheti azon sütiket, amelyeket ismételten bekapcsolt. A böngésző súgó funkciója használatával részletes tájékoztatást kaphat az Érintett ennek működéséről.  A sütik használatának általános érvénnyel történő kikapcsolása korlátozhatja a honlap bizonyos funkcióinak működését.</p>
					</li>
					<li>
						<p>8.10. <strong>Google Analytics</strong></p>
						<ul class="list-unstyled ml-2">
							<li>
								<p>8.10.1. A honlap Google Analytics-et, a Google Inc. („Google”) webes elemző szolgáltatását használja. Ennek során a Google Analytics a süti egy meghatározott formáját használja, amelyet az Érintett számítógépe tárol, és amely lehetővé teszi a honlap Érintett által történő használatának elemzését. A süti által a honlap Érintett általi használatáról létrehozott információt általában egy, az Egyesült Államokban található Google szerverre továbbítják, majd ott tárolják.</p>
							</li>
							<li>
								<p>8.10.2. A sütik tárolását megakadályozhatja az Érintett, ha a böngészőszoftverének használata során a megfelelő beállításokat alkalmazza. Emellett a <a href="https://tools.google.com/dlpage/gaoptout?hl=en" target="_blank">https://tools.google.com/dlpage/gaoptout?hl=en</a> címen elérhető böngésző beépülő modul letöltésével és telepítésével megakadályozhatja az Érintett, hogy a Google rögzítse és kezelje a süti által létrehozott, a honlap Érintett által történő használatával kapcsolatos adatokat (beleértve az IP címét).</p>
							</li>
							<li>
								<p>8.10.3. A honlap a Google Analytics-et használja a felhasználói azonosítón keresztül megvalósuló látogatóáramlás eszköztől független elemzésére is. Az Érintett által történő használat különböző eszközök közötti követését kikapcsolhatja a Google fiókjában az „Információim”, „Személyes információk” alatt.</p>
							</li>
							<li>
								<p>8.10.4. Jogalap: GDPR 6. cikk (1) bekezdés f) pont.</p>
							</li>
						</ul>
					</li>
					<li>
						<p>8.11. <strong>Adatkezelő hirdetéseinek külső szolgáltatók általi megjelenítése</strong></p>
						<ul class="list-unstyled ml-2">
							<li>
								<p>8.11.1. Az Adatkezelő hirdetéseit külső szolgáltatók – így a Google is – megjelenítik különböző weboldalokon. Az Adatkezelő és a külső szolgáltatók, például a Google, saját sütiket (például a Google Analytics sütijeit) és harmadik féltől származó sütiket (például a DoubleClick sütiket) használnak együttesen a felhasználók által a webhelyen tett korábbi látogatások alapján történő tájékozódásra, illetve a hirdetések optimalizálására és megjelenítésére.</p>
							</li>
							<li>
								<p>8.11.2. Jogalap: GDPR 6. cikk (1) bekezdés f) pont.</p>
							</li>
						</ul>
					</li>
					<li>
						<p>8.12. <strong>Közösségi Beépülő Modulok / Social Plug-ins</strong></p>
						<ul class="list-unstyled ml-2">
							<li>
								<p>8.12.1. A honlapon Adatkezelő használja a közösségi hálók beépülő moduljait (Social plug-ins / „beépülő modulok”), különösen a Facebook „Megosztás” vagy „Megosztás az ismerősökkel” gombját. A Facebook honlapját, a <a href="https://www.facebook.com/" target="_blank">https://www.facebook.com/</a> weboldalt a Facebook Inc. (1601 S. California Ave, Palo Alto, CA 94304, USA) üzemelteti, melyért a Facebook Ireland Limited (Hanover Reach, 5-7 Hanover Quay, Dublin 2, Írország) a felelős Európában. A beépülő modulok általában Facebook logóval vannak megjelölve.</p>
							</li>
							<li>
								<p>8.12.2. A Facebook mellett a „Google+” (szolgáltató: Google Inc., Amphitheatre Parkway, Mountain View, CA 94043, USA),a „Twitter” (szolgáltató: Twitter, Inc., 1355 Market St, Suite 900, San Francisco, CA 94103) beépülő moduljait is használjuk.</p>
							</li>
							<li>
								<p>8.12.3. A megfelelő gombra kattintással (pl.: „Továbbítás”, „Megosztás” vagy „Megosztás ismerősökkel”) az Érintett hozzájárul ahhoz, hogy a böngészője kapcsolatot teremt az adott közösségi háló szervereivel, és továbbítja a használati adatokat a közösségi háló megfelelő üzemeltetőjéhez, és fordítva. Adatkezelő nem rendelkezik ráhatással arra, hogy a közösségi hálók ezt követően milyen jellegű és terjedelmű adatokat gyűjtenek. A közösségi háló szolgáltató felhasználói profilként tárolja az Érintettről gyűjtött adatokat és azokat reklámozás, piackutatás és/vagy a honlap keresletorientált tervezése céljából használja. Az ilyen értékelés különösen a keresletnek megfelelő reklámtevékenység kialakítása érdekében történik (a nem bejelentkezett felhasználóknak is), valamint azért, hogy a közösségi háló más felhasználóit tájékoztassa az Érintett honlapunk folytatott tevékenységeiről. Az Érintett jogosult tiltakozni ezen felhasználói profilok létrehozása ellen, amely esetben e jog gyakorlása érdekében fel kell vennie a kapcsolatot az adott beépülő modul szolgáltatójával. A beépülő modulokon keresztül Adatkezelő biztosítja az Érintett számára a lehetőséget, hogy a közösségi hálókkal és más felhasználókkal felvehesse a kapcsolatot, hogy az Adatkezelő fejleszteni tudja a kínálatát és érdekesebbé tehesse azt az Érintett, mint felhasználó számára.</p>
							</li>
							<li>
								<p>8.12.4. Az adatok arra tekintet nélkül kerülnek továbbításra, hogy van-e az Érintettnek fiókja a beépülő modul szolgáltatójánál és be van-e oda jelentkezve. Ha az Érintett bejelentkezett a beépülő modul szolgáltatójához, az Adatkezelő által gyűjtött adatait közvetlenül hozzárendelik a beépülő modul szolgáltatójánál meglévő fiókjához. Ha az "Aktivál" gombra kattint, és például kapcsolódik az oldalhoz, a beépülő modul szolgáltatója is tárolja az információt a felhasználói fiókjában és nyilvánosan megosztja azt az Érintett ismerőseivel. Adatkezelő javasolja, hogy az Érintett rendszeresen jelentkezzen ki a közösségi háló használata után, különösen mielőtt aktiválja a gombot, így elkerülheti, hogy hozzárendeljék a beépülő modul szolgáltatójánál meglévő profiljához:</p>
							</li>
							<li>
								<p>8.12.5.  Az adatgyűjtés céljával és terjedelmével, valamint a beépülő modul szolgáltatója általi adatkezeléssel kapcsolatos további információért az Érintett megtekintheti ezen szolgáltatók alább megjelölt adatvédelmi nyilatkozatait, amelyek további információval szolgálnak az ebbe a tárgykörbe tartozó jogaival, valamint az adatainak védelmét szolgáló beállítási lehetőségekkel kapcsolatban is.</p>
								<ol type="a">
									<li>
										<p>Facebook Inc. (1601 S California Ave, Palo Alto, California 94304, USA);</p>
										<ul>
											<li>
												<p><a href="http://www.facebook.com/policy.php" target="_blank">http://www.facebook.com/policy.php</a></p>
											</li>
											<li>
												<p>további információ az adatgyűjtésről: <a href="http://www.facebook.com/help/186325668085084" target="_blank">http://www.facebook.com/help/186325668085084</a>,</p>
											</li>
											<li>
												<p><a href="http://www.facebook.com/about/privacy/your-info-on-other#applications" target="_blank">http://www.facebook.com/about/privacy/your-info-on-other#applications</a>, valamint</p>
											</li>
											<li>
												<p><a href="http://www.facebook.com/about/privacy/your-info#everyoneinfo" target="_blank">http://www.facebook.com/about/privacy/your-info#everyoneinfo</a></p>
											</li>
										</ul>
										<p class="mt-2">A Facebook alávetette magát az EU-USA Adatvédelmi Pajzsnak, <a href="https://www.privacyshield.gov/EU-US-Framework" target="_blank">https://www.privacyshield.gov/EU-US-Framework</a>.</p>
									</li>
									<li>
										<p>Google Inc. (1600 Amphitheater Parkway, Mountainview, California 94043, USA);</p>
										<ul>
											<li>
												<p><a href="https://www.google.com/policies/privacy/partners/?hl=de" target="_blank">https://www.google.com/policies/privacy/partners/?hl=de</a></p>
											</li>
										</ul>
										<p class="mt-2">A Google alávetette magát az EU-USA Adatvédelmi Pajzsnak, <a href="https://www.privacyshield.gov/EU-US-Framework" target="_blank">https://www.privacyshield.gov/EU-US-Framework</a>.</p>
									</li>
									<li>
										<p>Twitter, Inc. (1355 Market St, Suite 900, San Francisco, California 94103, USA);</p>
										<ul>
											<li>
												<p><a href="https://twitter.com/privacy" target="_blank">https://twitter.com/privacy</a></p>
											</li>
										</ul>
										<p class="mt-2">A Twitter alávetette magát az EU-USA Adatvédelmi Pajzsnak, <a href="https://www.privacyshield.gov/EU-US-Framework" target="_blank">https://www.privacyshield.gov/EU-US-Framework</a>.</p>
									</li>
									<li>
										<p>Pinterest Inc. (808 Brannan Street San Francisco, CA 94103, USA);</p>
										<ul>
											<li>
												<p><a href="http://about.pinterest.com/privacy/" target="_blank">http://about.pinterest.com/privacy/</a></p>
											</li>
										</ul>
									</li>
									<li>
										<p>LinkedIn Corporation (2029 Stierlin Court, Mountain View, California 94043, USA);</p>
										<ul>
											<li>
												<p><a href="http://www.linkedin.com/legal/privacy-policy" target="_blank">http://www.linkedin.com/legal/privacy-policy</a></p>
											</li>
										</ul>
										<p class="mt-2">A LinkedIn alávetette magát az EU-USA Adatvédelmi Pajzsnak, <a href="https://www.privacyshield.gov/EU-US-Framework" target="_blank">https://www.privacyshield.gov/EU-US-Framework</a>.</p>
									</li>
								</ol>
							</li>
							<li>
								<p>8.12.6. Adatkezelő a honlapra YouTube videókat integrált, amelyeket a http://www.youtube.com  tárolnak és amelyek közvetlenül lejátszhatók a honlapról. Az Érintettről nem kerül továbbításra adat a YouTube részére, ha az Érintett nem kattint a videókra, hogy elindítsa a lejátszásukat. Kizárólag a videók lejátszása esetén kerülnek a következő bekezdésben hivatkozott adatok a YouTube részére továbbításra, mely adattovábbításra nincs befolyásunk. A honlap meglátogatásakor a YouTube tájékoztatást kap arról, hogy az Érintett belépett a honlap megfelelő aloldalára. Továbbá a jelen 8. pontban meghatározott adatok kerülnek továbbításra. Ez független attól, hogy a YouTube nyújt-e felhasználói fiókot, amelyen keresztül be van jelentkezve az Érintett, vagy nem létezik felhasználói fiók. Ha az Érintett be van jelentkezve a Google-ba, az adatait közvetlenül összekapcsolják a fiókjával. Ha az Érintett nem szeretné, hogy összekapcsolják a YouTube profiljával, a gomb aktiválása előtt kijelentkezési lehetősége van. A YouTube felhasználói profilokként tárolja az adatait és reklámozás, piackutatás és/vagy a honlapjának keresletorientált tervezése céljából használja azokat. Az ilyen értékelés különösen a keresletorientált reklámtevékenység nyújtása érdekében történik (a nem bejelentkezett felhasználóknak is), valamint azért, hogy a közösségi háló más felhasználóit tájékoztassa az Érintettet a honlapon folytatott tevékenységeiről. Az Érintett jogosult tiltakozni ezen felhasználói profilok létrehozása ellen, amely esetben ezen jog gyakorlása érdekében fel kell vennie a kapcsolatot a YouTube-bal. A YouTube általi adatgyűjtés céljával és terjedelmével, valamint a YouTube általi adatkezeléssel kapcsolatos további információért az Érintett megtekintheti a Youtube adatvédelmi szabályzatát, amelyben további információkat talál a jogairól, valamint az adatainak védelmét szolgáló beállítási lehetőségekről: <a href="https://www.google.de/intl/de/policies/privacy" target="_blank">https://www.google.de/intl/de/policies/privacy</a>;</p>
								<p>A Google alávetette magát az EU-USA Adatvédelmi Pajzsnak, <a href="https://www.privacyshield.gov/EU-US-Framework" target="_blank">https://www.privacyshield.gov/EU-US-Framework</a>.</p>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<li id="kilenc">
				<p><strong>Egyéb rendelkezések</strong></p>
				<ul class="list-unstyled">
					<li>
						<p>9.1. Amennyiben Adatkezelő módosítja az Adatkezelési Tájékoztatót, akkor erről a honlapon közleményt helyez el, illetve az Érintett által megadott e-mail címre továbbítja a módosított tájékoztatót annak érdekében, hogy az Érintett megismerhesse azt.</p>
					</li>
					<li>
						<p>9.2. Jelen Adatkezelési tájékoztatóban fel nem sorolt adatkezelésekről Adatkezelő az adat felvételekor ad tájékoztatást. A bíróság, az ügyészség, más nyomozóhatóság, a szabálysértési hatóság, a közigazgatási hatóság, a Nemzeti Adatvédelmi és Információszabadság Hatóság, illetőleg jogszabály felhatalmazása alapján más szervek tájékoztatás adása, adatok közlése, átadása, illetőleg iratok rendelkezésre bocsátása végett megkereshetik az Adatkezelőt. Az Adatkezelő e hatóságok részére – amennyiben a hatóság a pontos célt és az adatok körét megjelölte – személyes adatot csak annyit és olyan mértékben ad ki, amely a megkeresés céljának megvalósításához elengedhetetlenül szükséges.</p>
					</li>
					<li>
						<p>9.3. Ha az Adatkezelő a személyes adatokon a megszerzésük céljától eltérő célból további adatkezelést kíván végezni, a további adatkezelést megelőzően tájékoztatja az Érintettet erről az eltérő célról és minden releváns kiegészítő információról.</p>
					</li>
					<li>
						<p>9.4. Ha az Adatkezelő más címzettel is közli az adatokat, legkésőbb a személyes adatok első alkalommal való közlésekor tájékoztatja erről az Érintettet.</p>
					</li>
				</ul>
			</li>
		</ol>
	</div>
	<div class="col-md-2">
		<div class="sticky-top" id="mdb-scrollspy-ex">
			<ul class="nav nav-pills default-pills smooth-scroll">
				<li class="nav-item">
					<a class="nav-link" href="#adatkezeles_bemutatasa">1. Adatkezelés bemutatása</a>
					<?php /*
					<ul class="nav ml-3">
						<li class="nav-item">
							<a href="#tablazat_regisztracio" class="nav-link">Honlapon történő regisztrációval kapcsolatos adatkezelés összefoglalása</a>
						</li>
						<li class="nav-item">
							<a href="#tablazat_szolg_megrendel" class="nav-link">Adatkezelő által nyújtott szoltáltatással, termékeinek megrendelésével kapcsolatos adatkezelés összefoglalása</a>
						</li>
						<li class="nav-item">
							<a href="#tablazat_hirlevel" class="nav-link">Adatkezelő és partnerei által reklámot, akciót, promóciót, ajánlatot tartalmazó hírlevél küldéssel kapcsolatos adatkezelés összefoglalása</a>
						</li>
						<li class="nav-item">
							<a href="#tablazat_karrier" class="nav-link">Adatkezelő által toborzási, munkaerő-hiány betöltése céljával kapcsolatos adatkezelés összefoglalása</a>
						</li>
						<li class="nav-item">
							<a href="#tablazat_app" class="nav-link">Adatkezelő által üzemeltetett applikáció használatával kapcsolatos adatkezelés összefoglalása</a>
						</li>
						<li class="nav-item">
							<a href="#tablazat_cookie" class="nav-link">Adatkezelő sütikre vonatkozó adatkezelésének összefoglalása</a>
						</li>
						<li class="nav-item">
							<a href="#tablazat_contact" class="nav-link">Adatkezelő és az Érintett közötti kapcsolattartásra irányuló adatkezelés összefoglalása</a>
						</li>
							<a href="#tablazat_panasz" class="nav-link">Adatkezelő panaszkezelési célú adatkezelésének összefoglalása</a>
						</li>
						</li>
							<a href="#tablazat_bizonylatmegorzes" class="nav-link">Adatkezelő bizonylatmegőrzési kötelezettségének teljesítése célú adatkezelésének összefoglalása</a>
						</li>
					</ul>
					*/ ?>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#erintett">2. Érintett</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#szemelyes_adatok_forrasai">3. A személyes adatok Adatkezelő általi megismerésének forrásai</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#adatkezeles_jogalapja">4. Adatkezelés jogalapja, célja, a kezelt adatok köre és az adatkezelés időtartama</a>
					<?php /*
					<ul class="nav ml-3">
						<li class="nav-item">
							<a href="#negypontegy" class="nav-link">4.1. Az Adatkezelő honlapján történő regisztráció, felhasználói profil létrehozása céljából kezelt adatok, az adatkezelés jogalapja és időtartama</a>
						</li>
						<li class="nav-item">
							<a href="#negypontketto" class="nav-link">4.2. Az Adatkezelő által nyújtott szoltáltatással, termékeinek - regisztrációval/regisztráció nélkül történő - megrendelése céljából kezelt adatok, az adatkezelés jogalapja és időtartama</a>
						</li>
						<li class="nav-item">
							<a href="#negypontharom" class="nav-link">4.3 Adatkezelő és partnerei által reklámot, akciót, promóciót, ajánlatot tartalmazó hírlevél küldéssel kapcsolatos adatkezelés összefoglalása</a>
						</li>
						<li class="nav-item">
							<a href="#negypontnegy" class="nav-link">4.4 Toborzási célból kezelt adatok, az adatkezelés jogalapja és időtartama</a>
						</li>
						<li class="nav-item">
							<a href="#negypontot" class="nav-link">4.5 Adatkezelő által üzemeltetett applikáció Érintett általi használatának lehetővé tétele céljából kezelt adatok, az adatkezelés jogalapja és időtartama</a>
						</li>
						<li class="nav-item">
							<a href="#negyponthat" class="nav-link">4.6 Adatkezelő és az Érintett közötti kapcsolattartás céljából kezelt adatok, az adatkezelés jogalapja és időtartama</a>
						</li>
						<li class="nav-item">
							<a href="#negyponthet" class="nav-link">4.7 Adatkezelő által az Érintett által benyújtott panasz kivizsgálása céljából kezelt adatok, az adatkezelés jogalapja és időtartama</a>
						</li>
						<li class="nav-item">
							<a href="#negypontnyolc" class="nav-link">4.8 Az Adatkezelő által bizonylatmegőrzési kötelezettségének teljesítése céljából kezelt adatok, az adatkezelés jogalapja és időtartama</a>
						</li>
					</ul>
					*/ ?>
				</li>
				<li class="nav-item">
					<a href="#ot" class="nav-link">5. Adatfeldolgozás, további adatkezelők</a>
				</li>
				<li class="nav-item">
					<a href="#hat" class="nav-link">6. Adatbiztonság, az adatok megismerésére jogosultak</a>
				</li>
				<li class="nav-item">
					<a href="#het" class="nav-link">7. Érintetti jogok, jogorvoslati lehetőségek</a>
				</li>
				<li class="nav-item">
					<a href="#nyolc" class="nav-link">8. Sütik</a>
				</li>
				<li class="nav-item">
					<a href="#kilenc" class="nav-link">9. Egyéb rendelkezések</a>
				</li>
			</ul>
		</div>
	</div>
</div>
@section('footer_js')
<script>
$('body').scrollspy({ target: '#mdb-scrollspy-ex' });
</script>
@append
<style>
#mdb-scrollspy-ex .nav-item {
width: 100%;
}

#mdb-scrollspy-ex a {
font-size: .8rem;
font-weight: 400;
line-height: 1.1rem;
padding: 0 5px;
margin-top: 3px;
margin-bottom: 3px;
color: black;
}

#mdb-scrollspy-ex li .active {
font-weight: 600;
}

.mdb-scrollspy-ex-example {
height: 200px;
}
</style>