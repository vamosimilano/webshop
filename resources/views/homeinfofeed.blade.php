<? echo '<?xml version="1.0"?>'; ?>
<rss version="2.0">
	<channel>
	<?
	/*
		<generator>{{t('homeinfo_generator')}}</generator>
		<title>{{t('homeinfo_title')}}</title>
		<link>{{t('homeinfo_link')}}</link>
		<description>{{t('homeinfo_description')}}</description>
		<language>hu</language>
		<copyright>{{t('homeinfo_copyright')}}</copyright>
		<image>
			<title>{{t('homeinfo_image_title')}}</title>
			<link>{{t('homeinfo_image_link')}}</link>
			<url>{{t('homeinfo_image_url')}}</url>
		</image>
	*/
	?>
    
    @foreach( $products as $product )
	<?
	if ($product->visible!='yes') {
		continue;
	}
	$kep = $product->getDefaultImageUrl('full');
	?>
		<item>
			<product>{{$product->getName()}}</product>
			<link>{{$product->getUrl()}}</link>
			<product_image>{{ $kep }}</product_image>
			<discount_price>{{$product->getDefaultPrice()}}</discount_price>
			<price>{{$product->getDefaultFullPrice()}}</price>
			<discount_percent>{{$product->getPercentPrice()}}</discount_percent>
			
		</item>
    @endforeach
</channel>
</rss>