@if ($_SERVER["REQUEST_URI"]=="/")
	<?php $headversion="newhead"; ?>
@endif
@extends('layouts.default')
@section('content')
	@if(isset($headversion) and $headversion==="newhead")
		@include('blocks.frontpage')
	@else
		@include('blocks.home_categories')
		@include('blocks.top_products_top')
		@include('blocks.top_products')
		<?php /* @include('blocks.seasonal') */ ?>
		@include('blocks.information')
		@include('blocks.banners')
		@include('blocks.aboutus')
		@include('blocks.stores')

		@include('blocks.facebook')
		<?php /* @include('blocks.bestellers') */ ?>
	@endif

@stop
@section('footer_js')
@append