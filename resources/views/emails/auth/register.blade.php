@include('emails.header')
		<h2>{{ t('Sikeresen regisztrált a vamosimilano.hu oldalra!') }}</h2>
        <p>
        {{ t('Az alábbi e-mail címmel, és jelszóval tud belépni a webshopunkba') }}:
        <br>{{ t('E-mail') }}: {{ $email }}
        <br>{{ t('Jelszó') }}: {{ $password }}

        </p>

@include('emails.footer')
