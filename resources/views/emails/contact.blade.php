<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Új kapcsolat üzenet érkezett</h2>
        <p>Címzett: {{ Config::get('website.contact_mails.'.Input::get('toaddress')) }} </p>
        <p>Tárgy: {{ t('subject_'.Input::get('subject')) }} </p>
		<p>Név: {{ $data['name'] }} </p>
		<p>E-mail: {{ $data['email'] }} </p>
		<p>Telefonszám: {{ $data['telephone'] }} </p>
		<p>Üzenet: {{ nl2br($data['comment']) }} </p>
	</body>
</html>
