<?php if(isset($headversion) && $headversion==="newhead"):
if(!isset($_COOKIE["wishlist"])) {
	$_COOKIE["wishlist"]="zz";
}
//print_r($_COOKIE);
?>
<header class="container-fluid elegant-color">
	@include('layouts.menu')
	<div id="slide-out" class="side-nav custom-scrollbar">
		<div class="brand-container p-4">
			<img src="{{ asset('media/logo/w/vamosimilano.png') }}" alt="{{ t('project_name', 'project') }}" class="img-fluid">
		</div>
		<div class="social-container">
			<p class="text-center">
				<a href="{{ url('https://www.facebook.com/vamosimilano') }}" class="white-text social-rounded waves-effect" type="button" role="button" target="_blank"><i class="fab fa-facebook-f"></i></a>
				<a href="{{ url('https://www.twitter.com/vamosimilano') }}" class="white-text social-rounded waves-effect" type="button" role="button" target="_blank"><i class="fab fa-twitter"></i></a>
				<a href="{{ url('https://www.linkedin.com/company/vamosimilano') }}" class="white-text social-rounded waves-effect" type="button" role="button" target="_blank"><i class="fab fa-linkedin-in"></i></a>
				<a href="{{ url('https://www.youtube.com/channel/UC6ckSCubYM-M4fdOmXkfb1g') }}" class="white-text social-rounded waves-effect" type="button" role="button" target="_blank"><i class="fab fa-youtube"></i></a>
				<a href="{{ url('https://instagram.com/vamosimilano/') }}" class="white-text social-rounded waves-effect" type="button" role="button" target="_blank"><i class="fab fa-instagram"></i></a>
				<a href="{{ url('https://www.pinterest.com/vamosimilano/') }}" class="white-text social-rounded waves-effect" type="button" role="button" target="_blank"><i class="fab fa-pinterest-p"></i></a>
			</p>
		</div>
		<div class="search-container d-lg-none md-form active-primary mx-2">
			<form method="get" action="{{ url('/kereses') }}" class="w-100">
			<?php
				if( isset($_GET["q"]) && $_GET["q"]!=""){
					$value='value="'.$_GET['q'].'"';
				}
				else {
					$value="";
				}
			?>
				<input class="form-control form-control-sm white-text animated" type="text" name="q" id="sform" {{$value}}>
				<label for="sform">{{ t('Keresés') }}</label>
			</form>
		</div>
		<div class="cart-container hidden custom-scrollbar">
			@include('webshop.cart.blocks.minicart_2')
		</div>
		<div class="menu-container hidden">
			<div class="row d-lg-none no-gutters">
				<div class="col-4 text-center sofont text-center">
					<a href="{{ url('/termekek/kanape/egyenes-kanapek') }}" class="pl-0">
						<p class="white-text head-menu-item m-0"><i class="icon-vonal-egyenes"></i></p>
						<p class="white-text head-menu-caption">{{ t('Egyenes kanapék') }}</p>
					</a>
				</div>
				<div class="col-4 text-center sofont text-center">
					<a href="{{ url('/termekek/kanape/l-alaku-kanapek') }}" class="pl-0">
						<p class="white-text head-menu-item m-0"><i class="icon-vonal-lalak"></i></p>
						<p class="white-text head-menu-caption">{{ t('Sarokkanapék') }}</p>
					</a>
				</div>
				<div class="col-4 text-center sofont text-center">
					<a href="{{ url('/termekek/kanape/u-alaku-kanapek') }}" class="pl-0">
						<p class="white-text head-menu-item m-0"><i class="icon-vonal-ualak"></i></p>
						<p class="white-text head-menu-caption">{{ t('U-alakú kanapék') }}</p>
					</a>
				</div>
				<div class="col-4 text-center sofont text-center">
					<a href="{{ url('/termekek/kanape/franciaagyak') }}" class="pl-0">
						<p class="white-text head-menu-item m-0"><i class="icon-vonal-agy"></i></p>
						<p class="white-text head-menu-caption">{{ t('Franciaágyak') }}</p>
					</a>
				</div>
				<div class="col-4 text-center sofont text-center">
					<a href="{{ url('/termekek/matrac') }}" class="pl-0">
						<p class="white-text head-menu-item m-0"><i class="icon-vonal-matrac"></i></p>
						<p class="white-text head-menu-caption">{{ t('Matracok') }}</p>
					</a>
				</div>
				<div class="col-4 text-center sofont text-center">
					<a href="{{ url('/termekek/kiegeszito/fotel') }}" class="pl-0">
						<p class="white-text head-menu-item m-0"><i class="icon-vonal-fotel"></i></p>
						<p class="white-text head-menu-caption">{{ t('Fotelek') }}</p>
					</a>
				</div>
				<div class="col-4 text-center sofont text-center">
					<a href="{{ url('/termekek/kiegeszito/puff') }}" class="pl-0">
						<p class="white-text head-menu-item m-0"><i class="icon-vonal-puff"></i></p>
						<p class="white-text head-menu-caption">{{ t('Puffok') }}</p>
					</a>
				</div>
				<div class="col-4 text-center sofont text-center">
					<a href="{{ url('/termekek/kiegeszito/dohanyzoasztal') }}" class="pl-0">
						<p class="white-text head-menu-item m-0"><i class="icon-vonal-asztal"></i></p>
						<p class="white-text head-menu-caption">{{ t('Dohányzóasztal') }}</p>
					</a>
				</div>
				<div class="col-4 text-center sofont text-center">
					<a href="{{ url('/termekek/kiegeszito/eloszobabutor') }}" class="pl-0">
						<p class="white-text head-menu-item m-0"><i class="icon-vonal-eloszoba"></i></p>
						<p class="white-text head-menu-caption">{{ t('Előszobafal') }}</p>
					</a>
				</div>
				<div class="col-4 text-center sofont text-center">
					<a href="{{ url('/termekek/lampahaz') }}" class="pl-0">
						<p class="white-text head-menu-item m-0"><i class="icon-vonal-lampa"></i></p>
						<p class="white-text head-menu-caption">{{ t('Lámpák') }}</p>
					</a>
				</div>
			</div>
			<ul class="custom-scrollbar">
				<li class="border-bottom border-b-transparent d-lg-none">
					<a href="{{ url('/kiemelt-akciok') }}" class="white-text"><i class="fal fa-percent fa-fw mr-2"></i>Akciók</a>
				</li>
				<li class="border-bottom border-b-transparent d-lg-none">
					<a href="{{ url('/termekek/outlet') }}" class="white-text"><i class="fal fa-wallet fa-fw mr-2"></i> Outlet</a> 
				</li>
				<li class="border-bottom border-b-transparent d-lg-none">
					<a href="http://blog.vamosimilano.hu" target="_blank" class="white-text"><i class="fal fa-keyboard fa-fw mr-2"></i> Blog</a> 
				</li>
				<li class="border-bottom border-b-transparent d-lg-none">
					<a href="{{ url('/kapcsolat') }}" class="white-text"><i class="fal fa-phone-office fa-fw mr-2"></i> Kapcsolat</a>
				</li>
				<li class="border-bottom border-b-transparent">
					<a href="{{ url('/rolunk') }}" class="white-text"><i class="fal fa-briefcase fa-fw mr-2"></i> {{ t('Rólunk') }}</a>
				</li>
				<?php if( date("Ymd") > "20190531" ) { ?>
				<li class="border-bottom border-b-transparent">
					<a href="{{ url('/fizetesi-modok') }}" class="white-text"><i class="fal fa-money-bill-alt fa-fw mr-2"></i> {{ t('Fizetési módok') }}</a>
				</li>
				<?php } ?>
				<li class="border-bottom border-b-transparent">
					<a href="{{ url('/aruhitel') }}" class="white-text"><i class="fal fa-landmark fa-fw mr-2"></i> {{ t('Áruhitel') }}</a>
				</li>
				<li class="border-bottom border-b-transparent">
					<a href="{{ url('/hazhozszallitas') }}" class="white-text"><i class="fas fa-people-carry fa-fw mr-2"></i> {{ t('Szállítás') }}</a>
				</li>
				<li class="border-bottom border-b-transparent">
					<a href="{{ url('/karrier') }}" class="white-text"><i class="far fa-user-tie fa-fw mr-2"></i> {{ t('Karrier') }}</a>
				</li>
				<li class="border-bottom border-b-transparent">
					<a href="{{ action('CmsController@index', t('aszf', 'url')) }}" class="white-text"><i class="fal fa-handshake fa-fw mr-2"></i> {{ t('ÁSZF') }}</a>
				</li>
				<li class="border-bottom border-b-transparent">
					<a href="{{ url('/adatvedelem') }}" class="white-text"><i class="fal fa-user-shield fa-fw mr-2"></i> {{ t('Adatvédelem') }}</a>
				</li>
			</ul>
		</div>
		<div class="sidenav-bg elegant-color"></div>
	</div>
</header>
<?php else: ?>
<header class="page-header type7">
    <div class="panel wrapper">
        <div class="panel header">
            @include('layouts.search')
            <div class="minicart-wrapper" data-block="minicart" id="minicart">

            </div>

            <span class="split"></span>
                <div class="toplinks-area">
                    @if (Auth::user())
                         <a class="toplinks-toggle upper" href="{{ action('ProfileController@index') }}">{{ t('hello_user', 'global', ['firstname' => Auth::user()->firstname]) }}</a>
                    @else
                         <a class="toplinks-toggle upper" href="{{ action('AuthController@login') }}">{{ t('Belépés / Regisztráció') }}</a>
                    @endif

                </div>
                <span class="split"></span>
                <div class="custom-block">
                    <em style="margin-right: 5px;" class="porto-icon-phone"></em>
                    <span>{{ t('phone_number') }}</span>
                    <span class="split"></span>
                    <a href="{{ action('ContactController@index') }}" class="upper" title="{{ t('Kapcsolat') }}">{{ t('Kapcsolat') }}</a>
                </div>
        </div>
    </div>
    @include('layouts.menu')
</header>
<?php endif; ?>