@section ("footer_js")
<script>
function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}
function cookieBar(sopen, mopen) {
	if(sopen === "closed" && mopen === "closed") {
		$(".cookie_container").removeClass("show");
		$(".cookie_container").addClass("hidden");
	}
	else {
		$(".cookie_container").removeClass("hidden");
		$(".cookie_container").addClass("show");
	}
	console.log("cookiebar lefutott");
};	
$(".modaltoggle").on("click", function(){
	"use strict";
	//$('#cookieModal').modal('toggle');
	$('#cookieModal').addClass("in show");
});
$(".btn-cookie").on("click", function(){
	"use strict";
	//$('#cookieModal').modal('toggle');
	$('#cookieModal').removeClass("in show");
});
var stat_cookie = getCookie("stat_cookie");
var marketing_cookie = getCookie("marketing_cookie");
var mopen = "open";
var sopen = "open";

if(stat_cookie==="yes") {
	//console.log("stat_cookie="+stat_cookie);
	//$("#cookie_stat").prop("checked", true);
	sopen = "closed";
	dataLayer.push({
		stat_cookie: "yes"
	});	
}
else if(stat_cookie==="no"){
	//console.log("stat_cookie="+stat_cookie);
	//$("#cookie_stat").prop("checked", false);
	sopen = "closed";
	dataLayer.push({
		stat_cookie: "no"
	});
}
else {
	//console.log("nincs stat cookie");
	//$("#cookie_stat").prop("checked", true);
	stat_cookie = "yes";
	sopen = "open";
	dataLayer.push({
		stat_cookie: "yes"
	});
}

if(marketing_cookie==="yes") {
	//console.log("marketing_cookie="+marketing_cookie);
	//$("#marketing_cookie").prop("checked", true);
	mopen = "closed";
	dataLayer.push({
		marketing_cookie: "yes"
	});	
}
else if(marketing_cookie==="no") {
	//console.log("marketing_cookie="+marketing_cookie);
	//$("#marketing_cookie").prop("checked", false);
	mopen = "closed";
	dataLayer.push({
		marketing_cookie: "no"
	});
}
else {
	//console.log("nincs marketing cookie");
	//marketing_cookie="no";
	$("#marketing_cookie").prop("checked", false);
	mopen = "open";
	dataLayer.push({
		marketing_cookie: "no"
	});
}
$("#cookie_stat").on("click", function(){
	if($("#cookie_stat").val()==="yes") {
		$("#cookie_stat").val("no");
		//$("#cookie_stat").prop("checked", false);
		//alert("stat cookie kikapcs");
		dataLayer.push({
			stat_cookie: "no"
		});
	}
	else {
		$("#cookie_stat").val("yes");
		//$("#cookie_stat").removeAttr("checked");
		dataLayer.push({
			stat_cookie: "yes"
		});
	}
});
$("#cookie_ad").on("click", function(){
	if($("#cookie_ad").val()==="yes") {
		$("#cookie_ad").val("no");
		//$("#cookie_ad").removeAttr("checked");
		//alert("z");
		dataLayer.push({
			marketing_cookie: "no"
		});
	}
	else {
		$("#cookie_ad").val("yes");
		//$("#cookie_ad").prop("checked", true);
		dataLayer.push({
			marketing_cookie: "yes"
		});
	}
});	
$(".btn-cookie").on("click", function(){
	var reload = "no";
	if($("#cookie_stat").prop("checked", true) && stat_cookie==="no"){
		reload="yes"
		//alert("1");
	}
	if ($("#cookie_ad").prop("checked", true) && marketing_cookie==="no"){
		reload = "yes";
		//alert("2");
	}
	stat_cookie=$("#cookie_stat").val();
	marketing_cookie=$("#cookie_ad").val();
	//alert(stat_cookie + " " + marketing_cookie);
	setCookie("marketing_cookie", marketing_cookie, 365);
	setCookie("stat_cookie", stat_cookie, 365);
	if(reload==="yes"){
	   setTimeout(location.reload(), 20);
	   }
	//alert("stat "+stat_cookie);
	//alert("mark "+marketing_cookie);
	if($("#cookie_ad").val()==="no") {
		$("#cookie_ad").prop("checked", false);
	}
	else {
		$("#cookie_ad").prop("checked", true);
	}

	if($("#cookie_stat").val()==="no") {
		$("#cookie_stat").prop("checked", false);
	}
	else {
		$("#cookie_stat").prop("checked", true);
	}

cookieBar("closed", "closed");
});
cookieBar(sopen, mopen);
</script>
@append
<style>
	.cookie_container {
		padding: 1.2rem 0;
		background-color: #000000;
		font-size: 11px;
		color: #ccc;
		margin: 0px;
	}
	.modal-content {
		background: white;
	}
</style>
<button class="modaltoggle hidden" type="button" id="ujbutton">Megnyit</button> 
<div class="container-fluid cookie_container hidden">
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<p>{{t('cookie_bar_text')}}</p>
			</div>
			<div class="col-md-2 text-right">
				<button type="button" class="btn btn-primary action primary btn-cookie">{{t('elfogadom')}}</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="cookieModalLabel" id="cookieModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" style="max-height: 90vh; overflow: auto;">
			<div class="modal-body">
				<ul class="nav nav-tabs">
					<li><a data-toggle="tab" href="#home">{{t('cookie_settings')}}</a></li>
					<li><a data-toggle="tab" href="#menu1">{{t('cookie_basic')}}</a></li>
					<li><a data-toggle="tab" href="#menu2">{{t('cookie_stat')}}</a></li>
					<li class="active"><a data-toggle="tab" href="#menu3">{{t('cookie_marketing')}}</a></li>
				</ul>
				<div class="tab-content">
					<div id="home" class="tab-pane fade">
						<h3>{{t('cookie_settings')}}</h3>
						{{t('cookie_bar_text')}}
					</div>
					<div id="menu1" class="tab-pane fade">
						<h3>{{t('cookie_basic')}}</h3>
						{{t('cookie_basic_text')}}
						<div class="checkbox disabled">
							<label><input type="checkbox" name="cookie_basic" id="cookie_basic" value="yes" checked readonly disabled>{{t('engedélyezem')}}</label>
						</div>
					</div>
					<div id="menu2" class="tab-pane fade">
						<h3>{{t('cookie_stat')}}</h3>
						{{t('cookie_stat_text')}}
						<div class="checkbox">
							<?php if(isset($_COOKIE["stat_cookie"]) && $_COOKIE["stat_cookie"]==="yes"): 
								$svalue="yes";
								$schecked="checked";
							else:
								$svalue="no";
								$schecked="unchecked";
							endif;
							if(!isset($_COOKIE["stat_cookie"])):
								$svalue="yes";
								$schecked="checked";

							endif;
							?>
							<label><input type="checkbox" name="cookie_stat" id="cookie_stat" value="{{$svalue}}" {{$schecked}}>{{t('engedélyezem')}}</label>
						</div>
					</div>
					<div id="menu3" class="tab-pane fade in active">
						<h3>{{t('cookie_marketing')}}</h3>
						{{t('cookie_marketing_text')}}
						<div class="checkbox">
							<?php if(isset($_COOKIE["marketing_cookie"]) && $_COOKIE["marketing_cookie"]==="yes"): 
								$mvalue="yes";
								$mchecked="checked";
							else:
								$mvalue="no";
								$mchecked="unchecked";
							endif;
							?>
							<label><input type="checkbox" name="cookie_ad" id="cookie_ad" value="{{$mvalue}}" {{$mchecked}}>{{t('engedélyezem')}}</label>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary action primary btn-cookie" data-dismiss="modal">{{t('elfogadom')}}</button>
			</div>
		</div>
	</div>
</div>