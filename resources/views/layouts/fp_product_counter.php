<div class="container-fluid white-text px-0" id="fp_product_counter">
	<div class="streak view mx-0">
		<?php if(ismobile()) { ?>
		<img src="<?php echo asset('/media/bg/counter_mobile.jpg'); ?>" class="img-fluid mx-auto" alt="Vamosi Milano">
		<?php } else { ?>
		<img src="<?php echo asset('/media/bg/counter_desktop.jpg'); ?>" class="img-fluid mx-auto" alt="Vamosi Milano">
		<?php } ?>
		<div class="mask rgba-black-strong flex-center">
			<nav class="d-block text-center w-75">
				<div class="row">
					<div class="col-sm-6 mb-4 mb-sm-0" id="spy_sofa_counter">
						<h2 class="h4-responsive text-uppercase title-center">Legyártott bútoraink</h2>
						<p class="h2-responsive sofa_counter" data-count="<?php echo $manufacturedcounter+10000; ?>" class="lead"></p>
					</div>
					<?php /*
					<div class="col-sm-4 mb-4 mb-sm-0" id="spy_customer_counter">
						<h2 class="h4-responsive text-uppercase title-center">Elégedett vásárlóink</h2>
						<p class="h2-responsive sofa_counter" data-count="<?php echo $ordercounter; ?>" class="lead"></p>
					</div>
					*/ ?>
					<div class="col-sm-6 mb-4 mb-sm-0" id="spy_customer_counter">
						<h2 class="h4-responsive text-uppercase title-center">Választható kárpitfajta</h2>
						<p class="h2-responsive sofa_counter" data-count="<?php echo $textile_count; ?>" class="lead"></p>
					</div>
				</div>
			</nav>
		</div>
	</div>
</div>