			<?php
			if( sizeof($showroom) ){
			?>
			<section id="showroom{{$sid}}" class="row mx-0">
			<?php
				if( sizeof($showroom) === 1) { //default slide
					//több slide van
					$slidenum=0;
					foreach($showroom as $slide ) {
				?>
				<article id="article-{{$slidenum}}" class="col-12 bg-elegant">
					<a href="{{$slide->link}}">
						<img src="{{$slide->getMobilImageUrl()}}" alt="{{$slide->title}}" class="img-fluid">
					</a>
						<div class="dark-panel">
							<h2 class="mt-2 text-bold">{{$slide->title}}</h2>
							<?php if( isset($slide->title2) && $slide->title2!=""){ ?>
							<p class="mb-3">{{$slide->title2}}</p>
							<?php
							}//if 
							if( isset($slide->title3) && $slide->title3!=""){
							?>
							<p class="text-center"><a href="{{$slide->link}}" class="btn btn-primary mb-3 text-uppercase">{{$slide->title3}}</a></p>
							<?php }//if ?>
						</div>
				</article>
				<?php
						$slidenum++;
					}//foreach
				}
				else {
					//több slide van
					$slidenum=0;
					foreach($showroom as $slide ) {
						if($slide->weight !== 0){
				?>
				<article id="article-{{$slidenum}}" class="col-12 bg-elegant">
					<a href="{{$slide->link}}">
						<img src="{{$slide->getMobilImageUrl()}}" alt="{{$slide->title}}" class="img-fluid">
					</a>
						<div class="dark-panel">
							<h2 class="mt-2 text-bold">{{$slide->title}}</h2>
							<?php if( isset($slide->title2) && $slide->title2!=""){ ?>
							<p class="mb-3">{{$slide->title2}}</p>
							<?php
							}//if 
							if( isset($slide->title3) && $slide->title3!=""){
							?>
							<p class="text-center"><a href="{{$slide->link}}" class="btn btn-primary mb-3 text-uppercase">{{$slide->title3}}</a></p>
							<?php }//if ?>
						</div>
				</article>
				<?php
						$slidenum++;
						}//if
					}//foreach
				}//else 
				?>
			</section><!-- //carousel -->
			<?php
			}//if van showroom
			?>