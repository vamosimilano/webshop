<div class="search-area desktop-view">
                <a class="search-toggle-icon" href="javascript:;"><em class="porto-icon-search"></em>{{ t('Keresés') }}</a>
                <div class="block block-search">
                    <div class="block block-title"><strong>{{ t('Keresés') }}</strong></div>
                    <div class="block block-content">
						<form method="get" action="{{ action('WebshopController@search') }}" id="search_mini_form" class="form minisearch">
                            <div class="field search">
                                <label data-role="minisearch-label" for="search" class="label">
                                    <span>{{ t('Keresés') }}</span>
                                </label>
                                <div class="control">
                                    <input type="text" autocomplete="off" aria-autocomplete="both" aria-haspopup="false" aria-expanded="false" role="combobox" class="input-text" placeholder="{{ t('Írja be a keresőszót') }}" value="" maxlength="128" name="q" required>
                                </div>
                            </div>
                            <div class="actions">
                                <button class="action search" title="{{ t('Keresés') }}" type="submit">
                                    <span>{{ t('Keresés') }}</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
