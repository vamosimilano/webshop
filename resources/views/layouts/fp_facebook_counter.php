<?php
$likes = DB::table('fb_data')->select('fan_count')->get();
foreach ($likes as $like) {}//endforeach
?>

<?php
if( ismobile() ) {
?>
<div class="container-fluid btn-fb white-text">
	<div class="row">
		<div class="col-12 text-center pt-3">
			<a href="https://www.facebook.com/vamosimilano" target="_blank" class="white-text">
				<h2 class="h4-responsive">Kövesse a Vamosi-t és legyen részese Magyarország egyik legnagyobb Facebook közösségének!</h2>
				<p><i class="fab fa-facebook fa-3x"></i></p>
				<p><?php echo number_format($like->fan_count, 0, ',', ' '); ?> követő</p>
				<p class="text-left">A Vamosi Milano Facebook oldalán mindig értesül fantasztikus kanapé és franciaágy akcióinkról, folyamatos kedvezményeinkről és újdonságainkról.</p>
				<p class="text-left">Ne feledje, havonta hatalmas nyereményjátékokat hirdetünk követőink, lájkolóink és megosztóink számára – vesse bele magát a játék izgalmába és szerezze meg álmai bútorát!</p>
			</a>
		</div>
	</div>
</div>
<?php
}//mobil
else {
?>
<div class="container-fluid btn-fb">
	<div class="row url mx-0" onClick="javascript:window.open('https://www.facebook.com/vamosimilano','_blank')">
		<div class="col-md-6 d-flex align-items-center">
			<div class="d-block text-center w-100">
				<i class="fab fa-facebook fa-5x"></i>
				<h2 class="mt-3"><?php echo number_format($like->fan_count, 0, ',', ' '); ?></h2>
				<p class="lead">követő</p>
			</div>
		</div>
		<div class="col-md-6 py-5">
			<h2 class="h4-responsive">Kövesse a Vamosi-t és legyen részese Magyarország egyik legnagyobb Facebook közösségének!</h2>
			<p>A Vamosi Milano Facebook oldalán mindig értesül fantasztikus kanapé és franciaágy akcióinkról, folyamatos kedvezményeinkről és újdonságainkról.</p>
			<p>Ne feledje, havonta hatalmas nyereményjátékokat hirdetünk követőink, lájkolóink és megosztóink számára – vesse bele magát a játék izgalmába és szerezze meg álmai bútorát!</p>
		</div>
	</div>
</div>
<?php
}//desktop
?>