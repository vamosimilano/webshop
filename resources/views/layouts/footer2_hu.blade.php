<div class="container">
	<div class="row mt-5">
		<div class="col-12">
			<h2 class="h2-responsive title-center text-uppercase mb-4">Maximalizálja az elérhető kedvezményeket!</h2>
		</div>
		<div class="col-md-4">
			<h2 class="h4-responsive text-uppercase">Kiterjesztett garancia</h2>
			<p>Vásároljon új bútora mellé kiterjesztett garanciát, akár 10 év időtartamra az első év gyártói garancia meghosszabbításaként. Tíz éves garancia esetén, önköltségi áron, akár öt alkalommal is újrakárpitozhatja bútorát.</p>
		</div>
		<div class="col-md-4">
			<h2 class="h4-responsive text-uppercase">Ingyenes kiszállítás</h2>
			<p>Rendelését az egész ország területén ingyen kiszállítjuk! A Vamosi Milano bútorai a kényelem és elegancia megtestesítői, így Önnek nem kell foglalkoznia a szállítással és összeszereléssel.</p>
		</div>
		<div class="col-md-4">
			<h2 class="h4-responsive text-uppercase">Utazási támogatás</h2>
			<p>Bemutatótermeinktől távol élő ügyfeleinknek is egyenlő lehetőséget szeretnénk teremteni bútoraink személyes megtekintésére, kipróbálására, ezért utazási visszatérítést biztosítunk számukra vásárlásuk alkalmával.</p>
		</div>
		<div class="col-12 text-center">
			<p>Bővebb információt kedvezményeinkről <a href="{{url('/kapcsolat')}}">ügyfélszolgálatunkon</a>, <a href="{{url('/hazhozszallitas')}}">házhozszállítási oldalunkon</a> és az <a href="{{url('/aszf')}}">ÁSZF</a>-ben kaphat.</p>
		</div>
	</div>
</div>
<div class="container-fluid bg-dark pt-5 border-bottom border-b-transparent">
	<div class="container pb-5">
		<div class="row text-center">
			<div class="col-sm-4">
				<a href="mailto:info@vamosimilano.hu" class="text-white"><i class="fal fa-at fa-fw fa-2x mb-3"></i><br><span class="d-none d-md-block">info@vamosimilano.hu</span></a>
			</div>
			<div class="col-sm-4">
				<a href="tel:+3613539410" class="text-white"><i class="fal fa-phone fa-fw fa-2x mb-3"></i><br><span class="d-none d-md-block">+36-1-353-9410</span></a>
			</div>
			<div class="col-sm-4">
				<a href="https://m.me/vamosimilano" target="_blank" class="text-white"><i class="fab fa-facebook-messenger fa-fw fa-2x mb-3"></i><br><span class="d-none d-md-block">Messenger Chat</span></a>
			</div>
		</div>
	</div>
</div>
<aside class="container-fluid bg-secondary bg-subscribe">
	<div class="container">
		<form id="subForm" class="js-cm-form cpntainer py-5" action="https://www.createsend.com/t/subscribeerror?description=" method="post" data-id="{{t('newlettersubscribeid')}}">
		<div class="row py-5">
			<div class="col-md-12">
				<h3 class="h3-responsive title">{{ t('Értesüljön akcióinkról!') }}</h3>
				<p>{{ t('Értesüljön elsőként a meghirdetett akcióinkról') }}</p>
			</div>
			<div class="col-md-6 col-lg-3">
				<div class="md-form px-0 active-primary">
					<input type="text" class="form-control" id="fieldName" name="cm-name" type="text" required >
					<label for="fieldName">{{ t('Név') }}</label>
				</div>
			</div>
			<div class="col-md-6 col-lg-3">
				<div class="md-form px-0 active-primary">
					<input id="fieldEmail" class="js-cm-email-input form-control" name="{{t('newlettersubscribename')}}" type="text" required>
					<label for="fieldEmail">{{ t('E-mail') }}</label>
				</div>
			</div>
			<div class="col-md-8 col-lg-4">
				<div class="form-check md-form form-check-inline px-0">
					<input  id="fieldok" class="form-check-input js-cm-email-input" type="checkbox" required >
					<label for="fieldok" class="form-check-label">{{t('Feliratkozasengedélyszöveg')}}</label>
				</div>
			</div>
			<div class="col-md-4 col-lg-2">
				<div class="md-form px-0">
					<button type="submit" class="btn btn-sm btn-primary float-right">{{t('Feliratkozás')}}</button>
				</div>
			</div>
		</div>
		</form>
	</div>
@section('footer_js')
<script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>
@append
</aside>
<div class="container-fluid elegant-color pt-5">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-3 col-sm-6 sofont-ribbon" data-tipus="lalak">
				<p><a href="{{ url('/termekek/kanape/l-alaku-kanapek') }}" class="text-white"><i class="icon-vonal-lalak sofont-large"></i><br>Sarokkanapék</a></p>
			</div>
			<div class="col-md-3 col-sm-6 sofont-ribbon" data-tipus="ualak">
				<p><a href="{{ url('/termekek/kanape/u-alaku-kanapek') }}" class="text-white"><i class="icon-vonal-ualak sofont-large"></i><br>U-alakú kanapék</a></p>
			</div>
			<div class="col-md-3 col-sm-6 sofont-ribbon" data-tipus="agy">
				<p><a href="{{ url('/termekek/kanape/franciaagyak') }}" class="text-white"><i class="icon-vonal-agy sofont-large"></i><br>Franciaágyak</a></p>
			</div>
			<div class="col-md-3 col-sm-6 sofont-ribbon" data-tipus="egyenes">
				<p><a href="{{ url('/termekek/kanape/egyenes-kanapek') }}" class="text-white"><i class="icon-vonal-egyenes sofont-large"></i><br>Egyenes kanapék</a></p>
			</div>
		</div>
	</div>
</div>
<footer class="container-fluid elegant-color pt-5 white-text">
	<div class="container my-0">
		<div class="row">
			<div class="col-lg-4 order-3 mb-md-5 pb-5">
				<h3 class="h3-responsive title text-center text-md-left">Üzletünk</h3>
				<ul class="list-unstyled mt-3">
					<li>
						<div class="row mb-1">
							<div class="col-1 text-center"><i class="fal fa-map-marked fa-fw"></i></div>
							<div class="col-10">1119 Budapest, Petzvál József utca 46-48.</div>
						</div>
					</li>
					<li>
						<div class="row mb-1">
							<div class="col-1 text-center"><i class="fal fa-clock fa-fw"></i></div>
							<div class="col-4">Hétfő:</div>
							<div class="col-6 text-right">10:00 - 18:30</div>
						</div>
						<div class="row mb-1">
							<div class="col-1"></div>
							<div class="col-4">Kedd:</div>
							<div class="col-6 text-right">10:00 - 18:30</div>
						</div>
						<div class="row mb-1">
							<div class="col-1"></div>
							<div class="col-4">Szerda:</div>
							<div class="col-6 text-right">10:00 - 18:30</div>
						</div>
						<div class="row mb-1">
							<div class="col-1"></div>
							<div class="col-4">Csütörtök:</div>
							<div class="col-6 text-right">10:00 - 18:30</div>
						</div>
						<div class="row mb-1">
							<div class="col-1"></div>
							<div class="col-4">Péntek:</div>
							<div class="col-6 text-right">10:00 - 18:30</div>
						</div>
						<div class="row mb-1">
							<div class="col-1"></div>
							<div class="col-4">Szombat:</div>
							<div class="col-6 text-right">10:00 - 18:30</div>
						</div>
						<div class="row mb-1">
							<div class="col-1"></div>
							<div class="col-4">Vasárnap:</div>
							<div class="col-6 text-right">11:00 - 18:00</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="col-lg-4 order-2 mb-md-5 pb-5">
				<h3 class="h3-responsive title text-center text-lg-left">Hasznos információk</h3>
				<ul class="list-unstyled mt-3 d-none d-lg-block">
					<li class="mb-1"><i class="fal fa-question-circle fa-fw mr-3"></i>{{ t('Gyakori kérdések') }}</li>
					<li class="mb-1"><i class="fal fa-handshake fa-fw mr-3"></i>{{ t('Partnereknek') }}</li>
					<li class="mb-1">
						<a href="{{ url('/aszf') }}" class="white-text">
							<i class="fal fa-balance-scale fa-fw mr-3"></i>{{ t('ÁSZF') }}
						</a>
					</li>
					<li class="mb-1">
						<a href="{{ url('/adatvedelem') }}" class="white-text">
							<i class="fal fa-user-shield fa-fw mr-3"></i>{{ t('Adatvédelem') }}
						</a>
					</li>
					<li class="mb-1">
						<a href="{{ url('/bankkartyas-fizetes-tajekoztato') }}" class="white-text">
							<i class="fal fa-credit-card fa-fw mr-3"></i>{{ t('Bankkártyás fizetés') }}
						</a>
					</li>
					<li class="mb-1">
						<a href="{{ url('/impresszum') }}" class="white-text">
							<i class="fal fa-users fa-fw mr-3"></i>{{ t('Impresszum') }}
						</a>
					</li>
					<li class="mb-1">
						<a href="{{ url('/karrier') }}" class="white-text">
							<i class="fal fa-id-card-alt fa-fw mr-3"></i>{{ t('Karrier') }}
						</a>
					</li>
					<li class="mb-1">
						<a href="javascript:void(0);" onClick="cookiebar('open')" class="white-text">
							<i class="fal fa-cookie-bite fa-fw mr-3"></i>{{ t('Cookie beállítások') }}
						</a>
					</li>
				</ul>
				<ul class="list-inline mt-3 d-lg-none text-center">
					<li class="list-inline-item mb-1">{{ t('Gyakori kérdések') }}</li>
					<li class="list-inline-item mb-1">{{ t('Partnereknek') }}</li>
					<li class="list-inline-item mb-1">
						<a href="{{ url('/aszf') }}" class="white-text">{{ t('ÁSZF') }}</a>
					</li>
					<li class="list-inline-item mb-1">
						<a href="{{ url('/adatvedelem') }}" class="white-text">{{ t('Adatvédelem') }}</a>
					</li>
					<li class="list-inline-item mb-1">
						<a href="{{ url('/bankkartyas-fizetes-tajekoztato') }}" class="white-text">{{ t('Bankkártyás fizetés') }}</a>
					</li>
					<li class="list-inline-item mb-1">
						<a href="{{ url('/impresszum') }}" class="white-text">{{ t('Impresszum') }}</a>
					</li>
					<li class="list-inline-item mb-1">
						<a href="{{ url('/karrier') }}" class="white-text">{{ t('Karrier') }}</a>
					</li>
					<li class="list-inline-item mb-1">
						<a href="javascript:void(0);" onClick="cookiebar('open')" class="white-text">{{ t('Cookie beállítások') }}</a>
					</li>
				</ul>
			</div>
			<div class="col-lg-4 text-center text-lg-left order-1 mb-md-5 pb-5">
				<h3 class="h3-responsive title mx-auto">Kövessen minket!</h3>
				<ul class="list-unstyled mt-3 d-none d-lg-block">
					<li>
						<a href="{{ url('https://www.facebook.com/vamosimilano') }}" class="white-text" target="_blank">
							<div class="row mb-1">
								<div class="col-1 text-center">
									<i class="fab fa-facebook-f fa-fw"></i>
								</div>
								<div class="col-10">
									{{ t('Facebook') }}
								</div>
							</div>
						</a>
					</li>
					<li>
						<a href="{{ url('https://www.twitter.com/vamosimilano') }}" class="white-text" target="_blank">
							<div class="row mb-1">
								<div class="col-1 text-center">
									<i class="fab fa-twitter fa-fw"></i>
								</div>
								<div class="col-10">
									{{ t('Twitter') }}
								</div>
							</div>
						</a>
					</li>
					<li>
						<a href="{{ url('https://www.linkedin.com/company/vamosimilano') }}" class="white-text" target="_blank">
							<div class="row mb-1">
								<div class="col-1 text-center">
									<i class="fab fa-linkedin-in fa-fw"></i>
								</div>
								<div class="col-10">
									{{ t('LinkedIn') }}
								</div>
							</div>
						</a>
					</li>
					<li>
						<a href="{{ url('https://www.youtube.com/channel/UC6ckSCubYM-M4fdOmXkfb1g') }}" class="white-text" target="_blank">
							<div class="row mb-1">
								<div class="col-1 text-center">
									<i class="fab fa-youtube fa-fw"></i>
								</div>
								<div class="col-10">
									{{ t('YouTube') }}
								</div>
							</div>
						</a>
					</li>
					<li>
						<a href="{{ url('https://instagram.com/vamosimilano/') }}" class="white-text" target="_blank">
							<div class="row mb-1">
								<div class="col-1 text-center">
									<i class="fab fa-instagram fa-fw"></i>
								</div>
								<div class="col-10">
									{{ t('Instagram') }}
								</div>
							</div>
						</a>
					</li>
					<li>
						<a href="{{ url('https://www.pinterest.com/vamosimilano/') }}" class="white-text" target="_blank">
							<div class="row mb-1">
								<div class="col-1 text-center">
									<i class="fab fa-pinterest-p fa-fw"></i>
								</div>
								<div class="col-10">
									{{ t('Pinterest') }}
								</div>
							</div>
						</a>
					</li>
				</ul>
				<ul class="list-inline mt-3 d-lg-none">
					<li class="list-inline-item"><a href="{{ url('https://www.facebook.com/vamosimilano') }}" class="white-text" target="_blank"><i class="fab fa-facebook-f fa-fw"></i></a></li>
					<li class="list-inline-item"><a href="{{ url('https://www.twitter.com/vamosimilano') }}" class="white-text" target="_blank"><i class="fab fa-twitter fa-fw"></i></a></li>
					<li class="list-inline-item"><a href="{{ url('https://www.linkedin.com/company/vamosimilano') }}" class="white-text" target="_blank"><i class="fab fa-linkedin-in fa-fw"></i></a></li>
					<li class="list-inline-item"><a href="{{ url('https://www.youtube.com/channel/UC6ckSCubYM-M4fdOmXkfb1g') }}" class="white-text" target="_blank"><i class="fab fa-youtube fa-fw"></i></a></li>
					<li class="list-inline-item"><a href="{{ url('https://instagram.com/vamosimilano/') }}" class="white-text" target="_blank"><i class="fab fa-instagram fa-fw"></i></a></li>
					<li class="list-inline-item"><a href="{{ url('https://www.pinterest.com/vamosimilano/') }}" class="white-text" target="_blank"><i class="fab fa-pinterest-p fa-fw"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
<div class="container-fluid bg-dark">
	<div class="container my-0 pt-3">
		<div class="row py-2">
			<div class="col-md-12 text-center">
				<p>
					<a href="{{url('/')}}" class=" text-white" title="{{ t('project_name', 'project') }}">
						<img src="{{ asset('media/logo/w/vamosimilano-inline-square.png') }}" alt="{{ t('project_name', 'project') }}" class="img-fluid ih-100">
					</a>
					<a href="{{url('/bankkartyas-fizetes-tajekoztato')}}" class=" text-white">
						<img src="/media/cc/maestro.png" alt="Maestro" class="img-fluid ih-100">
						<img src="/media/cc/mastercard.png" alt="Mastercard" class="img-fluid ih-100">
						<img src="/media/cc/visa.png" alt="Visa" class="img-fluid ih-100">
						<img src="/media/cc/visa_electron.png" alt="Visa Electron" class="img-fluid ih-100">
						<img src="/media/cc/unicredit.png" alt="Unicredit" class="img-fluid ih-100">
					</a>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 mx-auto">
				<p class="text-center white-text lead">Vamosi Milano - <?php echo date('Y'); ?></p>
			</div>
			<div class="col-12 my-4">
				<p class="text-muted"><small><strong>*</strong> A tájékoztatás nem teljeskörű, az áruhitel és az ingyenes házhoz szállítás részleteiről tájékozódjon weboldalunkon, illetve az ÁSZF-ben! Az egyes kedvezmények nem összevonhatók. Az akár 70% mértékű kedvezmény kizárólag az outlet bútorként forgalmazott bútorokra vonatkozik. Az outlet bútorokat a készlet erejéig lehet megvásárolni, az aktuális készlet folyamatosan változik.</small></p>
			</div>
		</div>
	</div>
</div>
@section('footer_js')
<script>
	new WOW().init();
</script>
<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
@append
@include('layouts.cookietop2')
<?php
/*
	1. Cookie Consent lebegő alert windowba
	2. Beállítások harmónikába az alerten belül
	3. A Cookie beállítások gomb megy a hasznos infók menübe
*/
?>