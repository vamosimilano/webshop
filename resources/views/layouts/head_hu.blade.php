 <script>

    var require = {

        "baseUrl": "{{ url('/') }}"

    };

	dataLayer = [];

</script>



    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/styles-m.css') }}" />
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/bootstrap.css') }}" />

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/bootstrap-theme.css') }}" />

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/owl.carousel.css') }}" />

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/animate.css') }}" />

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/jquery.fancybox.css') }}" />

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/transitions.css') }}" />

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/porto-icons-codes.css') }}" />

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/animation.css') }}" />

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/custom.css') }}?v3" />

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/magnific-popup.css') }}" />

    <link rel="stylesheet" href="{{ asset('toastr/toastr.min.css')}}" />

    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" media="screen and (min-width: 768px)" href="{{ asset('css/styles-l.css') }}" />

    <link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/print.css') }}" />

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/theme.css') }}?v16" />

    <link rel="stylesheet" href="{{ asset('js/fancybox/jquery.fancybox.css') }}" type="text/css" media="screen" />



    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicons/apple-icon-57x57.png') }}">

    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-icon-60x60.png') }}">

    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicons/apple-icon-72x72.png') }}">

    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicons/apple-icon-76x76.png') }}">

    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicons/apple-icon-114x114.png') }}">

    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicons/apple-icon-120x120.png') }}">

    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicons/apple-icon-144x144.png') }}">

    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicons/apple-icon-152x152.png') }}">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-icon-180x180.png') }}">

    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicons/android-icon-192x192.png') }}?v2">

    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}?v2">

    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicons/favicon-96x96.png') }}?v2">

    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}?v2">

    <link rel="manifest" href="{{ asset('favicons/manifest.json') }}">

    <meta name="msapplication-TileColor" content="#ffffff">

    <meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png') }}">

    <meta name="theme-color" content="#ffffff">
<?php
/*
A tagmanager triggereket elindítom, ha elfogadta a cookiekat.
*/
if(isset($_COOKIE["stat_cookie"]) && $_COOKIE["stat_cookie"]==="no"): ?>
	<script type="text/javascript">
	dataLayer.push({
		stat_cookie: "no"
	});	
	</script>
<?php
else:
?>
	<script type="text/javascript">
	dataLayer.push({
		stat_cookie: "yes"
	});	
	</script>
<?php
endif;
if(isset($_COOKIE["marketing_cookie"]) && $_COOKIE["marketing_cookie"]==="yes"): ?>
	<script type="text/javascript">
	dataLayer.push({
		marketing_cookie: "yes"
	});	
	</script>
<?php
else:
?>
	<script type="text/javascript">
	dataLayer.push({
		marketing_cookie: "no"
	});	
	</script>
<?php
endif;
?>



	<?php if (isset($order->order_number) and isset($order->payable_price) and ($order->order_number > 0) and ($order->payable_price > 0)) { ?>

	<script>

	dataLayer.push({

	   'transactionId': '{{ $order->order_number }}',

	   'transactionTotal': {{ $order->payable_price }},

	});



	dataLayer.push({

	  <?php $items = $order->items(); $osszeg=0; ?>

	  'fbprodids': [

	  @foreach ($items as $item)

	    <?php 

			$product = Product::lang()->active()->find($item->product_id); 

			if (isset($product)) $osszeg+=$item->item_payable_price;

	    ?>

		@if (isset($product))

		  '{{ App::getLocale() }}{{ $item->product_id }}',

		@endif

	  @endforeach

	  ],

	  'fbprodvalues': {{ number_format($osszeg, 2, '.', '') }},

	  'ecommerce': {

		'purchase': {

		  'actionField': {

			'id': '{{$order->order_number}}',                         // A megrendelés száma

			'affiliation': 'vamosimilano.hu',

			'revenue': {{ $order->payable_price }},                // Teljes tranzackiós összeg - szállítással, adókkal együtt

			'shipping': {{ $order->delivery_price+$order->cost_price }}

		  },

		  'products': [

		  

		  @foreach ($items as $item)

		  <?php $product = Product::lang()->active()->find($item->product_id); ?>

			@if (isset($product))

			  {

				'id': '{{ $item->product_id }}',

				'name': '{{ $item->item_name }}',

				'price': {{ $item->item_payable_price }},

				'category': '{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}',

				'variant': '{{ CartHelper::ecommercevariant(unserialize($item->options)) }}',

				'quantity': {{ $item->qty }}

			   },

			@endif

		   @endforeach

		   ]

		}

	  }

	});



	</script>

	<?php } else { ?>

	@if (($_SERVER["REQUEST_URI"]!="/") and ($_SERVER["REQUEST_URI"]!="/kosar"))

		@if (((isset($products)) and (sizeof($products)>1)) and ((isset($highlighted_products)) and (sizeof($highlighted_products))))

			<script>



			dataLayer.push({

			  'ecommerce': {

				'currencyCode': 'HUF',

				'impressions': [

				@if ((isset($products)) and (sizeof($products)))

					<?php $i=1; ?>

					@foreach ($products as $product)

						 {

						   'id': '{{ $product->product_id }}',

						   'name': '{{ $product->name }}',

						   'price': {{ $product->getDefaultPrice() }},

						   <?php /* 'brand': 'Vamosi', */?>

						   @if (isset($current_category))

							'category': '{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}',

						   @endif

						   'list': 'Product list',

						   'position': {{ $i }}

						 },

						 <?php $i++; ?>

					 @endforeach

				 @endif

				 @if (((isset($side_products)) and (sizeof($side_products))) and ((isset($products)) and (sizeof($products))))

					<?php $i=1; ?>

					@foreach ($side_products as $product)

						 {

						   'id': '{{ $product->product_id }}',

						   'name': '{{ $product->name }}',

						   'price': {{ $product->getDefaultPrice() }},

						   <?php /* 'brand': 'Vamosi', */?>

						   @if (isset($current_category))

							'category': '{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}',

						   @endif

						   'list': 'Highlighted list',

						   'position': {{ $i }}

						 },

						 <?php $i++; ?>

					 @endforeach

				 @endif

				 ]

			  }

			});

			</script>

		@else

			@if (isset($product))

				<script>

				dataLayer.push({

				  'fbprodid': '{{ App::getLocale().$product->product_id }}',

				  'fbprodname': '{{ $product->getName() }}',

				  'fbprodvalue': {{ $product->getDefaultPrice() }},

				  'ecommerce': {

					'detail': {

					  'products': [{

						'id': '{{ $product->product_id }}',

						'name': '{{ $product->getName() }}',

						'price': {{ $product->getDefaultPrice() }},

						<?php /* 'brand': 'Vamosi', */ ?>

					'category': '{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}'

					   }]

					 }

				   }

				});

				</script>

			@endif

		@endif

	@endif

	@if (($_SERVER["REQUEST_URI"]=="/kosar") or ($_SERVER["REQUEST_URI"]=="/kosar/szemelyes-adatok"))

		<?php $citems = Cart::getContent();	?>

		@if ((isset($citems)) and (sizeof($citems)))

		<script>

		dataLayer.push({

			'event': 'checkout',

			@if ($_SERVER["REQUEST_URI"]=="/kosar")

				'fbprodids':[

					@foreach ($citems as $item)

						<?php

							if ($item->id== 'floor') {

								continue;

							}

							$product = Product::lang()->active()->find($item['attributes']->product_id);

							if (!isset($product)) {

								continue;

							}

						?>

						'{{ App::getLocale() }}{{ $item['attributes']->product_id }}',

					@endforeach

				],

				<?php 

				$osszeg=0;

				foreach ($citems as $item) {

					if ($item->id== 'floor') {

						continue;

					}

					$product = Product::lang()->active()->find($item['attributes']->product_id);

					if (!isset($product)) {

						continue;

					}

					$osszeg+=$item->price;

				}

				?>

				'fbprodvalues': {{ number_format($osszeg, 2, '.', '') }} ,

			@endif

			'ecommerce': {

			  'checkout': {

				@if ($_SERVER["REQUEST_URI"]=="/kosar")

					'actionField': {'step': 1},

					'products': [

					@foreach ($citems as $item)

						<?php

							if ($item->id== 'floor') {

								continue;

							}

							$product = Product::lang()->active()->find($item['attributes']->product_id);

							if (!isset($product)) {

								continue;

							}

							$price_array = CartHelper::calculateItemPrice($item['attributes'],true);

						?>

						{

						  'id': '{{ $item['attributes']->product_id }}',

						  'name': '{{ $product->getName() }}',

						  'price': {{ $item->price }},

						  'category': '{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}',

						  @if ($product->type == 'scalable')

							<?php $string = ''; ?>

							@foreach ($item['attributes']['sizes'] as $size_item => $size_name)

								<?php $string .= $size_name." "; ?>

							@endforeach

							<?php if (isset($item['attributes']['mounting'])) $string .= $item['attributes']['mounting']; ?>

							'variant': '{{$string}}',

						  @endif

						  'quantity': {{ $item->quantity }}

						},

					@endforeach

				   ]

				@else

					'actionField': {'step': 2},

				@endif



			 }

		   }

		});

		</script>

		@endif

	@endif

	<?php } ?>



    <!-- Google Tag Manager -->

	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

	})(window,document,'script','dataLayer','{{ Config::get('website.tagmanager') }}');</script>

	<!-- End Google Tag Manager -->



    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Shadows+Into+Light" />

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2C400italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&amp;v1&amp;subset=latin%2Clatin-ext" type="text/css" media="screen" />

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/design_demo11_en.css') }}">

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/settings_demo11_en.css') }}">



	@if ($_SERVER["REQUEST_URI"]=="/")

	<script type="application/ld+json">

    {

		"@context": "http://schema.org/",

		"@type": "FurnitureStore",

		"name": "Vamosi Milano",

		"address": {

			"@type": "PostalAddress",

			"addressLocality": "Budapest",

			"addressCountry": "HU",

			"postalCode": "1119",

			"streetAddress": "Petzv&aacute;l J&oacute;zsef utca 46-48."

		},

		"email": "info@vamosimilano.hu",

		"telephone": "+36-1-353-9410",

		"openingHours": [ "Mo-Sa 10:00-18:30", "Su 11:00-18:00" ],

		"url": "https://vamosimilano.hu/",

		"sameAs": [

			"https://www.facebook.com/vamosimilano",

			"https://twitter.com/vamosimilano",

			"https://www.linkedin.com/company/vamosimilano",

			"https://www.youtube.com/channel/UC6ckSCubYM-M4fdOmXkfb1g",

			"https://instagram.com/vamosimilano/",

			"https://www.pinterest.com/vamosimilano/",

			"http://blog.vamosimilano.hu/"

		],

		"logo": "https://vamosimilano.hu/images/vamosi-sd-logo.png",

		"image": "https://vamosimilano.hu/images/vamosi-sd-logo.png",

		"foundingDate": "2013",

		"priceRange": "$$",

		"contactPoint": [{

			"@type": "ContactPoint",

			"telephone": "+36-1-353-9410",

			"contactType": "customer service",

			"hoursAvailable": [ "Mo-Sa 09:00-17:00", "Su 11:00-18:00" ]

		}]

	}

	</script>

	<script type="application/ld+json">

	{

		"@context": "http://schema.org/",

		"@type": "WebSite",

		"name": "Vamosi Milano",

		"alternateName": "Vamosi Milan",

		"url": "https://vamosimilano.hu/",

		"potentialAction": {

			"@type": "SearchAction",

			"target": "https://vamosimilano.hu/kereses?q={q}",

			"query-input": "required name=q"

		}

    }

    </script>

	@endif



	<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/npm.js') }}"></script>


    <script type="text/javascript" src="{{ asset('js/isotope.js') }}"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript" src="{{ asset('js/sw_megamenu.js') }}"></script>

    <script src="{{ asset('/js/fancybox/jquery.fancybox.pack.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/theme.js') }}?v3"></script>

    <script type="text/javascript" src="{{ asset('js/jquery.unveil.js') }}"></script>

    <script type="text/javascript" src="{{ asset('jquery/jquery.ui.touch-punch.min.js') }}"></script>

    @yield('header_js')



    <script src="{{ asset('toastr/toastr.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('js/system.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/cart.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/carousel.js') }}"></script>











