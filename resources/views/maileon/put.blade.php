{{ '<'.'?'.'xml version="1.0" encoding="UTF-8"?>' }}
<contact>
	@if ($email)
		<email>{{ $email }}</email>
	@endif
	@if ($external_id)
		<external_id>{{ $external_id }}</external_id>
	@endif
	<standard_fields>
    	@if ($standard_fields)
			@foreach  ($standard_fields as $key=>$value)
				<field>
					<name>{{ $key }}</name>
					<value>{{ $value }}</value>
				</field>
			@endforeach
		@endif
	</standard_fields>
	<custom_fields>
	@if ($custom_fields)
		@foreach ($custom_fields as $key=>$value)
			<field>
				<name>{{ $key }}</name>
				<value>{{ $value }}</value>
			</field>
		@endforeach
	@endif
    </custom_fields>
</contact>  