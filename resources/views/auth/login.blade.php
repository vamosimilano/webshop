<?php
$headversion = "newhead";
?>
@extends('layouts.default')
@section('content')
<?php
if( isset($headversion) && $headversion === "newhead" ) {
?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-lg-6 mx-auto py-5">
			<form id="login-form" method="post" action="{{ action('AuthController@loginSubmit') }}" class="form">
			<div class="card">
				<div class="card-body pb-0">
					<h1 class="h4-responsive title-center text-uppercase">Jelentkezzen be</h1>
					<div class="md-form md-outline">
						<input type="email" class="form-control" id="email" autocomplete="off" value="{{( Input::old('email') ? Input::old('email') : '')}}" name="email" required>
						<label for="email">{{ t('Email') }}</label>
					</div>
					<div class="md-form md-outline">
						<input type="password" class="form-control" id="pass" name="password" required>
						<label for="pass">{{ t('Jelszó') }}</label>
					</div>
					<div class="md-form mb-0">
						<div class="row">
							<div class="col-md-6 text-center text-md-left">
								<a href="{{ action('PasswordController@getEmail') }}" class="btn btn-flat">{{ t('Elfejeltette jelszavát?') }}</a>
							</div>
							<div class="col-md-6 text-center text-md-right">
								<button type="submit" class="btn btn-primary">{{ t('Bejelentkezés') }}</button>
							</div>
						</div>
					</div>
				</div>
				<div class="card-body pt-0">
					<hr>
					<p class="text-center text-uppercase"><strong>Vagy regisztráljon</strong></p>
					<div class="row">
						<div class="col-md-6 text-center text-md-left">
							<a href="{{url('/regisztracio')}}" class="btn btn-primary">Email címmel</a>
						</div>
						<div class="col-md-6 text-center text-md-right">
							<a href="{{url('/facebook-login')}}" class="btn btn-fb">Facebook fiókkal</a>
						</div>
					</div>
				</div>
			</div>
				{{ csrf_field() }}
			</form>
		</div>
	</div>
</div>
<?php
}
else {
?>
<main class="page-main" id="maincontent">

<div class="page-title-wrapper">
    <h1 class="page-title">
        <span data-ui-id="page-title-wrapper" class="base">{{ t('Bejelentkezés / Regisztráció') }}</span>    </h1>
    </div>


<div class="columns">
    <div class="column main">




<div class="login-container"><div class="block block-customer-login">
    <div class="block-title">
        <strong aria-level="2" role="heading" id="block-customer-login-heading">{{ t('Bejelentkezés') }}</strong>
    </div>
    <div aria-labelledby="block-customer-login-heading" class="block-content">
        <form id="login-form" method="post" action="{{ action('AuthController@loginSubmit') }}" class="form form-login">
            <fieldset  class="fieldset login">
                <div class="field note">{{ t('Ha már regisztrált a rendszerünkbe, adja meg e-mail címét és jelszavát') }}</div>
                <div class="field email required">
                    <label for="email" class="label"><span>{{ t('Email') }}</span></label>
                    <div class="control">
                        <input type="email" class="input-text login-email" id="email" autocomplete="off" value="{{( Input::old('email') ? Input::old('email') : '')}}" name="email" required=""  aria-required="true">
                    </div>
                </div>
                <div class="field password required">
                    <label class="label" for="pass"><span>{{ t('Jelszó') }}</span></label>
                    <div class="control">
                        <input type="password"  id="pass" class="input-text login-pass" autocomplete="off" name="password" required=""  aria-required="true">
                    </div>
                </div>
                                <div class="actions-toolbar">
                    <div class="primary"><button id="send2" name="send" class="action login primary" type="submit"><span>{{ t('Bejelentkezés') }}</span></button></div>
                    <div class="secondary"><a href="{{ action('PasswordController@getEmail') }}" class="action remind"><span>{{ t('Elfejeltette jelszavát?') }}</span></a></div>
                </div>
            </fieldset>
            {{ csrf_field() }}
        </form>
    </div>
</div>
<div class="block block-new-customer">
    <div class="block-title">
        <strong aria-level="2" role="heading" id="block-new-customer-heading"></strong>
    </div>
    <div aria-labelledby="block-new-customer-heading" class="block-content">
        <p>{{ t('Regisztráljon rendszerünkbe, pár perc alatt') }}</p>
        <div class="actions-toolbar">
            <div class="primary">
                <a class="action create primary" href="{{ action('AuthController@register') }}"><span>{{ t('Regisztráció') }}</span></a>
            </div>
        </div>
        <br>
        <p>{{ t('vagy') }}</p>
        <div class="actions-toolbar">
            <div class="primary">
                <a class="action create primary btn-fb" href="{{ action('FacebookController@facebookLogin') }}"><span>{{ t('Csatalkozás Facebookkal') }}</span></a>
            </div>
        </div>
    </div>
</div>
</div></div></div>

</main>
@section('footer_js')
@append
<?php
}
?>
@stop