<?php
$headversion = "newhead";
?>
@extends('layouts.default')
@section('content')
<?php
if( isset($headversion) && $headversion === "newhead" ) {
?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-lg-6 mx-auto py-5">
			<form id="login-form" method="post" action="{{ action('PasswordController@postEmail') }}" class="form">
			<div class="card">
				<div class="card-body pb-0">
					<h1 class="h4-responsive title-center text-uppercase">{{ t('Új jelszó kérése') }}</h1>
					<p class="strong text-center">{{ t('Adja meg e-mail címét, amelyre elküldhetjük a jelszóvisszaállító levelünket!') }}</p>
					<div class="md-form md-outline">
						<input type="email" class="form-control" value="{{ (Input::old('email') ? Input::old('email') : '') }}" name="email" id="email_address" name="email" required>
						<label for="email">{{ t('Email') }}<strong class="ml-1">*</strong></label>
					</div>
					<div class="md-form">
						<div class="row">
							<div class="col-md-6 text-center text-md-left">
								<a href="{{ action('AuthController@login') }}" class="btn btn-flat">{{ t('Vissza a belépéshez') }}</a>
							</div>
							<div class="col-md-6 text-center text-md-right">
								<button type="submit" class="btn btn-primary">{{ t('Kérem az e-mailt') }}</button>
							</div>
						</div>
					</div>
				</div>
			</div>
				{{ csrf_field() }}
			</form>
		</div>
	</div>
</div>
<?php
}
else {
?>
<main class="page-main" id="maincontent">
<div class="page-title-wrapper">
    <h1 class="page-title">
    <span data-ui-id="page-title-wrapper" class="base">{{ t('Új jelszó kérése') }}</span>    </h1>
</div>


<div class="columns">
    <div class="column main">
        <form id="form-validate" method="post" action="{{ action('PasswordController@postEmail') }}" class="form password forget" >
            <fieldset  class="fieldset">
                <div class="field note">{{ t('Adja meg e-mail címét, amelyre elküldhetjük a jelszóvisszaállító levelünket!') }}</div>
                <div class="field email required">
                    <label class="label" for="email_address"><span>{{ t('E-mail') }}</span></label>
                    <div class="control">
                        <input type="email"  value="{{ (Input::old('email') ? Input::old('email') : '') }}" class="input-text" required="" id="email_address" alt="email" name="email" aria-required="true">
                    </div>
                </div>
                    </fieldset>
            <div class="actions-toolbar">
                <div class="primary">
                    <button class="action submit primary" type="submit"><span>{{ t('Kérem az e-mailt') }}</span></button>
                </div>
                <div class="secondary">
                    <a href="{{ action('AuthController@login') }}" class="action back" style="display: block;"><span>{{ t('Vissza a belépéshez') }}</span></a>
                </div>
            </div>
            {{ csrf_field() }}
        </form>
    </div>
</div>
</main>
@section('footer_js')
@append

<?php
}
?>
@stop