@extends('layouts.default')



@section('content')



@include('webshop.block.breadcrumbs')

<?php

    $category_data = $product->getPrimaryCategory($sizes);

    $kategoria = $category_data['kategoria'];

    $kategoria_prefix = $category_data['kategoria_prefix'];

    $pages = $category_data['pages'];

?>



<main class="page-main texttile-change-container-main" id="maincontent">



<div class="entry-content min-height">





    <div class="row nofullscreen">

        <div class="row-wrapper container">

            <div class="row">

                <div class="col-md-6">

                <h2 class="">{{ t('Szövet választás') }}

                    </h2>

                    @include('webshop.block.textile_tab', ['textile_editor' => true, 'new_version_link' => true])



                </div>

                <div class="col-md-6 text-left relative">

					<div id="textile-preview-container">

					<img class="img-responsive" alt="{{ t('project_name', 'project') }}" src="{{ asset('images/textileditor/'. $kategoria_prefix .'-butor.png') }}">

					</div>

				</div>

            </div>			

				<div class="clearfix"></div>

			<div class="row">

                <div class="col-lg-12">

                    <br>

                        <a href="javascript:;" id="lastUsedTextiles" class="btn btn-primary btn-a color-white pull-right" style="display: none">{{ t('Utoljára használt szövetek betöltése') }}</a>





                    <a href="" id="set-textiles" class="btn btn-primary btn-a color-white">{{ t('Vissza a termék oldalra') }}</a>

                </div>

                <div class="clearfix"></div>

                @if ($product->type != 'general' and sizeof($parts) and !isMobile())

                    @foreach ($custom_parts as $part_key => $parts_data)

                            @foreach ($parts_data as $i => $part)

                            @include('webshop.block.product_textiles_slider_new', ['part' => $part, 'type' => $part->admin_name,'all_textiles' => $all_textiles])

                        @endforeach

                    @endforeach

                @endif



            </div>

        </div>

    </div>

</div>

</main>

@stop



@section('footer_js')

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.4/jquery.lazy.min.js"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.4/jquery.lazy.plugins.min.js"></script>



    <script>

    enable_changed_textile_target = false;

    disable_auto_scroll = false;

    var textile_preview = [];

    var lastusedtextiles;

    var furniture_type = '<?=$kategoria?>';

    textile_preview.push({'id':'keret-l-alaku-kanape', 'image':'{{ asset('images/textileditor/l-keret.png') }}'});

    textile_preview.push({'id':'karfa-l-alaku-kanape', 'image':'{{ asset('images/textileditor/l-karfa.png') }}'});

    textile_preview.push({'id':'tamla-l-alaku-kanape', 'image':'{{ asset('images/textileditor/l-tamla.png') }}'});

    textile_preview.push({'id':'kisparna-l-alaku-kanape', 'image':'{{ asset('images/textileditor/l-kisparna.png') }}'});

    textile_preview.push({'id':'nagyparna-l-alaku-kanape', 'image':'{{ asset('images/textileditor/l-nagyparna.png') }}'});

    textile_preview.push({'id':'uloresz-l-alaku-kanape', 'image':'{{ asset('images/textileditor/l-uloresz.png') }}'});

    textile_preview.push({'id':'also-l-alaku-kanape', 'image':'{{ asset('images/textileditor/l-also.png') }}'});

    textile_preview.push({'id':'felso-l-alaku-kanape', 'image':'{{ asset('images/textileditor/l-felso.png') }}'});

    textile_preview.push({'id':'butor-l-alaku-kanape', 'image':'{{ asset('images/textileditor/l-butor.png') }}'});



    textile_preview.push({'id':'keret-u-alaku-kanape', 'image':'{{ asset('images/textileditor/u-keret.png') }}'});

    textile_preview.push({'id':'karfa-u-alaku-kanape', 'image':'{{ asset('images/textileditor/u-karfa.png') }}'});

    textile_preview.push({'id':'tamla-u-alaku-kanape', 'image':'{{ asset('images/textileditor/u-tamla.png') }}'});

    textile_preview.push({'id':'kisparna-u-alaku-kanape', 'image':'{{ asset('images/textileditor/u-kisparna.png') }}'});

    textile_preview.push({'id':'nagyparna-u-alaku-kanape', 'image':'{{ asset('images/textileditor/u-nagyparna.png') }}'});

    textile_preview.push({'id':'uloresz-u-alaku-kanape', 'image':'{{ asset('images/textileditor/u-uloresz.png') }}'});

    textile_preview.push({'id':'also-u-alaku-kanape', 'image':'{{ asset('images/textileditor/u-also.png') }}'});

    textile_preview.push({'id':'felso-u-alaku-kanape', 'image':'{{ asset('images/textileditor/u-felso.png') }}'});

    textile_preview.push({'id':'butor-u-alaku-kanape', 'image':'{{ asset('images/textileditor/u-butor.png') }}'});



    textile_preview.push({'id':'keret-egyenes', 'image':'{{ asset('images/textileditor/e-keret.png') }}'});

    textile_preview.push({'id':'karfa-egyenes', 'image':'{{ asset('images/textileditor/e-karfa.png') }}'});

    textile_preview.push({'id':'tamla-egyenes', 'image':'{{ asset('images/textileditor/e-tamla.png') }}'});

    textile_preview.push({'id':'kisparna-egyenes', 'image':'{{ asset('images/textileditor/e-kisparna.png') }}'});

    textile_preview.push({'id':'nagyparna-egyenes', 'image':'{{ asset('images/textileditor/e-nagyparna.png') }}'});

    textile_preview.push({'id':'uloresz-egyenes', 'image':'{{ asset('images/textileditor/e-uloresz.png') }}'});

    textile_preview.push({'id':'also-egyenes', 'image':'{{ asset('images/textileditor/e-also.png') }}'});

    textile_preview.push({'id':'felso-egyenes', 'image':'{{ asset('images/textileditor/e-felso.png') }}'});

    textile_preview.push({'id':'butor-egyenes', 'image':'{{ asset('images/textileditor/e-butor.png') }}'});



    textile_preview.push({'id':'textil-puff', 'image':'{{ asset('images/textileditor/p-textil.png') }}'});

    textile_preview.push({'id':'butor-puff', 'image':'{{ asset('images/textileditor/p-butor.png') }}'});

    textile_preview.push({'id':'felso-puff', 'image':'{{ asset('images/textileditor/p-felso.png') }}'});

    textile_preview.push({'id':'also-puff', 'image':'{{ asset('images/textileditor/p-also.png') }}'});





    textile_preview.push({'id':'butor-fotel', 'image':'{{ asset('images/textileditor/f-butor.png') }}'});

    textile_preview.push({'id':'also-fotel', 'image':'{{ asset('images/textileditor/f-also.png') }}'});

    textile_preview.push({'id':'felso-fotel', 'image':'{{ asset('images/textileditor/f-felso.png') }}'});

    textile_preview.push({'id':'parna-fotel', 'image':'{{ asset('images/textileditor/f-parna.png') }}'});

    textile_preview.push({'id':'uloresz-fotel', 'image':'{{ asset('images/textileditor/f-uloresz.png') }}'});

    textile_preview.push({'id':'tamla-fotel', 'image':'{{ asset('images/textileditor/f-tamla.png') }}'});

    textile_preview.push({'id':'karfa-fotel', 'image':'{{ asset('images/textileditor/f-karfa.png') }}'});



    textile_preview.push({'id':'tamla-francia', 'image':'{{ asset('images/textileditor/fr-tamla.png') }}'});

    textile_preview.push({'id':'butor-francia', 'image':'{{ asset('images/textileditor/fr-butor.png') }}'});

    textile_preview.push({'id':'keret-francia', 'image':'{{ asset('images/textileditor/fr-keret.png') }}'});







    //textile_preview.push({'also-l-alaku-kanape': '{{ asset('images/textileditor/keret.png') }}'});



    $('#changeTextile .input-group-btn [data-part]').hover(function() {

            var textile_target = 0;

            var subtype = $(this).data('part')+"-"+furniture_type;

            $.each(textile_preview, function(value, item) {

                if (textile_target == 0) {

                    if (item.id == (subtype)) {

                        $('#textile-preview-container img').attr('src', item.image+"?r=12");

                        textile_target = 1;

                    }

                }

            });



    });

    $('#changeTextile .input-group-btn [data-part]').click(function() {

        $('.inprogress').removeClass('inprogress').html('{{t('Kiválasztás')}}');

        $(this).addClass('inprogress');

        $(this).html('{{t('Folyamatban')}}');

    });

    $('[data-slider-type]').click(function(){

      $('#changeTextile .input-group-btn [data-part="'+$(this).attr('data-slider-type')+'"]').removeClass('gray').removeClass('inprogress');

      $('#changeTextile .input-group-btn [data-part="'+$(this).attr('data-slider-type')+'"]').html('{{t('Kiválasztva')}}');

    });



    $(document).ready(function() {

        $('#changeTextile .input-group-btn [data-part]').addClass('gray');

        var decode =  getCookie('selected{{$product->product_id}}');

        if (decode) {

            //runProductTextileCookieChange(decode, {{$product->product_id}});

        }

        jQuery('.owl-lazy').Lazy({

            scrollDirection: 'vertical',

            effect: 'fadeIn',

            visibleOnly: true,

        });



        lastusedtextiles =  getCookie('lastusedtextiles{{$kategoria}}');

        if (lastusedtextiles) {

            $('#lastUsedTextiles').show();

        }



        $('img[data-toggle="tooltip"]').tooltip({

            animated: 'fade',

            placement: 'top',

            html: true,

            content: function() {

                return '<img src="'+$(this).data('bigimage')+'" style="width: 300px;">'

            }

        });





    });

    $('#lastUsedTextiles').click(function(){

        runProductTextileCookieChange(lastusedtextiles, {{$product->product_id}});

    });

    $('#set-textiles').click(function() {

			var post_object = {};

			post_object.tabpane = $('#changeTextile .nav-tabs li.active a').data('part-key');

			post_object.parts = [];

			$('#changeTextile #'+post_object.tabpane+' .input-group-btn [data-part]').each(function() {

				post_object.parts.push({

					'id' : $(this).data('part'),

					'name' : $(this).closest('.form-group').find('label').html(),

					'img' : $(this).closest('.input-group').find('img').attr('src'),

					'textile' : $(this).closest('.input-group').find('input').val(),

					'value' : ($(this).data('selected') ? $(this).data('selected') : $(this).data('alapszovet'))

				});

			});

			var jsonConvertedData = JSON.stringify(post_object);  // Convert to json

			var coded = Base64.encode(jsonConvertedData);



			setCookie('selected{{$product->product_id}}', coded, 10);

			setCookie('lastusedtextiles{{$kategoria}}', coded, 10);

        $(this).attr('href', '{{ $product->getUrl() }}');

        return true;

    });



    $('[data-filter]').click(function() {

        runFilter($(this));



    });







    function runFilter(element) {

        var container = element.attr('name');

        $('#'+container+" .change_textile_data").hide();

        var filters = [];

        $('[name="'+container+'"]').each(function() {

            if ($(this).is(':checked')) {

                filters.push({'filter': $(this).data('filter'), 'value' : $(this).val()});

            }

        });

        var exists = [];

        $('#'+container+" .change_textile_data").each(function() {

            var ok = 1;

            var product_item = $(this);

            var last_filter = '';

            $.each(filters, function(value, item) {

                if (!inObject(filters, item.filter, product_item.data(item.filter))) {

                    ok = 0;

                }

            });

            if (ok == 1) {

                product_item.show();

            }

        });





    }

    function inObject(filters, key, value) {

        console.log(key+"::"+value);

        var ff = [];

        $.each(filters, function(key_id, item) {

            if (key == item.filter) {

                ff.push(item.value*1);

            }

        });

        console.log(ff);

        if (ff.indexOf(value) != -1) {

            return true;

        }

        return false;

    }



    </script>

@append







