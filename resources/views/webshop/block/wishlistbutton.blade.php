<?php
if(isset($product_id)){
	if(isset($_COOKIE["wishlist"])) {
		$favs= Favorites::whereRaw("uid=".$_COOKIE["wishlist"]." AND product_id=".$product_id."")->first();
		if(isset($favs)){
			if($favs->product_id==$product_id){
				if( $is == 'icon'){
					$class="icon";
				}
				else {
					$class="btn btn-primary";
				}
?>
<span id="favorite-{{$product_id}}">
	<a id="wishlistbutton-{{$product_id}}" class="{{$class}}" href="javascript:void(0)" title="{{t('Eltávolítás a kedvencek közül')}}" data-wishlist="1" onClick="removeWishList('{{$favs->favorite_id}}', '{{$product_id}}', '{{$_COOKIE['wishlist']}}', '{{$class}}')">
		<i class="fas fa-heart" aria-hidden="true"></i>
	</a>
</span>
<?php
			}//if
		}//if issetfav
		else {
			if( $is == 'icon'){
				$class="icon";
			}
			else {
				$class="btn btn-outline-primary";
			}
?>
<span id="favorite-{{$product_id}}">
	<a id="wishlistbutton-{{$product_id}}" class="{{$class}}" href="javascript:void(0)" title="{{t('Hozzáadás a kedvencekhez')}}" data-wishlist="0" onClick="wishList('{{$product_id}}', '{{$_COOKIE['wishlist']}}', '{{$class}}')">
		<i class="fal fa-heart" aria-hidden="true"></i>
	</a>
</span>
<?php
		}//else
	}//if
	else {
		//ha nem látja a cookiet töltsük újra az oldalt
		//header("Location: ".$_SERVER['REQUEST_URI']);
		setcookie("wishlist","11111");
		//echo $_SERVER['REQUEST_URI'];
	}//else
}//if
?>