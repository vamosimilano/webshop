@if (sizeof($all_textiles))
    <div class="slidercontainer textileslider textileslider-new" id="slider_of_{{$type}}" data-slider-type="{{$type}}" style="display: none;">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12">
                    <h2 class="filterproduct-title">
                    <span class="content"><strong>{{ $part->name }}</strong>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="switch-carousel hidden" data-target="destroy_{{$type}}_Carousel()"></i>
                    <i class="switch-carousel hidden fa fa-th-list active hidden" data-target="init_{{$type}}_Carousel()"></i>
                    <i class='fa fa-times hidden' onclick="$('#slider_of_{{$type}}').slideUp()"></i>
                    </span>

                    </h2>
                    <hr>
                    <div class="row category-filter category-filter-new filter-cat">
					     <h4>{{ t('Kategóriák') }}</h4>
						 <div class="col-lg-6 item"><label class="block">
						 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="1">
						 {{ t('1. kategória') }}</label></div>
						 <div class="col-lg-6 item"><label class="block">
						 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="2">
						 {{ t('2. kategória') }} ({{t('+ 1200 Ft/fm')}})</label></div>
						 <div class="col-lg-6 item"><label class="block">
						 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="3">
						 {{ t('3. kategória') }} ({{t('+ 2500 Ft/fm')}})</label></div>
						 @if (App::getLocale()!='en')
							 <div class="col-lg-6 item"><label class="block">
							 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="4">
							 {{ t('4. kategória') }} ({{t('+ 3500 Ft/fm')}})</label></div>
							 <div class="col-lg-6 item"><label class="block">
							 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="5">
							 {{ t('5. kategória') }} ({{t('+ 4500 Ft/fm')}})</label></div>
							 <div class="col-lg-6 item"><label class="block">
							 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="6">
							 {{ t('6. kategória') }} ({{t('+ 5500 Ft/fm')}})</label></div>
							 <div class="col-lg-6 item"><label class="block">
							 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="7">
							 {{ t('7. kategória') }} ({{t('+ 6500 Ft/fm')}})</label></div>
							 <div class="col-lg-6 item"><label class="block">
							 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="9">
							 @if (date("YmdHi")<201804090000)
								{{ t('Bőr') }} ({{t('+ <s>7500</s> 5250 Ft/fm')}})</label></div>
							 @else
								{{ t('Bőr') }} ({{t('+ 7500 Ft/fm')}})</label></div>
							 @endif
							 <div class="col-lg-12 item"><label class="block">
							 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="10">
							 {{ t('Easy to clean only with water I') }} ({{t('+ 5500 Ft/fm')}})</label></div>
							 <div class="col-lg-12 item"><label class="block">
							 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="11">
							 {{ t('Easy to clean only with water II') }} ({{t('+ 6500 Ft/fm')}})</label></div>
							 <div class="col-lg-12 item"><label class="block">
							 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="12">
							 {{ t('Easy to clean only with water III') }} ({{t('+ 7500 Ft/fm')}})</label></div>
						 @else
							 <div class="col-lg-6 item"><label class="block">
							 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="5">
							 @if (date("YmdHi")<201804090000)
								{{ t('Bőr') }} ({{t('+ <s>£23</s> £16.1/m')}})</label></div>
							 @else
								{{ t('Bőr') }} ({{t('+ £23/m')}})</label></div>
							 @endif
							 <div class="col-lg-12 item"><label class="block">
							 <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="price" value="4">
							 {{ t('Easy to clean only with water') }} ({{t('+ £23/m')}})</label></div>
						 @endif
						 <br>
                    </div>
                    <div class="row category-filter category-filter-new filter-type">
                         <h4>{{ t('Típus') }}</h4>
                         <div class="col-lg-12 item" ><label class="block">
                         <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="category" value="2">
                         {{ t('Szövetek') }}</label></div>
                         <div class="col-lg-12 item" ><label class="block">
                         <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="category" value="1">
						 {{ t('Műbőrők') }}</label></div>
						 <div class="col-lg-12 item" ><label class="block">
                         <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="category" value="3">
						 {{ t('Bőr') }}</label></div>
					</div>
					<div class="row category-filter category-filter-new filter-subtype">
                         <h4>{{ t('Fajta') }}</h4>
                         <div class="col-lg-4 item" ><label class="block">

                         <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="subtype" value="0">
						 {{ t('textiltype_0') }} </label></div>
                         <div class="col-lg-4 item"><label class="block">
                         <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="subtype" value="1">
						 {{ t('textiltype_1') }}</label></div>
						 <div class="col-lg-4 item"><label class="block">
                         <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="subtype" value="2">
						 {{ t('textiltype_2') }}</label></div>
						 <div class="col-lg-4 item"><label class="block">
                         <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="subtype" value="3">
						 {{ t('textiltype_3') }}</label></div>
						 <div class="col-lg-4 item"><label class="block">
                         <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="subtype" value="4">
						 {{ t('textiltype_4') }}</label></div>
						 <div class="col-lg-4 item"><label class="block">
                         <input type="checkbox" name="featured_product_textiles_{{$type}}" data-filter="subtype" value="5">
						 {{ t('textiltype_5') }}</label></div>
					</div>
					<div class="clearfix"></div>

                    <hr>
                     <div id="featured_product_textiles_{{$type}}" class="owl-top-narrow">
                        <div class="products wrapper grid products-grid">
                           <div class="filterproducts products list items product-items owl-carousel" style="display: block;">
                             <?php
                                 $slider_items = [];
                             ?>
                             @foreach ($all_textiles as $i_ => $textile)
                             <?php
                                 $field = "on_".$type;
                                 if ($type == 'kisparna'){
                                     $field = "on_parna";
                                 }
                                 if (!isset($textile['original'][$field])) {
                                     $field = "on_butor";
                                 }
								 if (strpos(" ".$type,"elemes")) {
									 $field = "on_elemes";
								 }
                             ?>
                             @if ($textile->$field == 'yes')
                             <?php

                                 $image_src = ProductTextile::getImageUrl($textile->textile_id, 255);
                                 $image_src_big = ProductTextile::getImageUrl($textile->textile_id);

                                 $slider_items[] = '
							  <div data-category="'.$textile->textile_category_id.'" data-price="'.$textile->extraprice_category_id.'" data-subtype="'.$textile->textile_type_id.'" data-textile-target="'.$type.'-'.$part->type.'-'.$textile->textile_id.'" class="col-lg-3 product change_textile_data '.$part->type.'_textile_change_'.$textile->textile_id.''.$i.'" data-target="'.$part->type.''.$i.'" onclick="rewriteProductDetailsPrice(\'textile_'.$type.'\', '.$textile->textile_id.','.$product->product_id.'); triggerParnaValasztas(\''.$type.'\',\''.$part->type.'\','.$textile->textile_id.'); if(iklikk==0){scrollToElement(\'#changeTextile\');}">

                                    <div class="product photo slider-max-height-overflow extra-price-container'.($textile->extra_price>0 ? ' extra-price' : '' ).'">
									   <img class="product-image-photo default_image lazyOwl owl-lazy" title="'. $textile->name .'" data-toggle="tooltip" data-bigimage="'.$image_src_big.'" src="'.$image_src.'" data-src="'.$image_src.'" alt="'. $textile->name .'"/>
									</div>
                                    <div class="product details product-item-details">
                                       <strong class="product name product-item-name">
                                       <b>'. $textile->name .'</b>'.
                                       (($textile->extra_price) ? '<span class="pull-right">(+ '.money($textile->extra_price).'/'.t('fm').') &nbsp;&nbsp;</span>' : '').'
                                       <br>
                                       '. $textile->textile_catname .'

                                       </strong>


                                    </div>

                              </div>';
                              ?>
                              @endif
                              @endforeach
                              @foreach ($slider_items as $i => $item_data)
                                {{$item_data}}
                              @endforeach
                              <div class="clearfix"></div>
							</div>
                        </div>
                     </div>
                    </div>
               </div>
            </div>
         </div>
         <script>
             function init_{{$type}}_Carousel() {

             }

         </script>
@endif

