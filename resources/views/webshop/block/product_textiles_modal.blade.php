<?php
$category_data = $product->getPrimaryCategory($sizes);
$kategoria = $category_data['kategoria'];
$kategoria_prefix = $category_data['kategoria_prefix'];
if(sizeof($all_textiles)){
	$kat_1=0;
	$kat_2=0;
	$kat_3=0;
	$kat_4=0;
	$kat_5=0;
	$kat_6=0;
	$kat_7=0;
	$kat_8=0;
	$kat_9=0;
	$kat_10=0;
	$kat_11=0;
	$kat_12=0;
	$kat_13=0;
	
	$tip_1=0;
	$tip_2=0;
	$tip_3=0;
	
	$species_1=0;
	$species_2=0;
	$species_3=0;
	$species_4=0;
	$species_5=0;
	$species_0=0;
	
	foreach ($all_textiles as $i_ => $textile) {
		/*
			Textil kategóriák
		*/
		if($textile->extraprice_category_id === 1) {$kat_1++;}
		if($textile->extraprice_category_id === 2) {$kat_2++;}
		if($textile->extraprice_category_id === 3) {$kat_3++;}
		if($textile->extraprice_category_id === 4) {$kat_4++;}
		if($textile->extraprice_category_id === 5) {$kat_5++;}
		if($textile->extraprice_category_id === 6) {$kat_6++;}
		if($textile->extraprice_category_id === 7) {$kat_7++;}
		if($textile->extraprice_category_id === 8) {$kat_8++;}
		if($textile->extraprice_category_id === 9) {$kat_9++;}
		if($textile->extraprice_category_id === 10) {$kat_10++;}
		if($textile->extraprice_category_id === 11) {$kat_11++;}
		if($textile->extraprice_category_id === 12) {$kat_12++;}
		if($textile->extraprice_category_id === 13) {$kat_13++;} //akciós szövetek
		/*
			Textil típusok
		*/
		if($textile->textile_category_id === 1) {$tip_1++;}
		if($textile->textile_category_id === 2) {$tip_2++;}
		if($textile->textile_category_id === 3) {$tip_3++;}
		/*
			Textil fajták
		*/
		if($textile->textile_type_id === 1) {$species_1++;}
		if($textile->textile_type_id === 2) {$species_2++;}
		if($textile->textile_type_id === 3) {$species_3++;}
		if($textile->textile_type_id === 4) {$species_4++;}
		if($textile->textile_type_id === 5) {$species_5++;}
		if($textile->textile_type_id === 0) {$species_0++;}
	}
?>
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});
function filterTextile(prop, value, x) {
	"use strict";
	//var elem = '#filter-'.x;

	if( $("#"+x).hasClass("active") ){
		$(".label-container").removeClass("hidden");
		$(".label-container").removeClass("active");
		$("#"+x).prop('checked', false);
	}
	else {
	var set = '.filter-'+prop+'-'+value;
	$(".label-container").addClass("hidden");
	$(".filterinput.active").removeClass("active");
	$(".label-container").prop('checked', false);
	
	$(set).removeClass("hidden");
	$("#"+x).addClass("active");
	$("#"+x).prop('checked', true);
	}
}
</script>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<h1 class="text-center">{{ $part->name }}</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<ul class="nav nav-tabs col-12" id="textile_cat_tabs">
				<li class="nav-item">
					<a href="#pricecategories" class="textiletypetabs nav-link active" id="tab-pricecategories" role="tab" data-toggle="tab" aria-selected="true">{{ t('Kategóriák') }}</a>
				</li>
				<li class="nav-item">
					<a href="#typecategories" class="textiletypetabs nav-link" id="tab-typecategories" role="tab" data-toggle="tab" aria-selected="false">{{ t('Típus') }}</a>
				</li>
				<li class="nav-item">
					<a href="#speciescategories" class="textiletypetabs nav-link" id="tab-speciescategories" role="tab" data-toggle="tab" aria-selected="false">{{ t('Fajta') }}</a>
				</li>
			</ul>
			<div class="tab-content col-12 py-2 shadow" id="textiles-content">
				<div role="tabpanel" class="tab-pane fade show active pt-3" id="pricecategories" aria-labelledby="pricecategories-tab">
					<div class="row">
						<div class="col-md-5">
							@if($kat_1>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-1-category" name="category" value="1" onClick="filterTextile('cat', '1', 'radio-1-category')">
								<label class="custom-control-label" for="radio-1-category">{{ t('I. kategória') }} <span class="badge badge-primary">{{$kat_1}} db</span></label>
							</div>
							@endif
							@if($kat_2>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-2-category" name="category" value="2" onClick="filterTextile('cat', '2', 'radio-2-category')">
								<?php if( ( date('Ymd')>"20190425" ) && ( date('Ymd')<"20190429" ) ) {
								?>
								<label class="custom-control-label" for="radio-2-category">Felár nélküli akciós szövetek<span class="badge badge-primary">{{$kat_2}} db</span></label>
								<?php
								}
								else {
								?>
								<label class="custom-control-label" for="radio-2-category">{{ t('II. kategória') }} ({{t('+1 700 Ft/fm')}}) <span class="badge badge-primary">{{$kat_2}} db</span></label>
								<?php } ?>
							</div>
							@endif
							@if($kat_3>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-3-category" name="category" value="3" onClick="filterTextile('cat', '3', 'radio-3-category')">
								<label class="custom-control-label" for="radio-3-category">{{ t('III. kategória') }} ({{t('+2 500 Ft/fm')}}) <span class="badge badge-primary">{{$kat_3}} db</span></label>
							</div>
							@endif
							@if($kat_4>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-4-category" name="category" value="4" onClick="filterTextile('cat', '4', 'radio-4-category')">
								<label class="custom-control-label" for="radio-4-category">{{ t('IV. kategória') }} ({{t('+3 500 Ft/fm')}}) <span class="badge badge-primary">{{$kat_4}} db</span></label>
							</div>
							@endif
							@if($kat_5>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-5-category" name="category" value="5" onClick="filterTextile('cat', '5', 'radio-5-category')">
								<label class="custom-control-label" for="radio-5-category">{{ t('V. kategória') }} ({{t('+4 500 Ft/fm')}}) <span class="badge badge-primary">{{$kat_5}} db</span></label>
							</div>
							@endif
							@if($kat_6>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-6-category" name="category" value="6" onClick="filterTextile('cat', '6', 'radio-6-category')">
								<label class="custom-control-label" for="radio-6-category">{{ t('VI. kategória') }} ({{t('+5 500 Ft/fm')}}) <span class="badge badge-primary">{{$kat_6}} db</span></label>
							</div>
							@endif
							@if($kat_7>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-7-category" name="category" value="7" onClick="filterTextile('cat', '7', 'radio-7-category')">
								<label class="custom-control-label" for="radio-7-category">{{ t('VII. kategória') }} ({{t('+6 500 Ft/fm')}}) <span class="badge badge-primary">{{$kat_7}} db</span></label>
							</div>
							@endif
							@if($kat_8>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-8-category" name="category" value="8" onClick="filterTextile('cat', '8', 'radio-8-category')">
								<label class="custom-control-label" for="radio-8-category">{{ t('VIII. kategória') }}  ({{t('+7 500 Ft/fm')}}) <span class="badge badge-primary">{{$kat_8}} db</span></label>
							</div>
							@endif
						</div>
						<div class="col-md-7">
							@if($kat_9>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-9-category" name="category" value="9" onClick="filterTextile('cat', '9', 'radio-9-category')">
								<label class="custom-control-label" for="radio-9-category">{{ t('Bőr') }}  ({{t('+7 500 Ft/fm')}}) <span class="badge badge-primary">{{$kat_9}} db</span></label>
							</div>
							@endif
							@if($kat_10>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-10-category" name="category" value="10" onClick="filterTextile('cat', '10', 'radio-10-category')">
								<label class="custom-control-label" for="radio-10-category">{{ t('Easy to clean only with water I') }}  ({{t('+5 500 Ft/fm')}}) <span class="badge badge-primary">{{$kat_10}} db</span></label>
							</div>
							@endif
							@if($kat_11>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-11-category" name="category" value="11" onClick="filterTextile('cat', '11', 'radio-11-category')">
								<label class="custom-control-label" for="radio-11-category">{{ t('Easy to clean only with water II') }}  ({{t('+6 500 Ft/fm')}}) <span class="badge badge-primary">{{$kat_11}} db</span></label>
							</div>
							@endif
							@if($kat_12>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-12-category" name="category" value="12" onClick="filterTextile('cat', '12', 'radio-12-category')">
								<label class="custom-control-label" for="radio-12-category">{{ t('Easy to clean only with water III') }}  ({{t('+7 500 Ft/fm')}}) <span class="badge badge-primary">{{$kat_12}} db</span></label>
							</div>
							@endif
							@if($kat_13>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-13-category" name="category" value="13" onClick="filterTextile('cat', '13', 'radio-13-category')">
								<label class="custom-control-label" for="radio-13-category">{{ t('Akciós szövetek') }}  ({{t('0 Ft/fm')}}) <span class="badge badge-primary">{{$kat_13}} db</span></label>
							</div>
							@endif
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane fade pt-3" id="typecategories" aria-labelledby="typecategories-tab">
					<div class="row">
						<div class="col-md-12">
							@if($tip_1>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-1-type" name="type" value="1" onClick="filterTextile('type', '1', 'radio-1-type')">
								<label class="custom-control-label" for="radio-1-type">{{ t('Műbőr') }} <span class="badge badge-primary">{{$tip_1}} db</span></label>
							</div>
							@endif
							@if($tip_2>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-2-type" name="type" value="2" onClick="filterTextile('type', '2', 'radio-2-type')">
								<label class="custom-control-label" for="radio-2-type">{{ t('Szövet') }} <span class="badge badge-primary">{{$tip_2}} db</span></label>
							</div>
							@endif
							@if($tip_3>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-3-type" name="type" value="3" onClick="filterTextile('type', '3', 'radio-3-type')">
								<label class="custom-control-label" for="radio-3-type">{{ t('Bőr') }} <span class="badge badge-primary">{{$tip_3}} db</span></label>
							</div>
							@endif
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane fade pt-3" id="speciescategories" aria-labelledby="speciescategories-tab">
					<div class="row">
						<div class="col-12">
							@if($species_0>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-0-species" name="species" value="1" onClick="filterTextile('species', '1', 'radio-0-species')">
								<label class="custom-control-label" for="radio-0-species">{{ t('textiltype_0') }} <span class="badge badge-primary">{{$species_0}} db</span></label>
							</div>
							@endif
							@if($species_1>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-1-species" name="species" value="2" onClick="filterTextile('species', '2', 'radio-1-species')">
								<label class="custom-control-label" for="radio-1-species">{{ t('textiltype_1') }} <span class="badge badge-primary">{{$species_1}} db</span></label>
							</div>
							@endif
							@if($species_2>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-2-species" name="species" value="3" onClick="filterTextile('species', '3', 'radio-2-species')">
								<label class="custom-control-label" for="radio-2-species">{{ t('textiltype_2') }} <span class="badge badge-primary">{{$species_2}} db</span></label>
							</div>
							@endif
							@if($species_3>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-3-species" name="species" value="4" onClick="filterTextile('species', '4', 'radio-3-species')">
								<label class="custom-control-label" for="radio-3-species">{{ t('textiltype_3') }} <span class="badge badge-primary">{{$species_3}} db</span></label>
							</div>
							@endif
							@if($species_4>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-4-species" name="species" value="5" onClick="filterTextile('species', '5', 'radio-4-species')">
								<label class="custom-control-label" for="radio-4-species">{{ t('textiltype_4') }} <span class="badge badge-primary">{{$species_4}} db</span></label>
							</div>
							@endif
							@if($species_5>0)
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input filterinput" id="radio-5-species" name="species" value="6" onClick="filterTextile('species', '6', 'radio-5-species')">
								<label class="custom-control-label" for="radio-5-species">{{ t('textiltype_5') }} <span class="badge badge-primary">{{$species_5}} db</span></label>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row grid">
		<div class="col-12 mt-3">
			<div class="row">
			<?php if( ( date('Ymd')>"20190425" ) && ( date('Ymd')<"20190429" ) ) { ?>
			<style>
				.filter-cat-2 label {
					border: 2px solid #CD7126;
				}
				.filter-cat-2 p {
					color: #CD7126;
				}
			</style>
			<?php }//if
				foreach ($all_textiles as $i_ => $textile) {
					$image_src = ProductTextile::getImageUrl($textile->textile_id,40);
					$image_src_big = ProductTextile::getImageUrl($textile->textile_id);
					$class=array();
					if($textile->extraprice_category_id === 1) {
						array_push($class, "filter-cat-1");
					}
					if($textile->extraprice_category_id === 2) {
						array_push($class, "filter-cat-2");
					}
					if($textile->extraprice_category_id === 3) {
						array_push($class, "filter-cat-3");
					}
					if($textile->extraprice_category_id === 4) {
						array_push($class, "filter-cat-4");
					}
					if($textile->extraprice_category_id === 5) {
						array_push($class, "filter-cat-5");
					}
					if($textile->extraprice_category_id === 6) {
						array_push($class, "filter-cat-6");
					}
					if($textile->extraprice_category_id === 7) {
						array_push($class, "filter-cat-7");
					}
					if($textile->extraprice_category_id === 8) {
						array_push($class, "filter-cat-8");
					}
					if($textile->extraprice_category_id === 9) {
						array_push($class, "filter-cat-9");
					}
					if($textile->extraprice_category_id === 10) {
						array_push($class, "filter-cat-10");
					}
					if($textile->extraprice_category_id === 11) {
						array_push($class, "filter-cat-11");
					}
					if($textile->extraprice_category_id === 12) {
						array_push($class, "filter-cat-12");
					}
					if($textile->extraprice_category_id === 13) {
						array_push($class, "filter-cat-13");
					}
					
					if($textile->textile_category_id === 1) {
						array_push($class, "filter-type-1");
					}
					if($textile->textile_category_id === 2) {
						array_push($class, "filter-type-2");
					}
					if($textile->textile_category_id === 3) {
						array_push($class, "filter-type-3");
					}
					
					if($textile->textile_type_id === 0) {
						array_push($class, "filter-species-1");
					}
					if($textile->textile_type_id === 1) {
						array_push($class, "filter-species-2");
					}
					if($textile->textile_type_id === 2) {
						array_push($class, "filter-species-3");
					}
					if($textile->textile_type_id === 3) {
						array_push($class, "filter-species-4");
					}
					if($textile->textile_type_id === 4) {
						array_push($class, "filter-species-5");
					}
					if($textile->textile_type_id === 5) {
						array_push($class, "filter-species-6");
					}
					
					if(ismobile()){
						$divclass=" col-6";
					}
					else {
						$divclass=" col-md-2 col-sm-4";
					}
			?>
				<div class="{{$divclass}} text-center p-1 label-container {{$class[0]}} {{$class[1]}} {{$class[2]}}" onMouseover="setBigPix('{{$image_src_big}}', '{{$textile->textile_id}}')">
					<label class="img-label shadow text-right" style="background-image: url('{{$image_src}}');" data-toggle="tooltip" data-placement="top" title="{{$textile->name}}" id="textile-{{$textile->textile_id}}-mini">
						<?php /* <a href="{{$image_src_big}}" target="_blank" class="ml-auto openbutton"><i class="fa fa-external-link m-2" aria-hidden="true"></i></a> */ ?>
						<input type="radio" name="{{$type}}" value="{{$textile->textile_id}}" class="hidden" data-bigpix="{{$image_src_big}}" onClick="setTextile('{{$type}}', '{{$textile->textile_id}}'); textilePreview('{{$kategoria_prefix}}', '{{$type}}', '{{$textile->textile_id}}'); refreshTextilePreview('{{$image_src}}', '{{$image_src_big}}', '{{$type}}', '{{$textile->name}}', '{{(($textile->extra_price) ? money($textile->extra_price).'/'.t('fm') : '')}}')" data-dismiss="modal">
					</label>
					<p><small style="word-wrap:break-word;">{{$textile->name}}<br>{{(($textile->extra_price) ? money($textile->extra_price).'/'.t('fm').'<br>' : '')}}{{$textile->textile_catname}}</small></p>
				</div>

			<?php
				}//foreach
			?>
			</div>
		</div>
	</div>
</div>
<?php
}//if
else {
	//nincs textil
}//else
//die();
?>

                             <?php

                                 //$slider_items = [];

                             ?>



                             <?php /*

                                 $field = "on_".$type;

                                 if ($type == 'kisparna'){

                                     $field = "on_parna";

                                 }

                                 if (!isset($textile['original'][$field])) {

                                     $field = "on_butor";

                                 }

								 if (strpos(" ".$type,"elemes")) {

									 $field = "on_elemes";

								 }
*/
                             ?>


                             <?php /*



                                 $image_src = ProductTextile::getImageUrl($textile->textile_id, 255);

                                 $image_src_big = ProductTextile::getImageUrl($textile->textile_id);



                                 $slider_items[] = '

							  <div data-category="'.$textile->textile_category_id.'" data-price="'.$textile->extraprice_category_id.'" data-subtype="'.$textile->textile_type_id.'" data-textile-target="'.$type.'-'.$part->type.'-'.$textile->textile_id.'" class="col-lg-3 product change_textile_data '.$part->type.'_textile_change_'.$textile->textile_id.''.$i.'" data-target="'.$part->type.''.$i.'" onclick="rewriteProductDetailsPrice(\'textile_'.$type.'\', '.$textile->textile_id.','.$product->product_id.'); triggerParnaValasztas(\''.$type.'\',\''.$part->type.'\','.$textile->textile_id.'); if(iklikk==0){scrollToElement(\'#changeTextile\');}">



                                    <div class="product photo slider-max-height-overflow extra-price-container'.($textile->extra_price>0 ? ' extra-price' : '' ).'">'.$textile->extraprice_category_id.'

									   <img class="product-image-photo default_image lazyOwl owl-lazy" title="'. $textile->name .'" data-toggle="tooltip" data-bigimage="'.$image_src_big.'" src="" data-src="" alt="'. $textile->name .'"/>

									</div>

                                    <div class="product details product-item-details">

                                       <strong class="product name product-item-name">

                                       <b>'. $textile->name .'</b>'.

                                       (($textile->extra_price) ? '<span class="pull-right">(+ '.money($textile->extra_price).'/'.t('fm').') &nbsp;&nbsp;</span>' : '').'

                                       <br>

                                       '. $textile->textile_catname .'



                                       </strong>





                                    </div>



                              </div>';
							  
							  */

                              ?>





