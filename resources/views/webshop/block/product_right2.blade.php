</div>
<style>
	#nemgarancia .hidden {
		display: none !important;
	}
</style>
<?php
if( sizeof($related_products) ){
?>
<p class="mt-3 text-uppercase"><strong>Válasszon kiegészítő termékeket és szolgáltatásokat:</strong></p>
<div class="form-check" id="nemgarancia">
<?php
foreach($related_products as $related){
	if($related->size_id == -1){
		$garancia="";
		$datagarancia="";
		if ($related->product->category_id == Config::get('website.garancia_category')) {
			$garancia="garancia";
			$datagarancia='data-garancia="'.$related->product->getDefaultPrice().'"';
			$related->show_price = round(($price_array['furniture_price']+$price_array['textile_price'])*$related->product->getDefaultPrice(),-2);
			$garanciaplus=$related->show_price;
			continue;
		}
?>
	<input type="checkbox" class="form-check-input related {{ $garancia }}" name="related" value="{{ $related->related_id }}" data-product_id="{{ $related->product_id }}" data-default="{{ ($related->default == 1 ? '1' : '0') }}" data-size="{{ $related->size_id }}" data-priceplus2="{{ $related->product_price }}" {{ $datagarancia }} onChange="setFeature('{{ $related->related_id }}')" id="relatedinput-{{ $related->related_id }}">
	<label class="form-check-label w-100 sizelabel border-bottom-0" id="related-{{ $related->related_id }}" for="relatedinput-{{ $related->related_id }}">
		@if ($related->rel_id == -1)
		<span class="size">{{ $related->name }}</span>
		@else
			@if (isset($related->size))
		<span class="size">{{ $related->product->getName().' - '.$related->size->size_name }}</span>
			@else
		<span class="size">{{ $related->product->getName() }}</span>
			@endif
		@endif
		@if ($related->product->category_id != 128)
		<span class="price float-right">
			<span class="price mr-1 {{$garancia}}">
				{{ money($related->show_price) }}
			</span>
		</span>
		@endif
	</label>
	<?php
	}//endif
	else {
		//echo "<br>global a:".$GLOBALS['default_size']." ".$related->size_id;
		$garancia="";
		$datagarancia="";
		if ($related->product->category_id == Config::get('website.garancia_category')) {
			$garancia="garancia";
			$datagarancia='data-garancia="'.$related->product->getDefaultPrice().'"';
			$related->show_price = round(($price_array['furniture_price']+$price_array['textile_price'])*$related->product->getDefaultPrice(),-2);
			$garanciaplus=$related->show_price;
			continue;
		}
		else {
			
		}
		if(isset($GLOBALS['default_size'])){
			if($GLOBALS['default_size']==$related->size_id) {
				$defsize = "";
			}
			else {
				$defsize = " hidden";
			}
		}
		else {
			$defsize = "";
		}
	?>
		<input type="radio" class="form-check-input related {{ $garancia }}" name="related" data-default="{{ ($related->default == 1 ? '1' : '0') }}" data-product_id="{{ $related->product_id }}" value="{{ $related->related_id }}" data-size="{{ $related->size_id }}" data-priceplus2="{{ $related->product_price }}" {{ $datagarancia }} onChange="setFeature('{{ $related->related_id }}')" id="relatedinput-{{ $related->related_id }}">
		<label class="form-check-label w-100 sizelabel border-bottom-0 togleable{{$defsize}} size-{{ $related->size_id }}" id="related-{{ $related->related_id }}" for="relatedinput-{{ $related->related_id }}">
			@if ($related->rel_id == -1)
			<span class="size">{{ $related->name }}</span>
			@else
				@if (isset($related->size))
			<span class="size">{{ $related->product->getName().' - '.$related->size->size_name }}</span>
				@else
			<span class="size">{{ $related->product->getName() }}</span>
				@endif
			@endif
			@if ($related->product->category_id != 128)
			@if($related->product_price!=$related->show_price)
			<span class="price float-right">
				<em class="original-price">{{ money($related->show_price) }}</em>
				<span class="price mr-1 {{$garancia}}">
					{{ money($related->product_price) }}
				</span>
			</span>
			@else
			<span class="price float-right">
				<span class="price mr-1 {{$garancia}}">
					{{ money($related->show_price) }}
				</span>
			</span>
			@endif
			@endif
		</label>
		<?php
			} //else
		}//foreach
		?>
</div>
	<?php
	} //endif
	?>



<?php
if( sizeof($garancia_products) ){
/*
	Kiterjesztett garancia
	2019. 02. 22.
*/
?>
<style>
	.csuszka-conatainer {
		height: 30px;
		background-color: #E9ECEF !important;
		/*border-top: 3px solid #CD7126;*/
	}
	.year {
		background-color: #E9ECEF !important;
		width: 10%;
		height: 30px;
		line-height: 30px;
		display: inline-block;
		float:left;
	}
	.year.active, .text-target {
		background-color: #CD7126 !important;
		color: #fff;
	}
	.year.active {
		border-bottom-left-radius: 5px;
		border-bottom-right-radius: 5px;
	}
</style>
@section('footer_js')
<script>
	function switchYear(id, year) {
		//alert(year);
		$(".year").removeClass("active shadow");
		$("#related-"+id).addClass("active shadow");
		$(".text-target p").addClass("hidden");
		$(".yt"+year).removeClass("hidden");
	}
</script>
@append
<div class="row mx-0">
	<div class="col-12 px-0">
		<?php /* <h3 class="h4-responsive text-uppercase title-center">Kiterjesztett garancia</h3> */ ?>
		<p class="mt-2">Éljen a kiterjesztett garanciánk által nyújtott előnyökkel. Válasszon a következő opciók közül, amennyiben Ön szeretne ezzel a lehetőséggel élni. Amennyiben a maximális 10 év garanciát választja, önköltségi áron, akár öt alkalommal átkárpitoztathatja velünk bútorát.</p>
	</div>
	<div class="col-12 text-center text-target">
		<p class="mt-3 yt1" id="year1">Alapértelmezett garancia (évek)</p>
		<p class="mt-3 hidden yt2" id="year2">Kiterjesztett garancia 2 évig (a vételár 1,5%-a).</p>
		<p class="mt-3 hidden yt3" id="year3">Kiterjesztett garancia 3 évig (a vételár 3%-a).</p>
		<p class="mt-3 hidden yt4" id="year4">Kiterjesztett garancia 4 évig (a vételár 4,5%-a).</p>
		<p class="mt-3 hidden yt5" id="year5">Kiterjesztett garancia 5 évig (a vételár 6%-a).</p>
		<p class="mt-3 hidden yt6" id="year6">Kiterjesztett garancia 6 évig. (a vételár 7%-a)</p>
		<p class="mt-3 hidden yt7" id="year7">Kiterjesztett garancia 7 évig. (a vételár 8%-a)</p>
		<p class="mt-3 hidden yt8" id="year8">Kiterjesztett garancia 8 évig. (a vételár 9%-a)</p>
		<p class="mt-3 hidden yt9" id="year9">Kiterjesztett garancia 9 évig. (a vételár 10%-a)</p>
		<p class="mt-3 hidden yt10" id="year10">10 év garancia + akár ötszöri átkárpitozás (a vételár 11%-a)</p>
	</div>
	<div class="csuszka-conatainer mx-auto w-100">
<?php
	 $year=1;
	foreach($garancia_products as $related){
			if($related->size_id == -1){
	?>
		<?php 
			$garancia="";
			$datagarancia="";
			$selected = "";
			$checked = "";
			if ($related->product->category_id == Config::get('website.garancia_category')) {
				$garancia="garancia";
				$datagarancia='data-garancia="'.$related->product->getDefaultPrice().'"';
				$related->show_price = round(($price_array['furniture_price']+$price_array['textile_price'])*$related->product->getDefaultPrice(),-2);
				$garanciaplus=$related->show_price;
			}
			if( $year === 1 ) {
				$selected = " active shadow";
				$checked = "checked";
			}
		?>
		<label class="{{$selected}} year text-center" id="related-{{ $related->related_id }}" for="garancia-{{ $related->related_id }}" onClick="switchYear('{{ $related->related_id }}', '{{$year}}')">
			<input type="radio" id="garancia-{{ $related->related_id }}" class="form-check-input related {{ $garancia }} hidden" name="related" value="{{ $related->related_id }}" data-product_id="{{ $related->product_id }}" data-default="{{ ($related->default == 1 ? '1' : '0') }}" data-size="{{ $related->size_id }}" data-priceplus2="0" data-garanciaar="{{$related->show_price}}" {{ $datagarancia }} onChange="setFeature('{{ $related->related_id }}')" {{$checked}}>
			{{$year}}
		</label>
	<?php
			}//endif
			else {
				//echo "<br>global a:".$GLOBALS['default_size']." ".$related->size_id;
				if(isset($GLOBALS['default_size'])){
					if($GLOBALS['default_size']==$related->size_id) {
						$defsize = "";
					}
					else {
						$defsize = " hidden";
					}
				}
				else {
					$defsize = "";
				}
	?>

		<label class="col-sm-12 list-group-item sizelabel border-bottom-0 togleable{{$defsize}} size-{{ $related->size_id }}" id="related-{{ $related->related_id }}" >
			<input type="radio" name="related" data-default="{{ ($related->default == 1 ? '1' : '0') }}" data-product_id="{{ $related->product_id }}" class="related form-check-input " value="{{ $related->related_id }}" data-size="{{ $related->size_id }}" data-priceplus2="{{ $related->product_price }}" data-garanciaar="0.00" onChange="setFeature('{{ $related->related_id }}')" id="input-{{ $related->related_id }}">

			@if ($related->rel_id == -1)
			<span class="size">{{ $related->name }}</span>
			@else
				@if (isset($related->size))
			<span class="size">{{ $related->product->getName().' - '.$related->size->size_name }}</span>
				@else
			<span class="size">{{ $related->product->getName() }}</span>
				@endif
			@endif
			@if ($related->product->category_id != 128)
			@if($related->product_price!=$related->show_price)
			<span class="price pull-right">
				<em class="original-price">{{ money($related->show_price) }}</em>
				<span class="price">
					{{ money($related->product_price) }}
				</span>
			</span>
			@else
			<span class="price pull-right">
				<span class="price">
					{{ money($related->show_price) }}
				</span>
			</span>
			@endif
			@endif
		</label>
		<?php
			} //else
		$year++;
	}//foreach
?>
	</div>
</div>
<?php
}//if garancia
?>
<div class="row mx-0 px-0" id="y">