@extends('layouts.default')
@section('content')
<?php /* Új verzió */
//echo $headversion;
if(isset($headversion) && $headversion==="newhead"){
?>
@include('webshop.block.breadcrumbs_2')
<main class="page-main product-page" id="maincontent">
	@if (sizeof($all_categories))
	<div class="container d-flex align-items-stretch">
		<div class="row d-flex align-items-stretch">
			@foreach ($all_categories as $top_category)
				<div class="col-sm-4 mb-4 d-flex align-self-stretch">
					<div class="card shadow d-flex align-self-stretch">
						<a href="{{$top_category->getUrl()}}" title="{{ $top_category->name }}">
							<img src="{{ $top_category->getImageUrl(370) }}" alt="{{ $top_category->name }}" class="img-fluid card-img-top"/>
						</a>
						<div class="card-body pb-0">
							<a href="{{$top_category->getUrl()}}" title="{{ $top_category->name }}"><h2 class="h6 text-uppercase font-weight-bold">{{ $top_category->name }}</h2></a>
							<p class="small text-muted">{{ $top_category->meta_description }}</p>
						</div>
						<div class="card-body mt-0 pt-0 text-center d-flex align-items-end">
							<a href="{{$top_category->getUrl()}}" class="btn btn-block btn-outline-primary">{{ t('Tovább a termékekhez') }}</a>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
	@endif
</main>
<?php
}//if új verzió
else {
?>

@include('webshop.block.breadcrumbs')


<main class="page-main" id="maincontent">



<div class="columns">

<div class="column main width-100percent">




            @if (sizeof($all_categories))
            <div class="single-images paddingtop30">
        		<div class="row">
        		    @foreach ($all_categories as $top_category)
            		    <div class="col-sm-3 paddingbottom15">
            				<a class="image-link" href="{{$top_category->getUrl()}}">
            					<img src="{{ $top_category->getImageUrl(370) }}" title="{{ $top_category->name }}" width="370" height="254" alt="{{ $top_category->name }}" />
            					<span class="category-title">{{ $top_category->name }}</span>
            				</a>
            			</div>

                    @endforeach

        		</div>
        	</div>
        	@endif



</div>

</div>
</main>
<?php
}//else reégi verzió
?>
@stop
@section('footer_js')
@append