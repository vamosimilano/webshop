<aside class="col-md-12 col-lg-3{{ ( ismobile() ? ' order-1' : ' order-0' ) }}">
	<div class="row">
		<?php
		if( !isset($current_category) && !ismobile() && isset($search) ){
		?>
		<div class="col-12">
			<h1 class="h6 page-title">{{ t('Találatok a keresésre: ') }}"<em>{{ $search }}</em>"</h1>
		</div>
		<?php
		}//if
		?>
		<div class="col-12">
		<?php if( ismobile() ) { 
		/* Mobilon kap egy modal keretet */
		?>
			<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModal" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5>{{ t('Szűrők') }}</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
		<?php }//if ?>
		<?php
		if (sizeof($products)) {
			$min=$max=0;
			$x_min=$y_min=$x_max=$y_max=0;
			foreach ($products as $lazy_counter => $product) {
				$price = $product->getDefaultPrice();
				$price_full = $product->getDefaultFullPrice();
				if ($price < $min or $min == 0) {
					$min = $price;
				}
				if ($price > $max or $max == 0) {
					$max = $price;
				}
			$sizes = $product->getSizes();

			if (sizeof($sizes)){
				//echo "<pre>";
				foreach ($sizes as $size){
					//print_r($size['size_x']."<br>");
					if($x_min>$size['size_x'] || $x_min==0){
						//echo $x_min."<br>";
						$x_min=$size['size_x'];
					}
					if($size['size_x']>$x_max){
						$x_max=$size['size_x'];
					}
					if($y_min>$size['size_y'] || $y_min==0){
						$y_min=$size['size_y'];
					}
					if($size['size_y']>$y_max){
						$y_max=$size['size_y'];
					}
				}
				//echo "</pre>";
			}
			}
			/*echo "y ".$y_min." - ".$y_max."<br>";
			echo "x ".$x_min." - ".$x_max."<br>";*/
		?>
		<div class="filter-options-item mb-4">
			<div data-role="rangeslider">
				<label for="pricerange" class="filter-options-title text-uppercase mb-3">Ár <span id="selectedprice">(max: {{money($max)}})</span></label>
				<input type="range" class="custom-range" name="pricerange" id="pricerange" min="{{$min}}" max="{{$max}}" value="{{$max}}" step="{{ ($max-$min)/10 }}" data-max="{{$max}}" onChange="priceRange()">
				<div class="row">
					<div class="col-6 small">{{money($min)}}</div>
					<div class="col-6 small text-right">{{money($max)}}</div>
				</div>
			</div>
		</div>
		<div class="filter-options-item mb-4">
			<div data-role="rangeslider">
				<label for="xsize" class="filter-options-title text-uppercase mb-3">Szélesség <span id="selectedxsize">(max: {{$x_max}} cm)</span></label>
				<input type="range" class="custom-range" name="pricerange" id="xsize" min="{{$x_min}}" max="{{$x_max}}" value="{{$x_max}}" data-max="{{$x_max}}" onChange="sizeRange('x')">
				<div class="row">
					<div class="col-6 small">{{$x_min}} cm</div>
					<div class="col-6 small text-right">{{$x_max}} cm</div>
				</div>
			</div>
		</div>
		<div class="filter-options-item mb-4">
			<div data-role="rangeslider">
				<label for="ysize" class="filter-options-title text-uppercase mb-3">Mélység <span id="selectedysize">(max: {{$y_max}} cm)</span></label>
				<input type="range" class="custom-range" name="pricerange" id="ysize" min="{{$y_min}}" max="{{$y_max}}" value="{{$y_max}}" data-max="{{$y_max}}" onChange="sizeRange('y')">
				<div class="row">
					<div class="col-6 small">{{$y_min}} cm</div>
					<div class="col-6 small text-right">{{$y_max}} cm</div>
				</div>
			</div>
		</div>
		<?php

		}//if products
		?>

		<?php 
			if (sizeof($products)) {
				$replace = [','=>'', '	'=>'-', ' '=>'-', '"'=>'', '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '', '&quot;' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae', '&Auml;' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae','Ç' => 'C', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D', 'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E', 'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G', 'Ġ' => 'G', 'Ģ' => 'G',	'Ĥ' => 'H', 'Ħ' => 'H',	'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I',     'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'K', 'Ľ' => 'K',     'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N','Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O','Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O','Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S','Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T','Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U','&Uuml;' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U','Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z','Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a','ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a','æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c','ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e','ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e','ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h','ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i','ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j','ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l','ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n','ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe','&ouml;' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe','ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u','û' => 'u', 'ü' => 'ue', 'ū' => 'u', '&uuml;' => 'ue', 'ů' => 'u', 'ű' => 'u','ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y','ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'ß' => 'ss','ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G','Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I','Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O','П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F','Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '','Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a','б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo','ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l','м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's','т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch','ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e','ю' => 'yu', 'я' => 'ya'];
				$prod_id=[];
				$datakeys="";
				foreach($products as $x) {
					array_push($prod_id, $x->product_id);
					/*foreach ($size_data as  $key => $size_d) {
						$datakeys .= ' data-'.$key.'="'.$size_d.'"';
					}*/
				}
				?>
				<?php
				$y = implode(", ",$prod_id);
				$structure=DB::select('SELECT DISTINCT `property` as `title`, `value` as `item` FROM `product_properties` 
				INNER JOIN `products` ON (`product_properties`.`product_id` = `products`.`product_id` AND `products`.`deleted_at` IS NULL) 
				INNER JOIN `products_lang` ON (`products_lang`.`product_id` = `products`.`product_id` AND `products_lang`.`deleted_at` IS NULL AND `products_lang`.`visible`="yes")
				WHERE `product_properties`.`product_id` IN('.$y.') AND `product_properties`.`deleted_at` IS NULL GROUP BY `product_properties`.`property`, `product_properties`.`value` ORDER BY `product_properties`.`property` ASC');
				if(sizeof($structure)>1){
				$title=0;
				$item="";
				$i=0;
				foreach ($structure as $menu) {
					$title_slug=str_replace(array_keys($replace), $replace, strtolower($menu->title));
					$item_slug=str_replace(array_keys($replace), $replace, strtolower($menu->item));
				if($title===0) {
				?>
			<div class="filter-options-item">
				<div class="filter-options-title text-uppercase mb-3">{{ $menu->title }}</div>
				<div class="filter-options-content">
					<ul class="list-unstyled">
				<?php
				//$item=$menu->item;
				//$title=$menu->title;
				}//if
				if($menu->title!=$title){
				?>
					</ul>
				</div><!-- div zár -->
			</div><!-- 1. zár -->
			<div class="filter-options-item">
				<div class="filter-options-title text-uppercase mb-3">{{ $menu->title }}</div>
				<div class="filter-options-content">
					<ul class="list-unstyled">
					<?php
					}
					if($item!=$menu->item){
					?>
						<li class="filtertoggle" id="filter-{{$i}}">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input filtercheckbox" id="customCheck{{$i}}" onClick="modifyFilter('{{strtolower($item_slug)}}')" value="{{strtolower($item_slug)}}">
								<label class="custom-control-label" for="customCheck{{$i}}">{{$menu->item}}</label>
							</div>
						</li>
						<?php
						}//if
						if($title!=0) {
						?>
					</ul>
				</div><!-- div zár -->
			</div><!-- 1. zár -->
			<div class="filter-options-item">
				<div class="filter-options-title text-uppercase mb-3">{{ $menu->title }}</div>
				<div class="filter-options-content">
					<ul class="list-unstyled">
					<?php	
					}
					
					//aktuális értéket átadom a réginek
					$title=$menu->title;
					$item=$menu->item;
					$i++;
					}//foreach ?>
					</ul>
				</div>
			<div class="clearfix"></div>
			</div>
			<?php
				}//if 
			}//if
			?>
		<?php if( ismobile() ) { 
		/* Mobilon kap egy modal keretet */
		?>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary btn-block" data-dismiss="modal"> {{ t('Kész') }} </button>
						</div>
					</div>
				</div>
		<?php }//if ?>
		</div>
		<?php
		if( isset($current_category) && !ismobile() ){
		?>
		<div class="col-12">
			<h1 class="h6 page-title">{{ $current_category->name }}</h1>
			<?php
			if($current_category->description) {
			?>
			<p class="small">{{ $current_category->description }}</p>
			<?php
			}
			?>
		</div>
		<?php
		}//if
		?>
	</div>
</aside>