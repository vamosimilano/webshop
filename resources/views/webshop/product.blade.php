@extends('layouts.default')
@section('content')
<?php if(isset($headversion) && $headversion==="newhead"): ?>
@include('webshop.block.breadcrumbs_2')
<main class="page-main product-page" id="maincontent" itemscope itemtype="http://schema.org/Product">
	@include('webshop.block.product_top_2')
</main>
@stop
<?php else: ?>
@include('webshop.block.breadcrumbs')
<main class="page-main product-page" id="maincontent" itemscope itemtype="http://schema.org/Product">

<?php //echo date("Y-m-d H:i:s");
		/*print_r($_COOKIE["selected".$product->product_id]);
	$decode = base64_decode($_COOKIE["selected".$product->product_id]);
	echo $decode;*/
	?>

    @include('webshop.block.product_top')



    @if (sizeof($photos))

    <div class="container desktop-view">

        <div class="col-lg-12">

        <div class="collage">

            @foreach ($photos as $photo)

            <div class="col col-lg-{{ ($photo->num == 1 ? 6 : ($photo->num == 2 ? 2 : '4 min')) }}" ><div class="collage-item{{ ($photo->num >= 3 ? ' min' : '') }}" style="background-image: url({{$photo->getImageUrl()}});">

            </div></div>

            @endforeach

        </div>

        </div>

    </div>

    @endif



</main>



@stop

@section('header_js')

<?
$user_agent = $_SERVER['HTTP_USER_AGENT'];
if (stripos( $user_agent, 'Chrome') !== false)
{
	$load="$(document).ready(function(){";
    //echo "Google Chrome";
}

elseif (stripos( $user_agent, 'Safari') !== false)
{
   	$load="$(window).load(function(){";
	//echo "Safari";
}
else {
	$load="$(document).ready(function(){";
	//echo "nem safari";
}
?>

@append

@if (sizeof($photos))

@section('footer_js')
    <script type="text/javascript">

		<?php echo $load; ?>
            initCollage();
        });

    </script>
@append

@endif



@section('footer_js')

    <script type="text/javascript">

var Base64 = {

    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode : function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
            this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
            this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },

    // public method for decoding
    decode : function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    // private method for UTF-8 encoding
    _utf8_encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode : function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while ( i < utftext.length ) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

}


jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	  var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) {
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});

});


var iklikk=0;
var calculate_run = 0;
var enable_changed_textile_target = true;
$('document').ready(function(){

  $('.stores-stab div').click(function() {

    $clicked = $(this);
    if ($clicked.hasClass('active')) {
      return false;
    }
    $('.stores-stab div').removeClass('active');
    $clicked.addClass('active');
  });
  if ($('#store-slider-container').length) {
    setTimeout(function(){
      var count_div = $('.stores-stab div').length;
      var slider_height = $('#banner-slider-stores').height();

      $('.stores-stab div').height(slider_height/count_div).css('line-height', (slider_height/count_div)+"px");

    }, 2000);

  }

  $('.search-toggle-icon').click(function(){
      $('.block-search').fadeIn();
  });
  $('.block-search').mouseout(function() {
    $('.block-search').fadeOut();
  });

  $(".hover-lazy").unveil();

  setToken();

  $(".fancy").fancybox();


  $('.image-option img').hover(function() {
      var img_src = $(this).data('image-hover');
      var img_target = $(this).data('target-image');
      $(img_target).attr('src', img_src);

  });
  $('.image-option img').click(function() {
      var img_src = $(this).data('image-hover');
      var img_target = $(this).data('target-image');
      $(img_target).attr('src', img_src);


  });
  $('.change_textile_data').on('click', function(){

      var click_object = $(this);

      var target = '#ownTextile'+click_object.data('target');
      var targetimg = '#ownTextileImg'+click_object.data('target');
      if (typeof(click_object.find('img').attr('src')) == "undefined") {
          $(targetimg).attr('src', click_object.find('img').data('src'));
      }else{
          if (click_object.find('img').data('bigimage')) {
            $(targetimg).attr('src', click_object.find('img').data('bigimage'));
          }else {
            $(targetimg).attr('src', click_object.find('img').attr('src'));
          }
      }

      $(target).val(click_object.find('b').html());


  });
  $('.product-sizes .row.size').on('click', function(){

    var clicked_element = $(this);

    $('.product-sizes .row.size.active').removeClass('active');
    $(this).find('.radio').prop("checked", true);
    clicked_element.addClass('active');
    $('#default_price_').val(clicked_element.find('.radio').data('price'));

    $('#product_size_a').val(clicked_element.find('.radio').data('size-name'));
    $('#product_size_b').val(clicked_element.find('.radio').data('size-name'));
    $('#product_size_c').val(clicked_element.find('.radio').data('size-name'));
    relatedProductChange();
    changeOtherSize(clicked_element.find('.radio').data('size-name'));
    calcProductDeailPrice(clicked_element.find('.radio').data('product_id'));

  });
  $('#other-sizes .clicked').on('click', function(){

      $(this).parent('div').find('.active').removeClass('active');

      if ($(this).data('side') == 'A') {
        $('#product_size_a').val($(this).data('rel'));
      }
      if ($(this).data('side') == 'B') {
        $('#product_size_b').val($(this).data('rel'));
      }
      if ($(this).data('side') == 'C') {
        $('#product_size_c').val($(this).data('rel'));
      }
      $(this).addClass('active');
      calcProductDeailPrice($('#product_id_').val());
  });

  //$('.change-mounting').on('click', function(){
	
  $('.change-mounting').click(function(){
    var clicked_element = $(this);
   $('.change-mounting.active').find('.mounting').removeAttr('checked').prop("checked", false);

    $('.change-mounting.active').removeClass('active');
    $(this).find('.mounting').prop("checked", true);
    clicked_element.addClass('active');
	
	console.log('mounting lefutott');

  });

  $('.change-seat').on('click', function(){
    var clicked_element = $(this);
    $('.change-seat.active').find('.seat').removeAttr('checked').prop("checked", false);

    $('.change-seat.active').removeClass('active');
    $(this).find('.seat').prop("checked", true);
    clicked_element.addClass('active');

  });

  $('.switch-carousel').click(function(){
    $(this).parent('span').find('i.switch-carousel').removeClass('active');
    $(this).addClass('active');
    var tmpFunc = new Function($(this).data('target'));
    tmpFunc();
    $(this).hide();

  });

  $('.category-filter div').click(function(){
     $('.category-filter div').removeClass('active');
     $(this).addClass('active');
  });

  if ($('.paginator-container').length){
    $('.paginator-container .pagination').css('width',$('.paginator-container .pagination').width())
      .css('position', 'relative')
      .css('display','block')
      .css('margin','0 auto');
  }

  relatedProductChange();
  $('.related').change(function() {
	//$('#related_product').val($('.related:checked').val());
	//fd = $('#relatedfdata').val();
	//$('#relatedfdata').val(fd+';'+$('.related:checked').val());
	//alert($('#relatedfdata').val());
	//$('#priceplus_price_2_').val($('.related:checked').data('priceplus2'));
    //calcProductDeailPrice($(this).data('product_id'));
	$('#relatedfdata').val('');
	$('#relatedfprice').val(0);
	fd='';
	$('.related').each(function() {
		if ($(this).prop("checked")) {
			fd = $('#relatedfdata').val();
			$('#relatedfdata').val(fd+';'+$(this).val());
			$('#relatedfprice').val(parseFloat($('#relatedfprice').val())+parseFloat($(this).data('priceplus2')));
			var new_price = parseInt($('#relatedfprice').val());
			$('#priceother-target').html(new_price.format(0,'3',' ')+" "+$('#default_data').data('curr'));
		}
    });
	$('#related_product').val($('#relatedfdata').val());
	if (parseFloat($('#relatedfprice').val())>0) {
		$('#priceother-div').show();
	} else {
		$('#priceother-div').hide();
	}
	//var new_price = (parseInt($('#default_price_').val())*1) + (parseInt($('#priceplus_price_').val())*1) + (parseInt($('#priceplus_price_2_').val())*1) + (parseInt($('#relatedfprice').val())*1);
	var new_price = (parseInt($('#default_price_').val())*1) + (parseInt($('#relatedfprice').val())*1);
	$('#priceall-target').html(new_price.format(0,'3',' ')+" "+$('#default_data').data('curr'));
  });

  $('#filter-openlink').click(function(){
    $('.filter-options-item').slideToggle();
  });

});
var disable_auto_scroll = false;
function scrollToElement(selector) {
  if (disable_auto_scroll) {
    return;
  }
  if(jQuery(selector).length == 0){
            return;
        }


        var position = jQuery(selector).offset();

        var target_pos = position.top - 120;

        if (jQuery('html,body').scrollTop() == target_pos) {
          return false;
        }

        jQuery('html,body').stop().animate({
            scrollTop: target_pos
        }, 2000);
}


function initCollage(){

  $('.collage-item').each(function() {
    var image_url = $(this).css('background-image'), image;


    image_url = image_url.match(/^url\("?(.+?)"?\)$/);


    if (image_url[1]) {
        image_url = image_url[1];

        image = new Image();
        image.src = image_url;

        if (image.height > image.width) {
          $(this).addClass('toptoheight');
        }
    }
  });
}

function calcProductDeailPrice(product_id){

  calculateProductPrice('#form-product-'+product_id);
  return;

  //var new_price = (parseInt($('#default_price_').val())*1) + (parseInt($('#priceplus_price_').val())*1) + (parseInt($('#priceplus_price_2_').val())*1);
  var new_price = (parseInt($('#default_price_').val())*1) + (parseInt($('#relatedfprice').val())*1);
  $('#priceall-target').html(new_price.format(0,'3',' ')+" "+$('#default_data').data('curr'));

}

function rewriteProductDetailsPrice(field_id, field_value, product_id){
  if ($('[data-subid="'+field_id+'"]').length) {
    $('[data-subid="'+field_id+'"]').attr('data-selected', field_value);
  }
  $('#'+field_id).val(field_value);
  calcProductDeailPrice(product_id);

}
var _parna_triggered = false;
var _kivalasztasok = false;
function triggerParnaValasztas(type, subtype, textile_id) {
  _kivalasztasok = true;
  if (_parna_triggered) {
    return;
  }
  var _datacontainer = $('[data-textile-target="'+type+"-"+subtype+"-"+textile_id+'"]');
  var _dataimg = _datacontainer.find('img');


  $('#'+subtype+" .form-group").each(function() {
    var _container = $(this);
    var _subselected = $(this).find('.textileSelect');
    if (!_subselected.attr('data-selected')) {
      _subselected.attr('data-selected', textile_id);
      _container.find('.form-control').val(_dataimg.attr('alt'));
      _container.find('img').attr('src',_dataimg.attr('data-bigimage'));
    }
  });
  _parna_triggered = true;
  return;
  if ($('.filterproducts [data-textile-target="kisparna-'+subtype+'-'+textile_id+'"]').length) {
    $('.filterproducts [data-textile-target="kisparna-'+subtype+'-'+textile_id+'"]').trigger('click');
  }
  if ($('.filterproducts [data-textile-target="nagyparna-'+subtype+'-'+textile_id+'"]').length) {
    $('.filterproducts [data-textile-target="nagyparna-'+subtype+'-'+textile_id+'"]').trigger('click');
  }


}



function findAndRewriteTextileData(class1, class2) {
  if (disable_short_change) {
    return;
  }
  var active_part_key = $('#changeTextile .nav-tabs li.active a').data('part-key');
  class1 = active_part_key+"_"+class1;
  class2 = active_part_key+"_"+class2;

  var i = 0;

  if (active_part_key!='random') {
	  $('.textileslider').each(function(){
		if (i <= 1) {
		  if (class1 != class2){
			calculate_run = 0;
		  }
		  $(this).find('.'+class1+"0").trigger('click');
		}else{
		  calculate_run = 0;
		  $(this).find('.'+class2+"1").trigger('click');
		}
		i++;
	  });
  }
}

function calculateProductPrice(form){

    if (calculate_run == 1) {
      return '';
    }
    $('#priceall-target').html('<img src="/loader.gif" width="20px;">');
    calculate_run = 1;
    $.ajax({
			dataType: "json",
			type: "post",
			data: $(form).serialize(),
			url: '/cart/calculateprice',
		}).done(function(data) {
		  $('#priceall-target').html(data.price);
		  $('#priceplus-target').html(data.textile_price);
		  $('#default_price_').val(data.priceall);
		  $('#priceplus_price_').val(data.priceplus);
		  $('#priceplus_price_2_').val(0);
		  calculate_run = 0;

		  //var new_price = (parseInt($('#default_price_').val())*1) + (parseInt($('#priceplus_price_').val())*1) + (parseInt($('#priceplus_price_2_').val())*1) + (parseInt($('#relatedfprice').val())*1);
		  var new_price = (parseInt($('#default_price_').val())*1) + (parseInt($('#relatedfprice').val())*1);
		  $('#priceall-target').html(new_price.format(0,'3',' ')+" "+$('#default_data').data('curr'));

		});
	calculate_run = 0;
}

function searchCity(zip){
  if (zip == '') {
    return '';
  }
  $('#city').attr('readonly');
   $.ajax({
			dataType: "json",
			type: "post",
			data: {zip:zip},
			url: '/cart/searchcity',
		}).done(function(data) {
  		$('#city').removeAttr('readonly');
  		if (data.fn){
  		  var tmpFunc = new Function(data.fn);
        tmpFunc();
      }
      $('#city').val(data.city);
		});
}

function sliderFilterCategory(selector, data_id) {
  if (data_id == '3') {
    $("#"+selector).find('.change_textile_data').show();
    return;
  }

  $("#"+selector).find('.change_textile_data').each(function(){
    if ($(this).data('category') == data_id) {
      $(this).show();
    } else {
      $(this).hide();
    }
  });
}

function sliderFilterPrice(selector, data_id) {
  $("#"+selector).find('.change_textile_data').each(function(){
    if ($(this).data('price') == data_id) {
      $(this).show();
    }else{
      $(this).hide();
    }
  });
}


function closeSliders(id){
  $('.textileslider').each(function(){
    var c_id = '#'+$(this).attr('id');
      if (c_id != id){
        $(this).hide();
      }
  });
}

function setToken(){

  $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
  });
}

/**
 * Number.prototype.format(n, x, s, c)
 *
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 */
Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

function getUnityObject(){
  var unity_data = {uid:"911", session_id:"lllsdlksdlka2323kj2jk32jk32k2", product_id: "271", size_id:"43"};
  return unity_data;
}

function relatedProductChange() {
  if ($('.related').length) {
    if ($('input[name="feature_id"]').attr('type') == 'hidden') {
      var current_size_id = $('input[name="feature_id"]').val();
    } else {
      var current_size_id = $('input[name="feature_id"]:checked').val();
    }
    $('#related_product').val('');
	$('#relatedfdata').val('');
	$('#relatedfprice').val(0);
    $('.related:checked').prop('checked', false);
    $('.related').each(function() {
      $(this).parent().parent().removeClass('active');
      if ($(this).val() > 0 || $(this).val() == '') {
        $(this).attr('disabled','disabled');
        if ($(this).data('size') == current_size_id || $(this).data('size') == 0 || $(this).data('size') == -1) {
          $(this).removeAttr('disabled');
          $(this).parent().parent().addClass('active');
          if ($(this).data('default')) {
            $(this).prop("checked", true);
            //$('#related_product').val($('.related:checked').val());
			//$('#related_product').val().append($('.related:checked').val());
			fd = $('#relatedfdata').val();
			$('#relatedfdata').val(fd+';'+$(this).val());

			$('#relatedfprice').val(parseFloat($('#relatedfprice').val())+parseFloat($(this).data('priceplus2')));
			var new_price = parseInt($('#relatedfprice').val());
			$('#priceother-target').html(new_price.format(0,'3',' ')+" "+$('#default_data').data('curr'));
            calcProductDeailPrice($(this).data('product_id'));


          }
        }
      }
    });
	$('#related_product').val($('#relatedfdata').val());
	$('#related_product').val($('#relatedfdata').val());
	if (parseFloat($('#relatedfprice').val())>0) {
		$('#priceother-div').show();
	} else {
		$('#priceother-div').hide();
	}
	return true;
  }
}

function uncheckRelated() {
  $('#related_product').val('');
  $('#relatedfdata').val('');
  calcProductDeailPrice($('.related:checked').data('product_id'));
  $('.related:checked').prop('checked', false);
}

function changeOtherSize(size_name) {
  $('#other-sizes .active').removeClass('active');
  $('#other-sizes .clicked').each(function() {
    if($(this).data('rel') == size_name) {
      $(this).addClass('active');
    }
  });
}
function resetTextileInput() {
  $('.reset_click_').val('');
}
function switchTabTextile(){
  if (disable_short_change) {
    return;
  }
  setTimeout(function() {

    var part_key = $("#changeTextile .nav li.active a").data('part-key');

    $('.textileslider').hide();
    resetTextileInput();
    if (part_key != 'full') {
      $('.reset_click_').each(function(){
        if ($(this).data('default')) {
          $(this).val($(this).data('default'));
        }
      });
    }
  }, 200);
}
</script>
<script type="text/javascript">

function switchTabTextileselectpage(){
  _kivalasztasok = false;
  setTimeout(function() {
    var part_key = $("#changeTextile .nav li.active a").data('part-key');

    $('.textileslider').hide();
    resetTextileInput();
    if (part_key != 'full') {
      $('.reset_click_').each(function(){
        if ($(this).data('default')) {
          $(this).val($(this).data('default'));
        }
      });
    }
  }, 200);
}


function otherSizeShow(){
  $('#other-sizes').show();
  $('#other-size-shange-off').hide();
  $('#other-size-shange-on').show();
}

function otherSizeHide(){
  $('#other-sizes').hide();
  $('#other-size-shange-off').show();
  $('#other-size-shange-on').hide();
}
function openHelpImage(){
  if (!$("body #helpImageData_").length) {
    $("body").append("<img id='helpImageData_' src='"+$('#helpImageData').attr('src')+"' style='display: none'/>");
    $("body").append("<div id='helpImgDataOverlay' style='display: none'></div>");
  }

  var new_src = $('#helpImageData').data('src')+$('#helpImageData').data($(".mounting:checked").val());
  $("#helpImageData_").attr('src', new_src);
  var hwidth = $("#helpImageData_").outerWidth();
  var wwidth = $(window).width();
  $("#helpImageData_").css("left", (wwidth-hwidth)/2).show();
  $("#helpImgDataOverlay").show();

  $('#helpImgDataOverlay').click(function() {
    $("#helpImgDataOverlay").hide();
    $("#helpImageData_").hide();
  })

}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
</script>
<script type="text/javascript">
		
var disable_short_change = false;
function runProductTextileCookieChange(decode, product_id) {
	console.log("runProductTextileCookieChange");
  disable_short_change = true;
  calculate_run = 1;

  decode = Base64.decode(decode);
	console.log(decode);
  var obj = JSON.parse(decode);
  setTimeout(function() {
      resetTextileInput();
      $( "#changeTextile .data.item.active" ).removeClass('active');
      $( "#changeTextile" ).tabs({active:$('[data-part-key="'+obj.tabpane+'"]').parent('li').index()});
      //$('#changeTextile .tab-content .tab-pane').hide();
      //$('#changeTextile .tab-content #'+obj.tabpane).show();
      $('[data-part-key="'+obj.tabpane+'"]').parent('li').addClass('active');
      switchTabTextile();
  }, 500);
  var target_ = false;
  $.each(obj.parts, function(value, item) {
      if (item.value != '') {
          setTimeout(function() {
			  rewriteProductDetailsPrice('textile_'+item.id, item.value, product_id);
              $('[data-textile-target="'+item.id+"-"+obj.tabpane+'-'+item.value+'"]').trigger('click');
          }, 500);
      }
      if (enable_changed_textile_target) {
		  console.log("enable_changed_textile_target");
        var html = '';
        if (item.name) {
          html = '<div class="form-group">';
          html+= '<label>'+(item.name ? item.name : 'Ismeretlen')+'</label>';
          html+='<div class="input-group">';
          html+='<span class="input-group-addon v1"><a href="#" onclick="$(this).attr(\'href\',$(this).find(\'img\').attr(\'src\'));return true;" class="fancy"><img src="'+(item.img ? item.img : '')+'" style="width: 28px;"></a></span>';
          html+='<input name="" id="" readonly="" class="form-control" value="'+(item.textile ? item.textile : '')+'">';
          html+='<span class="input-group-btn"></span>';
          html+='</div></div>';
        }
        if (target_ == false && html != '') {
          $('#changed-textile-target').html('');
            target_ = true;

        }
        if (html) {
          $('#changed-textile-target').append(html);
          $(".fancy").fancybox();

        }
      }

  });

  switchTabTextile();

  setTimeout(function() {
    calculate_run = 0;
    calcProductDeailPrice(product_id);
  }, 1000);

}
        //$(document).ready(function(){
        //$(window).load(function(){
		<?php echo $load; ?>
        var decode =  getCookie('selected{{$product->product_id}}');
		
		//setTimeout(function() {console.log(decode)}, 1500);

            if (decode) {
				
				console.log("van runProductTextileCookieChange töltéskor");

                runProductTextileCookieChange(decode,{{$product->product_id}});

                $('#changeTextileBtn').html('{{ t('Szövetek újraválasztása!') }}');

				$('#changeTextileReset').removeClass('hidden');
				

            }


			var mounting = getCookie('lastusedmounting');
		
			console.log("mounting: "+mounting);

			if (mounting==="left") {
				console.log("bent: "+mounting);
				jQuery('#mounting_'+mounting).trigger('click');


			}
			if (mounting==="right") {

				console.log("bent: "+mounting);

				jQuery('#mounting_'+mounting).trigger('click');


			}

			var seat = getCookie('lastusedseat');

			if (seat) {
				$('#seat_'+seat).trigger('click');
			}

        });
</script>
<script type="text/javascript">
		
/*
2018 04 09 - Cookieval elmentjük milyen méreteket választott ki a vásárló korábban

Három cookiet használunk: A, B és C jelűeket, amik minden adott terméknél egyediek. (productid-a, productid-b, productid-c). Ez azért fontos, mert különböző termékeknél más méretek vannak így nem szabad megjegyezni a választást, hogy ne legyenek beépítési problémák.

A cookiek a méretek neveit tárolják adatként (S,M,L,XL) ha mindháromnál/kettőnél megegyeznek (sarokkanapénál csak A és B van), akkor az alap mérettáblát is beállítja, ha különbözőek, akkor a további méretek táblát állítja csak.

A cookiekat a setCookie javascripttel hozom létre és a getCookie fügvénnyel vizsgálom meg őket. A beállításokat jQueryvel végzem el.

*/
		
//1. Létrehozom a szükséges cookiekat, ha méretet vált a felhasználó /setCookie(cname, cvalue, exdays)/
$('#meret-S').on('click', function(){
	setCookie('{{$product->product_id}}-a', 'S', 3);
	setCookie('{{$product->product_id}}-b', 'S', 3);
	setCookie('{{$product->product_id}}-c', 'S', 3);
	setCookie('{{$product->product_id}}-merettipus', 'alap', 3);
	console.log("set S cookie");
});
$('#meret-M').on('click', function(){
	setCookie('{{$product->product_id}}-a', 'M', 3);
	setCookie('{{$product->product_id}}-b', 'M', 3);
	setCookie('{{$product->product_id}}-c', 'M', 3);
	setCookie('{{$product->product_id}}-merettipus', 'alap', 3);
	console.log("set M cookie");
});
$('#meret-L').on('click', function(){
	setCookie('{{$product->product_id}}-a', 'L', 3);
	setCookie('{{$product->product_id}}-b', 'L', 3);
	setCookie('{{$product->product_id}}-c', 'L', 3);
	setCookie('{{$product->product_id}}-merettipus', 'alap', 3);
	console.log("set L cookie");
});
$('#meret-XL').on('click', function(){
	setCookie('{{$product->product_id}}-a', 'XL', 3);
	setCookie('{{$product->product_id}}-b', 'XL', 3);
	setCookie('{{$product->product_id}}-c', 'XL', 3);
	setCookie('{{$product->product_id}}-merettipus', 'alap', 3);
	console.log("set XL cookie");
});

$('#AS').on('click', function(){
	setCookie('{{$product->product_id}}-a', 'S', 3);
	setCookie('{{$product->product_id}}-merettipus', 'vegyes', 3);
});
$('#AM').on('click', function(){
	setCookie('{{$product->product_id}}-a', 'M', 3);
	setCookie('{{$product->product_id}}-merettipus', 'vegyes', 3);
});
$('#AL').on('click', function(){
	setCookie('{{$product->product_id}}-a', 'L', 3);
	setCookie('{{$product->product_id}}-merettipus', 'vegyes', 3);
});
$('#AXL').on('click', function(){
	setCookie('{{$product->product_id}}-a', 'XL', 3);
	setCookie('{{$product->product_id}}-merettipus', 'vegyes', 3);
});

$('#BS').on('click', function(){
	setCookie('{{$product->product_id}}-b', 'S', 3);
	setCookie('{{$product->product_id}}-merettipus', 'vegyes', 3);
});
$('#BM').on('click', function(){
	setCookie('{{$product->product_id}}-b', 'M', 3);
	setCookie('{{$product->product_id}}-merettipus', 'vegyes', 3);
});
$('#BL').on('click', function(){
	setCookie('{{$product->product_id}}-b', 'L', 3);
	setCookie('{{$product->product_id}}-merettipus', 'vegyes', 3);
});
$('#BXL').on('click', function(){
	setCookie('{{$product->product_id}}-b', 'XL', 3);
	setCookie('{{$product->product_id}}-merettipus', 'vegyes', 3);
});
		
$('#CS').on('click', function(){
	setCookie('{{$product->product_id}}-c', 'S', 3);
	setCookie('{{$product->product_id}}-merettipus', 'vegyes', 3);
});
$('#CM').on('click', function(){
	setCookie('{{$product->product_id}}-c', 'M', 3);
	setCookie('{{$product->product_id}}-merettipus', 'vegyes', 3);
});
$('#CL').on('click', function(){
	setCookie('{{$product->product_id}}-c', 'L', 3);
	setCookie('{{$product->product_id}}-merettipus', 'vegyes', 3);
});
$('#CXL').on('click', function(){
	setCookie('{{$product->product_id}}-c', 'XL', 3);
	setCookie('{{$product->product_id}}-merettipus', 'vegyes', 3);
});		
var side_sizetype = getCookie('{{$product->product_id}}-merettipus');		
var side_a = getCookie('{{$product->product_id}}-a');		
var side_b = getCookie('{{$product->product_id}}-b');		
var side_c = getCookie('{{$product->product_id}}-c');	

if(side_sizetype=="alap"){
	console.log("Minden oldal egyforma.");
	console.log("Minden oldal mérete: "+side_a);
	$('.product-sizes .size').removeClass("active");
	$('.sizecontainer-'+side_a).addClass("active");
	}
if(side_sizetype=="vegyes"){
	console.log("Különböző oldalak.");
	console.log("A oldal mérete: "+side_a);
	console.log("B oldal mérete: "+side_b);
	console.log("C oldal mérete: "+side_c);
	//az S/M/L/XL kijelölést deaktiválom, mert vegyes méret van
	$('.product-sizes .size').removeClass("active");
	otherSizeShow(); //kinyítom a további méretek ablakot
	if(side_a) {
		$('[data-side=A]').removeClass("active");
		$('#A'+side_a).addClass("active");
		}
	if(side_b) {
		$('[data-side=B]').removeClass("active");
		$('#B'+side_b).addClass("active");
		}
	if(side_c) {
		$('[data-side=C]').removeClass("active");
		$('#C'+side_c).addClass("active");
		}
	}
	if(side_a==side_b && side_a==side_c && side_b==side_c){
		console.log("egyforma");
		$('.sizecontainer-'+side_a).addClass("active");
		otherSizeHide();
	}
	
</script>


@append


<?php endif; ?>