@extends('layouts.default')

@section('content')
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Fizetés') }}</strong>
                        </li>
            </ul>
</div>

<main class="page-main" id="maincontent">

<div class="entry-content min-height">


    <div class="container">
		<div class="gateway--info">
			<div class="gateway--desc">
				@if(session()->has('message'))
					<p class="message">
						{{ session('message') }}
					</p>
				@endif
				<p><strong>{{ t('Rendelés') }}:{{ $order->order_number}}</strong></p>
				<hr>
				<p>{{ $description }}</p>
				<p>Amount : {{ money($log->price) }}</p>
				<hr>
			</div>
			<div class="gateway--paypal">
				<form id="ppform" method="POST" action="{{ route('checkout.payment.paypal', ['order' => $log->payment_id]) }}">
					{{ csrf_field() }}
					<button class="btn btn-pay">
						<i class="fa fa-paypal" aria-hidden="true"></i> Pay with PayPal
					</button>
				</form>
				<script>
				$("#ppform").submit();
				</script>
			</div>
		</div>
	</div>



</div>
</div></div>
</main>

@stop
@section('footer_js')


@append

