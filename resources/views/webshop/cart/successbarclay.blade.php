@extends('layouts.default')

@section('content')
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Sikeres rendelés feladás') }}</strong>
                        </li>
            </ul>
</div>

<main class="page-main" id="maincontent">

<div class="entry-content min-height">


    <div class="row">
        <div class="row-wrapper container">
            <div class="row">
                <div class="col-md-12">
                    <h3>{{ t('Kedves')." ".$order->firstname }}!</h3>
			<p>{{ t('Rendelése sikeresen megérkezett hozzánk!') }}</p>
            <p>{{ t('Rendelés azonosítója') }}: {{ $order->order_number }}</p>
            @if (sizeof($payment))
                        <div class="alert alert-success" role="alert">
                        <p><b>{{ t('Sikeres tranzakciót hajtott végre!') }}</b></p>
						<?php
							$opt = unserialize($payment['data']);
						?>
                        <p>{{ t('Tranzakció azonosítója') }}: {{ $opt['PAYID'] }}</p>

                        <p><b>{{ t('Összeg') }}</b>: {{ money($payment['price']) }}</p>
                        </div>
                    @endif

            <p>{{ t('A rendelés státuszának változásáról e-mailben értesítjük, valamit nyomon követheti rendeléseit a "Korábbi rendelések" menüpontban, ide kattintva') }}:
            <br>
            <a href="{{ action('ProfileController@orders') }}">{{ action('ProfileController@orders') }}</a>
            </p>


            @if (Session::get('payment_success'))
            <div style="border: 5px solid green; padding: 5px;"><p><b>{{ t('Sikeresen kifizette a rendelését') }}:</b></p>
                <p>{{ Session::get('payment_success') }}</p>
                           </div>
            <br>
            @endif
                </div>
            </div>
        </div>
    </div>



</div>
</div></div>
</main>

@stop
@section('footer_js')


@append

