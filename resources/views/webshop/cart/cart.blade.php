@extends('layouts.default')
@section('content')
<?php 
$headversion="newhead";
if(isset($headversion) && $headversion==="newhead"){
	$items_main = CartHelper::getCartMainItemsCount();
	$items_additional = CartHelper::getCartAdditionalItemsCount();
	$items_outlet = CartHelper::getCartOutletItemsCount();
	$items_value = CartHelper::getCartItemsSum(); //???
	$freeshipping_main = false;
	//$freeshipping_outlet megszünik
	//$freeshipping_outlet = false;
	if ($items_main>1) $freeshipping_main = true;
	//if (($items_main==1) and ($items_additional+$items_outlet>0)) $freeshipping_main = true;
	if (($items_main==1) and ($items_additional>0)) $freeshipping_main = true;
	//if ($items_outlet>1) $freeshipping_outlet = true;
	//if (($items_outlet==1) and ($items_additional>0)) $freeshipping = true;
	/*
	if ($items_value>=500000) {
		$freeshipping_main = true;
		$freeshipping_outlet = true;
	}
	*/
	//200e limitnél van csak ingyenes szállítás
	if ($items_value<200000) {
		$freeshipping_main = false;
	}
	//echo $freeshipping_main."frees_main<br>";
	//echo $freeshipping_outlet."frees-out<br>";
	//if (isset($cart_data[ 'delivery_mode' ]) and ($cart_data[ 'delivery_mode' ] == 'COURIER') and (($freeshipping_main) or ($freeshipping_outlet)) and isset($cart_data[ 'COURIER_FREE' ]) and ($cart_data[ 'COURIER_FREE' ] ) ) {
	if (isset($cart_data[ 'delivery_mode' ]) and ($cart_data[ 'delivery_mode' ] == 'COURIER') and ($freeshipping_main) and isset($cart_data[ 'COURIER_FREE' ]) and ($cart_data[ 'COURIER_FREE' ] ) ) {
		//$shippingcost_timeout = $shippingOutlet + $shippingManufacture;
		//$teljesosszeg = $teljesosszeg - $shippingOutlet - $shippingManufacture;
		if ($freeshipping_main) {
			$shippingcost_timeout = $shippingcost_timeout + $shippingManufacture;
			$teljesosszeg = $teljesosszeg - $shippingManufacture;
			$shippingcost = $shippingcost - $shippingManufacture;
		}
		/*
		if ($freeshipping_outlet) {
			$shippingcost_timeout = $shippingcost_timeout + $shippingOutlet;
			$teljesosszeg = $teljesosszeg - $shippingOutlet;
			$shippingcost = $shippingcost - $shippingOutlet;
		}
		*/
		if ($shippingcost>0) {
			$mpl_shipping = $shippingcost - $shippingOutlet;
			$shippingcost = $shippingcost - $mpl_shipping;
		}
	}
?>
<?php
//if ($outletmore || $manufacturemore || $freeshipping==false) {
//if ( ( ($freeshipping_main==false) and ($items_main+$items_additional>0) ) or ( ($freeshipping_outlet==false) and ($items_outlet>0) ) ) {
	//if( $items_main>0 || $items_outlet > 0 ) {
if ( ($freeshipping_main==false) and ($items_main+$items_additional>0) ) {
	if( $items_main>0 ) {
?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="alert alert-primary mt-3 alert-dismissible fade show" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="row">
				@if($items_main>0)
					<div class="col-12">
						<p><strong>Válasszon még egy terméket az alábbi kategóriákból és bútorait ingyen házhozszállítjuk Önnek!</strong></p>
					</div>
					<div class="col-md-2 text-center">
						<a href="/termekek/kiegeszito/puff" class="text-white">
							<img src="{{ asset('media/icon/w/puff.png') }}" class="img-fluid" style="width:50%;"><br>
							Puff
						</a>
					</div>
					<div class="col-md-2 text-center">
						<a href="/termekek/kiegeszito/dohanyzoasztal" class="text-white">
							<img src="{{ asset('media/icon/w/asztal.png') }}" class="img-fluid" style="width:50%;"><br>
							Dohányzóasztal
						</a>
					</div>
					<div class="col-md-2 text-center">
						<a href="/termekek/kiegeszito/eloszobabutor" class="text-white">
							<img src="{{ asset('media/icon/w/eloszoba.png') }}" class="img-fluid" style="width:50%;"><br>
							Előszobabútor
						</a>
					</div>
					<div class="col-md-2 text-center">
						<a href="/termekek/kiegeszito/fotel" class="text-white">
							<img src="{{ asset('media/icon/w/fotel.png') }}" class="img-fluid" style="width:50%;"><br>
							Fotel
						</a>
					</div>
					<div class="col-md-2 text-center">
						<a href="/termekek/kanape" class="text-white">
							<img src="{{ asset('media/icon/w/kanape-l.png') }}" class="img-fluid" style="width:50%;"><br>
							Kanapé
						</a>
					</div>
					<div class="col-md-2 text-center">
						<a href="/termekek/kanape/franciaagyak" class="text-white">
							<img src="{{ asset('media/icon/w/agy.png') }}" class="img-fluid" style="width:50%;"><br>
							Franciaágy
						</a>
					</div>
					<div class="col-12 mb-3">&nbsp;</div>
				@endif
				<?php
				/*
				@if( $items_outlet>0 and $items_main>0 )
					<div class="col-12">
						<p>Az összeállított kosár tartalmaz késztermékeket (outletes) illetve új gyártású bútort is. Outletes bútorainkhoz is vállalunk ingyenes házhozszállítást.</p>
					</div>
				@endif
				@if($items_outlet>0)
					<div class="col-12">
						<p>Válasszon outletes termékeink közül kanapéjához vagy franciaágyához puffot, asztalt, előszobafalat, fotelt vagy egy másik kanapét vagy franciaágyat és vásárolt bútorait ingyen házhozszállítjuk Önnek!</p>
					</div>
				@endif
				*/
				?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	}
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
		<?php
		if( ismobile() ) {
		?>
			<h1 class="h1-responsive text-uppercase title-center my-4">Kosár tartalma</h1>
		<?php
		}
		else {
		?>
			<ul class="stepper stepper-horizontal" id="cart-stepper">
				<li class="active">
					<a href="{{ action('CartController@index') }}">
						<span class="circle">1</span>
						<span class="label text-uppercase">Kosár tartalma</span>
					</a>
				</li>
				<li class="">
					<a href="{{ action('CartController@personal') }}">
						<span class="circle">2</span>
						<span class="label text-uppercase">Személyes adatok</span>
					</a>
				</li>
				<li class="">
					<a href="!#">
						<span class="circle">3</span>
						<span class="label text-uppercase">Fizetés és befejezés</span>
					</a>
				</li>
			</ul>
		<?php
		}
		?>
		</div>
		<div class="col-md-8">
			<h1 class="h3-responsive text-uppercase title my-4">{{ t('Kosárba tett termékek') }}</h1>
			<table id="shopping-cart-table" class="table table-striped table-bordered">
				<thead>
					<tr class="text-center">
						<th width="5%"></th>
						<th width="40%">Termék</th>
						<th width="30%">Árak</th>
						<th width="5%">db</th>
						<th width="20%">Összeg</th>
					</tr>
				</thead>
				<tbody>
				@foreach (Cart::getContent() as $item)
					<?php
					if ($item->id== 'floor') {
						continue;
					}
					$product = Product::lang()->active()->find($item['attributes']->product_id);
					if (!isset($product)) {
						continue;
					}
					$price_full = 30000;
					if ($product->type == 'scalable' or $product->type == 'non-scalable') {
						$feature = $product->getDefaultSize();
						$size_count = sizeof($item['attributes']['sizes']);
					}
					$price_array = CartHelper::calculateItemPrice($item['attributes'],true);
					?>
					@if ($product->type == 'general')
						@include('webshop.cart.blocks.general_cart_item')
					@else
						@include('webshop.cart.blocks.scalable_cart_item')
					@endif
				@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-md-4">
			@include('webshop.cart.blocks.summary_right')
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			@if (isset($default_data['coupon']) and $default_data['coupon'] != '')
			<h2 class="h3-responsive text-uppercase title my-4">{{ t('Aktivált kupon kód') }}: {{$default_data['coupon']}}</h2>
			@else
			<h2 class="h3-responsive text-uppercase title my-4">{{ t('Rendelkezik kuponkóddal?') }}</h2>
			@endif
		</div>
		<div class="col-md-12">
			@include('layouts.messages')
		@if (isset($default_data['coupon']) and $default_data['coupon'] != '')
			<div class="row">
				<div class="col-md-8">
				<p>Sikeresen aktiválta a(z) {{$default_data['coupon']}} kupont. A kedvezmény levonásra kerül a rendeléséből. Amennyiben mégsem szeretné most felhasználni a kupont kattintson az alábbi gombra.</p>
				<p class="text-center text-md-right"><a href="{{ action('CartController@removeCoupon') }}" class="btn btn-outline-secondary"><i class="fas fa-trash mr-2"></i>"{{$default_data['coupon']}}" kuponkód eltávolítása</a></p>
				</div>
			</div>
		@else
		<form id="discount-coupon-form" class="ajax-form" action="{{ action('CartController@validateCoupon') }}" method="post">
			<div class="row">
				<div class="col-md-6">
					<div class="md-form md-outline">
						<input type="text" class="form-control" id="coupon_code" name="coupon_code" value="{{(isset($default_data['coupon']) ? $default_data['coupon'] : '')}}">
						<label for="coupon_code">Kuponkód</label>
					</div>
				</div>
				<div class="col-md-6">
					<div class="md-form">
						<button type="button" class="btn btn-primary btn-sm" onclick="$('#discount-coupon-form').submit()">Kuponkód érvényesítése</button>
					</div>
				</div>
			</div>
			<input name="remove" id="remove-coupon" value="0" type="hidden">
			{{ csrf_field() }}
		</form>
		@endif
		</div>
	</div>
</div>
<?php
}// új design vége
else {
?>

<div class="clearfix"></div>
	<main class="page-main" id="maincontent">
<?php
	
	//ingyenes házhozszállítás 1.0
	/*
	$manufacturemore = 0;
	$outletmore = 0;
	$gyartassum = CartHelper::getCartManufactureItemsSum();
	$outletsum = CartHelper::getCartOutletItemsSum();
	$egyebsum = CartHelper::getCartOtherItemsSum();
	$gyartasrelatedsum = CartHelper::getCartManufactureRelatedItemsSum();
	$outletrelatedsum = CartHelper::getCartOutletRelatedItemsSum();
	$egyebsum = $egyebsum - $gyartasrelatedsum - $outletrelatedsum;
		
	if ($outletsum>0) {
		if ($outletsum+$egyebsum+$outletrelatedsum >= 200000) {
			///
		} else {
			$outletmore = 200000 - $outletsum - $egyebsum - $outletrelatedsum;
		}
	}
	if ($gyartassum>0) {
		if ($gyartassum+$gyartasrelatedsum >= 200000) {
			//
		} else {
			$manufacturemore = 200000 - $gyartassum - $gyartasrelatedsum;
		}
	}
	*/
	
	//Ingyenes házhozszállítás 2.0
	//fő temékek száma $items_main
	//mellék termékek száma $items_additional
	//outlet termék count $items_outlet
	//rendelés összértéke $items_value
	//
	
	$items_main = CartHelper::getCartMainItemsCount();
	$items_additional = CartHelper::getCartAdditionalItemsCount();
	$items_outlet = CartHelper::getCartOutletItemsCount();
	$items_value = CartHelper::getCartItemsSum(); //???
	$freeshipping_main = false;
	//$freeshipping_outlet megszünik
	//$freeshipping_outlet = false;
	if ($items_main>1) $freeshipping_main = true;
	//if (($items_main==1) and ($items_additional+$items_outlet>0)) $freeshipping_main = true;
	if (($items_main==1) and ($items_additional>0)) $freeshipping_main = true;
	//if ($items_outlet>1) $freeshipping_outlet = true;
	//if (($items_outlet==1) and ($items_additional>0)) $freeshipping = true;
	/*
	if ($items_value>=500000) {
		$freeshipping_main = true;
		$freeshipping_outlet = true;
	}
	*/
	//200e limitnél van csak ingyenes szállítás
	if ($items_value<200000) {
		$freeshipping_main = false;
	}
	//echo $freeshipping_main."<br>";
	//echo $freeshipping_outlet."<br>";
	//if (isset($cart_data[ 'delivery_mode' ]) and ($cart_data[ 'delivery_mode' ] == 'COURIER') and (($freeshipping_main) or ($freeshipping_outlet)) and isset($cart_data[ 'COURIER_FREE' ]) and ($cart_data[ 'COURIER_FREE' ] ) ) {
	if (isset($cart_data[ 'delivery_mode' ]) and ($cart_data[ 'delivery_mode' ] == 'COURIER') and ($freeshipping_main) and isset($cart_data[ 'COURIER_FREE' ]) and ($cart_data[ 'COURIER_FREE' ] ) ) {
		//$shippingcost_timeout = $shippingOutlet + $shippingManufacture;
		//$teljesosszeg = $teljesosszeg - $shippingOutlet - $shippingManufacture;
		if ($freeshipping_main) {
			$shippingcost_timeout = $shippingcost_timeout + $shippingManufacture;
			$teljesosszeg = $teljesosszeg - $shippingManufacture;
			$shippingcost = $shippingcost - $shippingManufacture;
		}
		/*
		if ($freeshipping_outlet) {
			$shippingcost_timeout = $shippingcost_timeout + $shippingOutlet;
			$teljesosszeg = $teljesosszeg - $shippingOutlet;
			$shippingcost = $shippingcost - $shippingOutlet;
		}
		*/
		if ($shippingcost>0) {
			$mpl_shipping = $shippingcost - $shippingOutlet;
			$shippingcost = $shippingcost - $mpl_shipping;
		}
	}
?>
<?php
//if ($outletmore || $manufacturemore || $freeshipping==false) {
//if ((($freeshipping_main==false) and ($items_main+$items_additional>0)) or (($freeshipping_outlet==false) and ($items_outlet>0))) {
if (($freeshipping_main==false) and ($items_main+$items_additional>0)) {
?>
<style>
	.modalcontainer {
		display: block;
		position: fixed;
		width: 100vw;
		height: 100vh;
		overflow: hidden;
		top: 0;
		left: 0;
		background: rgba(0,0,0,0.80);
		z-index: 999;
	}
	.btn-close {
		background: transparent !important;
		color: white !important;
		border: none !important;
		float: right;
		margin-right: 2rem;
	}
	.modalwindow {
		background-color: white;
		max-height: calc(100vh - 10rem);
		max-width:  50vw;
		margin: 5rem auto;
		border-top: solid #CD7126 5px;
		overflow-x: hidden;
		overflow-y:auto;
	}
	@media (max-width: 768px) {
		.modalwindow {
			max-width: 90vw;
		}
	}
	.modalwindow header {
		margin: .5rem 2.5rem;
	}
	.modalwindow section {
		margin: 0 2.5rem;
	}
	.modalwindow h5 {
		color: #CD7126;
		font-weight: bold;
	}
	.img-icon {
		height: 80px;
		width: auto;
	}
</style>
<div class="modalcontainer">
	<button type="button" class="btn-close" onClick="$('.modalcontainer').addClass('hidden');"><span>&times;</span></button>
	<div class="modalwindow">
		<button type="button" class="btn close" onClick="$('.modalcontainer').addClass('hidden');" style="opacity: 1 !important;"><span style="padding-left: 1rem; margin-right: 1rem;">&times;</span></button>
		<header>
			<h3>Információ az ingyenes szállításról</h3>
			<!--<img src="{{ asset('static/ikon/ikon/Armchair_Symbol_v5_001.png') }}" title="full">-->
		</header>
		<section>
			@foreach (Cart::getContent() as $item)
			<?php
			$product = Product::lang()->active()->find($item['attributes']->product_id);
			$kategorialista = array(4, 12, 24, 9, 110, 122, 126, 106);
			$rendeltbutor = "";
			if (!isset($product)) {
				continue;
			}
			else{
				if( $product->getCategory()->category_id===4 || $product->getCategory()->category_id===12 || $product->getCategory()->category_id===24 ){
					$rendeltbutor = "kanapé";
				}
				elseif ($product->getCategory()->category_id===9) {
					$rendeltbutor = "franciaágy";
				}
				elseif ($product->getCategory()->category_id===110) {
					$rendeltbutor = "fotel";
				}
				elseif ($product->getCategory()->category_id===122) {
					$rendeltbutor = "puff";
				}
				elseif ($product->getCategory()->category_id===106) {
					$rendeltbutor = "előszoba bútor";
				}
				elseif ($product->getCategory()->category_id===126) {
					$rendeltbutor = "dohányzóasztal";
				}
				else {
					$rendeltbutor = "termék";
				}
			}
			?>
			@endforeach
			<?php if (($freeshipping_main==false) and ($items_main+$items_additional>0)) { ?>
			<p>Kedves Ügyfelünk!</p>
			<p>Válasszon megrendelt {{$rendeltbutor}} mellé az alább felsorolt terméktípusok közül legalább egyet, hogy jogosult legyen az országosan ingyenes házhozszállításunkat igénybe venni.</p>
			<ul class="list-inline text-center">
				<?php if ($product->getCategory()->category_id!=122) { ?>
				<li>
					<a href="/termekek/kiegeszito/puff">
						<img src="{{ asset('media/icon/b/puff.png') }}" class="img-icon"><br>
						Puff
					</a>
				</li>
				<?php } ?>
				<?php if ($product->getCategory()->category_id!=126) { ?>
				<li>
					<a href="/termekek/kiegeszito/dohanyzoasztal">
						<img src="{{ asset('media/icon/b/asztal.png') }}" class="img-icon"><br>
						Dohányzóasztal
					</a>
				</li>
				<?php } ?>
				<?php if ($product->getCategory()->category_id!=106) { ?>
				<li>
					<a href="/termekek/kiegeszito/eloszobabutor">
						<img src="{{ asset('media/icon/b/eloszoba.png') }}" class="img-icon"><br>
						Előszobabútor
					</a>
				</li>
				<?php } ?>
				<?php if ($product->getCategory()->category_id!=110) { ?>
				<li>
					<a href="/termekek/kiegeszito/fotel">
						<img src="{{ asset('media/icon/b/fotel.png') }}" class="img-icon"><br>
						Fotel
					</a>
				</li>
				<?php } ?>
				<li>
					<a href="/termekek/kanape">
						<img src="{{ asset('media/icon/b/kanape-l.png') }}" class="img-icon"><br>
						Kanapé
					</a>
				</li>
				<li>
					<a href="/termekek/kanape/franciaagyak">
						<img src="{{ asset('media/icon/b/agy.png') }}" class="img-icon"><br>
						Franciaágy
					</a>
				</li>
			</ul>
			<?php }
			/*
			if (($freeshipping_outlet==false) and ($items_outlet>0)) {
			?>
			<p>Kedves Ügyfelünk!</p>
			<p>Válasszon megrendelt outlet {{$rendeltbutor}} mellé az alább felsorolt outlet terméktípusok közül legalább egyet, hogy jogosult legyen az országosan ingyenes házhozszállításunkat igénybe venni.</p>
			<ul class="list-inline text-center">
				<?php if ($product->getCategory()->category_id!=122) { ?>
				<li>
					<a href="/termekek/outlet">
						<img src="{{ asset('media/icon/b/puff.png') }}" class="img-icon"><br>
						Puff
					</a>
				</li>
				<?php } ?>
				<?php if ($product->getCategory()->category_id!=126) { ?>
				<li>
					<a href="/termekek/outlet">
						<img src="{{ asset('media/icon/b/asztal.png') }}" class="img-icon"><br>
						Dohányzóasztal
					</a>
				</li>
				<?php } ?>
				<?php if ($product->getCategory()->category_id!=106) { ?>
				<li>
					<a href="/termekek/outlet">
						<img src="{{ asset('media/icon/b/eloszoba.png') }}" class="img-icon"><br>
						Előszobabútor
					</a>
				</li>
				<?php } ?>
				<?php if ($product->getCategory()->category_id!=110) { ?>
				<li>
					<a href="/termekek/outlet">
						<img src="{{ asset('media/icon/b/fotel.png') }}" class="img-icon"><br>
						Fotel
					</a>
				</li>
				<?php } ?>
				<li>
					<a href="/termekek/outlet">
						<img src="{{ asset('media/icon/b/kanape-l.png') }}" class="img-icon"><br>
						Kanapé
					</a>
				</li>
				<li>
					<a href="/termekek/outlet">
						<img src="{{ asset('media/icon/b/agy.png') }}" class="img-icon"><br>
						Franciaágy
					</a>
				</li>
			</ul>
			<?php 
			}
			*/
			?>
			<div class="row" style="margin-top:1rem;">
				<div class="col-12 text-center">
					<p><a href="#" class="btn btn-primary" onClick="$('.modalcontainer').addClass('hidden');">Nem élek az ingyenes házhozszállítás lehetőségével</a></p>
				</div>
				<div class="col-12">
					<p class="text-center"><small>A részletes feltételekről tájékozódjon az <a href="/aszf">ÁSZF</a>-ben!</small></p>
				</div>
			</div>
		</section>
	</div>
</div>
<?php
}// if van popup
?>

<div class="page-title-wrapper">
    <h1 class="page-title">
        <span class="base" data-ui-id="page-title-wrapper">{{ t('Kosár tartalma') }}</span>    </h1>
</div>

<div class="columns">
<div class="column main">


    @if($content->count())
        <div id="checkout" class="checkout-container">
        @include('webshop.cart.blocks.top',['step' => 1])
        <div class="opc-wrapper width-100percent">
            <div class="cart-container">

                @include('webshop.cart.blocks.summary_right')

                <div class="form form-cart">
                <div class="cart table-wrapper">
                <table id="shopping-cart-table" class="cart items data table">
                    <caption role="heading" aria-level="2" class="table-caption">{{ t('Termékek') }}</caption>
                    <thead>
                        <tr>
                            <th class="col item" scope="col"><span>{{ t('Termék') }}</span></th>
                            <th class="col price" scope="col"><span>{{ t('Ár') }}</span></th>
                            <th class="col qty" scope="col"><span>{{ t('Darab') }}</span></th>
                            <th class="col subtotal" scope="col"><span>{{ t('Összeg') }}</span></th>
                        </tr>
                    </thead>
                    <tbody class="cart item">
                @foreach (Cart::getContent() as $item)
                    <?php
                        if ($item->id== 'floor') {
                            continue;
                        }
                        $product = Product::lang()->active()->find($item['attributes']->product_id);
						if (!isset($product)) {
							continue;
						}
                        $price_full = 30000;
                        if ($product->type == 'scalable' or $product->type == 'non-scalable') {
                            $feature = $product->getDefaultSize();
                            $size_count = sizeof($item['attributes']['sizes']);
                        }

                        $price_array = CartHelper::calculateItemPrice($item['attributes'],true);

                    ?>
                    @if ($product->type == 'general')
                        @include('webshop.cart.blocks.general_cart_item')

                    @else
                        @include('webshop.cart.blocks.scalable_cart_item')
                    @endif
                    <tr class="item-actions cart-item-last">
                    <td colspan="100">

                        <div class="actions-toolbar">
                        @if (
                            (isset($item['attributes']['parent_cartitem_id']) and $item['attributes']['parent_cartitem_id'] != '0' and $item['attributes']['parent_cartitem_id'] != '')
                            or
                            (isset($item['attributes']['related']) and $item['attributes']['related'] != '0' and $item['attributes']['related'] != '')

                        )
                            <small class="pull-right padding-right-50">{{ t('Amennyiben törli a terméket, a kedvezménye is elveszik') }}</small>
                        @endif
                                <a onclick="gtmRemoveFromCart('{{ $product->product_id }}','{{ $product->getName() }}','{{ $item->price }}','{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}','<?php
								if ($product->type == 'general') {
									$s = '';
								} else {
									$s = '';
									foreach ($item['attributes']['sizes'] as $size_item => $size_name) {
										$s.=$size_name.' ';
									}
									if (isset($item['attributes']['mounting'])) $s.=$item['attributes']['mounting'];
									if (isset($item['attributes']['seat'])) $s.=$item['attributes']['seat'];
								}
								?>{{ $s }}','{{ $item->quantity }}')" href="{{ action('CartController@removeCart', $item->id) }}" title="{{t('Eltávolítás')}}" class="action action-delete"><span>{{ t('Törlés') }}</span></a>
                        </div>
                    </td>
                    </tr>

                @endforeach
                    </tbody>
                </table>
                </div>
                </div>

            </div>
			
			<p>{{ t('A képek illusztrációk. A bútor arányai és a párnák darabszáma a bútor méretétől függően változhatnak.') }}<br><br></p>
			
            <div class="cart-discount"><div class="block discount" id="block-discount" data-collapsible="true" role="tablist">
                @if (isset($default_data['coupon']) and $default_data['coupon'] != '')
                <div class="title" data-role="title" role="tab" aria-selected="false" onclick="$('.coupon_ctrl').show();" aria-expanded="false" tabindex="0">
                    <strong id="block-discount-heading" role="heading" aria-level="2">{{ t('Aktivált kupon kód') }}</strong>
                </div>
                @else
                 <div class="title" data-role="title" role="tab" aria-selected="false" onclick="$('.coupon_ctrl').show();" aria-expanded="false" tabindex="0">
                    <strong id="block-discount-heading" role="heading" aria-level="2">{{ t('Rendelkezik kuponkóddal?') }}</strong>
                </div>
                @endif

                <div class="content coupon_ctrl" style="display: none;">
                    @if (isset($default_data['coupon']) and $default_data['coupon'] != '')
                        <p> {{$default_data['coupon']}} <a href="{{ action('CartController@removeCoupon') }}"><i class="fa fa-remove"></i></a></p>
                    @else
                    <form id="discount-coupon-form" class="ajax-form" ajax-action="{{ action('CartController@validateCoupon') }}" method="post">
                        <div class="fieldset coupon">
                            <input name="remove" id="remove-coupon" value="0" type="hidden">
                            <div class="field">
                                <label for="coupon_code"  class="label"><span>{{ t('Rendelkezik kuponkóddal?') }}</span></label>
                                <div class="control">
                                    <input class="input-text" id="coupon_code" name="coupon_code" value="{{(isset($default_data['coupon']) ? $default_data['coupon'] : '')}}" placeholder="{{ t('Kuponkód') }}" type="text">
                                </div>
                            </div>
                            <div class="actions-toolbar">
                                                    <div class="primary">
                                    <button class="action apply primary" onclick="$('#discount-coupon-form').submit()" type="button" >
                                        <span>{{ t('Aktiválom') }}</span>
                                    </button>
                                </div>
                           </div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                    @endif
                </div>
            </div>
            </div>


        </div>

        </div>
    @else
    <div class="cart-empty">
        <p>{{ t('Az Ön kosara üres!') }}</p>
        <p>{{ t('A vásárlás folytatásához rakjon a kosarába terméket') }}</p>
    </div>
    @endif
</div></div>
</main>

@stop
@section('footer_js')
    <script type="text/javascript" src="{{asset('/js/plusminus.js')}}"></script>
<?php }//else ?>

@append
