@extends('layouts.default')

@section('content')
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Hiba történt') }}</strong>
                        </li>
            </ul>
</div>

<main class="page-main" id="maincontent">

<div class="entry-content min-height">


    <div class="row">
        <div class="row-wrapper container">
            <div class="row">
                <div class="col-md-12">
                    @if (empty($order))
                        <p>{{ t('Hiba történt a rendelés közben!') }}</p>
                    @else
                    <h3>{{ t('Kedves')." ".$order->firstname }}!</h3>
        			<p>{{ t('Hiba történt a rendelés közben!') }}</p>
                    <p>{{ t('Rendelés azonosítója') }}: {{ $order->order_number }}</p>
                    @if (sizeof($payment))
                        <div class="alert alert-danger" role="alert">
                        <p><b>{{ t('Tranzakciója sikertelen volt!') }}</b></p>

                        <p>{{ t('Tranzakció azonosítója') }}: {{ $payment['trans_id'] }}</p>

                        <p>{{ t('Rendelés státusza').": <b>" .$order->getStatus() }}</b></p>

                        <p><b>{{ t('Tranzakció részleti') }}</b>: {{ UniCredit::getResultString($payment['result']) }}</p>

                        <p><b>{{ t('Összeg') }}</b>: {{ money($payment['price']) }}</p>
                        </div>
                    @endif
                    @if ($order->status == 'NEW')
                        @if ($order->payment == 'FULL_CARD')
                        <button class="action submit primary margin-top2" onclick="window.location.href='{{action('UnicreditController@fullStart', $order->order_number)}}'" title="Elküldés" type="submit">
                    <span>{{ t('Fizetés újrapróbálása') }}</span>
                </button>

                        @endif

                    @endif
                    @endif


                </div>
            </div>
        </div>
    </div>



</div>
</div></div>
</main>

@stop
@section('footer_js')


@append

