<?php
	$termekek = Cart::getSubTotal() ? Cart::getSubTotal()-CartHelper::getFloorCost($default_data) : 0 ;

	$manufacturemore = 0;
	$outletmore = 0;
	$gyartassum = CartHelper::getCartManufactureItemsSum();
	$outletsum = CartHelper::getCartOutletItemsSum();
	$egyebsum = CartHelper::getCartOtherItemsSum();
	$gyartasrelatedsum = CartHelper::getCartManufactureRelatedItemsSum();
	$outletrelatedsum = CartHelper::getCartOutletRelatedItemsSum();
	$egyebsum = $egyebsum - $gyartasrelatedsum - $outletrelatedsum;
		
	if ($outletsum>0) {
		if ($outletsum+$egyebsum+$outletrelatedsum >= 200000) {
			//
		} else {
			$outletmore = 200000 - $outletsum - $egyebsum - $outletrelatedsum;
		}
	}
	if ($gyartassum>0) {
		if ($gyartassum+$gyartasrelatedsum >= 200000) {
			//
		} else {
			$manufacturemore = 200000 - $gyartassum - $gyartasrelatedsum;
		}
	}

?>
<h2 class="h3-responsive text-uppercase title my-4">{{ t('Összesítés') }}</h2>
<table class="table table-striped table-bordered">
	<tbody>
		<tr>
			<th class="text-weight-bold">
				{{ t('Részösszeg') }}
			</th>
			<td class="text-right">
				{{ money($termekek) }}
			</td>
		</tr>
	@if (($gyartassum) and (($outletsum) or ($egyebsum) or ($gyartasrelatedsum)))
		<tr>
			<th class="text-weight-bold">
				{{ t('Gyártandó termékek') }}
			</th>
			<td class="text-right">
				{{ money($gyartassum) }}
			</td>
		</tr>
	@endif
	@if (($gyartasrelatedsum) and (($outletsum) or ($egyebsum) or ($gyartassum)))
		<tr>
			<th class="text-weight-bold">
				{{ t('Kiegészítő termékek') }}
			</th>
			<td class="text-right">
				{{ money($gyartasrelatedsum) }}
			</td>
		</tr>
	@endif
	@if ($manufacturemore)
	@endif
	@if (($outletsum) and (($gyartassum) or ($egyebsum)))
	<tr>
		<th class="text-weight-bold">
			{{ t('Outlet termékek') }}
		</th>
		<td class="text-right">
			{{ money($outletsum) }}
		</td>
	</tr>
	@endif
	@if (($outletrelatedsum) and (($outletsum) or ($egyebsum) or ($gyartassum)))
		<tr>
			<th class="text-weight-bold">
				{{ t('Outlet kiegészítő termékek') }}
			</th>
			<td class="text-right">
				{{ money($outletrelatedsum) }}
			</td>
		</tr>
	@endif
	@if ($outletmore)
	@endif
	@if (($egyebsum) and (($outletsum) or ($gyartassum)))
		<tr>
			<th class="text-weight-bold">
				{{ t('Egyéb termékek') }}
			</th>
			<td class="text-right">
				{{ money($egyebsum) }}
			</td>
		</tr>
	@endif
	<?php
		$discount = Cart::getDiscount();
	?>
	@if($discount)
	<tr>
		<th class="text-weight-bold">
			{{ t('Kedvezmény') }}
		</th>
		<td  class="text-right">
			-{{ money($discount) }}
		</td>
	</tr>
	@endif

	<?php /*
	@if(Cart::getShipping())
	<tr class="totals-tax">
		<th  class="text-weight-bold" colspan="1" scope="row">{{ t('Szállítási költség') }}</th>
		<td  class="text-right">
			{{ money(Cart::getShipping()) }}
		</td>
	</tr>
	@endif
	@if(Cart::get('floor'))
	<tr class="totals-tax">
		<th  class="text-weight-bold" colspan="1" scope="row">{{ t('Emelet költség') }}</th>
		<td  class="text-right">
			{{ money(Cart::get('floor')->getPriceSum()) }}
		</td>
	</tr>

	@endif


	@if (CartHelper::getAdvPrice())
	<tr class="grand totals">
		<th class="text-weight-bold" scope="row">
			<strong >{{ t('Előleg') }}</strong>
		</th>
		<td class="text-right" >
			<strong>{{ money(CartHelper::getAdvPrice()) }}</strong>
		</td>
	</tr>
	@endif

	<tr class="grand totals">
		<th class="text-weight-bold" scope="row">
			<strong >{{ t('Fizetendő') }}</strong>
		</th>
		<td class="text-right" >
			<strong>{{ money(Cart::getCartTotal()) }}</strong>
		</td>
	</tr>
	*/ ?>

	</tbody>
</table>
<!-- /ko -->
<p class="text-center"><a href="{{ action('CartController@personal') }}" class="btn btn-primary btn-lg">{{ t('Tovább a pénztárhoz') }}</a></p>

