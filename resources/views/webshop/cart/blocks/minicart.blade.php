
                <a  href="{{ action('CartController@index') }}" class="action showcart">
                    <span class="text">{{ t('Kosaram') }}</span>
                    <span class="counter qty empty">

                        @if(Cart::countItems())
                            <span class="counter-number">
                                {{Cart::countItems()}}
                            </span>
                            <span class="counter-label">
                               <span>{{ t('termék') }}</span>
                            </span>
                        @else
                            <span class="counter-number">
                                {{ t('Az Ön kosara üres!') }}
                            </span>
                        @endif

                    </span>
                </a>



                <div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front mage-dropdown-dialog block-minicart-container" style="display: none;" tabindex="-1" role="dialog" aria-describedby="ui-id-3">
                    <div data-role="dropdownDialog" class="block block-minicart empty ui-dialog-content ui-widget-content" id="ui-id-3" style="display: none;">
                        <div id="minicart-content-wrapper">

                            @if (Cart::countItems())
                            <div class="block-content">
                                <span class="fa fa-times pull-right" id="btn-minicart-close"></span>
                                <div class="items-total">
                                    <span class="count">{{ Cart::countItems() }}</span>
                                    <span>{{ t('Termék') }}</span>
                                </div>


                                <div class="subtotal">
                                    <span class="label">
                                        <span>{{ t('Fizetendő') }}</span>
                                    </span>
                                    <div class="amount price-container">
										<span class="price-wrapper" ><span class="price"> {{money(Cart::getCartTotal())}} </span></span>
                                    </div>
                                </div>
                                <div class="actions">
                                    <div class="primary">
                                        <button id="top-cart-btn-checkout" type="button" onclick="document.location.href='{{action('CartController@index')}}'" class="action primary checkout">
                                            <span>{{ t('Kosár megtekintése') }}</span>
                                        </button>
                                    </div>
                                </div>

                                <div data-action="scroll" class="minicart-items-wrapper">
                                    <ol id="mini-cart" class="minicart-items" style="height: 157px;">

                                    @foreach($content as $item)
                                        <?php
                                            if ($item->id == 'floor'){
                                                continue;
                                            }
                                            $product = Product::lang()->active()->find($item['attributes']->product_id);
											if (!isset($product)) {
												continue;
											}
											if ($product->category_id == 119) {
												continue;
											}
                                        ?>
                                        <li class="item product product-item odd last" data-role="product-item" data-collapsible="true">
                                            <div class="product">
                                                  <a  class="product-item-photo" href="{{ $product->getUrl() }}" title="{{ $product->getName() }}">
                                                        <img src="{{ $product->getDefaultImageUrl() }}" style="width: 75px">
                                                  </a>
                                                  <div class="product-item-details">
                                                        <strong class="product-item-name">
                                                        <a  href="{{ $product->getUrl() }}" title="{{ $product->getName() }}">
                                                               {{ $product->getName() }}
                                                        </a>
                                                        </strong>
                                                        <div class="product-item-pricing">
                                                            <div class="price-container">
                                                            <span class="price-wrapper">
                                                                <span class="price-excluding-tax">
                                                                        <span class="minicart-price">
                                                                        <span class="qty-span">{{$item->quantity." ".t('db')}}</span>
                                                                        <span class="price">{{money($item->price)}}</span>        </span>

                                                                    </span>
                                                            </span>
                                                            </div>
                                                        </div>

                                                  </div>
                                            </div>

                                        </li>

                                    @endforeach
                                    </ol>
                                </div>


                            @else
                            <div class="block-content">
                                <button  data-action="close" class="action close" id="btn-minicart-close" type="button" >
                                    <span>{{ t('Bezárás') }}</span>
                                </button>
                                    <strong class="subtitle empty">
                                        <span>{{ t('Az Ön kosara üres') }}</span>
                                    </strong>
                                <div class="minicart-widgets" id="minicart-widgets"> </div>
                            </div>
                            @endif

                        </div>
                    </div>
                </div>

