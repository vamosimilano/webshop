<?php
if ( Session::has( 'cart_data' ) ) {
	$cart_data   = Session::get( 'cart_data' );
}

$teljesosszeg = Cart::getCartTotal();
$shippingcost = Cart::getShipping();
$shippingManufacture = Cart::getShippingManufacture();
$shippingOutlet = Cart::getShippingOutlet();
$mpl_shipping = 0;
$shippingcost_timeout = 0;
//$shippingManufactureItems = Cart::countManufactureShippingItems();
//$shippingOutletItems = Cart::countOutletShippingItems();

if (getShopCode()=='hu') {
	/*
	$termekek = Cart::getSubTotal() ? Cart::getSubTotal()-CartHelper::getFloorCost($default_data) : 0 ;
	$shippingcost_shipping = Cart::getShipping(false); //ez ingyenes ha elfogadja
	$shippingcost_extra = $shippingcost-$shippingcost_shipping;
	$termekek = $termekek + $shippingcost_extra; //ez beleszámít a 200e ft-ba
	$shippingcost_timeout = 0;
	$termekekmore = 0;

	if (isset($cart_data[ 'delivery_mode' ]) and ($cart_data[ 'delivery_mode' ] == 'COURIER') and ($termekek >= 200000) and isset($cart_data[ 'COURIER_FREE' ]) and ($cart_data[ 'COURIER_FREE' ] ) ) {
		$teljesosszeg = $teljesosszeg-$shippingcost_shipping;
		$shippingcost = $shippingcost_extra;
		$shippingcost_timeout = $shippingcost_shipping;
	} else {
		if (isset($cart_data[ 'delivery_mode' ]) and ($cart_data[ 'delivery_mode' ] == 'COURIER')) {
			$termekekmore = 200000-$termekek;
			if ($termekekmore<0) $termekekmore=0;
		}
	}
	*/
	
	
	$termekek = Cart::getSubTotal() ? Cart::getSubTotal()-CartHelper::getFloorCost($default_data) : 0 ;
	
	//ingyenes házhozszállítás 1.0
	/*
	$manufacturemore = 0;
	$outletmore = 0;
	$shippingcost_timeout = 0;
	$gyartassum = CartHelper::getCartManufactureItemsSum();
	$outletsum = CartHelper::getCartOutletItemsSum();
	$egyebsum = CartHelper::getCartOtherItemsSum();
	$gyartasrelatedsum = CartHelper::getCartManufactureRelatedItemsSum();
	$outletrelatedsum = CartHelper::getCartOutletRelatedItemsSum();
	$egyebsum = $egyebsum - $gyartasrelatedsum - $outletrelatedsum;
	
	$manufacture_shipping = 0;
	$outlet_shipping = 0;
	$mpl_shipping = 0;
		
	if (isset($cart_data[ 'delivery_mode' ]) and ($cart_data[ 'delivery_mode' ] == 'COURIER') and ($termekek >= 200000) and isset($cart_data[ 'COURIER_FREE' ]) and ($cart_data[ 'COURIER_FREE' ] ) ) {
		if ($outletsum>0) {
			if ($outletsum+$egyebsum+$outletrelatedsum >= 200000) {
				$shippingcost = $shippingcost - $shippingOutlet;
				$shippingcost_timeout = $shippingcost_timeout + $shippingOutlet;
				$teljesosszeg = $teljesosszeg - $shippingOutlet;
			} else {
				$outletmore = 200000 - $outletsum - $egyebsum - $outletrelatedsum;
			}
		}
		if ($gyartassum>0) {
			if ($gyartassum+$gyartasrelatedsum >= 200000) {
				$shippingcost = $shippingcost - $shippingManufacture;
				$shippingcost_timeout = $shippingcost_timeout + $shippingManufacture;
				$teljesosszeg = $teljesosszeg - $shippingManufacture;
				if ($shippingcost>0) {
					$mpl_shipping = $shippingcost - $shippingOutlet;
					$shippingcost = $shippingcost - $mpl_shipping;
				}
			} else {
				$manufacturemore = 200000 - $gyartassum - $gyartasrelatedsum;
			}
		}
	}
	*/
	//Ingyenes házhozszállítás 2.0
	//fő temékek száma $items_main
	//mellék termékek száma $items_additional
	//outlet termék count $items_outlet
	//rendelés összértéke $items_value
	//
	
	$items_main = CartHelper::getCartMainItemsCount();
	$items_additional = CartHelper::getCartAdditionalItemsCount();
	$items_outlet = CartHelper::getCartOutletItemsCount();
	$items_value = CartHelper::getCartItemsSum(); //???
	$freeshipping_main = false;
	//$freeshipping_outlet megszünik
	//$freeshipping_outlet = false;
	if ($items_main>1) $freeshipping_main = true;
	//if (($items_main==1) and ($items_additional+$items_outlet>0)) $freeshipping_main = true;
	if (($items_main==1) and ($items_additional>0)) $freeshipping_main = true;
	//if ($items_outlet>1) $freeshipping_outlet = true;
	//if (($items_outlet==1) and ($items_additional>0)) $freeshipping = true;
	/*
	if ($items_value>=500000) {
		$freeshipping_main = true;
		$freeshipping_outlet = true;
	}
	*/
	//200e limitnél van csak ingyenes szállítás
	if ($items_value<200000) {
		$freeshipping_main = false;
	}
	//if (isset($cart_data[ 'delivery_mode' ]) and ($cart_data[ 'delivery_mode' ] == 'COURIER') and (($freeshipping_main) or ($freeshipping_outlet)) and isset($cart_data[ 'COURIER_FREE' ]) and ($cart_data[ 'COURIER_FREE' ] ) ) {
	if (isset($cart_data[ 'delivery_mode' ]) and ($cart_data[ 'delivery_mode' ] == 'COURIER') and ($freeshipping_main) and isset($cart_data[ 'COURIER_FREE' ]) and ($cart_data[ 'COURIER_FREE' ] ) ) {
		//$shippingcost_timeout = $shippingOutlet + $shippingManufacture;
		//$teljesosszeg = $teljesosszeg - $shippingOutlet - $shippingManufacture;
		if ($freeshipping_main) {
			$shippingcost_timeout = $shippingcost_timeout + $shippingManufacture;
			$teljesosszeg = $teljesosszeg - $shippingManufacture;
			$shippingcost = $shippingcost - $shippingManufacture;
		}
		/*
		if ($freeshipping_outlet) {
			$shippingcost_timeout = $shippingcost_timeout + $shippingOutlet;
			$teljesosszeg = $teljesosszeg - $shippingOutlet;
			$shippingcost = $shippingcost - $shippingOutlet;
		}
		*/
		if ($shippingcost>0) {
			$mpl_shipping = $shippingcost - $shippingOutlet;
			$shippingcost = $shippingcost - $mpl_shipping;
		}
	}
	
}

?>
<p class="lead text title text-uppercase">{{ t("Összesítés") }} <span class="text-weight-normal">({{ Cart::countItems() }} termék)</span></p>
<table class="table table-striped table-bordered">
	<tbody>
		<tr>
			<th>
				{{ t('Részösszeg') }}
			</th>
			<td class="text-right">
				{{ Cart::getSubTotal() ? money(Cart::getSubTotal()-CartHelper::getFloorCost($default_data)) : money(0) }}
			</td>
		</tr>
		<?php
		if($shippingcost) {
		?>
		<tr>
			<th>
				{{ t('Szállítási költség') }}
			</th>
			<td class="text-right">
			@if ($shippingcost>0)
				{{ money($shippingcost) }}
			@else
				{{ $shippingcost }}
			@endif
			</td>
		</tr>
		<?php
		}//if
		if($mpl_shipping) {
		?>
		<tr>
			<th>
				{{ t('Futárszolgálat') }}
			</th>
			<td class="text-right">
			@if ($mpl_shipping>0)
				{{ money($mpl_shipping) }}
			@else
				{{ $mpl_shipping }}
			@endif
			</td>
		</tr>
		<?php
		}//if
		if(isset($default_data['floor']) and Cart::get('floor')) {
		?>
		<tr>
			<th>
				{{ t('Emelet költség') }}
			</th>
			<td class="text-right">
				{{ money(Cart::get('floor')->getPriceSum()) }}
			</td>
		</tr>
		<?php
		}//if
		$discount = Cart::getDiscount();
		if($discount) {
		?>
		<tr>
			<th>
				{{ t('Kedvezmény') }}
			</th>
			<td class="text-right">
				-{{ money($discount) }}
			</td>
		</tr>
		<?php
		}//if
		if(CartHelper::getAdvPrice()) {
		?>
		<tr>
			<th>
				{{ t('Előleg') }}
			</th>
			<td class="text-right">
				{{ money(CartHelper::getAdvPrice()) }}
			</td>
		</tr>
		<?php
		}//if
		?>
		<tr>
			<th>
				<strong>{{ t('Fizetendő') }}</strong>
			</th>
			<th class="text-right">
				<strong>{{ money($teljesosszeg) }}</strong>
			</th>
		</tr>
	</tbody>
</table>
@if (getShopCode()=='hu')
<div class="form-check mb-3">
	<input type="checkbox" id="courier_free" name="COURIER_FREE" {{ (isset($default_data['COURIER_FREE']) ? 'checked' : '') }} value="1" class="form-check-input" onclick="loadOverViewBlock();">
	<label for="courier_free">
		{{ t('Vevő megismerte és elfogadja az ingyenes házhozszállítás – <a href="/aszf" target="_blank">ÁSZF-ben is rögzített</a> - feltételeit.') }}
		@if ($shippingcost_timeout)
		{{ t(' Vevő tudomásul veszi, hogy amennyiben bármely feltétel nem teljesül, úgy köteles megfizetnie ').money($shippingcost_timeout).t(' házhozszállítás díjat.') }}
		@endif
	</label>
</div>
@endif
<div class="form-check">
	<input type="checkbox" id="check_aszf" name="check_aszf" value="1" onblur="loadOverViewBlock();" class="form-check-input" >
	<label for="check_aszf">Elolvastam és elfogadom az <a href="/aszf" target="_blank">Általános Szerződési Feltételeket</a> és az <a href="/adatvedelem" target="_blank">Adatvédelmi tájékoztatót.</a></label>
</div>
<div class="md-form">
	<button type="submit" class="btn btn-primary btn-block" id="gotoCartButton">
		{{ t('Rendelés feladása') }}
	</button>
</div>