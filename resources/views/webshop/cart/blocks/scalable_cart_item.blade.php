<?php
if(isset($headversion) && $headversion==="newhead") {
?>
					<tr>
						<td class="text-center">
						<?php
						$title="Termék törlése";
						if ( (isset($item['attributes']['parent_cartitem_id']) and $item['attributes']['parent_cartitem_id'] != '0' and $item['attributes']['parent_cartitem_id'] != '') or (isset($item['attributes']['related']) and $item['attributes']['related'] != '0' and $item['attributes']['related'] != '') ) {
							$title="Amennyiben törli a terméket, a kedvezménye is elveszik";
						}
						if ($product->type == 'general') {
							$s = '';
						} else {
							$s = '';
							foreach ($item['attributes']['sizes'] as $size_item => $size_name) {
								$s.=$size_name.' ';
							}
							if (isset($item['attributes']['mounting'])) $s.=$item['attributes']['mounting'];
							if (isset($item['attributes']['seat'])) $s.=$item['attributes']['seat'];
						}
						?>
							<a data-toggle="tooltip" data-placement="right" onclick="gtmRemoveFromCart('{{ $product->product_id }}','{{ $product->getName() }}','{{ $item->price }}','{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}','{{ $s }}','{{ $item->quantity }}')" href="{{ action('CartController@removeCart', $item->id) }}" title="{{$title}}" class="material-tooltip-main"><i class="fas fa-trash-alt"></i></a>
						</td>
						<td>
							<a href="{{ $product->getUrl() }}">{{ $product->getName() }}</a><br>
							<small><strong><a href="javascript:void(0)" id="modalActivate" data-toggle="modal" data-target="#features{{$item['attributes']->product_id}}" class="text-primary">Részletek</a></strong></small>
							<div class="modal fade top" id="features{{$item['attributes']->product_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
								<div class="modal-dialog modal-frame modal-top" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<p class="modal-title title text-uppercase lead" id="exampleModalPreviewLabel"><strong>A bútor jellemzői</strong></p>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div class="row">
											<?php
											if ($product->type == 'scalable') {
											?>
												<div class="col-md-3">
													<img src="{{ $product->getDefaultImageUrl() }}" alt="{{ $product->getName() }}" class="img-fluid z-depth-1">
												</div>
												<div class="col-md-3">
													<p><strong>Méretek (cm)</strong></p>
													<ul class="list-unstyled">
												@foreach ($item['attributes']['sizes'] as $size_item => $size_name)
													@if ($size_count == 1)
														<li><strong>{{$size_name}} méret</strong> - {{ProductSize::getSizeString($product->product_id,$size_item,$size_name,false)}} cm</li>
													@else
														<li>
														<?php
															$string = ProductSize::getSizeString($product->product_id,$size_item,$size_name, true);
														?>
														@if ($string)
														{{t('oldal_'.$size_item)}}: {{$string}}<br>
														@endif
														</li>
													@endif
												@endforeach
													</ul>
												</div>
												<div class="col-md-3">
													@if (isset($item['attributes']['mounting']))
													<p><strong>Fekvőfelület tájolása</strong></p>
													<ul class="list-unstyled">
														<li>{{ ($item['attributes']['mounting'] == 'left' ? t('Balos elrendezés') : t('Jobbos elrendezés')) }}</li>
													</ul>
													@endif
													@if (isset($item['attributes']['seat']))
													<p><strong>Ülőfelület típusa</strong></p>
													<ul class="list-unstyled">
														<li>{{ ($item['attributes']['seat'] == 'szivacs' ? t('Szivacsos ülőfelület') : t('Rugós ülőfelület')) }}</li>
													</ul>
													@endif
													<?php if (isset($item['attributes']['related']) and $item['attributes']['related'] > 0) {
														$related = ProductRelated::renderToCart($item['attributes']['related']);
													
														if ($related) {
													?>
													
													<p><strong>{{ t('Kapcsolódó termék') }}</strong></p>
													<ul class="list-unstyled">
														<li>{{ $related }}</li>
													<?php
														}//endif
													?>
													</ul>
													<?php
													}//endif
													?>
												</div>
												<div class="col-md-3">
													<?php if (isset($item['attributes']['textile']) and sizeof($item['attributes']['textile'])) { ?>
													<p><strong>Színek és szövetek</strong></p>
													<ul class="list-unstyled">
													<?php 
													foreach ($item['attributes']['textile'] as $key => $textile_id) {
													?>
														<?php
															$textile_data = ProductTextile::lang()->where('product_textiles.textile_id', $textile_id)->first();
															$part = ProductPart::lang()->where('admin_name', $key)->first();
														if (isset($part)) {
														?>
														<li><img src="{{ ProductTextile::getImageUrl($textile_id, 40) }}" title="{{empty($textile_data) ? '' : $textile_data->name}}"> {{$part->name}}{{ ": ".$textile_data->name }}</li>
														<?
														}//if
														else {
														?>
														<li><img src="{{ ProductTextile::getImageUrl($textile_id, 40) }}" title="{{empty($textile_data) ? '' : $textile_data->name}}"> {{$key}}</li>
													<?php
														}//else 
													}//foreach
													?>
													</ul>
													<?php 
													}//endif
													?>
												</div>
											<?php
											}// if scalable
											elseif ($product->type == 'non-scalable') {
											?>
												<div class="col-md-3">
													<img src="{{ $product->getDefaultImageUrl() }}" alt="{{ $product->getName() }}" class="img-fluid z-depth-1">
												</div>
												@if (isset($item['attributes']['sizes']) and sizeof($item['attributes']['sizes']))
												<div class="col-md-3">
													<p><strong>Méretek (cm)</strong></p>
													<ul class="list-unstyled">
												@foreach ($item['attributes']['sizes'] as $size_item => $size_name)
													@if ($size_count == 1)
														<li><strong>{{$size_name}} méret</strong> - {{ProductSize::getSizeString($product->product_id,$size_item,$size_name,false)}} cm</li>
													@else
														<li>
														<?php
															$string = ProductSize::getSizeString($product->product_id,$size_item,$size_name, true);
														?>
														@if ($string)
														{{t('oldal_'.$size_item)}}: {{ProductSize::getSizeString($product->product_id,$size_item,$size_name, true)}}<br>
														@endif
														</li>
													@endif
												@endforeach
													</ul>
												</div>
												@endif
												<div class="col-md-3">
												@if (isset($item['attributes']['related']) and $item['attributes']['related'] > 0)
													<?php
														$related = ProductRelated::renderToCart($item['attributes']['related']);
													?>
													@if ($related)
													<p><strong>{{ t('Kapcsolódó termék') }}</strong></p>
													<ul class="list-unstyled">
														<li>{{ $related }}</li>
													</ul>
													@endif
												@endif
												</div>
												<div class="col-md-3">
													<?php if (isset($item['attributes']['textile']) and sizeof($item['attributes']['textile'])) { ?>
													<p><strong>Színek és szövetek</strong></p>
													<ul class="list-unstyled">
													<?php 
													foreach ($item['attributes']['textile'] as $key => $textile_id) {
														$textile_data = ProductTextile::lang()->where('product_textiles.textile_id', $textile_id)->first();
														$part = ProductPart::lang()->where('admin_name', $key)->first();
													?>
														<li><img src="{{ ProductTextile::getImageUrl($textile_id, 40) }}" title="{{empty($textile_data) ? '' : $textile_data->name}}"> {{$part->name}}{{ ": ".$textile_data->name }}</li>
													<?php
													}//foreach
													?>
													</ul>
													<?php 
													}//endif
													?>
												</div>
											<?php
											}//elseif non-scalable
											?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</td>
						<td class="text-left">
						@if ($price_array['textile_price'])
						{{ t('Bútor') }}: <span class="float-right">{{money($price_array['furniture_price']) }}</span>
						<br>{{ t('Szövet felár') }}: <span class="float-right">{{ money(($item->getPriceSum()-$price_array['furniture_price']*$item->quantity)/$item->quantity) }}</span>
						@else
							Bútor: <span class="float-right">{{ money($item->price) }}</span>
						@endif
						
						</td>
						<td class="text-center">{{ $item->quantity }}</td>
						<td class="text-right font-weight-bold">
							<?php
							$osszprice = $item->getPriceSum();
							if (isset($kedvezmeny)) $osszprice = $osszprice - $kedvezmeny;
							?>
							{{ money($osszprice) }}
						</td>
					</tr>
<?php	
}
else {
?>
<tr class="item-info">
                        <td class="col" colspan="4">
                            <a href="{{ $product->getUrl() }}" title="{{ $product->getName() }}" tabindex="-1" class="product-item-photo">
                            <span class="product-image-container" style="width:165px;">
                                <span class="product-image-wrapper" style="padding-bottom: 100%;">
                                    <img class="product-image-photo" src="{{ $product->getDefaultImageUrl() }}" alt="{{ $product->getName() }}" width="165"></span>
                            </span>
                            </a>
                            <div class="product-item-details">
                                <div class="col-lg-12">
                                    <strong class="product-item-name"><a href="{{ $product->getUrl() }}">{{ $product->getName() }}</a></strong>
                                </div>
                            <div class="col-lg-12">
                                @if ($product->type == 'scalable')

                                            <div class="col-lg-4 nopaddingleft" >
                                                <label class="nomarginbottom">{{ t('Méretek') }}</label>
                                                @foreach ($item['attributes']['sizes'] as $size_item => $size_name)
                                                    @if ($size_count == 1)
                                                    {{$size_name}} {{ProductSize::getSizeString($product->product_id,$size_item,$size_name,false)}}
                                                    @else
                                                        <?php
                                                            $string = ProductSize::getSizeString($product->product_id,$size_item,$size_name, true);
                                                        ?>
                                                        @if ($string)
                                                        {{t('oldal_'.$size_item)}}: {{$string}}<br>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </div>
                                            @if (isset($item['attributes']['mounting']))
                                            <div class="col-lg-3 nopaddingleft">
                                                <label class="nomarginbottom">{{ t('Elrendezés') }}</label>
                                                <span>{{ ($item['attributes']['mounting'] == 'left' ? t('Balos') : t('Jobbos')) }}</span>
                                            </div>
                                            @endif

											@if (isset($item['attributes']['seat']))
                                            <div class="col-lg-3 nopaddingleft">
                                                <label class="nomarginbottom">{{ t('Ülőfelület') }}</label>
                                                <span>{{ ($item['attributes']['seat'] == 'szivacs' ? t('Szivacs') : t('Rugó')) }}</span>
                                            </div>
                                            @endif

                                            @if (isset($item['attributes']['textile']) and sizeof($item['attributes']['textile']))
                                                <div class="col-lg-6">
                                                    <label class="nomarginbottom">{{ t('Színek') }}</label>
                                                    @foreach ($item['attributes']['textile'] as $key => $textile_id)
                                                        <?php
                                                            $textile_data = ProductTextile::lang()->where('product_textiles.textile_id', $textile_id)->first();
                                                            $part = ProductPart::lang()->where('admin_name', $key)->first();
                                                        ?>
														@if (isset($part))
															<span class="item-size half-size">{{$part->name}}<br><img src="{{ ProductTextile::getImageUrl($textile_id, 40) }}" title="{{empty($textile_data) ? '' : $textile_data->name}}"></span>
														@else
															<span class="item-size half-size">{{$key}}<br><img src="{{ ProductTextile::getImageUrl($textile_id, 40) }}" title="{{empty($textile_data) ? '' : $textile_data->name}}"></span>
														@endif
                                                    @endforeach

                                                </div>
                                            @endif
                                            @if (isset($item['attributes']['related']) and $item['attributes']['related'] > 0)
                                                <?php
                                                    $related = ProductRelated::renderToCart($item['attributes']['related']);
                                                ?>
                                                @if ($related)
                                                <div class="clearfix"></div>
                                                <div class="col-lg-12 nopaddingleft">
                                                    <label class="nomarginbottom">{{ t('Kapcsolódó termék') }}</label>
                                                    <span>{{ $related }}</span>
                                                </div>
                                                @endif
                                            @endif
                                @elseif ($product->type == 'non-scalable')

                                    @if (isset($item['attributes']['sizes']) and sizeof($item['attributes']['sizes']))
                                    <div class="col-lg-12 nopaddingleft" >
                                                <label class="nomarginbottom">{{ t('Méretek') }}</label>
                                                @foreach ($item['attributes']['sizes'] as $size_item => $size_name)
                                                    @if ($size_count == 1)
                                                    {{ProductSize::getSizeString($product->product_id,$size_item,$size_name,false)}}
                                                    @else
                                                    {{t('oldal_'.$size_item)}}: {{ProductSize::getSizeString($product->product_id,$size_item,$size_name, true)}}<br>
                                                    @endif
                                                @endforeach
                                    </div>
                                    @endif



                                    @if (isset($item['attributes']['textile']) and sizeof($item['attributes']['textile']))
                                        <div class="col-lg-12">
                                            <label>{{ t('Színek') }}</label>
                                            @foreach ($item['attributes']['textile'] as $key => $textile_id)
                                                <?php
                                                    $textile_data = ProductTextile::lang()->where('product_textiles.textile_id', $textile_id)->first();

                                                    $part = ProductPart::lang()->where('admin_name', $key)->first();
                                                ?>
                                                <span class="item-size half-size">{{$part->name}}<br><img src="{{ ProductTextile::getImageUrl($textile_id, 40) }}" title="{{$textile_data->name}}"></span>
                                            @endforeach

                                        </div>
                                    @endif
                                    @if (isset($item['attributes']['related']) and $item['attributes']['related'] > 0)
                                            <?php
                                                $related = ProductRelated::renderToCart($item['attributes']['related']);
                                            ?>
                                            @if ($related)
                                            <div class="clearfix"></div>
                                            <div class="col-lg-12 nopaddingleft">
                                                <label class="nomarginbottom">{{ t('Kapcsolódó termék') }}</label>
                                                <span>{{ $related }}</span>
                                            </div>
                                            @endif
                                    @endif
                                @endif
                            </div>

                        </td>
                        </tr>
                        <tr class="item-info">
                        <td class="col price empty-cell" colspan="2" align="right">
						
						<span class="small">{{t('Szállítási mód:')}}
						@if (Shipping::MPLOK())
							{{t('Futárszolgálat')}}
						@else
							@if ((in_array($product->category_id, getConfig('manufacturecategory'))) or (in_array($product->category_id, getConfig('outletcategory'))))
								{{t('Házhozszállítás')}}
							@else
								<?php
									//@if ($outletsum>0)
								?>
								@if ($items_outlet>0)
									{{t('Házhozszállítás')}}
								@else
									@if ((isset($item['attributes']['parent_cartitem_id'])) and ($item['attributes']['parent_cartitem_id'] != '0'))
										{{t('Házhozszállítás')}}
									@else 
										{{t('Futárszolgálat')}}
									@endif
								@endif
							@endif
						@endif
						
						@if ($product->ship_type != "mpl")
							
						@else
							
						@endif
						</span>

                        <span class="price-excluding-tax">
                            <span class="cart-price">
                                <span class="price">
                                @if ($price_array['textile_price'])
                                <? /* {{ t('Bútor alapár').": ".money($price_array['furniture_price']) }} + {{ t('Szövet felár').": ".money($price_array['textile_price']) }} */ ?>
							    {{ t('Bútor alapár').": ".money($price_array['furniture_price']) }} 
								<?php
									//kupon kedvezmény számolása
									$kedvezmeny = Cart::getItemDiscount($item);
									
								?>
								@if ((isset($kedvezmeny)) and ($kedvezmeny))
								- {{ t('Kedvezmény').': '.money($kedvezmeny/$item->quantity) }}
								@endif
								+ {{ t('Szövet felár').": ".money(($item->getPriceSum()-$price_array['furniture_price']*$item->quantity)/$item->quantity) }}
                                @else
                                    {{ money($item->price) }}
									<?php
									//kupon kedvezmény számolása
									$kedvezmeny = Cart::getItemDiscount($item);
									?>
									@if ((isset($kedvezmeny)) and ($kedvezmeny))
									- {{ t('Kedvezmény').': '.money($kedvezmeny/$item->quantity) }}
									@endif
                                @endif
                                </span>
                            </span>

                            </span>
                        </td>
						@if ((isset($item['attributes']['related']) and ($item['attributes']['related'] != '0')) or (isset($item['attributes']['parent_cartitem_id']) and ($item['attributes']['parent_cartitem_id'] != '0')))
							<td class="col qty">
								<div class="field qty">
								<label class="label" for="cart-{{$item->id}}">
									<span>{{ t('db') }}</span>
								</label>
								<div class="control qty">
									<span>{{ $item->quantity }}</span>
									{{ csrf_field() }}
								</div>
								</div>
							</td>
						@else
							<td class="col qty">
								<div class="field qty">
								<label class="label" for="cart-{{$item->id}}">
									<span>{{ t('db') }}</span>
								</label>
								<div class="control qty">
									<form action="{{ action('CartController@updateItemQty', $item->id) }}" method="post">
									<input id="cart-{{$item->id}}" name="qty[{{$item->id}}]" onchange="this.form.submit()" value="{{ $item->quantity }}" size="4" class="input-text qty" maxlength="12"  type="number">
									<input type="hidden" name="product_id" value="{{ $item['attributes']->product_id }}">
									<input type="hidden" name="qty_def" value="{{ $item->quantity }}">
									{{ csrf_field() }}
									</form>
								</div>
								<div class="qty-changer">
									<a onclick="gtmAddToCart('{{ App::getLocale() }}','{{ $product->product_id }}','{{ $product->getName() }}','{{ $item->price }}','{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}','<?php
								if ($product->type == 'general') {
									$s = '';
								} else {
									$s = '';
									foreach ($item['attributes']['sizes'] as $size_item => $size_name) {
										$s.=$size_name.' ';
									}
									if (isset($item['attributes']['mounting'])) $s.=$item['attributes']['mounting'];
									if (isset($item['attributes']['seat'])) $s.=$item['attributes']['seat'];
								}
								?>{{ $s }}','1')" href="javascript:void(0)" class="qty-inc"><i class="porto-icon-up-dir"></i></a>
									<a onclick="gtmRemoveFromCart('{{ $product->product_id }}','{{ $product->getName() }}','{{ $item->price }}','{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}','<?php
								if ($product->type == 'general') {
									$s = '';
								} else {
									$s = '';
									foreach ($item['attributes']['sizes'] as $size_item => $size_name) {
										$s.=$size_name.' ';
									}
									if (isset($item['attributes']['mounting'])) $s.=$item['attributes']['mounting'];
									if (isset($item['attributes']['seat'])) $s.=$item['attributes']['seat'];
								}
								?>{{ $s }}','1')" href="javascript:void(0)" class="qty-dec"><i class="porto-icon-down-dir"></i></a>
								</div>
								</div>
							</td>
						@endif
                        <td class="col subtotal">

                            <span class="price-excluding-tax">
                            <span class="cart-price">
								<?php
								$osszprice = $item->getPriceSum();
								if (isset($kedvezmeny)) $osszprice = $osszprice - $kedvezmeny;
								?>
                                <span class="price">{{ money($osszprice) }}</span>            </span>

                            </span>
                        </td>
                </tr>
<?php
}
?>
