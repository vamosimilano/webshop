<ul class="opc-progress-bar">

        <li class="opc-progress-bar-item {{($step >= 1 ? '_active' : '')}}">
            <span>{{ t('Kosár tartalma') }}</span>
        </li>
        <li class="opc-progress-bar-item {{($step >= 2 ? '_active' : '')}}">
            <span>{{ t('Személyes adatok') }}</span>
        </li>
        <li class="opc-progress-bar-item {{($step >= 3 ? '_active' : '')}}">
            <span >{{ t('Fizetés és befejezés') }}</span>
        </li>

</ul>
