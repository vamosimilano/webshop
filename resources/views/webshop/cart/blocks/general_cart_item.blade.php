<?php
if(isset($headversion) && $headversion==="newhead") {
?>
					<tr>
						<td class="text-center">
						<?php
						$title="Termék törlése";
						if ( (isset($item['attributes']['parent_cartitem_id']) and $item['attributes']['parent_cartitem_id'] != '0' and $item['attributes']['parent_cartitem_id'] != '') or (isset($item['attributes']['related']) and $item['attributes']['related'] != '0' and $item['attributes']['related'] != '') ) {
							$title="Amennyiben törli a terméket, a kedvezménye is elveszik";
						}
						if ($product->type == 'general') {
							$s = '';
						} else {
							$s = '';
							foreach ($item['attributes']['sizes'] as $size_item => $size_name) {
								$s.=$size_name.' ';
							}
							if (isset($item['attributes']['mounting'])) $s.=$item['attributes']['mounting'];
							if (isset($item['attributes']['seat'])) $s.=$item['attributes']['seat'];
						}
						?>
							<a data-toggle="tooltip" data-placement="right" onclick="gtmRemoveFromCart('{{ $product->product_id }}','{{ $product->getName() }}','{{ $item->price }}','{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}','{{ $s }}','{{ $item->quantity }}')" href="{{ action('CartController@removeCart', $item->id) }}" title="{{$title}}" class="material-tooltip-main"><i class="fas fa-trash-alt"></i></a>
						</td>
						<td>
							{{ $product->getName() }}
							@if ($product->isOutlet())
								Outlet
							@endif
						</td>
						<td>
							@if ($price_array['textile_price'])
							{{ t('Bútor') }} <span class="float-right">{{ money($price_array['furniture_price']) }}</span>
							{{ t('Szövet felár') }} <span class="float-right">{{ money($price_array['textile_price']) }}
							@else
								<span class="float-right">{{ money($item->price) }}</span>
							@endif
						</td>
						<td class="text-center">
							{{ $item->quantity }}
						</td>
						<td class="text-right font-weight-bold">
						<?php
						$osszprice = $item->getPriceSum();
						//if (isset($kedvezmeny)) $osszprice = $osszprice - $kedvezmeny;
						?>
							{{ money($osszprice) }}
						</td>
					</tr>
<?php
}
else {
?>
<tr class="item-info">
                        <td class="col" >
                            <a href="{{ $product->getUrl() }}" title="{{ $product->getName() }}" tabindex="-1" class="product-item-photo">
                            <span class="product-image-container" style="width:165px;">
                                <span class="product-image-wrapper" style="padding-bottom: 100%;">
                                    <img class="product-image-photo" src="{{ $product->getDefaultImageUrl() }}" alt="{{ $product->getName() }}" width="165"></span>
                            </span>
                            </a>

                            <div class="product-item-details">
                                <div class="col-lg-12">
                                    <strong class="product-item-name"><a href="{{ $product->getUrl() }}">{{ $product->getName() }}</a></strong>
									@if ($product->isOutlet())
										Outlet
									@endif
                                </div>

                            </div>

                        </td>

                        <td class="col price" align="right">
						
						<span class="small">{{t('Szállítási mód:')}}
						@if (Shipping::MPLOK())
							{{t('Futárszolgálat')}}
						@else
							@if ((in_array($product->category_id, getConfig('manufacturecategory'))) or (in_array($product->category_id, getConfig('outletcategory'))))
								{{t('Házhozszállítás')}}
							@else
								<?php
									//@if ($outletsum>0)
								?>
								@if ($items_outlet>0)
									{{t('Házhozszállítás')}}
								@else
									@if ((isset($item['attributes']['parent_cartitem_id'])) and ($item['attributes']['parent_cartitem_id'] != '0'))
										{{t('Házhozszállítás')}}
									@else 
										{{t('Futárszolgálat')}}
									@endif
								@endif
							@endif
						@endif
						
						@if ($product->ship_type != "mpl")
							
						@else
							
						@endif
						</span>

                        <span class="price-excluding-tax">
						
							
                            <span class="cart-price">
                                <span class="price">
                                @if ($price_array['textile_price'])
                                {{ t('Bútor alapár').": ".money($price_array['furniture_price']) }} 
								<?php
									//kupon kedvezmény számolása
									$kedvezmeny = Cart::getItemDiscount($item);
								?>
								@if ((isset($kedvezmeny)) and ($kedvezmeny))
								- {{ t('Kedvezmény').': '.money($kedvezmeny/$item->quantity) }}
								@endif
								+ {{ t('Szövet felár').": ".money($price_array['textile_price']) }}
                                @else
                                    {{ money($item->price) }}
									<?php
									//kupon kedvezmény számolása
									$kedvezmeny = Cart::getItemDiscount($item);
									?>
									@if ((isset($kedvezmeny)) and ($kedvezmeny))
									- {{ t('Kedvezmény').': '.money($kedvezmeny/$item->quantity) }}
									@endif
                                @endif
                                </span>
                            </span>

                            </span>
                        </td>
                        <td class="col qty">
                            <div class="field qty">
                            <label class="label" for="cart-{{$item->id}}">
                                <span>{{ t('db') }}</span>
                            </label>
							@if (((isset($item['attributes']['related']) and ($item['attributes']['related'] != '0')) or (isset($item['attributes']['parent_cartitem_id']) and ($item['attributes']['parent_cartitem_id'] != '0'))) or ($product->isOutlet()))
								<div class="control qty">
									<span>{{ $item->quantity }}</span>
									{{ csrf_field() }}
								</div>
							@else
								<div class="control qty">
									<form action="{{ action('CartController@updateItemQty', $item->id) }}" method="post">
									<input id="cart-{{$item->id}}" name="qty[{{$item->id}}]" onchange="this.form.submit()" value="{{ $item->quantity }}" size="4" class="input-text qty" maxlength="12"  type="number">
									<input type="hidden" name="product_id" value="{{ $item['attributes']->product_id }}">
									<input type="hidden" name="qty_def" value="{{ $item->quantity }}">
									{{ csrf_field() }}
									</form>
								</div>
								<div class="qty-changer">
									<a onclick="gtmAddToCart('{{ App::getLocale() }}','{{ $product->product_id }}','{{ $product->getName() }}','{{ $item->price }}','{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}','<?php
								if ($product->type == 'general') {
									$s = '';
								} else {
									$s = '';
									foreach ($item['attributes']['sizes'] as $size_item => $size_name) {
										$s.=$size_name.' ';
									}
									if (isset($item['attributes']['mounting'])) $s.=$item['attributes']['mounting'];
									if (isset($item['attributes']['seat'])) $s.=$item['attributes']['seat'];
								}
								?>{{ $s }}','1')" href="javascript:void(0)" class="qty-inc"><i class="porto-icon-up-dir"></i></a>
									<a onclick="gtmRemoveFromCart('{{ $product->product_id }}','{{ $product->getName() }}','{{ $item->price }}','{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}','<?php
								if ($product->type == 'general') {
									$s = '';
								} else {
									$s = '';
									foreach ($item['attributes']['sizes'] as $size_item => $size_name) {
										$s.=$size_name.' ';
									}
									if (isset($item['attributes']['mounting'])) $s.=$item['attributes']['mounting'];
									if (isset($item['attributes']['seat'])) $s.=$item['attributes']['seat'];
								}
								?>{{ $s }}','1')" href="javascript:void(0)" class="qty-dec"><i class="porto-icon-down-dir"></i></a>
								</div>								
							@endif                           
                            </div>
                        </td>
                        <td class="col subtotal">

                            <span class="price-excluding-tax">
                            <span class="cart-price">
								<?php
								$osszprice = $item->getPriceSum();
								if (isset($kedvezmeny)) $osszprice = $osszprice - $kedvezmeny;
								?>
                                <span class="price">{{ money($osszprice) }}</span>            </span>

                            </span>
                        </td>
                </tr>
<?php
}
?>
