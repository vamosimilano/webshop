<aside class="col-md-3{{ ( ismobile() ? ' order-1' : ' order-0' ) }}">
	<div class="container">
		<div class="row">
		@include('webshop.sidebar_properties')
		@if (isset($highlighted_products))
			<?php $i=1; ?>
			@foreach($side_products as $highlighted_product)
				<?php 
					$pclick = "gtmProductClick('Highlighted list','".$highlighted_product->getName()."','".$highlighted_product->product_id."','".$highlighted_product->getDefaultPrice()."','".$highlighted_product->getParentCategoryAdminName().$highlighted_product->getCategory()->admin_name."','".$i."');";
					$i++;
				?>
				<div class="desktop-view">
					<div class="filter-options-item sidebar-highlighted">
						<div class="filter-options-title" >{{ t('A hónap bútora')." - ".$highlighted_product->getName() }}</div>
						<div class="filter-options-content nopadding text-center">

							<a onclick="{{$pclick}}" href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}"><img alt="{{$highlighted_product->getName()}}" src="{{ $highlighted_product->getDefaultImageUrl(600) }}" class="img-fluid"/></a>
							<?php
								$price_full = $highlighted_product->getDefaultFullPrice();
								$price = $highlighted_product->getDefaultPrice();
							?>
							@if ($price_full > $price)
							<span class="old-price highlighted">{{money($price_full)}}</span>
							@endif
							<span class="price highlighted">{{money($price)}}</span>
							<br>
							<a onclick="{{$pclick}}" href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}" class="btn btn-primary white-text btn-block">{{ t('Részletek') }}</a>
						</div>
						@if ($highlighted_product->getPercentPrice())
						<div style="position:relative;width:43px;top:-230px;left:213px;border-radius:15px;background-color:#CD7126;"><a onclick="{{$pclick}}" href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}" style="text-decoration: none;"><span style="font-weight:bold;font-size:16px;color:white">&nbsp;-{{ $highlighted_product->getPercentPrice() }}%&nbsp;</span></a></div>
						@endif
					</div>
				</div>
			@endforeach
		@endif
		</div>
	</div>

</aside>