$("#searchtoggle").click(function(){
	"use strict";
	var toggle;
	if($(".search-form").hasClass("hidden")===true) {
		toggle="hidden";
	}
	if($(".search-form").hasClass("show")===true) {
		toggle="show";
	}
	if(toggle==="hidden"){
		$(".search-form").removeClass("hidden");
		$(".search-form").removeClass("fadeOutDownBig");
		$(".search-form").addClass("fadeInUp");
		$(".search-form").addClass("show");
		
	}
	if(toggle==="show"){
		$(".search-form").removeClass("fadeInUp");
		$(".search-form").addClass("fadeOutDownBig");
		setTimeout( function(){ $(".search-form").removeClass("show");}, 500);
		setTimeout( function(){ $(".search-form").addClass("hidden");}, 500);
	}
	//alert(toggle);
});