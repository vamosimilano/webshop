var run_event = 0,
    answerId=0;
$(document).ready(function(){

            $('#subscribe-footer').append('<input type="hidden" name="key" value="value">');
            $("body").addClass("layout-1170");
            $("body").addClass("wide");
            $(document).ready(function() {
                $(".drop-menu > a").off("click").on("click", function() {
                    if ($(this).parent().children(".nav-sections").hasClass("visible"))
                        $(this).parent().children(".nav-sections").removeClass("visible");
                    else
                        $(this).parent().children(".nav-sections").addClass("visible")
                });
            });
            var scrolled = false;
            $(window).scroll(function() {
                if (!$('.page-header').hasClass('type10')) {
                    if ($(window).width() >= 768) {
                        if (140 < $(window).scrollTop() && !scrolled) {
                            $('.page-header').addClass("sticky-header");
                            scrolled = true;
                            if (!$(".page-header").hasClass("type12")) {
                                $('.page-header .minicart-wrapper').after('<div class="minicart-place hide"></div>');
                                var minicart = $('.page-header .minicart-wrapper').detach();
                                if ($(".page-header").hasClass("type8"))
                                    $('.page-header .menu-wrapper').append(minicart);
                                else
                                    $('.page-header .navigation').append(minicart);
                                var logo_image = $('<div>').append($('.page-header .header > .logo').clone()).html();
                                if ($(".page-header").hasClass("type8")){
                                    if ($('.page-header .navigation .sticky-logo').length == 0){
                                      $('.page-header .menu-wrapper').prepend('<div class="sticky-logo">' + logo_image + '</div>');
                                    }
                                }
                                else{
                                    if ($('.page-header .navigation .sticky-logo').length == 0){
                                    $('.page-header .navigation').prepend('<div class="sticky-logo">' + logo_image + '</div>');
                                    }
                                }
                            } else {
                                $('.page-header.type12 .logo').append('<span class="sticky-logo"><img src=""/></span>');
                                $('.page-header .logo > img').addClass("hide");
                            }
                        }
                        if (140 >= $(window).scrollTop() && scrolled) {
                            $('.page-header').removeClass("sticky-header");
                            scrolled = false;
                            if (!$(".page-header").hasClass("type12")) {
                                var minicart;
                                if ($(".page-header").hasClass("type8"))
                                    minicart = $('.page-header .menu-wrapper .minicart-wrapper').detach();
                                else
                                    minicart = $('.page-header .navigation .minicart-wrapper').detach();
                                $('.minicart-place').after(minicart);
                                $('.minicart-place').remove();
                                $('.page-header .minicart-wrapper-moved').addClass("minicart-wrapper").removeClass("minicart-wrapper-moved").removeClass("hide");
                            }
                            if ($(".page-header").hasClass("type8"))
                                $('.page-header .menu-wrapper > .sticky-logo').remove();
                            else if ($(".page-header").hasClass("type12")) {
                                $('.page-header .sticky-logo').remove();
                                $('.page-header .logo > img').removeClass("hide");;
                            } else
                                $('.page-header .navigation > .sticky-logo').remove();
                        }
                    }
                }
            });
            if ($("body").hasClass("cms-index-index")) {
                    var check_cookie = null;
                    if (window.location != window.parent.location) {
                        $('#newsletter_popup').remove();
                    } else {
                        if (check_cookie == null || check_cookie == 'shown') {
                            setTimeout(function() {
                                beginNewsletterForm();
                            }, 5000);
                        }
                        $('#newsletter_popup_dont_show_again').on('change', function() {
                           beginNewsletterForm();
                        });
                    }
            }




            function beginNewsletterForm() {
                return '';
                $.fancybox({
                    'padding': '0px',
                    'autoScale': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'type': 'inline',
                    'href': '#newsletter_popup',
                    'onComplete': function() {

                    },
                    'tpl': {
                        closeBtn: '<a title="Close" class="fancybox-item fancybox-close fancybox-newsletter-close" href="javascript:;"></a>'
                    },
                    'helpers': {
                        overlay: {
                            locked: false
                        }
                    }
                });
                $('#newsletter_popup').trigger('click');
            }

        });
