
function filterNowProducts(){
  if ($('#slider-range').length){

    var filter_price = showProductItems($('#slider-range').slider("values", 0),$('#slider-range').slider("values", 1));
  }else{
    var filter_price = '.product-item';
  }
  if ($('#slider-range-x').length){
    var filter_width = showProductItemsWidth($('#slider-range-x').slider("values", 0),$('#slider-range-x').slider("values", 1));
  }else{
    var filter_width = '.product-item';
  }
  if ($('#slider-range-y').length){
    var filter_height = showProductItemsHeight($('#slider-range-y').slider("values", 0),$('#slider-range-y').slider("values", 1));
  }else{
    var filter_height = '.product-item';
  }
  if ($('#filter-lining').length){
    var filter_linig = showProductItemsLining();
  }else{
    var filter_linig = '.product-item';
  }
  if (!filter_linig) {
    var filter_linig = '.product-item';
  }



  var $elements = $('.products .product-item').filter(filter_price).filter(filter_width).filter(filter_height).filter(filter_linig);
  $grid.isotope({ filter: $elements });
  $('.toolbar-number .current').html($elements.length);

}


function initRange(min,max,cur) {
  $( "#slider-range" ).slider({
              range: true,
              min: min,
              max: max,
              step: 100,
              values: [ min, max ],
              stop: function( event, ui ) {
                filterNowProducts();
                //showProductItems(ui.values[ 0 ],ui.values[ 1 ]);
              },
              slide: function( event, ui ) {
                $( "#amount" ).html( "" + ui.values[ 0 ] + " "+cur+" - " + ui.values[ 1 ]+" "+cur );

              }
            });
}
function initWidthRange(min,max) {
  $( "#slider-range-x" ).slider({
              range: true,
              min: min,
              max: max,
              step: 1,
              values: [ min, max ],
              stop: function( event, ui ) {
                filterNowProducts();
                //showProductItemsWidth(ui.values[ 0 ],ui.values[ 1 ]);
              },
              slide: function( event, ui ) {
                $( "#max-x" ).html( "" + ui.values[ 0 ] + " -  " + ui.values[ 1 ]);

              }
            });
}
function initHeightRange(min,max) {
  $( "#slider-range-y" ).slider({
              range: true,
              min: min,
              max: max,
              step: 1,
              values: [ min, max ],
              stop: function( event, ui ) {
                filterNowProducts();
               // showProductItemsHeight(ui.values[ 0 ],ui.values[ 1 ]);
              },
              slide: function( event, ui ) {
                $( "#max-y" ).html( "" + ui.values[ 0 ] + " -  " + ui.values[ 1 ]);

              }
            });
}
function initIsotope(){
  $grid = $('.products .product-items').isotope({
                itemSelector: '.product-item',
                layoutMode: 'fitRows',
                getSortData: {
                   price: '.price-order parseInt',
                   symbol: '.symbol',
                   number: '.number parseInt'
                }
            });
}
function shortProduct(mode){

  if (mode == 'null') {
        $grid.isotope({ sortBy: 'original-order'  });
  }else if (mode == 'price_desc') {
        $grid.isotope({ sortBy: 'price' , sortAscending: false});
  }else{
      $grid.isotope({ sortBy: mode, sortAscending: true });
  }
}
var orun = false;
function showProductItems(minP, maxP){

    minP = parseInt(minP);
    maxP = parseInt(maxP);


            var isotope_ids = '';
            $('.products .product-item').each(function(){
                var data_price = parseInt($(this).data('price'));
                if (data_price >= minP && data_price <= maxP){
                      isotope_ids+= "."+$(this).attr('id')+", ";
                }
            });
            isotope_ids = isotope_ids.substring(0, isotope_ids.length - 2);

            return isotope_ids;
}

function showProductItemsWidth(minP, maxP){
    if (orun == true) {
     // return false;
    }
    orun = true;
            var isotope_ids = '';
            $('.products .product-item').each(function(){

                var data_min = parseInt($(this).data('item_min_x'));
                var data_max = parseInt($(this).data('item_max_x'));

                if (data_max >= minP && data_min <= maxP){


                    isotope_ids+= "."+$(this).attr('id')+", ";

                }
            });

            isotope_ids = isotope_ids.substring(0, isotope_ids.length - 2);
            return isotope_ids;
            $grid.isotope({ filter: isotope_ids });
            orun = false;

            return;

}

function showProductItemsHeight(minP, maxP){
    if (orun == true) {
      //return false;
    }
    orun = true;
            var isotope_ids = '';
            $('.products .product-item').each(function(){

                var data_min = parseInt($(this).data('item_min_y'));
                var data_max = parseInt($(this).data('item_max_y'));

                if (data_max >= minP && data_min <= maxP){

                    isotope_ids+= "."+$(this).attr('id')+", ";

                }
            });

            isotope_ids = isotope_ids.substring(0, isotope_ids.length - 2);
            return isotope_ids;
            $grid.isotope({ filter: isotope_ids });
            orun = false;

            return;

}

function showProductItemsLining(){
  var isotope_ids = '';
  if ($('#spring').hasClass('active') && $('#foam').hasClass('active')) {
    $('.product-item').each(function(){
        if ($(this).attr('id') != undefined) {
          isotope_ids+= "."+$(this).attr('id')+", ";
        }


    });

    isotope_ids = isotope_ids.substring(0, isotope_ids.length - 2);
    return isotope_ids;
  }
  if ($('#spring').hasClass('active')){
    $('.products .product-item').each(function(){
        if ($(this).hasClass('lining-spring')){
          isotope_ids+= "."+$(this).attr('id')+", ";
        }
    });
  }
  if ($('#foam').hasClass('active')){
    $('.products .product-item').each(function(){
        if ($(this).hasClass('lining-foam')){
          isotope_ids+= "."+$(this).attr('id')+", ";
        }
    });
  }
  isotope_ids = isotope_ids.substring(0, isotope_ids.length - 2);
  return isotope_ids;

}

