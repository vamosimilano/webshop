$(document).ready(function(){
  $('#contact-form').append('<input type="hidden" name="key" value="AHH">');

})

  var map;

  function initBigMap(){

     var pos = new google.maps.LatLng(47.100172, 19.157183);

        var mapOptions = {

            center: pos,

            panControl: true,

            zoomControl: true,

            mapTypeControl: true,

            scaleControl: true,

            scrollwheel: false,

            streetViewControl: true,

            overviewMapControl: true,

            zoom: 7        };

        map = new google.maps.Map(document.getElementById("big_map"), mapOptions);

        initMarkerBp();

        initMarkerDeb();

        initMarkerSzeged();

        //initMarkerVeszprem();

		initMarkerMiskolc();

		//initMarkerRaktar();
	  
	  //initMarkerGyor();
	  initMarkerCsepel();

  }



    function initMarkerBp(){

      var pos = new google.maps.LatLng(47.4646476,19.0417743);

        var infowindow = new google.maps.InfoWindow({

          content: $("#content-bp").html()

        });

        var marker_bp = new google.maps.Marker({

            position: pos,

            map: map,

            title: 'Budapesti üzlet'

        });

        marker_bp.addListener('click', function() {

          infowindow.open(map, marker_bp);

        });

    }
function initMarkerCsepel(){

      var pos = new google.maps.LatLng(47.427789, 19.066879);

        var infowindow = new google.maps.InfoWindow({

          content: $("#content-csepel").html()

        });

        var marker_csepel = new google.maps.Marker({

            position: pos,

            map: map,

            title: 'Csepeli Outlet Áruház'

        });

        marker_csepel.addListener('click', function() {

          infowindow.open(map, marker_csepel);

        });

    }

     function initMarkerVeszprem(){

      var pos = new google.maps.LatLng(47.096724,17.920521);

        var infowindow = new google.maps.InfoWindow({

          content: $("#content-vesz").html()

        });

        var marker_vesz = new google.maps.Marker({

            position: pos,

            map: map,

            title: 'Store'

        });

        marker_vesz.addListener('click', function() {

          infowindow.open(map, marker_vesz);

        });

    }

    function initMarkerDeb(){

      var pos = new google.maps.LatLng(47.538777, 21.585785);

        var infowindow = new google.maps.InfoWindow({

          content: $("#content-deb").html()

        });
    }

	

    function initMarkerSzeged(){

      var pos = new google.maps.LatLng(46.268548, 20.118107);

        var infowindow = new google.maps.InfoWindow({

          content: $("#content-szeged").html()

        });

        var marker_szeged = new google.maps.Marker({

            position: pos,

            map: map,

            title: 'Szeged bútorátvételi pont'

        });

        marker_szeged.addListener('click', function() {

          infowindow.open(map, marker_szeged);

        });

    }

	

	function initMarkerMiskolc(){

      var pos = new google.maps.LatLng(48.121898, 20.860243);

        var infowindow = new google.maps.InfoWindow({

          content: $("#content-miskolc").html()

        });

        var marker_miskolc = new google.maps.Marker({

            position: pos,

            map: map,

            title: 'Miskolc bútorátvételi pont'

        });

        marker_miskolc.addListener('click', function() {

          infowindow.open(map, marker_miskolc);

        });

    }
