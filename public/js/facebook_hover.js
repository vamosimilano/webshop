var selector_like_show;
var thisismobile = false;
function likeShow(selector_like_show) {
    $(selector_like_show).fadeIn();
}

/**
 * FB START
 */

var facebook_tooken;
var likers = [];
var shares = [];
function initFBLikers(url, target, limit, page) {
  return '';
    setTimeout(function () {
        getFBObject(url, target, limit, page);
    }, 1000);
}


function getFBObject(url, target, limit, page) {
    var showed = 0;
    var fbobjtarget_tmp = $("#" + target);
    FB.api(
        '/?id=' + url,
        'GET',
        function (response) {
          console.log(response.error);
            if (response && !response.error) {
                if (response.share) {
                    if (fbobjtarget_tmp.length) {

                        if (response.share.share_count > 100) {

                            $("#" + target + " strong").html(response.share.share_count);
                            fbobjtarget_tmp.show();

                            if (page == 'page') {
                                likeShow(".fb-like.standard");
                                showed = 1;
                            }

                        } else {
                            if (page == 'page') {
                                likeShow(".fb-like.button");
                                showed = 1;
                            }
                        }
                    }

                } else {
                    if (page == 'page') {
                        likeShow(".fb-like.button");
                        showed = 1;
                    }

                }
                if (response.shares) {

                    if (fbobjtarget_tmp.length) {
                        if (response.shares > 1) {
                            $("#" + target + " strong").html(response.shares);
                            fbobjtarget_tmp.show();
                            if (page == 'page' && showed == 0) {
                                likeShow(".fb-like.standard");
                            }

                        } else {
                            if (page == 'page' && showed == 0) {
                                likeShow(".fb-like.button");
                            }

                        }
                    }

                } else {
                    if (page == 'page' && showed == 0) {
                        likeShow(".fb-like.button");
                    }
                }

                if (response.og_object && !thisismobile) {
                    likers[response.og_object.id] = [];
                    shares[response.og_object.id] = response.share.share_count;

                    getFBLikes(response.og_object.id, limit);
                    setTimeout(function () {
                        if (likers[response.og_object.id]) {
                            var found = 0;
                            for (i = 0; i < limit; i++) {
                                if (likers[response.og_object.id][i] !== undefined) {
                                    if (i < limit) {
                                        $("#" + target + " ul").append('<li><img src="' + likers[response.og_object.id][i] + '" alt="facebook liker"></li>');
                                        found++;
                                    }
                                }
                            }
                            var share = (shares[response.og_object.id] - limit);
                            if (found > 0) {
                                $("#" + target + " ul").append('<li><span class="more-friends">+' + share + '</span></li>');
                            }
                        }
                    }, 1000);
                }
            } else {
                if (page == 'page' && showed == 0) {
                    likeShow(".fb-like.button");
                }
            }
        }
    );

}
function getFBLikes(object_id, limit) {
    if (!limit) {
        limit = 5;
    }
    limit = (limit * 5);
    var count = 0;
    FB.api(
        "/v2.1/" + object_id + "/likes",
        "GET",
        function (response) {

            if (response && !response.error) {

                $.each(response.data, function (row, val) {
                    if (val.id) {

                        if (count > limit) {
                            return '';
                        } else {
                            getFBPicture(val.id, object_id, count);
                        }
                        count = count + 1;

                    }
                });


            }
        }
    );

}

function getFBPicture(user_id, object_id, count) {

    FB.api(
        "/v2.1/" + user_id + "/picture?redirect=false",
        "GET",
        function (response) {
            if (response && !response.error) {
                likers[object_id][count] = response.data.url;
            }
        }
    );
}
/**
 * FB END
 */
