var timeout_minicart;
$(document).ready(function(){

  toastr.options = {
    "positionClass": "toast-bottom-right",
    "timeOut": "10000",
  }

   $('.minicart-wrapper').hover(function(){
      clearTimeout(timeout_minicart);
      showMiniCart();

  },function(){
    timeout_minicart = setTimeout(function(){
      hideMiniCart()
    },500);
  });
  $('body').on('click','#btn-minicart-close', function(){

    hideMiniCart();
  });


    $('body').on('submit','.ajax-form',function(){


		if(run_event){return false;} /*A duplán kattintagó userek kivédésére.*/
		run_event = 1;

		var form = $(this);
		var ajax_url = form.attr('ajax-action');

		if(!ajax_url){return false;}

		$.ajax({
			dataType: "json",
			type: "POST",
			url: ajax_url,
			data: form.serialize()
		}).done(function(data) {
			run_event = 0;

			/*PHP-adja vissza.*/
			if(data.fn){
				var tmpFunc = new Function(data.fn);
                tmpFunc();
			}

			elementAfterCall(form);


		}).fail(function() {
			run_event = 0;
		}).always(function() {
			run_event = 0;
		});

		return false;
	});

	jQuery("body").on('click','.ajax-get',function(){
		if(run_event){return false;} /*A duplán kattintagó userek kivédésére.*/
		run_event = 1;

		var a = $(this);
		var ajax_url = a.attr('ajax-action');
		$.ajax({
			dataType: "json",
			type: "get",
			url: ajax_url,
		}).done(function(data) {
  		if(data.fn){
				var tmpFunc = new Function(data.fn);
                tmpFunc();
			}

			run_event = 0;
			elementAfterCall(a);


		}).fail(function() {
			run_event = 0;
		}).always(function() {
			run_event = 0;
		});

		return false;
	});


	$('.billing-copy').blur(function(){
   copyBillingAddress();
	});

	$('.qty-inc').on('click', function(){
  	var qty = $(this).parent().parent().find('.input-text.qty');


  	var current_val = parseInt(qty.val());
  	qty.val(current_val+1);
  	qty.parent('form').submit();
	});
	$('.qty-dec').on('click', function(){
  	var qty = $(this).parent().parent().find('.input-text.qty');


  	var current_val = parseInt(qty.val());
  	if ((current_val-1) <= 0){
    	return false;
  	}
  	qty.val(current_val-1);
  	qty.parent('form').submit();
	});
	$('.copy-personal').click(function(){
    $('#lastname').val($('#user_lastname').val());
    $('#firstname').val($('#user_firstname').val());
	});


});



/**
 * elementAfterCall function.
 * Ha az elemnek létezik a after-call attr-ja, akkor azt meghívja
 *
 * @access public
 * @param mixed form
 * @return void
 */
function elementAfterCall(element){
	if(element.attr('after-call')){
		var fn = element.attr('after-call');
		var tmpFunc = new Function(fn); tmpFunc();
	}
	return false;
}

function clearAfterCall(element){
	element.find("input[type=text]").val("");
	element.find("textarea").val("");
}
function flash(type, message, target) {
  if (type == 'success') {
    toastr.success(message);
  }
  if (type == 'error') {
    toastr.error(message);
  }
  if (type == 'info') {
    toastr.info(message);
  }


}

function reloadCartPreview(){
   $.ajax({
			dataType: "json",
			type: "get",
			url: '/cart/preview',
		}).done(function(data) {
      $('#minicart').html(data.html);
		});
}

function showMiniCart(){

  $('.minicart-wrapper').addClass('active');

    $('.minicart-wrapper').find('.showcart').addClass('active');
    $('.minicart-wrapper').find('.ui-dialog').show();
    $('.block-minicart').show();

}

function hideMiniCart(){
  $('.minicart-wrapper').find('.ui-dialog').hide(100);
  $('.block-minicart').hide(100);
}
function copyBillingAddress(){
  if($('#billing_exist').prop('checked')){
    $('#billing_name').val($('#lastname').val()+" "+$('#firstname').val())
    $('.billing-copy').each(function(){
      var id = $(this).data('target');

      $('#'+id).val($(this).val());
    });
  }
  return '';
}
function checkCartRequired(){
  var empty_flds = 0;
  $('#carform input,textarea,select').filter('[required]:visible').each(function() {
    if(!$.trim($(this).val())) {

        empty_flds++;
    }
  });

  if (empty_flds == 0) {
    $('#gotoCartButton').removeAttr('disabled');
  } else {
     $('#gotoCartButton').attr('disabled','disabled');

  }
}
function loadOverViewBlock(){
	if ($('#extradata').is(":visible")==false){
		$('#extradata input:checkbox').each(function() {
			$(this).prop('checked',false); 
			$(this).removeAttr('checked');
		});
	}
  $.ajax({
			dataType: "json",
			type: "post",
			url: '/cart/loadoverviewblock',
			data: $('#carform').serialize()
		}).done(function(data) {
      if(data.fn){
				var tmpFunc = new Function(data.fn);
        tmpFunc();
			}
      $('#cart-right-sidebar').html(data.html);
		});

}
