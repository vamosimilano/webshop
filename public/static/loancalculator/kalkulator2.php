<!DOCTYPE html>
<html>
	<head>
		<script>
			var szorzo = {
				6:0.18360450,
				7:0.15952481,
				8:0.14148151,
				9:0.12746247,
				10:0.11626038,
				11:0.10710697,
				12:0.09949005,
				13:0.09305503,
				14:0.08754863,
				15:0.08278510,
				16:0.07862514,
				17:0.07496221,
				18:0.07171346,
				19:0.06881348,
				20:0.06620993,
				21:0.06386044,
				22:0.06173035,
				23:0.05979103,
				24:0.05801861,
				25:0.05639304,
				26:0.05489736,
				27:0.05351713,
				28:0.05223994,
				29:0.05105513,
				30:0.04995344,
				31:0.04892681,
				32:0.04796818,
				33:0.04707135,
				34:0.04623085,
				35:0.04544184,
				36:0.04470000,
				37:0.04400149,
				38:0.04334288,
				39:0.04272108,
				40:0.04213331,
				41:0.04157707,
				42:0.04105007,
				43:0.04055028,
				44:0.04007581,
				45:0.03962496,
				46:0.03919617,
				47:0.03878802,
				48:0.03839920
			};
			var thm = {
				6:39.80,
				7:39.80,
				8:39.80,
				9:39.80,
				10:39.80,
				11:39.80,
				12:39.80,
				13:39.80,
				14:39.80,
				15:39.80,
				16:39.80,
				17:39.80,
				18:39.80,
				19:39.80,
				20:39.80,
				21:39.80,
				22:39.80,
				23:39.80,
				24:39.80,
				25:39.80,
				26:39.80,
				27:39.80,
				28:39.80,
				29:39.80,
				30:39.80,
				31:39.80,
				32:39.80,
				33:39.80,
				34:39.80,
				35:39.80,
				36:39.80,
				37:39.80,
				38:39.80,
				39:39.80,
				40:39.80,
				41:39.80,
				42:39.80,
				43:39.80,
				44:39.80,
				45:39.80,
				46:39.80,
				47:39.80, 
				48:39.80
			};
			function computeLoan(){
				var amount = document.getElementById('amount').value;
				var interest_rate = document.getElementById('interest_rate').value;
				var months = document.getElementById('futamido').value;
				var interest = (amount * (interest_rate * .01)) / months; //5527,7
				var payment = Math.round(((amount / months) + interest).toFixed(2));//19416,6
				payment = payment.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
				var onero = Math.round(amount*0.3980);
				onero = onero.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
				document.getElementById('payment').innerHTML = "Havonta fizetendő = "+payment+" Ft";
				if (+amount > 1000000 || +amount < 19999) {
				document.getElementById('hitelosszeg_error').style.display = 'block';
				document.getElementById('hitelosszeg_error').innerHTML = "A hitelösszeg minimum 20 000 Ft és maximum 1 000 000 Ft.";
				} else {
					document.getElementById('hitelosszeg_error').style.display = 'none';
					if (+amount > 19999) {
						document.getElementById('figyelmeztetes').style.display = 'none';
						document.getElementById('figyelmeztetes').innerHTML = "300 000 Forint hitelösszeg felett 20% önerő szükséges, ami jelen esetben "+onero+" Ft. ";
						document.getElementById('onero').innerHTML = "Önerő: "+onero;
						var onero = Math.round(amount*0.3980);
						var hitelosszegOnerovel = amount-onero;
						var torleszto = Math.round(((hitelosszegOnerovel / months) + interest).toFixed(2));
						torleszto = torleszto.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						document.getElementById('payment').innerHTML = "Havonta fizetendő = "+torleszto+" Ft";
					} else {
						document.getElementById('figyelmeztetes').style.display = 'none';
						document.getElementById('onero').innerHTML = "Önerő: 0 Ft.";
					} 
				}
			}
		</script>
	</head>
	<body>
		<?php
		include "kalkulator_header.php";
		$oldal = "standard";
		include "kalkulator_topbar.php";
		?>
		<div class="row">
			<div class="col-sm-4">Érvényes: 2018 július 1. és 2018 augusztus 31. között.</div>
			<div class="col-sm-4">
				<?php include "kalkulator_footer.php"; ?>
				<div id="figyelmeztetes" style="display:none" class="alert alert-info"></div>
				<div class="form-group">
					<label for="amount">Hitelösszeg</label>
					<div id="hitelosszeg_error" style="display:none" class="alert alert-danger"></div>
					<input class="form-control" id="amount" type="number" min="20000" max="100000" placeholder="20 000 - 1 000 000 Ft" onkeyup="computeLoan()">
				</div>
				<div class="form-group">
					<label for="interest_rate">THM (%)</label>
					<input class="form-control" id="interest_rate" type="number"  value="39.80"  onkeyup="computeLoan()" disabled>
					<div class="form-group">
						<label for="futamido">Futamidő (hónap)</label>
						<select class="form-control" id="futamido" onchange="computeLoan()">
							<option value="6" selected>6 hónap</option>
							<option value="7">7 hónap</option>
							<option value="8">8 hónap</option>
							<option value="9">9 hónap</option>
							<option value="10">10 hónap</option>
							<option value="11">11 hónap</option>
							<option value="12">12 hónap</option>
							<option value="13">13 hónap</option>
							<option value="14">14 hónap</option>
							<option value="15">15 hónap</option>
							<option value="16">16 hónap</option>
							<option value="17">17 hónap</option>
							<option value="18">18 hónap</option>
							<option value="19">19 hónap</option>
							<option value="20">20 hónap</option>
							<option value="21">21 hónap</option>
							<option value="22">22 hónap</option>
							<option value="23">23 hónap</option>
							<option value="24">24 hónap</option>
							<option value="25">25 hónap</option>
							<option value="26">26 hónap</option>
							<option value="27">27 hónap</option>
							<option value="28">28 hónap</option>
							<option value="29">29 hónap</option>
							<option value="30">30 hónap</option>
							<option value="31">31 hónap</option>
							<option value="32">32 hónap</option>
							<option value="33">33 hónap</option>
							<option value="34">34 hónap</option>
							<option value="35">35 hónap</option>
							<option value="36">36 hónap</option>
							<option value="37">37 hónap</option>
							<option value="38">38 hónap</option>
							<option value="39">39 hónap</option>
							<option value="40">40 hónap</option>
							<option value="41">41 hónap</option>
							<option value="42">42 hónap</option>
							<option value="43">43 hónap</option>
							<option value="44">44 hónap</option>
							<option value="45">45 hónap</option>
							<option value="46">46 hónap</option>
							<option value="47">47 hónap</option>
							<option value="48">48 hónap</option>
						</select>
					</div>
					<!-- 
					<div class="form-group">
						<label for="torleszto">Havi törlesztő</label>
						<input class="form-control" id="torleszto" type="number" value="0" disabled>
					</div>
					-->
					<br>
					<div class="alert alert-warning">
						<strong>Hitelösszeg:</strong> 20 000 Ft és 1 000 000 Ft között.<br>
						<strong>Futamidő</strong>: 6-48 hónap. <br><strong>Önerő</strong>: 300 000 Ft-ig minimum 0%, 300 000 Ft felett minimum 20% <br><strong>Éves ügyleti kamat</strong>: 34,05%. <br>
					</div>
					<h2 id="payment"></h2>
					<h2 id="onero"></h2>
					<br>
					<?php include "kalkulator_hirdetmeny.php"; ?>
				</div>
			</div>
			<div class="col-sm-4"></div>
		</div>
	</body>
</html>