		<script src="https://use.fontawesome.com/b490f19f77.js"></script>
		<style>
			.topbar {
				line-height:25px;
				text-align:center;
				background-color:#001a33;
				color:white;
			}
			.topbar a {
				padding-right:25px;
				text-decoration:none;
				color:white;
			}
			.topbar a:hover {
				color:white;
			}
		</style>
		<div class="row hidden">
			<div class="col-sm-12 topbar">
				<a target="_blank" href="https://www.vamosimilano.hu">Ugrás a Vamosi weboldalára <i class="fa fa-share"></i></a>
				<a target="_blank" href="https://www.otp.hu">Ugrás az OTP weboldalára <i class="fa fa-share"></i></a>
			</div>
		</div>