<?php
$oldal = "0%";
?>

<!DOCTYPE html>
<html>
<head>
<script>



function computeLoan(){
	var amount = document.getElementById('amount').value;
	var interest_rate = document.getElementById('interest_rate').value;
	var months = document.getElementById('months').value;
	var interest = (amount * (interest_rate * .01)) / months;
	var payment = Math.round(((amount / months) + interest).toFixed(2));
	payment = payment.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
	
	var onero = Math.round(amount*0.2);
	onero = onero.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
	
	if (+amount > 499999 || +amount < 29999) {
		document.getElementById('hitelosszeg_error').style.display = 'block';
		document.getElementById('hitelosszeg_error').innerHTML = "A hitel�sszeg minimum 30 000 Ft �s maximum 500 000 Ft.";
	} else {
		document.getElementById('hitelosszeg_error').style.display = 'none';
		if (+amount > 300000) {
			document.getElementById('figyelmeztetes').style.display = 'block';
			document.getElementById('figyelmeztetes').innerHTML = "300 000 Forint hitel�sszeg felett 20% �nero sz�ks�ges, ami jelen esetben "+onero+" Ft. ";
			document.getElementById('onero').innerHTML = "�nero: "+onero;
		} else {
			document.getElementById('figyelmeztetes').style.display = 'none';
			document.getElementById('onero').innerHTML = "�nero: 0 Ft.";

		}
		document.getElementById('payment').innerHTML = "Havonta fizetendo = "+payment+" Ft";
	
	}
}
</script>
</head>
<body>
<br>






<div class="row">
  <div class="col-sm-4"></div>
<div class="col-sm-4">
<?php
include "kalkulator_footer.php";
?>

<h2>0% THM akci�</h2>

<div id="figyelmeztetes" style="display:none" class="alert alert-info"></div>

<div class="form-group">
  <label for="amount">Hitel�sszeg</label>
  <div id="hitelosszeg_error" style="display:none" class="alert alert-danger"></div>
  <input class="form-control" id="amount" type="number" min="30000" max="500000" placeholder="30 000 Ft - 500 000 Ft" onkeyup="computeLoan()">
</div>

<div class="form-group">
  <label for="interest_rate">THM (%)</label>
  <input class="form-control" id="interest_rate" type="number" min="0" max="100" value="0" step=".1" disabled> 
</div>

<div class="form-group">
  <label for="months">Futamido (h�nap)</label>
  <input class="form-control" id="months" type="number" min="1" max="10" value="10" step="1" onchange="computeLoan()" disabled>
</div>

<h2 id="payment"></h2>
<h2 id="onero"></h2>

<br>
<div class="alert alert-warning">
<strong>Hitel�sszeg:</strong> 30 000 Ft �s 500 000 Ft k�z�tt.<br>
<strong>Futamido</strong>: 10 h�nap. <br><strong>�nero</strong>: 0%. <br><strong>Kezel�si k�lts�g</strong>: 0%. <br><strong>�ves �gyleti kamat</strong>: 0%. <br>
<br>300 000 Ft-ig �nero n�lk�l, 300 000 Ft felett 20% �nero sz�ks�ges.
</div>


</div>
<div class="col-sm-4"></div>
</div>


</body>
</html>
