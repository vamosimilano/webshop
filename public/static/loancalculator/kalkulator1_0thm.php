<?php
include "kalkulator_topbar.php";

?>
<!DOCTYPE html>
<html>
<head>
<script>



function computeLoan(){
	var amount = document.getElementById('amount').value;
	var interest_rate = document.getElementById('interest_rate').value;
	var months = document.getElementById('months').value;
	var interest = (amount * (interest_rate * .01)) / months;
	var payment = Math.round(((amount / months) + interest).toFixed(2));
	payment = payment.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
	
	var onero = Math.round(amount*0.2);
	onero = onero.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
	
	if (+amount > 500000 || +amount < 29999) {
		document.getElementById('hitelosszeg_error').style.display = 'block';
		document.getElementById('hitelosszeg_error').innerHTML = "A hitelösszeg minimum 30 000 Ft és maximum 500 000 Ft.";
	} else {
		document.getElementById('hitelosszeg_error').style.display = 'none';
		if (+amount > 300000) {
			document.getElementById('figyelmeztetes').style.display = 'block';
			document.getElementById('figyelmeztetes').innerHTML = "300 000 Forint hitelösszeg felett 20% önero szükséges, ami jelen esetben "+onero+" Ft. ";
			document.getElementById('onero').innerHTML = "Önerő: "+onero+" Ft";
		} else {
			document.getElementById('figyelmeztetes').style.display = 'none';
			document.getElementById('onero').innerHTML = "Önerő:<br>0 Ft.";

		}
		document.getElementById('payment').innerHTML = "Havonta fizetendő:<br>"+payment+" Ft";
	
	}
}
</script>
</head>
<?php
include "kalkulator_header.php";
$oldal = "215";
?>
<body>
<br>






<div class="row">
  <div class="col-sm-4"></div>
<div class="col-sm-4">
<?php
include "kalkulator_footer.php";
?>

<h2>0% THM akció</h2>

<div id="figyelmeztetes" style="display:none" class="alert alert-info"></div>

<div class="form-group">
  <label for="amount">Hitelösszeg</label>
  <div id="hitelosszeg_error" style="display:none" class="alert alert-danger"></div>
  <input class="form-control" id="amount" type="number" min="30000" max="500000" placeholder="30 000 Ft - 500 000 Ft" onkeyup="computeLoan()">
</div>

<div class="form-group">
  <label for="interest_rate">THM (%)</label>
  <input class="form-control" id="interest_rate" type="number" min="0" max="100" value="0" step=".1" disabled> 
</div>

<div class="form-group">
  <label for="months">Futamido (hónap)</label>
  <input class="form-control" id="months" type="number" min="1" max="10" value="10" step="1" onchange="computeLoan()" disabled>
</div>

<h2 id="payment"></h2>
<h2 id="onero"></h2>

<br>
<div class="alert alert-warning">
<strong>Hitelösszeg:</strong> 30 000 Ft és 500 000 Ft között.<br>
<strong>Futamido</strong>: 10 hónap. <br><strong>Önero</strong>: 0%. <br><strong>Kezelési költség</strong>: 0%. <br><strong>Éves ügyleti kamat</strong>: 0%. <br>
<br>300 000 Ft-ig Önero nélkül, 300 000 Ft felett 20% önero szükséges.
</div>

<br> <?php include "kalkulator_hirdetmeny.php"; ?>

</div>
<div class="col-sm-4"></div>
</div>


</body>
</html>
