<!DOCTYPE html>
<html>
	<head>
		<script>
			function computeLoan(){
				var amount = document.getElementById('amount').value;
				var interest_rate = document.getElementById('interest_rate').value;
				var months = document.getElementById('months').value;
				var interest = (amount * (interest_rate * .01)) / months;
				var payment = Math.round(((amount / months) + interest).toFixed(2));
				payment = payment.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
				var onero = Math.round(amount*0.2);
				onero = onero.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
				if (+amount > 500000 || +amount < 29999) {
					document.getElementById('hitelosszeg_error').style.display = 'block';
					document.getElementById('hitelosszeg_error').innerHTML = "A hitelösszeg minimum 30 000 Ft és maximum 500 000 Ft.";
				} else {
					document.getElementById('hitelosszeg_error').style.display = 'none';
					if (+amount > 300000) {
						document.getElementById('figyelmeztetes').style.display = 'block';
						document.getElementById('figyelmeztetes').innerHTML = "300 000 Forint hitelösszeg felett 20% önero szükséges, ami jelen esetben "+onero+" Ft. ";
						document.getElementById('onero').innerHTML = "Önerő: "+onero+" Ft";
					} else {
						document.getElementById('figyelmeztetes').style.display = 'none';
						document.getElementById('onero').innerHTML = "Önerő:<br>0 Ft.";
					}
					document.getElementById('payment').innerHTML = "Havonta fizetendő:<br>"+payment+" Ft";
				}
			}
		</script>
	</head>
	<body>
		<?php
		include "kalkulator_header.php";
		$oldal = "215";
		include "kalkulator_topbar.php";
		?>
		<div class="row">
			<div class="col-sm-4">Érvényes: 2018 november 2. és 2018 december 31. között.</div>
			<div class="col-sm-4">
				<?php include "kalkulator_footer.php"; ?>
				<h2>0% THM akció</h2>
				<div id="figyelmeztetes" style="display:none" class="alert alert-info"></div>
				<div class="form-group">
					<label for="amount">Hitelösszeg</label>
					<div id="hitelosszeg_error" style="display:none" class="alert alert-danger"></div>
					<input class="form-control" id="amount" type="number" min="30000" max="500000" placeholder="30 000 Ft - 500 000 Ft" onkeyup="computeLoan()">
				</div>
				<div class="form-group">
					<label for="interest_rate">THM (%)</label>
					<input class="form-control" id="interest_rate" type="number" min="0" max="100" value="0" step=".1" disabled> 
				</div>
				<div class="form-group">
					<label for="months">Futamido (hónap)</label>
					<input class="form-control" id="months" type="number" min="1" max="10" value="10" step="1" onchange="computeLoan()" disabled>
				</div>
				<h2 id="payment"></h2>
				<h2 id="onero"></h2>
				<br>
				<div class="alert alert-warning">
					<strong>Hitelösszeg:</strong> 30 000 Ft és 500 000 Ft között.<br>
					<strong>Futamido</strong>: 10 hónap. <br><strong>Önero</strong>: 0%. <br><strong>Kezelési költség</strong>: 0%. <br><strong>Éves ügyleti kamat</strong>: 0%. <br>
					<br>300 000 Ft-ig Önero nélkül, 300 000 Ft felett 20% önerő szükséges.
				</div>
				<br><?php include "kalkulator_hirdetmeny.php"; ?>
				<p><small>Az extra kedvezményes hitelt az OTP Bank nyújtja. Hitelösszeg: 30 ezer Ft és 500 ezer Ft között. Futamidő: 10 hónap.  Saját erő 0%. Kezelési költség 0%. Éves Ügyleti Kamat: 0%. 300 000 Ft-ig saját erő nélkül, 300 000 Ft felett 20% saját erő szükséges. A hitel nyújtója az OTP Bank Nyrt. A WORE Hungary Kft az OTP Bank Nyrt. hitelközvetítője. Ez a hirdetés nem minősül ajánlattételnek, a Bank a hitelbírálat jogát fenntartja. A hitelkonstrukció a 2018. december 1.-31. között benyújtott hitelkérelmek esetén, a kijelölt áruházakban, meghatározott termékekre vehető igénybe és a készlet erejéig érvényes. Példa THM: 500.000 Ft hitelösszeg és 10 hónapos futamidő esetén 0 % THM. A THM meghatározása a 83/2010. (III.25.) Korm. R.9§(3) bekezdésében megjelölt szabályozáson alapul. A THM számítás során a folyósítás és az első esedékesség közötti lehetséges legrövidebb időszak - 31 nap - került figyelembe vételre. A tájékoztatás nem teljes körű, ezért további részletes információért kérjük, tájékozódjon az ügyintézőnél, vagy az áruházakban elhelyezett hatályos Hirdetményből, valamint az OTP Bank Nyrt. bankfiókjaiban és honlapján közzétett vonatkozó Üzletszabályzatokból. <a href="https://www.otpbank.hu" target="_blank">www.otpbank.hu</a></small>
				</p>
			</div>
			<div class="col-sm-4"></div>
		</div>
	</body>
</html>