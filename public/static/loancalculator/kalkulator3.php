<!DOCTYPE html>
<html>
	<head>
		<script>
			var szorzo = {
				15	:	0.08126239,
				20	:	0.06490700
			};
			var thm = {
				15	:	36.4,
				20	:	36.4
			};
			function computeLoan(){
				var amount = document.getElementById('amount').value;
				var onero = Math.round(amount*0.2);
				onero = onero.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
				var interest_rate = document.getElementById('interest_rate').value;
				var futamido = document.getElementById('futamido').value;
				var torleszto = Math.round(amount * szorzo[futamido]);
				document.getElementById('interest_rate').value = thm[futamido];
				if (amount > 1000000 || amount < 20000) {
					document.getElementById('hitelosszeg_error').style.display = 'block';
					document.getElementById('hitelosszeg_error').innerHTML = "A. hitelösszeg minimum 20 000 Ft és maximum 1 000 000 Ft.";
				} else {
					document.getElementById('hitelosszeg_error').style.display = 'none';
					torleszto = torleszto.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
					//document.getElementById('torleszto_blokk').style.display = 'block';
					document.getElementById('payment').innerHTML = "Havonta fizetendő = "+torleszto+" Ft";
					document.getElementById('onero').innerHTML = "Önerő: "+onero;
				}
			}
		</script>
	</head>
	<?php
	include "kalkulator_header.php";
	$oldal = "203";
	?>
	<body>
		<?php
		include "kalkulator_topbar.php";
		?>
		<div class="row">
			<div class="col-sm-4">Érvényes: 2018 november 2. és 2018 december 31. között.</div>
			<div class="col-sm-4">
				<?php
				include "kalkulator_footer.php";
				?>
				<div id="figyelmeztetes" style="display:none" class="alert alert-info"></div>
				<div class="form-group">
					<label for="amount">Hitelösszeg</label>
					<div id="hitelosszeg_error" style="display:none" class="alert alert-danger"></div>
					<input class="form-control" id="amount" type="number" min="20000" max="1000000" placeholder="100 000 Ft" onkeyup="computeLoan()">
				</div>
				<div class="form-group">
					<label for="interest_rate">THM (%)</label>
					<input class="form-control" id="interest_rate" type="text"  value="36.4"  onkeyup="computeLoan()"disabled>
				</div>
				<div class="form-group">
					<label for="futamido">Futamidő (hónap)</label>
					<select class="form-control" id="futamido" onchange="computeLoan()">
						<option value="15" selected>15 hónap (Éves ügyleti kamat (fix) 31%)</option><option value="20">20 hónap (Éves ügyleti kamat (fix) 31.5%)</option>
					</select>
				</div>
				<h2 id="payment"></h2>
				<h2 id="onero"></h2>
				<br>
				<?php
				include "kalkulator_hirdetmeny.php";
				?>
			</div>
			<div class="col-sm-4"></div>
		</div>
	</body>
</html>